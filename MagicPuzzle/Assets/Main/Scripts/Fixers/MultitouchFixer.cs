﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {
    public class MultitouchFixer: MonoBehaviour {
        void Start () {
            Input.multiTouchEnabled = false;
        }
    }
}
