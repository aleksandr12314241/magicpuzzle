﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Common {
    public class ScreenFixer: MonoBehaviour {
        Coroutine coroutineScreenFixerUpdate;
        Coroutine coroutineScreenSleep;

        float widthTemp;
        bool isInit = false;

        internal bool IsInit {
            get {
                return isInit;
            }
        }

        void Awake () {
            widthTemp = 0f;

            isInit = false;

            float devider = ( float )Screen.height / ( float )Screen.width;

            //GetComponent<CanvasScaler>().matchWidthOrHeight = ( devider >= 1.9f ) ? 0 : 1;

            StartCoroutine( ScreenFixerUpdate() );

            Screen.fullScreen = false;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            //StartSleep();
        }

        //private void Start () {
        //    Cursor.visible = false;
        //}

        //private void OnApplicationFocus ( bool focus ) {
        //    if ( !focus ) {
        //        return;
        //    }
        //    StartSleep();
        //}

        IEnumerator ScreenFixerUpdate () {
            RectTransform rectTransform = GetComponent<RectTransform>();
            int counter = 0;

            for ( ;; ) {
                if ( Mathf.Abs( widthTemp - rectTransform.rect.width ) <= 0.1f ) {
                    break;
                }

                yield return new WaitForEndOfFrame();

                widthTemp = rectTransform.rect.width;
                counter++;
            }

            isInit = true;
        }

        //void StartSleep () {
        //    Screen.sleepTimeout = SleepTimeout.NeverSleep;

        //    if ( coroutineScreenSleep != null ) {
        //        StopCoroutine( coroutineScreenSleep );
        //    }

        //    coroutineScreenSleep = StartCoroutine( ScreenSleep() );
        //}

        //IEnumerator ScreenSleep () {
        //    yield return new WaitForSeconds( 240 );

        //    EndSleep();

        //}

        //void EndSleep () {
        //    Screen.sleepTimeout = SleepTimeout.SystemSetting;
        //}
    }
}
