﻿using UnityEngine;
using UnityEngine.UI;

namespace Common {
    public class CupFill: MonoBehaviour {
        [SerializeField]
        Image cupImage;
        [SerializeField]
        Image cupFillImage;

        internal void SetImage ( IconDataSet.CupsSet cupsSet ) {
            cupImage.sprite = cupsSet.CategoriesIconSilhouette;
            cupFillImage.sprite = cupsSet.CategoriesIcon;
        }

        internal void SetFill ( int currentLevel, int maxCountLevels ) {
            float fillCount = currentLevel / ( float ) maxCountLevels;
            cupFillImage.fillAmount = fillCount;
        }
    }
}
