﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Common {
    public class TabButton: MonoBehaviour {
        [SerializeField]
        internal Button Button;
        [SerializeField]
        Image _imageIcon;
        [SerializeField]
        Text textButton;
        [SerializeField]
        Animator downButtonAnim;

        RectTransform _rectTransformButton;
        Color _colorButton;
        RectTransform _rectTransformBackButton;
        RectTransform _rectTransformButtonIcon;
        float _startSizeXButton;
        float _endSizeXButton;
        float endPosIcon = 30f;
        float startSizeDeltaIcon = 90f;
        float endSizeDeltaIcon = 120f;
        float timeAnimation = 0.3f;
        Vector2 startSizeDelta;

        internal void Init () {
            _rectTransformButton = Button.GetComponent<RectTransform>();
            _rectTransformButtonIcon = _imageIcon.GetComponent<RectTransform>();
            _startSizeXButton = _rectTransformButton.sizeDelta.x;
            _endSizeXButton = 300f;
            endPosIcon = 30f;
            timeAnimation = 0.1f;
            startSizeDelta = _rectTransformButton.sizeDelta;
        }

        internal void ShowTab () {
            _rectTransformButtonIcon.DOAnchorPosY( endPosIcon, timeAnimation );
            _rectTransformButtonIcon.DOSizeDelta( new Vector2( endSizeDeltaIcon, endSizeDeltaIcon ), timeAnimation );
            textButton.DOColor( new Color( 1f, 1f, 1f, 1f ), timeAnimation );
            _rectTransformButton.DOSizeDelta( new Vector2( _endSizeXButton, startSizeDelta.y ), timeAnimation );
            downButtonAnim.SetTrigger( "show" );
        }

        internal void HideTab () {
            _rectTransformButtonIcon.DOAnchorPosY( 0, timeAnimation );
            _rectTransformButtonIcon.DOSizeDelta( new Vector2( startSizeDeltaIcon, startSizeDeltaIcon ), timeAnimation );
            textButton.DOColor( new Color( 1f, 1f, 1f, 0f ), timeAnimation );
            _rectTransformButton.DOSizeDelta( new Vector2( _startSizeXButton, startSizeDelta.y ), timeAnimation );
        }

        internal void ActivateButton () {
            gameObject.SetActive( true );
        }

        internal void DiactivateButton () {
            gameObject.SetActive( false );
        }
    }
}
