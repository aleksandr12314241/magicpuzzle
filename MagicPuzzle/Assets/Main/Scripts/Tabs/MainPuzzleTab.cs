﻿using System.Collections.Generic;
using UnityEngine;
using Common;
using Menu;
using Utility;
using UnityEngine.UI;
using System.IO;
using Newtonsoft.Json;

namespace Puzzle {
    public class MainPuzzleTab: Tab {
        [SerializeField]
        CategoryUI taskUIPrf;
        [SerializeField]
        SubTaskUI subTaskUIPrf;
        [SerializeField]
        GridLayoutGroup categoryGridLayoutGroup;
        [SerializeField]
        GridLayoutGroup subCategoryGridLayoutGroup;
        [SerializeField]
        GridLayoutGroup levelGridLayoutGroup;
        [SerializeField]
        MainPuzzleScroll _mainPuzzleScroll;
        [SerializeField]
        PuzzleController _puzzleController;
        [SerializeField]
        Button subCategoryToCategoryButton;
        [SerializeField]
        Button levelToCategoryButton;
        [SerializeField]
        SubLevelProgress subLevelProgress;
        [SerializeField]
        TabsContainer tabsContainer;
        [SerializeField]
        Text progressCatigoriesText;
        [SerializeField]
        UpperPanel upperPanel;
        [SerializeField]
        PuzzlePiecesDataSet puzzlePiecesDataSet;
        [SerializeField]
        PuzzleImageDataSet puzzleImageDataSet;

        InfoUI infoUI;
        SubLevelButtonUI subLevelButtonUI;

        List<bool> isSeeThatSubLevel;
        GameController _GC;
        CategoryUI[] subCategoryUIPool;
        SubTaskUI[] subTaskUIPool;

        List<CategoryUI> categoriesUI;
        List<CategoryUI> subCategoriesUI;
        RectTransform categoryRectTransform;
        RectTransform subCategoryRectTransform;
        RectTransform levelRectTransform;

        int subLevelPoolSize;
        string[] lettersCup;

        PuzzelTemplatesNameByImageName puzzelTemplatesNameByImageName;

        internal override void Init () {
            MenuController menuController = FindObjectOfType<MenuController>();
            InitCups();

            _puzzleController.Init();
            _puzzleController.GetPuzzleModel().SetPuzzelType( PuzzleType.Common );

            _mainPuzzleScroll.Init();

            categoryRectTransform = categoryGridLayoutGroup.GetComponent<RectTransform>();
            subCategoryRectTransform = subCategoryGridLayoutGroup.GetComponent<RectTransform>();
            levelRectTransform = levelGridLayoutGroup.GetComponent<RectTransform>();

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            subLevelProgress.Init( _GC );

            infoUI = ( InfoUI ) menuController.GetWindow<InfoUI>();
            WinUI _winUI = ( WinUI ) menuController.GetWindow<WinUI>();
            subLevelButtonUI = ( SubLevelButtonUI ) menuController.GetWindow<SubLevelButtonUI>();

            //catigoriesQuests = _GC.GetCatigoriesQuests();
            subCategoriesUI = new List<CategoryUI>();

            InitCategory();

            InitSubCategoryPool();
            InitLevelsPool();

            _winUI.OnStartFinLevel += OnStartFinLevel;
            _puzzleController.OnClickButtonBack += OnClickButtonBack;
            _GC.player.OnChangeQuest += OnChangeQuest;
            _GC.player.OnChangeCategiries += OnChangeCategiries;

            OpenMainPuzzleScreen();

            subCategoryToCategoryButton.onClick.AddListener( SubCategoryToCategory );
            levelToCategoryButton.onClick.AddListener( LevelToCategory );

            upperPanel.Init();

            string pathToJson = GameHelper.GetPathToResourceData( "puzzle_template_by_image" );
            string saveObject = File.ReadAllText( pathToJson );
            puzzelTemplatesNameByImageName = JsonConvert.DeserializeObject<PuzzelTemplatesNameByImageName>( saveObject );
        }

        void InitCups () {
            lettersCup = new string[20] {
                "\"A\"", "\"B\"", "\"C\"", "\"D\"", "\"E\"", "\"F\"", "\"G\"", "\"H\"", "\"I\"", "\"J\"",
                "\"K\"", "\"L\"", "\"M\"", "\"N\"", "\"O\"", "\"P\"", "\"Q\"", "\"R\"", "\"S\"", "\"T\""
            };
        }

        void OpenMainPuzzleScreen () {
            string language = LocalizationManager.GetLanguageString();

            //if ( true ) {
            if ( _GC.player.isFirstStart ) {
                OpenCategories();

                _GC.player.isFirstStart = false;
                _GC.SavePlayer();
                return;
            }

            //TODO
            if ( false ) {
                //if ( _GC.IsEndGame( categiriesValue ) ) {
                OpenCategories();
                return;
            }

            //SetNumCatigories( categiriesValue );

            _GC.numCatigory = new SecureInt( 0 );
            //_GC.numLevel = new SecureInt( progressByLanguage.quest.GetValue() );

            _mainPuzzleScroll.SetScreenHide( 0, -1 );
            _mainPuzzleScroll.SetScreenHide( 1, -1 );
            _mainPuzzleScroll.SetScreenHide( 2, -1 );
            _mainPuzzleScroll.SetScreenShow( 3 );

            //SetActiveLevels();
            SetSolvedSubQuest();

            RestoreQuest();
        }

        void OpenCategories () {
            _mainPuzzleScroll.SetScreenShow( 0 );
            _mainPuzzleScroll.SetScreenHide( 1, 1 );
            _mainPuzzleScroll.SetScreenHide( 2, 1 );
            _mainPuzzleScroll.SetScreenHide( 3, 1 );
        }

        protected override void ApplyShowTab  () {

            _puzzleController.SetActivePuzzle( true );
        }

        protected override void ApplySecondShowTab () {
            OnSecondClickTabButton();
        }

        protected override void ApplyHideTab () {
            _puzzleController.SetActivePuzzle( false );
        }

        void InitCategory () {
            categoriesUI = new List<CategoryUI>();

            CategoryList categoryList = _GC.GetCategoryList();
            for ( int i = 0 ; i < categoryList.GetCategory().Count; i++ ) {
                CategoryData categoryData = categoryList.GetCategoryByIndex( i );

                CategoryUI taskUIObj = Instantiate( taskUIPrf, Vector2.zero, Quaternion.identity );
                taskUIObj.gameObject.name = taskUIPrf.name + "_" + i;
                taskUIObj.transform.SetParent( categoryRectTransform.transform, false );

                IconDataSet.CupsSet cupSet = _GC.GameLinks.IconDataSet.GetCategoriesIconByIndex( 0 );
                taskUIObj.SetStart( cupSet, categoryData.GetNameIdCategory(), i );

                taskUIObj.OnClickCategoryUI += OpenCategory2;
                categoriesUI.Add( taskUIObj );
            }

            int nameQuestCount = categoryList.GetCategory().Count / categoryGridLayoutGroup.constraintCount;
            categoryRectTransform.sizeDelta = new Vector2( categoryRectTransform.rect.width,
                    ( categoryGridLayoutGroup.spacing.y + categoryGridLayoutGroup.cellSize.y )*nameQuestCount );
            categoryRectTransform.sizeDelta = new Vector2( categoryRectTransform.rect.width, categoryRectTransform.rect.height + 600f );
        }

        //void InitTasks () {
        //    string language = LocalizationManager.GetLanguageString();
        //    ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

        //    _taskUIObj = new List<TaskUI>();

        //    isSeeThatSubLevel = new List<bool>();

        //    for ( int i = 0; i < catigoriesQuests.Count; i++ ) {

        //        TaskUI taskUIObj = Instantiate( taskUIPrf, Vector2.zero, Quaternion.identity );
        //        taskUIObj.gameObject.name = taskUIPrf.name + "_" + i;
        //        taskUIObj.transform.SetParent( scrollContentTasks.transform, false );


        //        IconDataSet.CupsSet cupSet = _GC.GameLinks.IconDataSet.GetCategoriesIconByIndex( i );

        //        taskUIObj.SetStart( cupSet, i, catigoriesQuests[i].nameCatigorie, catigoriesQuests[i].nameQuest.Count, lettersCup[i] );
        //        isSeeThatSubLevel.Add( false );

        //        taskUIObj.OnClickTaskUI += LevesToSubLevels;
        //        _taskUIObj.Add( taskUIObj );
        //    }

        //    int nameQuestCount = catigoriesQuests.Count / tasksGridLayoutGroup.constraintCount;
        //    scrollContentTasks.sizeDelta = new Vector2( scrollContentTasks.rect.width,
        //            ( tasksGridLayoutGroup.spacing.y + tasksGridLayoutGroup.cellSize.y )*nameQuestCount );
        //    scrollContentTasks.sizeDelta = new Vector2( scrollContentTasks.rect.width, scrollContentTasks.rect.height + 600f );

        //    SetSolvedCategiries();
        //}

        void SetSolvedCategiries () {
            //for ( int i = 0; i < _GC.GetCategoryList().GetCategory().Count; i++ ) {
            //    categoriesUI[i].DiactivateAnimator();

            //    if ( progressByLanguage.categiries >= i ) {
            //        if ( progressByLanguage.categiries == i ) {
            //            categoriesUI[i].SetCurrent( progressByLanguage.quest.GetValue() );
            //        }
            //        else {
            //            categoriesUI[i].SetSolved();
            //        }
            //    }
            //    else {
            //        categoriesUI[i].SetNotOpen();
            //    }
            //}

            //int categiriesCompleated = progressByLanguage.categiries.GetValue() + 1;
            //progressCatigoriesText.text = categiriesCompleated.ToString() + "/" + _GC.GetCategoryList().GetCategory().Count;

            //int numCatigories = progressByLanguage.categiries.GetValue() + 1;
            //float dividerProgress = ( numCatigories + 1 ) / ( float ) tasksGridLayoutGroup.constraintCount;
            //float countRow = Mathf.Ceil( dividerProgress );
            //float lastDoneCatigories = countRow * tasksGridLayoutGroup.cellSize.y * -1;

            //float scrollPosition = 0f;
            //if ( lastDoneCatigories * -1 >= 1360 ) {
            //    scrollPosition = tasksRectTransform.sizeDelta.y - ( lastDoneCatigories * -1f ) - 200f;
            //}

            //Debug.Log( "scrollPosition  task   "  + scrollPosition );

            //_mainPuzzleScroll.SetScrollContentPosition( 0, scrollPosition );
        }

        private void InitSubCategoryPool () {
            subLevelPoolSize = 30;

            subCategoryUIPool = new CategoryUI[subLevelPoolSize];
            float stepTask = 300f;
            for ( int i = 0; i < subLevelPoolSize; i++ ) {
                CategoryUI subCategoryObj = Instantiate( taskUIPrf, Vector2.zero, Quaternion.identity );

                subCategoryUIPool[i] = subCategoryObj;
                subCategoryObj.gameObject.SetActive( false );
                subCategoryObj.transform.SetParent( subCategoryRectTransform, false );
                subCategoryObj.GetComponent<RectTransform>().anchoredPosition = new Vector2( 0f, -130f - stepTask * i );
                subCategoryObj.OnClickCategoryUI += OpenLevels;
            }
        }

        private void InitLevelsPool () {
            subLevelPoolSize = 20;

            subTaskUIPool = new SubTaskUI[subLevelPoolSize];
            float stepTask = 300f;
            for ( int i = 0; i < subLevelPoolSize; i++ ) {
                SubTaskUI temSubTaskUI = Instantiate( subTaskUIPrf, Vector2.zero, Quaternion.identity );
                subTaskUIPool[i] = temSubTaskUI;
                temSubTaskUI.gameObject.SetActive( false );
                temSubTaskUI.transform.SetParent( levelRectTransform, false );
                temSubTaskUI.GetComponent<RectTransform>().anchoredPosition = new Vector2( 0f, -130f - stepTask * i );
                temSubTaskUI.OnStartLevel += StartLevel;
            }
        }

        //void SetActiveLevels () {
        //    string language = LocalizationManager.GetLanguageString();
        //    ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

        //    for ( int i = 0; i < catigoriesQuests[_GC.numCatigories.GetValue()].nameQuest.Count; i++ ) {
        //        SubTaskUI temSubTaskUI = subTaskUIPool[i];
        //        subTaskUIPool[i].gameObject.SetActive( true );
        //        temSubTaskUI.SetStart( i, _GC.GetTotalIndexOfQuest( _GC.numCatigories.GetValue(), i ) );
        //    }

        //    int nameQuestCount = catigoriesQuests[_GC.numCatigories.GetValue()].nameQuest.Count / subTasksGridLayoutGroup.constraintCount;
        //    subTasksRectTransform.sizeDelta = new Vector2( subTasksRectTransform.rect.width,
        //            ( subTasksGridLayoutGroup.spacing.y + subTasksGridLayoutGroup.cellSize.y )*nameQuestCount );
        //    subTasksRectTransform.sizeDelta = new Vector2( subTasksRectTransform.rect.width, subTasksRectTransform.rect.height + 600f );
        //}

        public void OpenCategory2 ( int numCatigory ) {
            CategoryData categoryData = _GC.GetCategoryList().GetCategoryByIndex( numCatigory );

            _GC.SetCategory( numCatigory );

            if ( categoryData.subCategoryData.Count > 1 ) {
                OpenSubCategory( numCatigory );
            }
            else {
                _GC.SetSubCategory( 0 );
                OpenLevels( 0 );
            }
        }

        void OpenSubCategory ( int numCSubatigory ) {
            _GC.SetSubCategory( numCSubatigory );

            for ( int i = 0; i < subCategoriesUI.Count; i++ ) {
                subCategoriesUI[i].gameObject.SetActive( false );
            }

            CategoryData categoryData = _GC.GetCategoryList().GetCategoryByIndex( numCSubatigory );

            subCategoriesUI = new List<CategoryUI>();
            for ( int i = 0; i < categoryData.GetSubCategoryData().Count; i++ ) {
                subCategoryUIPool[i].gameObject.SetActive( true );
                IconDataSet.CupsSet cupSet = _GC.GameLinks.IconDataSet.GetCategoriesIconByIndex( 0 );
                subCategoryUIPool[i].SetStart( cupSet, categoryData.GetNameIdCategory(), i );
                subCategoriesUI.Add( subCategoryUIPool[i] );
            }
            _mainPuzzleScroll.ShowScreen( 1, -1, 0 );
        }

        void OpenLevels ( int numSubCatigory ) {
            CategoryData categoryData = _GC.GetCategoryList().GetCategoryByIndex( _GC.numCatigory.GetValue() );
            SubCategoryData subCategoryData = categoryData.GetSubCategoryDataByIndex( _GC.numSubCatigory.GetValue() );

            for ( int i = 0; i < subLevelPoolSize; i++ ) {
                subTaskUIPool[i].gameObject.SetActive( false );
            }

            for ( int i = 0; i < subCategoryData.GetImagesName().Count; i++ ) {
                subTaskUIPool[i].gameObject.SetActive( true );
                subTaskUIPool[i].SetStart( i );
            }

            _mainPuzzleScroll.ShowScreen( 2, -1, 0 );
        }

        public void SubCategoryToCategory () {
            float scrollPosition = GetCategoryScrollPosition();

            _mainPuzzleScroll.ShowScreen( 0, 1, scrollPosition );
            SetSolvedCategiries();

            AudioManager.instance.PlaySoundByType( SoundType.Click );
        }

        public void LevelToCategory () {
            CategoryData categoryData = _GC.GetCategoryList().GetCategoryByIndex( _GC.numCatigory.GetValue() );

            float scrollPosition = GetCategoryScrollPosition();

            if ( categoryData.subCategoryData.Count > 1 ) {
                _mainPuzzleScroll.ShowScreen( 1, 1, scrollPosition );
            }
            else {
                _mainPuzzleScroll.ShowScreen( 0, 1, scrollPosition );
            }

            SetSolvedCategiries();

            AudioManager.instance.PlaySoundByType( SoundType.Click );
        }

        void SetSolvedSubQuest () {


            for ( int i = 0; i < subLevelPoolSize; i++ ) {
                subTaskUIPool[i].gameObject.SetActive( false );
            }

            float lastDoneQuest = 70f;

            int numCatigories = _GC.numCatigory.GetValue();

            //if ( _GC.IsEndGame( numCatigories ) ) {
            //    numCatigories = catigoriesQuests.Count - 1;
            //    numQuest = catigoriesQuests[numCatigories].nameQuest.Count;
            //}

            int nameQuestsCount = _GC.GetCategoryList().GetCategory()[numCatigories].GetSubCategoryData()[0].GetImagesName().Count;

            for ( int i = 0; i < nameQuestsCount; i++ ) {
                SubTaskUI temSubTaskUI = subTaskUIPool[i];
                temSubTaskUI.gameObject.SetActive( true );
                temSubTaskUI.SetStart( i );

                //if ( numQuest >= i ) {
                //    if ( numQuest == i ) {
                //        temSubTaskUI.SetCurrent();
                //    }
                //    else {
                //        temSubTaskUI.SetSolved();
                //    }
                //}
                //else {
                //    temSubTaskUI.SetNotOpen();
                //}
            }
            float dividerProgress = ( 1 + 1 ) / ( float ) levelGridLayoutGroup.constraintCount;
            //float dividerProgress = ( numQuest + 1 ) / ( float ) levelGridLayoutGroup.constraintCount;
            float countRow = Mathf.Ceil( dividerProgress );
            lastDoneQuest = countRow * levelGridLayoutGroup.cellSize.y * -1;

            float scrollPosition = 0f;
            if ( lastDoneQuest * -1 >= 1360 ) {
                scrollPosition = levelRectTransform.sizeDelta.y - ( lastDoneQuest * -1f ) - 200f;
            }

            _mainPuzzleScroll.SetScrollContentPosition( 1, scrollPosition );
            subLevelProgress.SetSubLevelProgress( _GC.numCatigory.GetValue() );
        }

        void OnChangeQuest () {
            SetSolvedSubQuest();
        }

        void OnChangeCategiries () {
            SetSolvedCategiries();
        }

        public void StartLevel ( int level ) {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

            //if ( progressByLanguage.categiries == _GC.numCatigory ) {
            //    if ( progressByLanguage.quest < numQuest ) {

            //        //infoUI.ShowInfo( 0 );
            //        infoUI.ShowInfoMousePosition( LocalizationManager.GetText( "txt_info_window_complete_other_level_first" ), 400f );
            //        return;
            //    }
            //    else {
            //        _mainPuzzleScroll.ShowScreen( 3, -1 );
            //        _GC.numLevel = new SecureInt( numQuest );
            //        AudioManager.instance.PlaySoundByType( SoundType.Click );
            //        StartQuest();
            //    }
            //}
            //else {
            //    _mainPuzzleScroll.ShowScreen( 3, -1 );
            //    _GC.numLevel = new SecureInt( numQuest );
            //    AudioManager.instance.PlaySoundByType( SoundType.Click );
            //    StartQuest();
            //}

            _mainPuzzleScroll.ShowScreen( 3, -1 );
            _GC.SetLevel( level );
            AudioManager.instance.PlaySoundByType( SoundType.Click );
            //if ( _GC.player ) {

            //}
            StartQuest();
        }

        void StartQuest () {
            string imagesName = _GC.GetCategoryList().GetCategoryByIndex( _GC.numCatigory.GetValue() ).
                                GetSubCategoryDataByIndex( _GC.numSubCatigory.GetValue() ).GetImageNameByIndex( _GC.numLevel.GetValue() );

            string[] images = imagesName.Split( ',' );
            LevelInitingData[] levelsInitingData = new LevelInitingData[images.Length];

            int[] counPieces = new int[images.Length];
            string pathToJson;
            string saveObject;

            for ( int i = 0; i < images.Length; i++ ) {
                images[i] = images[i].Replace( " ", "" );

                List<string> puzzleTemplateS = puzzelTemplatesNameByImageName.GetPuzzelTemplatesNameByImageName( images[i] );
                string puzzleTemplate = puzzleTemplateS[Random.Range( 0, puzzleTemplateS.Count )];

                pathToJson = GameHelper.GetPathToResourceDataNoExtension( "PuzzleTemplate/" + puzzleTemplate );
                Debug.Log( puzzleTemplate );
                Debug.Log( pathToJson );

                saveObject = File.ReadAllText( pathToJson );
                PuzzleTemplate puzzelTemplate = JsonConvert.DeserializeObject<PuzzleTemplate>( saveObject );

                counPieces[i] = ( puzzelTemplate.sizeMatrixImage.x * puzzelTemplate.sizeMatrixImage.y );
                for ( int j = 0; j < puzzelTemplate.connectionPuzzlePiece.Count; j++ ) {
                    counPieces[i] -= puzzelTemplate.connectionPuzzlePiece[j].Count - 1;
                }

                Debug.Log( "imageName  " + images[i] );

                Sprite spriteImage = puzzleImageDataSet.GetSpriteByName( images[i] );

                levelsInitingData[i] = new LevelInitingData( spriteImage, puzzelTemplate );
            }

            string levelTemplateCounPieces = "";
            int maxLength = 0;
            for ( int i = 0; i < counPieces.Length; i++ ) {
                levelTemplateCounPieces += "_" + counPieces[i];
                maxLength += counPieces[i];
            }
            Debug.Log( "levelTemplateCounPieces  " + levelTemplateCounPieces );

            string levelTemplateName = "level_teplate" + levelTemplateCounPieces + ".json";

            Debug.Log( levelTemplateName );

            pathToJson = GameHelper.GetPathToResourceDataNoExtension( "LevelTemplate/" + levelTemplateName );

            Debug.Log( pathToJson );

            saveObject = File.ReadAllText( pathToJson );

            LevelTemplate levelTemplate = JsonConvert.DeserializeObject<LevelTemplate>( saveObject );
            LevelTemplateData levelTemplateData = levelTemplate.GetRandonLevelsTemplateData();

            float root2 = Mathf.Sqrt( maxLength );
            int sizeField = Mathf.CeilToInt( root2 );

            _puzzleController.InitPuzzle2( levelsInitingData, sizeField );
        }

        void RestoreQuest () {
            Sprite[] spritesImages = new Sprite[_GC.player.puzzleStateData.Count];
            int indexImage = 0;
            foreach ( var item in _GC.player.puzzleStateData ) {
                spritesImages[indexImage] = puzzleImageDataSet.GetSpriteByName( item.Key );
            }
            _puzzleController.RestorePuzzle2( _GC.player.sizeI, _GC.player.puzzleStateData, spritesImages );
        }

        private void OnClickButtonBack () {
            float dividerProgress = ( 1 + 1 ) / ( float ) levelGridLayoutGroup.constraintCount;
            //float dividerProgress = ( progressByLanguageQuest + 1 ) / ( float ) levelGridLayoutGroup.constraintCount;

            float countRow = Mathf.Ceil ( dividerProgress );
            float lastDoneQuest = countRow * levelGridLayoutGroup.cellSize.y * -1;

            float scrollPosition = 0f;
            if ( lastDoneQuest * -1 >= 1360 ) {
                scrollPosition = levelRectTransform.sizeDelta.y - ( lastDoneQuest * -1f ) - 200f;
            }

            _mainPuzzleScroll.ShowScreen( 2, 1 );
            AudioManager.instance.PlaySoundByType( SoundType.Click );
        }

        void OnStartFinLevel ( PuzzleType puzzelType, CloseWinUIType levelFinishType ) {

            if ( !puzzelType.Equals( PuzzleType.Common ) ) {
                return;
            }

            if ( ( levelFinishType.Equals( CloseWinUIType.GoToMenu ) || levelFinishType.Equals( CloseWinUIType.NextCategory ) ) ) {
                //if ( !levelFinishType.Equals( CloseWinUIType.GoToMenu ) || !levelFinishType.Equals( CloseWiUIType.NextCategory ) ) {
                //    return;n
                //}
                //if ( _GC.numQuest != -1 ) {
                //    return;
                //}
                float scrollPosition = GetCategoryScrollPosition();

                _mainPuzzleScroll.ShowScreen( 0, 1, scrollPosition );
                _mainPuzzleScroll.SetScreenHide( 1, 1 );
                _mainPuzzleScroll.SetScreenHide( 2, 1 );

                if ( levelFinishType.Equals( CloseWinUIType.NextCategory ) ) {
                    if ( _GC.numCatigoriesAnimation != -1 ) {
                        categoriesUI[_GC.numCatigoriesAnimation].SetCurrent( 0 );
                        categoriesUI[_GC.numCatigoriesAnimation].ShowOpenCategoryAnimation();

                        _GC.numCatigoriesAnimation = -1;
                        int numCatigoriesAnimation = _GC.numCatigoriesAnimation;
                    }
                }
            }
        }

        float GetCategoryScrollPosition () {
            float dividerProgress = ( 1 ) / ( float ) categoryGridLayoutGroup.constraintCount;
            //float dividerProgress = ( progressByLanguage.categiries.GetValue() ) / ( float ) categoryGridLayoutGroup.constraintCount;

            float countRow = Mathf.Ceil( dividerProgress );
            float lastDoneQuest = countRow * categoryGridLayoutGroup.cellSize.y * -1;

            float scrollPosition = 0f;
            if ( lastDoneQuest * -1 >= 1360 ) {
                scrollPosition = categoryRectTransform.sizeDelta.y - ( lastDoneQuest * -1f );
            }

            return scrollPosition;
        }

        void SetNumCatigories ( int numCatigories ) {
            _GC.numCatigory = new SecureInt( numCatigories );
        }

        void OnSecondClickTabButton () {
            if ( _mainPuzzleScroll.IndexOpenScreen == 2 ) {
                return;
            }

            _mainPuzzleScroll.SetScreenHide( 0, -1 );
            _mainPuzzleScroll.SetScreenHide( 1, -1 );
            _mainPuzzleScroll.SetScreenShow( 2 );

            OpenMainPuzzleScreen();
        }
    }
}

