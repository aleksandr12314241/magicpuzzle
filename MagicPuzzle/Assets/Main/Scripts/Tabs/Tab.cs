﻿using UnityEngine;

namespace Common {
    public abstract class Tab: MonoBehaviour {

        internal bool IsShown {
            private set;
            get;
        }

        internal abstract void Init ();

        internal void ShowTab () {
            if ( IsShown ) {
                return;
            }

            IsShown = true;
            gameObject.SetActive( true );

            ApplyShowTab ();
        }

        internal void SecondShowTab () {
            ApplySecondShowTab();
        }

        protected abstract void ApplyShowTab ();
        protected abstract void ApplySecondShowTab ();

        internal void HideForce () {
            IsShown = false;
            gameObject.SetActive( false );

            ApplyHideTab();
        }

        internal void HideTab () {
            if ( !IsShown ) {
                return;
            }

            IsShown = false;
            gameObject.SetActive( false );

            ApplyHideTab();
        }

        protected abstract void ApplyHideTab ();
    }
}
