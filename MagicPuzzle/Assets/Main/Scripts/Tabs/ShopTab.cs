﻿using UnityEngine;
using Common;
using UnityEngine.UI;

namespace Shop {
    public class ShopTab: Tab {

        [SerializeField]
        private BuyUI _buyUI;
        [SerializeField]
        private Button detailsButton;
        [SerializeField]
        Button shopButton;

        internal override void Init () {
            //detailsButton.onClick.AddListener( GoToSite );
            //shopButton.onClick.AddListener( GoToSite );

            _buyUI.Init();
        }

        void GoToSite () {
            Application.OpenURL( "https://vk.com/podzorovsergei" );
        }

        protected override void ApplyShowTab  () {
            _buyUI.Show();
        }

        protected override void ApplySecondShowTab () {
            _buyUI.TryHideMoreShop();
        }

        protected override void ApplyHideTab () {
            _buyUI.Hide();
        }
    }
}
