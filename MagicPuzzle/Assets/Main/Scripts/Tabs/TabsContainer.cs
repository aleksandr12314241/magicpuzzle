﻿using UnityEngine;
using System;

namespace Common {
    public class TabsContainer: MonoBehaviour {
        internal event Action<int> OnOpenTab = ( int param ) => { };

        [SerializeField]
        TabAndButton[] _tabs;
        GameController _GC;

        private int _openTab = -1;

        internal int OpenTab {
            get {
                return _openTab;
            }
        }

#if UNITY_EDITOR
        private void OnValidate () {
            if ( _tabs.Length <= 0 ) {
                Debug.LogError( "|TabsContainer|<OnValidate> массив табо пустой" );
                return;
            }

            for ( int i = 0; i < _tabs.Length; i++ ) {
                if ( _tabs[i].Button == null ) {
                    Debug.LogError( "|TabsContainer|<OnValidate>  у " + i + "ого таба кнопка =  null" );
                    return;
                }
                else if ( _tabs[ i ].Tab == null ) {
                    Debug.LogError( "|TabsContainer|<OnValidate>  у " + i + "ого таба таб =  null" );
                    return;
                }
            }
        }
#endif

        void Start () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            for ( var i = 0; i < _tabs.Length; i++ ) {
                var index = i;
                _tabs[i].Tab.Init();
            }

            for ( var i = 0; i < _tabs.Length; i++ ) {
                var index = i;
                _tabs[ i ].Button.Button.onClick.AddListener( () => {
                    ShowTab( index );
                } );
            }

            for ( int i = 0; i < _tabs.Length; i++ ) {
                _tabs[i].Init();
            }

            _openTab = 0;
            _tabs[_openTab].ShowTab();
            for ( int i = 0; i < _tabs.Length; i++ ) {
                if ( _openTab == i ) {
                    continue;
                }

                _tabs[i].HideForceTab();
            }
        }

        private void ShowTab ( int indexTab ) {

            if ( _tabs.Length < 0 ) {
                Debug.Log( "|TabsContainer|<OnValidate> массивом  = " + _tabs.Length + " пуст " );
                return;
            }

            if ( _tabs.Length <= indexTab ) {
                Debug.Log( "|TabsContainer|<OnValidate> слишком большой индекс = " + indexTab +
                           " по сравнениею с массивом  = " + _tabs.Length );
                return;
            }
            if ( _openTab == indexTab ) {
                _tabs[_openTab].SecondShowTab();
                return;
            }

            _openTab = indexTab;
            for ( int i = 0; i < _tabs.Length; i++ ) {
                if ( _tabs[i] == null ) {
                    Debug.Log( "|TabsContainer|<OnValidate> экземпляр класса _tabs[" + i + "]  = null" );
                    return;
                }

                if ( _tabs[i].Tab == null ) {
                    Debug.Log( "|TabsContainer|<OnValidate> экземпляр класса _tabs[" + i + "].Tab  = null" );
                    return;
                }

                if ( indexTab == i ) {
                    _tabs[i].ShowTab();
                }
                else {
                    _tabs[i].HideTab();
                }
            }
            OnOpenTab( indexTab );
        }

        void OnAllowDailyPuzzle () {
            _tabs[1].Button.ActivateButton();
        }

        void OnAllowShop () {
            _tabs[3].Button.ActivateButton();
        }

        [System.Serializable]
        private class TabAndButton {
            [SerializeField]
            internal Tab Tab;
            [SerializeField]
            internal TabButton Button;

            internal void Init () {
                Button.Init();
            }

            internal void ShowTab () {
                Tab.ShowTab();
                Button.ShowTab();
            }

            internal void SecondShowTab () {
                Tab.SecondShowTab();
            }

            internal void HideTab () {
                Tab.HideTab();
                Button.HideTab();
            }

            internal void HideForceTab () {
                Tab.HideForce();
                Button.HideTab();
            }
        }
    }
}


