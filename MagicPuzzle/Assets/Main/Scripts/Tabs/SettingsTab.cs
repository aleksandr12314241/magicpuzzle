﻿using UnityEngine;
using Common;
using UnityEngine.UI;
using Utility;

namespace Menu {
    public class SettingsTab: Tab {
        [SerializeField]
        Button soundOffButton;
        [SerializeField]
        Button soundOnButton;
        [SerializeField]
        Button rateUsButton;
        [SerializeField]
        Button _howToPlayButton;
        [SerializeField]
        Button _languageButton;
        [SerializeField]
        Button secretСodeButton;
        [SerializeField]
        HowToPlayUI _howToPlayUI;
        [SerializeField]
        LanguageUI _languageUI;
        [SerializeField]
        Image _iconLanguage;
        [SerializeField]
        Sprite[] _iconLanguageRes;
        [SerializeField]
        Button actionAnimationButton;
        [SerializeField]
        Animator settingsAnimator;
        [SerializeField]
        Text languageText;
        [SerializeField]
        Text projectVersionText;
        [SerializeField]
        RectTransform contentRectTransform;
        [SerializeField]
        GameObject lineSecretCodeImage;
        [SerializeField]
        Button privacyPolicyButton;

        GameController _GC;
        AudioListener mainCameraAudioListener;
        AudioSource settingsTabAudioSource;
        InfoUI infoUI;
        SecretCodeUI secretCodeUI;

        internal override void Init () {
            mainCameraAudioListener = Camera.main.GetComponent<AudioListener>();

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            settingsTabAudioSource = GetComponent<AudioSource>();

            soundOffButton.onClick.AddListener( SoundOn );
            soundOnButton.onClick.AddListener( SoundOff );

            privacyPolicyButton.onClick.AddListener( PrivacyPolicyButton );

            _howToPlayButton.onClick.AddListener( ShowTutorialWnd );
            _languageButton.onClick.AddListener( ShowLanguageWindow );
            //actionAnimationButton.onClick.AddListener( ActionAnimationSettings );
            rateUsButton.onClick.AddListener( RateApp );
            secretСodeButton.onClick.AddListener( ShowSecretCodeWindow );

            LocalizationManager.OnChangeLanguage += OnChangeLanguage;
            SetStateLanguage();

            SetSoundButtons( _GC.player.isSound );

            projectVersionText.text = "ver " + Application.version;

            MenuController menuController = FindObjectOfType<MenuController>();
            infoUI = ( InfoUI ) menuController.GetWindow<InfoUI>();
            secretCodeUI = ( SecretCodeUI ) menuController.GetWindow<SecretCodeUI>();
        }

        void SetSoundButtons ( bool isSound ) {
            soundOffButton.gameObject.SetActive( !isSound );
            soundOnButton.gameObject.SetActive( isSound );

            if ( isSound ) {
                AudioListener.volume = 1;
            }
            else {
                AudioListener.volume = 0f;
            }
        }

        protected override void ApplyShowTab  () {
        }
        protected override void ApplySecondShowTab () {
        }

        protected override void ApplyHideTab () {
        }

        void SoundOff () {
            _GC.player.SetIsSound( false );
            _GC.SavePlayer();

            SetSoundButtons( false );
        }

        void SoundOn () {
            _GC.player.SetIsSound( true );
            _GC.SavePlayer();

            SetSoundButtons( true );

            settingsTabAudioSource.Play();
        }

        void ShowTutorialWnd () {
            _howToPlayUI.ShowTutorialWnd();
        }

        void ShowLanguageWindow () {
            _languageUI.ShowWin();
        }

        void ShowSecretCodeWindow () {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

            //if ( progressByLanguage.categiries.GetValue() < 10 ) {
            //    infoUI.ShowInfo( LocalizationManager.GetText( "txt_info_window_secret_code_not_available" ), BlockType.Block );
            //    return;
            //}

            secretCodeUI.Show();
        }

        void OnChangeLanguage ( LocalizationManager.Language language ) {
            SetStateLanguage();
        }

        void SetStateLanguage () {
            SetInconLanguage();

            if ( LocalizationManager.GetLanguage().Equals( LocalizationManager.Language.Ru ) ) {
                secretСodeButton.gameObject.SetActive( true );
                lineSecretCodeImage.gameObject.SetActive( true );
                contentRectTransform.sizeDelta = new Vector2( contentRectTransform.sizeDelta.x, 1256f );
            }
            else {
                secretСodeButton.gameObject.SetActive( false );
                lineSecretCodeImage.gameObject.SetActive( false );
                contentRectTransform.sizeDelta = new Vector2( contentRectTransform.sizeDelta.x, 1091f );
            }
        }

        void SetInconLanguage () {
            _iconLanguage.sprite = GetSpriteByEnum( LocalizationManager.GetLanguage() );
            string localizationString = "txt_language_window_" + LocalizationManager.GetLanguageString().ToLower();

            languageText.text = LocalizationManager.GetText( localizationString );
        }

        Sprite GetSpriteByEnum ( LocalizationManager.Language language ) {
            switch ( language ) {
                case LocalizationManager.Language.Ru:
                    return _iconLanguageRes[0];
                default:
                    return _iconLanguageRes[1];
            }
        }

        void ActionAnimationSettings () {
            settingsAnimator.SetTrigger( "startAnimation" );
        }

        public void RateApp () {

#if UNITY_IOS

            Application.OpenURL( "http://itunes.apple.com/US/app/id1459676740" );
            //Application.OpenURL( "itms - apps://itunes.apple.com/US/app/id1459676740" );

#endif

#if UNITY_ANDROID
            Application.OpenURL( "market://details?id=com.zeusgamelab.wordsquare" );
#endif

#if UNITY_STANDALONE_OSX

            Application.OpenURL( "http://itunes.apple.com/US/app/id1459676740" );
            Debug.Log( "Stand Alone OSX" );
#endif

#if UNITY_STANDALONE_WIN
            Debug.Log( "Stand Alone Windows" );
#endif
        }

        void PrivacyPolicyButton () {
            Application.OpenURL( "https://zeusgamelab.wixsite.com/privacypolicy" );
        }
    }
}
