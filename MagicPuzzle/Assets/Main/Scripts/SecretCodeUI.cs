﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using UnityEngine.UI;

namespace Menu {
    public class SecretCodeUI: WindowMonoBehaviour {
        [SerializeField]
        Button sendButton;
        [SerializeField]
        Button linkButton;
        [SerializeField]
        InputField textEditor;
        [SerializeField]
        Button blackButton;
        [SerializeField]
        Button closeButton;

        GameController _GC;

        internal override void InitStart () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            sendButton.onClick.AddListener( Send );
            linkButton.onClick.AddListener( OpenURL );
            blackButton.onClick.AddListener( Hide );
            closeButton.onClick.AddListener( Hide );
        }

        public override void Show () {
            base.Show();

            textEditor.text = _GC.GetSecretCode();
        }

        void Send () {
            Mail.SendMail( "ethergift@gmail.com", LocalizationManager.GetText( "txt_secret_code_window_tittle" ), textEditor.text );
        }

        void OpenURL () {
            Application.OpenURL( "https://vk.com/id540273993" );
        }
    }
}
