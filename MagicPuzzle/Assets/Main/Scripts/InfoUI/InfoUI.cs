﻿using UnityEngine;
using UnityEngine.UI;

namespace Common {
    public class InfoUI: WindowMonoBehaviour {
        [SerializeField]
        Image back;
        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Image backImg;
        [SerializeField]
        Text _infoText;
        [SerializeField]
        Animator _myAnimator;
        [SerializeField]
        GameObject blockObject;
        [SerializeField]
        RectTransform tailUp;
        [SerializeField]
        RectTransform tailDown;
        [SerializeField]
        RectTransform panelRectTransform;

        float _hideAnimationLength;

        Vector2 rangeXTailDown;
        Vector2 rangeXTailUp;

        Camera camera;

        internal override void InitAwake () {

            _hideAnimationLength = 0f;
            RuntimeAnimatorController _runtimeAnimatorController = _myAnimator.runtimeAnimatorController;
            AnimationClip[] clips = _runtimeAnimatorController.animationClips;

            for ( int i = 0; i < clips.Length; i++ ) {
                if ( clips[i].name.Equals( "hide_info" ) ) {
                    _hideAnimationLength = clips[i].length;
                }
            }

            rangeXTailDown = new Vector2( -263f, -70f );
            rangeXTailUp = new Vector2( 263f, -19f );

            camera = Camera.main;
        }

        private void Update () {
            if ( !IsShown ) {
                return;
            }

            if ( Input.GetMouseButtonDown( 0 ) ) {
                StartHide();
            }
        }

        internal void ShowInfoMousePosition ( string textInfo, float offset = 0f ) {
            Vector2 mousPosition = Input.mousePosition;
            Vector2 ancoredPositionOnScreen;

            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRectTransform, mousPosition, camera, out ancoredPositionOnScreen );
            TailType tailType = TailType.None;

            if ( ancoredPositionOnScreen.y >= 0 ) {
                tailType = TailType.Up;
                ancoredPositionOnScreen = new Vector2( ancoredPositionOnScreen.x, ancoredPositionOnScreen.y - offset );
            }
            else {
                tailType = TailType.Down;
                ancoredPositionOnScreen = new Vector2( ancoredPositionOnScreen.x, ancoredPositionOnScreen.y + offset );
            }

            ShowInfoWithTailAndPositionY( textInfo, ancoredPositionOnScreen.y, BlockType.Block, tailType, ancoredPositionOnScreen.x, true );
        }

        internal void ShowInfo ( string textInfo, BlockType blockType = BlockType.None ) {
            ShowInfoWithTailAndPositionY( textInfo, 0f, blockType, TailType.None, 0f, true );
        }

        internal void ShowInfoWithPosition ( string textInfo, float panelPosY, BlockType blockType = BlockType.None ) {
            ShowInfoWithTailAndPositionY( textInfo, panelPosY, blockType, TailType.None, 0f, true );
        }

        internal void ShowInfoWithTail ( string textInfo, BlockType blockType, TailType tailType, float tailPosX ) {
            ShowInfoWithTailAndPositionY( textInfo, 0f, blockType, tailType, tailPosX, true );
        }

        internal void ShowInfoWithTailAndPositionY ( string textInfo, float panelPosY, BlockType blockType, TailType tailType, float tailPosX,
                bool isShowBack ) {
            base.Show();

            _infoText.text = textInfo;

            panelRectTransform.anchoredPosition = new Vector2( 0f, panelPosY );

            ShowTail( tailType, tailPosX );

            if ( blockType == BlockType.None ) {
                blockObject.SetActive( false );
            }
            else {
                blockObject.SetActive( true );
            }

            back.gameObject.SetActive( isShowBack );
        }

        void ShowTail ( TailType tailType, float posX = 0f ) {
            float posXTemp = posX;

            switch ( tailType ) {
                case TailType.Up:
                    tailUp.gameObject.SetActive( true );
                    tailDown.gameObject.SetActive( false );

                    if ( posXTemp < rangeXTailUp.x ) {
                        posXTemp = rangeXTailUp.x;
                    }
                    else if ( posXTemp > rangeXTailUp.y ) {
                        posXTemp = rangeXTailUp.y;
                    }
                    tailUp.anchoredPosition = new Vector2( posXTemp, tailUp.anchoredPosition.y );

                    break;
                case TailType.Down:
                    tailUp.gameObject.SetActive( false );
                    tailDown.gameObject.SetActive( true );

                    if ( posXTemp < rangeXTailDown.x ) {
                        posXTemp = rangeXTailDown.x;
                    }
                    else if ( posXTemp > rangeXTailDown.y ) {
                        posXTemp = rangeXTailDown.y;
                    }
                    tailDown.anchoredPosition = new Vector2( posXTemp, tailDown.anchoredPosition.y );

                    break;
                default:
                    tailUp.gameObject.SetActive( false );
                    tailDown.gameObject.SetActive( false );
                    break;
            }
        }

        void StartHide () {
            //_myAnimator.SetTrigger( "startHide" );

            //Invoke( "HideInfo", _hideAnimationLength );

            base.Hide();
        }

        void HideInfo () {
            base.Hide();
        }
    }
}
