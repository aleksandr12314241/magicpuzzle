﻿
namespace Common {
    public enum BlockType {
        Block,
        None
    }

    public enum TailType {
        Up,
        Down,
        None
    }
}
