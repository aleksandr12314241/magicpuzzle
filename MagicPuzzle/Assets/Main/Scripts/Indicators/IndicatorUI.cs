﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Menu {
    public class IndicatorUI: MonoBehaviour {
        [SerializeField]
        GameObject _indicatorObject;

        protected internal void SetIndicator ( int count ) {
            if ( count <= 0 ) {
                _indicatorObject.gameObject.SetActive( false );
                return;
            }

            _indicatorObject.gameObject.SetActive( true );
        }
    }
}
