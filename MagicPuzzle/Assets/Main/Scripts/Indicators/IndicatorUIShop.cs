﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace Menu {
    public class IndicatorUIShop: IndicatorUI {
        GameController _GC;

        private void Start () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            CheckShop();

            _GC.player.OnDoneTutorial2 += OnDoneTutorial2;
        }

        void OnDoneTutorial2 ( string tutorial ) {
            CheckShop();
        }

        void CheckShop () {
            if ( _GC.player.tutorialProgress2["is_anim_shop"] && !_GC.player.tutorialProgress2["see_shop_tutorial"] ) {
                SetIndicator( 1 );
            }
            else {
                SetIndicator( 0 );
            }
        }
    }
}
