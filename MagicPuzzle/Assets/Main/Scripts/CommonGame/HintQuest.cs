﻿using System.Collections.Generic;
using Utility;

namespace Common {
    public class HintQuest {
        public SecureInt category;
        public SecureInt quest;
        public List<SecureInt> countShow;

        public HintQuest () {
            category = new SecureInt( -1 );
            quest = new SecureInt( -1 );
            countShow = new List<SecureInt>();
        }
    }
}
