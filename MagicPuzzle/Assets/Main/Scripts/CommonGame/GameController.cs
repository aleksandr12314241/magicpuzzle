﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Utility;
using Newtonsoft.Json;

namespace Common {
    public class GameController: MonoBehaviour {
        internal event Action OnShowNonRewardAds = () => { };

        [SerializeField]
        GameSerialization _GS;
        [SerializeField]
        GameLinks _GL;

        public Player player;

        internal SecureInt numCatigory;
        internal SecureInt numSubCatigory;
        internal SecureInt numLevel;

        internal int numCatigoriesAnimation;

        //public List<Quests> tempQuests;

        CategoryList categoryList;

        public DateTime needTime;

        public HintFingerDataSet hintFingerDataSet;

        List<List<int>> tottalIndexOfQuest;

        public int countWinsInSession;
        public int maxWinsInSession = 5;

        public GameLinks GameLinks {
            get {
                return _GL;
            }
        }

        public GameSerialization GameSerialization {
            get {
                return _GS;
            }
        }

        // не уничтожать этот объект в тчении игры
        public void Awake () {

            DontDestroyOnLoad( this );
            if ( FindObjectsOfType( GetType() ).Length > 1 ) {

                Destroy( gameObject );
                return;
            }

            Init();
        }

        void Init () {
            numCatigory = new SecureInt( -1 );
            numLevel = new SecureInt( -1 );
            numCatigoriesAnimation = -1;

            InitCategoryList();
            LocalizationManager.Init();
            _GS.Init();
            InitHintFingerDataSet();
        }

        void InitCategoryList () {
            string pathToCategoryList = GameHelper.GetPathToResourceData( "category_list" );
            string saveObject = File.ReadAllText( pathToCategoryList );
            categoryList = JsonConvert.DeserializeObject<CategoryList>( saveObject );
        }

        void InitHintFingerDataSet () {
            string pathToQuest = GameHelper.GetPathToResourceData( "hint_finger_dataset" );
            string saveObject = _GS.GetJsonFromEncrypterString( pathToQuest );
            //JsonData jsonPlayerLevel = JsonMapper.ToObject( saveObject );
            hintFingerDataSet = JsonConvert.DeserializeObject<HintFingerDataSet>( saveObject );
        }

        public bool IsEndCategiries () {
            return false;
        }

        public void InitPlayer ( Player newPlayer ) {
            player = newPlayer;
            LocalizationManager.Language language = ( LocalizationManager.Language ) Enum.Parse( typeof( LocalizationManager.Language ),
                                                    player.language );

            LocalizationManager.SetLanguageNoEvent( language );
        }

        //Quests GetQuestsByNameAndPath ( string nameLevel, string fullPathToLevelJson ) {
        //    string saveObject = _GS.GetJsonFromEncrypterString( fullPathToLevelJson );

        //    Quests tempLoadQuest = JsonConvert.DeserializeObject<Quests>( saveObject );
        //    tempLoadQuest.nameLevel = nameLevel;

        //    return tempLoadQuest;
        //}

        //public void SaveHintTrue ( int indexQuest, int indexWord, int indexLetter ) {

        //    tempQuests[indexQuest].indexLetters[indexWord].indexLetters[indexLetter].isHint = true;
        //}

        //public void SaveHintFalse ( int indexQuest, int indexWord, int indexLetter ) {

        //    tempQuests[indexQuest].indexLetters[indexWord].indexLetters[indexLetter].isHint = false;
        //}

        public void SavePlayer () {
            _GS.SaveResult( player );
        }

        public int AddNewHintQuest ( int newCategory, int newQuest ) {

            //progressByLanguage.hintQuest.Add( new HintQuest() );

            //progressByLanguage.hintQuest[progressByLanguage.hintQuest.Count - 1].category = new SecureInt( newCategory );
            //progressByLanguage.hintQuest[progressByLanguage.hintQuest.Count - 1].quest = new SecureInt( newQuest );

            //return progressByLanguage.hintQuest.Count - 1;
            return 1 - 1;
        }

        public int FoundHintQuest ( int newCategory, int newQuest ) {

            //for ( int i = 0; i < progressByLanguage.hintQuest.Count; i++ ) {

            //    if ( progressByLanguage.hintQuest[i].category == newCategory &&
            //            progressByLanguage.hintQuest[i].quest == newQuest ) {

            //        return i;
            //    }
            //}
            return -1;
        }

        public void AddHintCountShow ( int numHint, int numWord ) {

            //progressByLanguage.hintQuest[numHint].countShow[numWord]++;
            SavePlayer();
        }

        public void DelHintCountShow ( int numHint ) {

            //if ( progressByLanguage.hintQuest.Count <= 0 ) {
            //    return;
            //}
            //progressByLanguage.hintQuest.RemoveAt( numHint );
            SavePlayer();
        }

        public int FoundHintDailyPuzzle ( int newDaily ) {

            //for ( int i = 0; i < progressByLanguage.hintDailyPuzzle.Count; i++ ) {

            //    if ( progressByLanguage.hintDailyPuzzle[i].ind == newDaily ) {

            //        return i;
            //    }
            //}
            return -1;
        }

        public int AddNewHintDailyPuzzle ( int newDailyPuzleHint ) {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = player.GetProgressByLanguage( language );

            //progressByLanguage.hintDailyPuzzle.Add( new HintDailyPuzzle() );
            //progressByLanguage.hintDailyPuzzle[progressByLanguage.hintDailyPuzzle.Count - 1].ind = new SecureInt( newDailyPuzleHint );

            //return progressByLanguage.hintDailyPuzzle.Count - 1;
            return 1 - 1;
        }

        public void AddHintCountShowDailyPuzzle ( int numHint, int numWord ) {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = player.GetProgressByLanguage( language );

            //progressByLanguage.hintDailyPuzzle[numHint].countShow[numWord]++;
            SavePlayer();
        }

        public void DelHintCountShowDailyPuzzle ( int numHint ) {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = player.GetProgressByLanguage( language );

            //if ( progressByLanguage.hintDailyPuzzle.Count <= 0 ) {
            //    return;
            //}

            //progressByLanguage.hintDailyPuzzle.RemoveAt( numHint );
            SavePlayer();
        }

        public int GetTotalIndexOfQuest ( int catigories, int quest ) {
            return tottalIndexOfQuest[catigories][quest];
        }

        public string GetSecretCode () {
            string code = player.name + " ; " + player.id_device;
            return AuxiliaryTools.GetEncryptString( code );
        }

        public string GetUnSecretCode ( string ddd ) {
            return AuxiliaryTools.GetJsonFromEncrypterString( ddd );
        }

        internal void AddCountWinsInSession () {
            countWinsInSession++;
            if ( countWinsInSession >= maxWinsInSession ) {
                countWinsInSession = 0;

                OnShowNonRewardAds();
            }
        }

        //GET
        internal CategoryList GetCategoryList () {
            return categoryList;
        }

        //SET
        internal void SetCategory ( int category ) {
            numCatigory = new SecureInt( category );
        }
        internal void SetSubCategory ( int subCategory ) {
            numSubCatigory = new SecureInt( subCategory );
        }
        internal void SetLevel ( int level ) {
            numLevel = new SecureInt( level );
        }
    }
}
