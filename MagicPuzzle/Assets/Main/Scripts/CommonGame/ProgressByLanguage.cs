﻿using System.Collections.Generic;
using Utility;
using UnityEngine;

namespace Common {

    public class ProgressByLanguage {
        public SecureInt categiries;
        public SecureInt quest;
        public SecureInt numDaylyPuzzleQuest;
        public List<HintQuest> hintQuest;
        public List<HintDailyPuzzle> hintDailyPuzzle;
        public List<int> indexWordFeildState;
        public List<List<Vector2Int>> inexLettersFeildState;
        public List<int> indexWordFeildStateDailyPuzzle;
        public List<List<Vector2Int>> inexLettersFeildStateDailyPuzzle;

        public ProgressByLanguage () {
            categiries = new SecureInt( 0 );
            quest = new SecureInt( 0 );
            numDaylyPuzzleQuest = new SecureInt( 0 );
            hintQuest = new List<HintQuest>();
            hintDailyPuzzle = new List<HintDailyPuzzle>();
            indexWordFeildState = new List<int>();
            inexLettersFeildState = new List<List<Vector2Int>>();
            indexWordFeildStateDailyPuzzle = new List<int>();
            inexLettersFeildStateDailyPuzzle = new List<List<Vector2Int>>();
        }

        public void AddNumDaylyPuzzleQuest () {
            numDaylyPuzzleQuest++;
        }

        public void SetCategiriesIgonreСheck ( int newCategiries ) {
            categiries = new SecureInt( newCategiries );
        }

        public void SetCategiries ( int newCategiries ) {
            if ( newCategiries <= categiries ) {
                return;
            }

            categiries = new SecureInt( newCategiries );
        }

        public void SetQuest ( int newQuest ) {
            quest = new SecureInt( newQuest );
        }

        public void AddQuest ( int newQuest ) {
            if ( newQuest <= quest ) {
                return;
            }
            quest = new SecureInt( newQuest );
        }

        internal void ClearFeildState () {
            indexWordFeildState = new List<int>();
            inexLettersFeildState = new List<List<Vector2Int>>();
        }

        internal void AddFeildState ( int hideIndexWordEntryFeildState, List<Vector2Int> hideWordsEntryFeildState ) {
            indexWordFeildState.Add( hideIndexWordEntryFeildState );
            inexLettersFeildState.Add( hideWordsEntryFeildState );
        }

        internal void ClearFeildStateDailyPuzzle () {
            indexWordFeildStateDailyPuzzle = new List<int>();
            inexLettersFeildStateDailyPuzzle = new List<List<Vector2Int>>();
        }

        internal void AddFeildStateDailyPuzzle ( int hideIndexWordEntryFeildState, List<Vector2Int> hideWordsEntryFeildState ) {
            indexWordFeildStateDailyPuzzle.Add( hideIndexWordEntryFeildState );
            inexLettersFeildStateDailyPuzzle.Add( hideWordsEntryFeildState );
        }
    }
}