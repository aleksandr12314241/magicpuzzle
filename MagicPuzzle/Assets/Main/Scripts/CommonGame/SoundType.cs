﻿
namespace Common {
    public enum SoundType {
        Click,
        ChooseCategory,
        CupMoving,
        AddLamp,
        NoHints,
        CupFill,
        UseHint,
        CupHitting
    }
}
