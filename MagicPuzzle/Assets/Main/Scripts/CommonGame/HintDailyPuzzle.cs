﻿using System.Collections.Generic;
using Utility;

namespace Common {
    public class HintDailyPuzzle {
        public SecureInt ind;
        public List<SecureInt> countShow;

        public HintDailyPuzzle () {

            ind = new SecureInt( -1 );
            countShow = new List<SecureInt>();
        }
    }
}
