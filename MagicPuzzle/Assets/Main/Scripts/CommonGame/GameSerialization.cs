﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System;
using Utility;
using Newtonsoft.Json;

namespace Common {

    public class GameSerialization: MonoBehaviour {

        GameController _GC;

        internal void Init () {

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            MainSerializer();
        }

        public void MainSerializer () {

            string savePath = GetPathSaves();

            Player player = new Player();

            if ( !File.Exists( savePath ) ) {

                string deviceID = SystemInfo.deviceUniqueIdentifier;
                string deviceName = SystemInfo.deviceName;

                player.id_device = deviceID;
                player.name = deviceName;

                player.isSound = true;
                player.isFirstStart = true;
                player.upPieces = new List<CustomVector2Int>();

                SetStartState( player );

                LocalizationManager.Language language = GetLanguageBySystemLanguage();
                player.language = language.ToString();

                player.puzzleStateData = new Dictionary<string, PuzzleStateData>();

                string json = JsonConvert.SerializeObject( player );
                File.WriteAllText( savePath, json, Encoding.UTF8 );
                SaveResult( player );
            }
            else {
                string saveObject = GetJsonFromEncrypterString( savePath );

                player = ProcessPlayer( saveObject );
            }
            _GC.InitPlayer( player );
        }

        internal void SetStartState ( Player player ) {
            player.SetHints( 11 );
            //player.progressByLanguage = new Dictionary<string, ProgressByLanguage>();

            string[] arrayLanguages = Enum.GetNames( typeof( LocalizationManager.Language ) );
            for ( int i = 0; i < arrayLanguages.Length; i++ ) {
                ProgressByLanguage progressByLanguageTemp = new ProgressByLanguage();
                progressByLanguageTemp.hintQuest = new List<HintQuest>();
                progressByLanguageTemp.hintDailyPuzzle = new List<HintDailyPuzzle>();

                //player.progressByLanguage.Add( arrayLanguages[i], progressByLanguageTemp );
            }

            player.InitTutorial();
        }

        public Player ProcessPlayer ( string jsonString ) {

            Player player;
            player = JsonConvert.DeserializeObject<Player>( jsonString );
            return player;
        }

        #region Get's
        public string GetPathSaves () {

            string savePath;
#if UNITY_EDITOR
            savePath = Path.Combine( Application.dataPath, "Data/saves.json" );
#elif UNITY_ANDROID||UNITY_IOS
            savePath = Path.Combine( Application.persistentDataPath, "saves.json" );
#else
            savePath = Path.Combine( Application.persistentDataPath, "saves.json" );
#endif

            //if ( Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android ) {
            //    savePath = Application.persistentDataPath + "/saves.json";
            //}
            //else if ( Application.platform == RuntimePlatform.WindowsPlayer ) {
            //    savePath = Application.persistentDataPath + "/saves.json";
            //}
            //else {
            //    savePath = Application.dataPath + "/Data/saves.json";
            //}

            return savePath;
        }

        LocalizationManager.Language GetLanguageBySystemLanguage () {
            switch ( Application.systemLanguage ) {
                case SystemLanguage.Russian:
                    return LocalizationManager.Language.Ru;
                default:
                    return LocalizationManager.Language.En;
            }
        }
        #endregion

        #region saves
        public void SaveResult ( Player newPlayer ) {

            string savePath = GetPathSaves();
            string json = JsonConvert.SerializeObject( newPlayer );

            byte[] bytes = Encoding.UTF8.GetBytes( json );
            string hex = BitConverter.ToString( bytes );

#if UNITY_EDITOR
            string savePathDecipher = Application.dataPath + "/Data/saves_decipher.json";
            File.WriteAllText( savePathDecipher, json, Encoding.UTF8 );
#endif
            File.WriteAllText( savePath, hex.Replace( "-", "" ), Encoding.UTF8 );
        }
        #endregion

        internal string GetJsonFromEncrypterString ( string savePath ) {
            string saveObject = File.ReadAllText( savePath );
            return AuxiliaryTools.GetJsonFromEncrypterString( saveObject );
        }
    }
}