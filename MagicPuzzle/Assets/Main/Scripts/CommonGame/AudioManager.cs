﻿using UnityEngine;

namespace Common {
    public class AudioManager : MonoBehaviour {
        public static AudioManager instance = null; // Экземпляр объекта

        [SerializeField]
        AudioDataSet audioDataSet;
        [SerializeField]
        AudioSource myAudioSource;

        internal void Awake () {
            // Теперь, проверяем существование экземпляра
            if ( instance == null ) { // Экземпляр менеджера был найден
                instance = this; // Задаем ссылку на экземпляр объекта
            }
            else if ( instance == this ) { // Экземпляр объекта уже существует на сцене
                Destroy( gameObject ); // Удаляем объект
            }
            DontDestroyOnLoad( gameObject );
        }

        internal void PlaySoundByType ( SoundType soundType ) {
            myAudioSource.PlayOneShot( audioDataSet.GetAudioClipByType( soundType ) );
        }
    }
}
