﻿using System;
using Utility;

namespace Common {
    public class MyTimeClass {
        public SecureInt year;
        public SecureInt month;
        public SecureInt day;
        public SecureInt hour;
        public SecureInt minute;
        public SecureInt second;

        public MyTimeClass () {
        }

        public MyTimeClass ( int newYear, int newMonth, int newDay, int newHour, int newMinute, int newSecond ) {

            year = new SecureInt( newYear );
            month = new SecureInt( newMonth );
            day = new SecureInt( newDay );
            hour = new SecureInt( newHour );
            minute = new SecureInt( newMinute );
            second = new SecureInt( newSecond );
        }

        public MyTimeClass ( MyTimeClass newTime ) {

            year = newTime.year;
            month = newTime.month;
            day = newTime.day;
            hour = newTime.hour;
            minute = newTime.minute;
            second = newTime.second;
        }

        public MyTimeClass ( DateTime newTime ) {

            year = new SecureInt( newTime.Year );
            month = new SecureInt( newTime.Month );
            day = new SecureInt( newTime.Day );
            hour = new SecureInt( newTime.Hour );
            minute = new SecureInt( newTime.Minute );
            second = new SecureInt( newTime.Second );
        }

        internal DateTime GetDateTime () {

            if ( year <= 0 ) {
                return DateTime.MinValue;
            }

            return new DateTime( year.GetValue(), month.GetValue(), day.GetValue(), hour.GetValue(), minute.GetValue(), second.GetValue() );
        }
    }
}
