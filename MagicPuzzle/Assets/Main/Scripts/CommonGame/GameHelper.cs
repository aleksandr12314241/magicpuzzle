﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

internal static class GameHelper {
    internal static string GetPathToResourceData ( string fileName ) {

        string path;
        if ( Application.platform == RuntimePlatform.IPhonePlayer ) {
            path = Application.dataPath + "/Raw/" + fileName + ".json";
        }
        else if ( Application.platform == RuntimePlatform.Android ) {
            //string oriPath = System.IO.Path.Combine( Application.streamingAssetsPath, "/" + levelName + ".json" );
            string oriPath = Application.streamingAssetsPath + "/" + fileName + ".json";

            // Android only use WWW to read file
            WWW reader = new WWW( oriPath );
            while ( !reader.isDone ) { }

            string realPath = Application.persistentDataPath + "/" + fileName + ".json";
            File.WriteAllBytes( realPath, reader.bytes );

            path = realPath;
        }
        else {
            path = Application.dataPath + "/StreamingAssets/" + fileName + ".json";
        }

        return path;
    }

    internal static string GetPathToResourceDataNoExtension ( string fileName ) {

        string path;
        if ( Application.platform == RuntimePlatform.IPhonePlayer ) {
            path = Application.dataPath + "/Raw/" + fileName;
        }
        else if ( Application.platform == RuntimePlatform.Android ) {
            //string oriPath = System.IO.Path.Combine( Application.streamingAssetsPath, "/" + levelName + ".json" );
            string oriPath = Application.streamingAssetsPath + "/" + fileName;

            // Android only use WWW to read file
            WWW reader = new WWW( oriPath );
            while ( !reader.isDone ) { }

            string realPath = Application.persistentDataPath + "/" + fileName;
            File.WriteAllBytes( realPath, reader.bytes );

            path = realPath;
        }
        else {
            path = Application.dataPath + "/StreamingAssets/" + fileName;
        }

        return path;
    }

    internal static bool IsCorrectImageCanvas ( int col, int row, Dictionary<CustomVector2Int, PuzzlePieceConnect> puzzlePiecesConnect ) {
        if ( puzzlePiecesConnect.Count < col * row ) {
            return false;
        }

        int indexA = 0;

        for ( int i = 0; i < col; i++ ) {
            for ( int j = 0; j < row; j++ ) {
                indexA++;

                CustomVector2Int leftPiece = new CustomVector2Int( i - 1, j );
                CustomVector2Int upPiece = new CustomVector2Int( i, j - 1 );

                PuzzlePieceConnect puzzlePieceConnect = puzzlePiecesConnect[new CustomVector2Int( i, j )];
                PuzzlePieceConnectType checkPieceLeft = puzzlePieceConnect.Left;
                PuzzlePieceConnectType checkPieceUp = puzzlePieceConnect.Up;

                if ( leftPiece.x >= 0 ) {
                    PuzzlePieceConnect puzzlePieceConnectLeft = puzzlePiecesConnect[leftPiece];
                    PuzzlePieceConnectType right = puzzlePieceConnectLeft.Right;

                    if ( !CanPuzzlePieceConnectConnect( checkPieceLeft, right ) ) {
                        return false;
                    }
                }

                if ( upPiece.y >= 0 ) {
                    PuzzlePieceConnect puzzlePieceConnectUp = puzzlePiecesConnect[upPiece];
                    PuzzlePieceConnectType down = puzzlePieceConnectUp.Down;

                    if ( !CanPuzzlePieceConnectConnect( checkPieceUp, down ) ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    internal static bool CanPuzzlePieceConnectConnect ( PuzzlePieceConnectType first, PuzzlePieceConnectType second ) {
        if ( first == PuzzlePieceConnectType.Out && second == PuzzlePieceConnectType.In ) {
            return true;
        }
        else if ( first == PuzzlePieceConnectType.In && second == PuzzlePieceConnectType.Out ) {
            return true;
        }
        else if ( first == PuzzlePieceConnectType.Flat && second == PuzzlePieceConnectType.Flat ) {
            return true;
        }
        return false;
    }

    internal static float GetOffsetPivot ( PuzzlePieceConnectType puzzlePieceConnectType ) {
        if ( puzzlePieceConnectType.Equals( PuzzlePieceConnectType.Out ) ) {
            return 1f;
        }

        return 0f;
    }

    internal static Dictionary<string, List<PuzzlePieceConnectType>> GetPieceConnection ( string exitPiece ) {
        Dictionary<string, List<PuzzlePieceConnectType>> connectionTypeByExitPiece = new Dictionary<string, List<PuzzlePieceConnectType>>();
        switch ( exitPiece ) {
            case "all"://all
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out, PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out, PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out, PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out, PuzzlePieceConnectType.Flat
                } );
                break;
            case "up_left"://up_left
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                break;
            case "up"://up
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                break;
            case "up_right"://up_right
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                break;
            case "right"://right
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                break;
            case "down_right"://down_right
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                break;
            case "down"://down
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                break;
            case "down_left"://down_left
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                break;
            case "left"://left
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.Flat
                } );
                break;
            case "center"://center
                connectionTypeByExitPiece.Add( "u", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "r", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "d", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                connectionTypeByExitPiece.Add( "l", new List<PuzzlePieceConnectType>() {
                    PuzzlePieceConnectType.In, PuzzlePieceConnectType.Out
                } );
                break;
        }

        return connectionTypeByExitPiece;
    }

    internal static string GetExitPieceByIndex ( PuzzlePieceConnectType up, PuzzlePieceConnectType right, PuzzlePieceConnectType down,
            PuzzlePieceConnectType left ) {
        if ( up == PuzzlePieceConnectType.Flat && left == PuzzlePieceConnectType.Flat ) { //up_left
            return "up_left";
        }
        else if ( up == PuzzlePieceConnectType.Flat && right == PuzzlePieceConnectType.Flat ) { //up_right
            return "up_right";
        }
        else if ( down == PuzzlePieceConnectType.Flat && right == PuzzlePieceConnectType.Flat ) { //down_right
            return "down_right";
        }
        else if ( down == PuzzlePieceConnectType.Flat && left == PuzzlePieceConnectType.Flat ) { //down_left
            return "down_left";
        }
        else if ( up == PuzzlePieceConnectType.Flat ) {
            return "up";
        }
        else if ( right == PuzzlePieceConnectType.Flat ) {
            return "right";
        }
        else if ( down == PuzzlePieceConnectType.Flat ) {
            return "down";
        }
        else if ( left == PuzzlePieceConnectType.Flat ) {
            return "left";
        }

        return "center";
    }
}
