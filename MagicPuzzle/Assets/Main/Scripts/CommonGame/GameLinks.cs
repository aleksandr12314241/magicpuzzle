﻿using UnityEngine;

namespace Common {
    public class GameLinks: MonoBehaviour {
        [SerializeField]
        ColorDataSet colorDataSet;
        [SerializeField]
        AudioDataSet audioDataSet;
        [SerializeField]
        IconDataSet iconDataSet;
        [SerializeField]
        PuzzlePiecesDataSet puzzlePiecesDataSet;
        [SerializeField]
        PuzzleImageDataSet puzzleImageDataSet;

        internal ColorDataSet ColorDataSet {
            get {
                return colorDataSet;
            }
        }

        internal AudioDataSet AudioDataSet {
            get {
                return audioDataSet;
            }
        }

        internal IconDataSet IconDataSet {
            get {
                return iconDataSet;
            }
        }

        internal PuzzlePiecesDataSet PuzzlePiecesDataSet {
            get {
                return puzzlePiecesDataSet;
            }
        }

        internal PuzzleImageDataSet PuzzleImageDataSet {
            get {
                return puzzleImageDataSet;
            }
        }
    }
}
