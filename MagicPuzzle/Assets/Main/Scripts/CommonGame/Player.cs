﻿using System.Collections.Generic;
using System;
using Utility;
using UnityEngine;
using Puzzle;

namespace Common {
    public class Player {

        internal event Action OnChangeQuest = () => { };
        internal event Action OnChangeHintCount = () => { };
        internal event Action OnChangeCategiries = () => { };
        internal event Action OnSetLevel2RevertSequence = () => { };
        internal event Action<string> OnDoneTutorial2 = ( string param ) => { };

        public string name;
        public string id_device;

        public bool isFirstStart;

        public SecureInt hints;
        public string language;
        public Dictionary<string, bool> tutorialProgress;
        public Dictionary<string, bool> tutorialProgress2;
        public Dictionary<string, PuzzleStateData> puzzleStateData;
        public List<CustomVector2Int> upPieces;
        public int sizeI; // row
        public int sizeJ; // collumn

        public bool level2RevertSequence;
        public bool isSound;

        //SETTERS
        public void InitTutorial() {
            tutorialProgress = new Dictionary<string, bool>();
            tutorialProgress.Add( "first_puzzel", false );
            tutorialProgress.Add( "see_categiries", false );
            tutorialProgress.Add( "palindrome", false );
            tutorialProgress.Add( "end_level_2", false );
            tutorialProgress.Add( "hint_button", false );
            tutorialProgress.Add( "shop_daily_level_end", false );
            tutorialProgress.Add( "shop_daily_show", false );
            tutorialProgress.Add( "two_words", false );

            tutorialProgress2 = new Dictionary<string, bool>();
            tutorialProgress2.Add( "rewert_word", false );
            tutorialProgress2.Add( "is_click_on_hint", false );
            tutorialProgress2.Add( "is_anim_hint", false );
            tutorialProgress2.Add( "is_anim_shop", false );
            tutorialProgress2.Add( "see_shop_tutorial", false );
            tutorialProgress2.Add( "see_daily_tutorial", false );
            tutorialProgress2.Add( "two_words_wrong_word_first", false );
            tutorialProgress2.Add( "two_words_wrong_word_second", false );
            tutorialProgress2.Add( "two_words_restart", false );
        }

        //SETTERS
        public void SetProgressByLanguageQuest () {
            OnChangeQuest();
        }

        public void SetHints( int value ) {
            hints = new SecureInt( value );
            OnChangeHintCount();
        }

        internal void SetLevel2RevertSequence ( bool level2RevertSequence ) {
            this.level2RevertSequence = level2RevertSequence;

            OnSetLevel2RevertSequence();
        }

        internal void SetIsSound ( bool isSound ) {
            this.isSound = isSound;
        }

        //ADD
        public void AddHints ( int value ) {
            hints += value;

            if ( hints == 0 ) {
                hints = new SecureInt( 0 );
            }
            OnChangeHintCount();
        }

        public void DoneTutorial ( string tutorialId ) {
            tutorialProgress[tutorialId] = true;
        }
        public void DoneTutorial2 ( string tutorialId ) {
            tutorialProgress2[tutorialId] = true;

            OnDoneTutorial2( tutorialId );
        }

        public void SetProgressByLanguageCategiriesIgonreСheck ( string languageString, int categiries ) {

            OnChangeCategiries();
        }

        public void SetProgressByLanguageCategiries ( string languageString, int categiries ) {

            OnChangeCategiries();
        }

        public void AddProgressByLanguageQuest ( string languageString, int quest ) {
            OnChangeQuest();
        }

        //public void SetPuzzleStateData ( LevelInitingData[] levelsInitingData, LevelTemplateData levelTemplateData ) {
        //    int indexLevelTemplateData = 0;
        //    List<CustomVector2Int> visitedIndex = new List<CustomVector2Int>();
        //    List<List<CustomVector2Int>> puzzlePieceConnection = new List<List<CustomVector2Int>>();
        //    for ( int i = 0; i < levelsInitingData.Length; i++ ) {
        //        string imageSpriteName = levelsInitingData[i].spriteImage.name;
        //        CustomVector2Int sizeMatrixImage = levelsInitingData[i].puzzelTemplate.sizeMatrixImage;
        //        List<PuzzlePieceIndex> puzzlePieceByIndex = new List<PuzzlePieceIndex>();
        //        for ( int j = 0; j < levelsInitingData[i].puzzelTemplate.puzzlePieceByIndex.Count; i++ ) {
        //            PuzzlePieceIndex puzzlePieceIndexTemp = levelsInitingData[i].puzzelTemplate.puzzlePieceByIndex[j];
        //            PuzzlePieceIndex puzzlePieceIndex = new PuzzlePieceIndex( puzzlePieceIndexTemp.pzzlePieceIndex, puzzlePieceIndexTemp.pzzlePieceType );
        //            CustomVector2Int indexPuzzlePiece = puzzlePieceIndex.pzzlePieceIndex;
        //            if ( visitedIndex.Contains( indexPuzzlePiece ) ) {
        //                continue;
        //            }
        //        }
        //        List<PuzzlePieceIndex> puzzle = new List<PuzzlePieceIndex>();
        //    }
        //}

        internal void SetPuzzleStateData ( PuzzleModel puzzleModel ) {
            puzzleStateData = new Dictionary<string, PuzzleStateData>();

            for ( int i = 0; i < puzzleModel.puzzleLevelPieceDoneData.Length; i++ ) {
                PuzzleStateData puzzleStateDataEntry = new PuzzleStateData();
                puzzleStateDataEntry.sizeMatrixImage = puzzleModel.puzzleLevelPieceDoneData[i].GetSizePuzzleIndex();
                puzzleStateDataEntry.pieceFieldIndex = new List<CustomVector2Int>();
                puzzleStateDataEntry.playerPieces = new List<List<PlayerPuzzlePieceIndex>>();

                puzzleStateData.Add( puzzleModel.puzzleLevelPieceDoneData[i].GetMainImageName(), puzzleStateDataEntry );
            }

            for ( int i = 0; i < puzzleModel.listPieces.Length; i++ ) {
                for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
                    if ( puzzleModel.listPieces[i][j].isEmpty ) {
                        continue;
                    }

                    CustomVector2Int indexField = new CustomVector2Int( i, j );
                    List<PlayerPuzzlePieceIndex> playerPieces = new List<PlayerPuzzlePieceIndex>();

                    PuzzlePieceField puzzlePieceField = puzzleModel.listPieces[i][j].pieceCntr.GetPuzzlePieceField();
                    List<PuzzlePiece> puzzlePieces = puzzlePieceField.PuzzlePieces;
                    for ( int k = 0; k < puzzlePieces.Count; k++ ) {

                        playerPieces.Add( new PlayerPuzzlePieceIndex( puzzlePieces[k].GetIndexInMainImageMatrixPrimary(),
                                          puzzlePieces[k].GetNumPuzzlePiece(), puzzlePieceField.GetStartAngle(), puzzlePieceField.GetCurrentAngle() ) );
                    }

                    string spriteImageName = puzzlePieces[0].PuzzlePieceViewField.GetMainImageName();
                    puzzleStateData[spriteImageName].pieceFieldIndex.Add( indexField );
                    puzzleStateData[spriteImageName].playerPieces.Add( playerPieces );
                }
            }

            sizeI = puzzleModel.sizeI;
            sizeJ = puzzleModel.sizeJ;
        }

        internal void SetUpPieces ( List<CustomVector2Int> upPieces ) {
            this.upPieces = upPieces;
        }
    }
}
