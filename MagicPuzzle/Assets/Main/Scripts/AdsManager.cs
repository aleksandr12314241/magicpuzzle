﻿using UnityEngine;
using UnityEngine.Advertisements;
using System;

public class AdsManager : MonoBehaviour {
    public event Action OnErrorAds = () => { };

    public event Action OnEndAdsHint = () => { };
    public event Action OnEndAdsHint2 = () => { };

    //    void Start () {
    //        string gameId = "1234567";
    //        bool testMode = true;

    //#if UNITY_IOS
    //        gameId = "3025924";
    //#elif UNITY_ANDROID
    //        gameId = "3025925";
    //#endif
    //        Advertisement.Initialize( gameId, testMode );
    //    }
    //

    public void ShowAd () {
        const string RewardedPlacementId = "video";

        if ( !Advertisement.IsReady( RewardedPlacementId ) ) {
            Debug.Log( string.Format( "Ads not ready for placement '{0}'", RewardedPlacementId ) );
            //OnErrorAds();
            return;
        }

        //var options = new ShowOptions { resultCallback = HandleShowResultHint };
        Advertisement.Show( RewardedPlacementId );

        //Advertisement.Show( RewardedPlacementId, options );
    }

    public void ShowRewardedAdHint () {

        const string RewardedPlacementId = "rewardedVideo";

        if ( !Advertisement.IsReady( RewardedPlacementId ) ) {
            Debug.Log( string.Format( "Ads not ready for placement '{0}'", RewardedPlacementId ) );
            OnErrorAds();
            return;
        }

        var options = new ShowOptions { resultCallback = HandleShowResultHint };
        Advertisement.Show( RewardedPlacementId, options );
    }

    //#if UNITY_ADS
    private void HandleShowResultHint ( ShowResult result ) {
        switch ( result ) {
            case ShowResult.Finished:
                Debug.Log( "The ad was successfully shown." );
                OnEndAdsHint();
                break;
            case ShowResult.Skipped:
                Debug.Log( "The ad was skipped before reaching the end." );
                break;
            case ShowResult.Failed:
                OnErrorAds();
                //Debug.LogError("The ad failed to be shown.");
                break;
        }
    }


    internal bool ShowRewardedAdHint2 () {
        const string RewardedPlacementId = "rewardedVideo_2";

        if ( !Advertisement.IsReady( RewardedPlacementId ) ) {
            Debug.Log( string.Format( "Ads not ready for placement '{0}'", RewardedPlacementId ) );
            OnErrorAds();
            return false;
        }

        var options = new ShowOptions { resultCallback = HandleShowResultHint2 };
        Advertisement.Show( RewardedPlacementId, options );
        return true;
    }

    private void HandleShowResultHint2 ( ShowResult result ) {
        switch ( result ) {
            case ShowResult.Finished:
                Debug.Log( "The ad was successfully shown." );
                OnEndAdsHint2();
                break;
            case ShowResult.Skipped:
                Debug.Log( "The ad was skipped before reaching the end." );
                break;
            case ShowResult.Failed:
                OnErrorAds();
                //Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
