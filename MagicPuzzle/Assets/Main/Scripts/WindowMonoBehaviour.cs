﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {
    public class WindowMonoBehaviour: MonoBehaviour {
        internal virtual void InitAwake () {

        }

        internal virtual void InitStart () {

        }

        internal bool IsShown {
            get;
            private set;
        }

        public virtual void Show () {
            gameObject.SetActive( true );

            SetIsShown( true );
        }

        internal virtual void Hide () {
            gameObject.SetActive( false );

            SetIsShown( false );
        }

        protected void SetIsShown ( bool isShown ) {
            this.IsShown = isShown;
        }
    }
}

