﻿using UnityEngine;
using Common;
using UnityEngine.UI;
using DG.Tweening;

namespace Shop {
    public class ShopUI: WindowMonoBehaviour {
        [SerializeField]
        BuyUI _buyUI;
        [SerializeField]
        Button _close;
        [SerializeField]
        Button backBlack;
        [SerializeField]
        RectTransform shopUiContainerRectTransform;
        [SerializeField]
        GameObject shopTittles;

        float shopUiContainerSizeX;
        float hideMoreShopSizeY = 1500f;
        float showMoreShopSizeY = 2216f;

        GameController _GC;
        AdsManager adsManager;

        internal override void InitStart () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            adsManager = _GC.GetComponent<AdsManager>();

            adsManager.OnEndAdsHint += OnEndAdsHint;
            adsManager.OnEndAdsHint2 += OnEndAdsHint2;

            _close.onClick.AddListener( Close );
            backBlack.onClick.AddListener( Close );

            _buyUI.OnShowMoreShop += OnShowMoreShop;
            _buyUI.OnHideMoreShop += OnHideMoreShop;

            shopUiContainerSizeX = shopUiContainerRectTransform.sizeDelta.x;

            _buyUI.Init();
        }

        public override void Show () {
            base.Show();

            shopUiContainerRectTransform.sizeDelta = new Vector2( shopUiContainerSizeX, hideMoreShopSizeY );
            shopTittles.gameObject.SetActive( true );

            _buyUI.ShowWithountMore();
        }

        void Close () {
            base.Hide();
        }

        void OnShowMoreShop () {
            shopTittles.gameObject.SetActive( false );
            shopUiContainerRectTransform.DOSizeDelta( new Vector2( shopUiContainerSizeX, showMoreShopSizeY ), _buyUI.TimeAnimation / 2f );
        }

        void OnHideMoreShop () {
            shopTittles.gameObject.SetActive( true );
            shopUiContainerRectTransform.DOSizeDelta( new Vector2( shopUiContainerSizeX, hideMoreShopSizeY ), _buyUI.TimeAnimation / 2f );
        }

        void OnEndAdsHint () {
            if ( IsShown ) {
                Close();
            }
        }

        void OnEndAdsHint2 () {
            if ( IsShown ) {
                Close();
            }
        }
    }
}
