﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Menu;
using System;
using System.Collections;

namespace Common {
    public class MenuController: MonoBehaviour {
        internal event Action OnInitPrice = () => { };

        [SerializeField]
        WindowMonoBehaviour[] windowsScence;

        GameController _GC;
        AdsManager adsManager;
        PurchaserManager _purchaser;

        [SerializeField]
        HintAddedUI hintAddedUI;

        bool isInitPrice = false;
        internal bool IsInitPrice {
            get {
                return isInitPrice;
            }
        }

        InfoUI InfoUI;

        private void Awake () {
            for ( int i = 0; i < windowsScence.Length; i++ ) {
                windowsScence[i].InitAwake();
            }
        }

        void Start () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            adsManager = _GC.GetComponent<AdsManager>();
            adsManager.OnEndAdsHint += OnEndAdsHint;
            adsManager.OnEndAdsHint2 += OnEndAdsHint2;
            adsManager.OnErrorAds += OnErrorAds;

            _purchaser = _GC.GetComponent<PurchaserManager>();
            _purchaser.OnAddPurchasePack1 += OnAddPurchasePack1;
            _purchaser.OnAddPurchasePack2 += OnAddPurchasePack2;
            _purchaser.OnAddPurchasePack3 += OnAddPurchasePack3;
            _purchaser.OnAddPurchasePack4 += OnAddPurchasePack4;
            _purchaser.OnAddPurchasePack5 += OnAddPurchasePack5;
            _purchaser.OnAddPurchasePack6 += OnAddPurchasePack6;
            _purchaser.OnErrorBuy += OnErrorBuy;

            _GC.OnShowNonRewardAds += OnShowNonRewardAds;

            LocalizationManager.OnChangeLanguage += OnChangeLanguage;
            ChangeLanguage();

            Screen.fullScreen = false;

            for ( int i = 0; i < windowsScence.Length; i++ ) {
                windowsScence[i].InitStart();
            }

            InfoUI = ( InfoUI ) GetWindow<InfoUI>();

            //#if !UNITY_IOS
            StartCoroutine( InitPrice() );
            //#endif

            GameObject[] getRootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            for ( int i = 0; i < getRootGameObjects.Length; i++ ) {
                LampsCounterUI[] lampsCounterUI = getRootGameObjects[i].transform.GetComponentsInChildren<LampsCounterUI>( true );
                for ( int j = 0; j < lampsCounterUI.Length; j++ ) {
                    lampsCounterUI[j].Init();
                }
            }
        }

        internal WindowMonoBehaviour GetWindow<T>() where T: WindowMonoBehaviour {
            for ( int i = 0; i < windowsScence.Length; i++ ) {
                if ( windowsScence[i] is T ) {
                    return ( T )windowsScence[i];
                }
            }
            return null;
        }

        void StartQuestOrDailyPuzzle () {

            SceneManager.LoadScene( "level" );
        }

        #region pay
        void OnEndAdsHint () {
            GetRewordForAds();
        }
        void OnEndAdsHint2 () {
            GetRewordForAds2();
        }

        void OnErrorAds () {
            InfoUI.ShowInfo( LocalizationManager.GetText( "txt_info_window_error_ads" ), BlockType.Block );
        }

        void GetRewordForAds () {
            _GC.player.AddHints( 2 );
            hintAddedUI.ShowHintAdd( 2, HintAddedType.ByVideo1 );
            _GC.SavePlayer();
        }

        void GetRewordForAds2 () {
            _GC.player.AddHints( 4 );
            hintAddedUI.ShowHintAdd( 4, HintAddedType.ByVideo2 );
            _GC.SavePlayer();
        }

        void OnAddPurchasePack1 () {
            AddPurchasePack1();
        }
        void OnAddPurchasePack2 () {
            AddPurchasePack2();
        }
        void OnAddPurchasePack3 () {
            AddPurchasePack3();
        }
        void OnAddPurchasePack4 () {
            AddPurchasePack4();
        }
        void OnAddPurchasePack5 () {
            AddPurchasePack5();
        }

        void OnAddPurchasePack6 () {
            AddPurchasePack6();
        }

        void AddPurchasePack1 () {
            _GC.player.AddHints( 10 );
            hintAddedUI.ShowHintAdd( 10, HintAddedType.ByBuying );
            _GC.SavePlayer();
        }
        void AddPurchasePack2 () {

            _GC.player.AddHints( 22 );
            hintAddedUI.ShowHintAdd( 22, HintAddedType.ByBuying );
            _GC.SavePlayer();
        }
        void AddPurchasePack3 () {
            _GC.player.AddHints( 56 );
            hintAddedUI.ShowHintAdd( 56, HintAddedType.ByBuying );
            _GC.SavePlayer();
        }
        void AddPurchasePack4 () {

            _GC.player.AddHints( 126 );
            hintAddedUI.ShowHintAdd( 126, HintAddedType.ByBuying );
            _GC.SavePlayer();
        }
        void AddPurchasePack5 () {
            _GC.player.AddHints( 276 );
            hintAddedUI.ShowHintAdd( 276, HintAddedType.ByBuying );
            _GC.SavePlayer();
        }

        void AddPurchasePack6 () {
            _GC.player.AddHints( 424 );
            hintAddedUI.ShowHintAdd( 424, HintAddedType.ByBuying );
            _GC.SavePlayer();
        }

        void OnErrorBuy () {
            InfoUI.ShowInfo( LocalizationManager.GetText( "txt_info_window_error_buy" ), BlockType.Block );
        }
        #endregion

        public void RateApp () {

#if UNITY_IOS

            Application.OpenURL( "http://itunes.apple.com/US/app/id922488002" );
            //Application.OpenURL( "itms - apps://itunes.apple.com/US/app/id922488002" );

#endif

#if UNITY_ANDROID
            Application.OpenURL ( "market://details?id=com.apprope.wordbubbles" );
#endif

#if UNITY_STANDALONE_OSX

            Application.OpenURL( "http://itunes.apple.com/US/app/id922488002" );
            Debug.Log( "Stand Alone OSX" );
#endif

#if UNITY_STANDALONE_WIN
            Debug.Log( "Stand Alone Windows" );
#endif
        }

        void OnChangeLanguage ( LocalizationManager.Language language ) {
            ChangeLanguage();
        }

        public void ChangeLanguage () {
            GameObject[] getRootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            for ( int i = 0; i < getRootGameObjects.Length; i++ ) {
                TextPlaceholder[] _textSubstituters = getRootGameObjects[i].transform.GetComponentsInChildren<TextPlaceholder>( true );
                for ( int j = 0; j < _textSubstituters.Length; j++ ) {
                    _textSubstituters[j].UpdateTextById();
                }
            }
        }

        IEnumerator InitPrice () {

            while ( !_purchaser.GetIsReady() ) {
                yield return null;
            }

            OnInitPrice();
            isInitPrice = true;
        }

        void OnShowNonRewardAds () {
            adsManager.ShowAd();
        }
    }
}
