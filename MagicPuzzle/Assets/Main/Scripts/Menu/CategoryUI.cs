﻿using UnityEngine;
using Common;
using System;
using UnityEngine.UI;
using Utility;

namespace Menu {
    public class CategoryUI: MonoBehaviour {
        internal event Action<int> OnClickCategoryUI = ( int param ) => { };

        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Button _button;
        [SerializeField]
        Image cup;
        [SerializeField]
        Image cupFill;
        [SerializeField]
        Image lockImage;
        [SerializeField]
        Image backImage;
        [SerializeField]
        TextPlaceholder nameLevel;
        [SerializeField]
        Text nameLevelText;
        [SerializeField]
        Animator myAnimator;
        [SerializeField]
        GameObject fxAnimation;
        [SerializeField]
        Animator rayAnimator;
        [SerializeField]
        Image backLock;
        [SerializeField]
        Image backLock2;

        public RectTransform MyRectTransform {
            get {
                return myRectTransform;
            }
        }

        int num;
        int maxCountLevels;
        RectTransform cupRectTransform;
        Vector2 cupPosition;

        internal void SetStart ( IconDataSet.CupsSet cupsSet, string newNameLevel, int num ) {
            cupRectTransform = cup.GetComponent<RectTransform>();
            cupPosition = cupRectTransform.anchoredPosition;

            cup.sprite = cupsSet.CategoriesIconSilhouette;
            lockImage.sprite = cupsSet.CategoriesIconSilhouette;
            cupFill.sprite = cupsSet.CategoriesIcon;
            nameLevel.SetTextById( newNameLevel );
            //nameLevelText = nameLevel.GetComponent<Text>();
            //nameLevelText.text = cupLetters;

            this.num = num;
            //this.maxCountLevels = maxCountLevels;
            _button.onClick.AddListener( ShowSubLevels );
        }

        public void ShowSubLevels () {
            OnClickCategoryUI( num );
        }

        internal void SetNotOpen () {
            lockImage.gameObject.SetActive( true );
            cup.gameObject.SetActive( false );

            backImage.gameObject.SetActive( false );
            backLock2.gameObject.SetActive( true );

            myAnimator.enabled = false;
            cupRectTransform.anchoredPosition = cupPosition;
            cup.color = new Color( cup.color.r, cup.color.r, cup.color.b, 0f );
            lockImage.color = new Color( lockImage.color.r, lockImage.color.g, lockImage.color.b, 1f );
        }

        internal void SetSolved () {
            lockImage.gameObject.SetActive( false );
            cup.gameObject.SetActive( true );

            cupFill.fillAmount = 1f;

            backImage.gameObject.SetActive( true );
            backLock2.gameObject.SetActive( false );

            myAnimator.enabled = false;
            cupRectTransform.anchoredPosition = cupPosition;
            cup.color = new Color( cup.color.r, cup.color.r, cup.color.b, 1f );
            lockImage.color = new Color( lockImage.color.r, lockImage.color.g, lockImage.color.b, 0f );
            rayAnimator.gameObject.SetActive( false );
        }

        internal void SetCurrent ( int currentLevel ) {
            lockImage.gameObject.SetActive( false );
            cup.gameObject.SetActive( true );

            float fillCount = currentLevel / ( float ) maxCountLevels;
            cupFill.fillAmount = fillCount;

            backImage.gameObject.SetActive( true );
            backLock2.gameObject.SetActive( false );

            myAnimator.enabled = false;
            cupRectTransform.anchoredPosition = cupPosition;
            cup.color = new Color( cup.color.r, cup.color.r, cup.color.b, 1f );
            lockImage.color = new Color( lockImage.color.r, lockImage.color.g, lockImage.color.b, 0f );
            rayAnimator.gameObject.SetActive( false );
        }

        internal void ShowOpenCategoryAnimation () {
            SetNotOpen();
            cup.gameObject.SetActive( true );

            fxAnimation.gameObject.SetActive( true );

            this.CustomInvoke( 0.2f, () => {
                backLock2.gameObject.SetActive( false );

                backImage.gameObject.SetActive( true );

                myAnimator.enabled = true;
                rayAnimator.gameObject.SetActive( true );
            } );
            this.CustomInvoke( 4f, () => {
                DiactivateAnimator();
            } );
        }

        internal void DiactivateAnimator () {
            fxAnimation.gameObject.SetActive( false );
            myAnimator.enabled = false;
        }
    }
}
