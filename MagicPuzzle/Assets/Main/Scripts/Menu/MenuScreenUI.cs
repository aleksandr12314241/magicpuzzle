﻿using UnityEngine;
using System.Collections;

namespace Menu {
    public class MenuScreenUI: MonoBehaviour {
        [SerializeField]
        GameObject upperLockObject;

        Coroutine _showUpdate;
        Coroutine _hideUpdate;

        protected float step = 10000f;

        internal bool IsShown {
            private set;
            get;
        }

        RectTransform myTransform;
        Vector2 needPosition;

        internal void Init () {
            myTransform = this.gameObject.GetComponent<RectTransform>();
        }

        internal void ShowScreeenImmediately () {
            myTransform.anchoredPosition = Vector2.zero;

            Show();

            StartShowScreeen();
            EndShowScreeen();
        }

        internal void ShowScreeen () {
            Show();
            if ( _hideUpdate != null ) {
                StopCoroutine( _hideUpdate );
                _hideUpdate = null;
            }
            if ( _showUpdate != null ) {
                StopCoroutine( _showUpdate );
                _showUpdate = null;
            }
            _showUpdate = StartCoroutine( ShowUpdate() );
            StartShowScreeen();
        }


        internal void Show () {
            gameObject.SetActive( true );

            if ( IsShown ) {
                return;
            }

            IsShown = true;
            upperLockObject.SetActive( false );
        }

        protected virtual void StartShowScreeen () { }
        protected virtual void EndShowScreeen () { }

        IEnumerator ShowUpdate () {
            for ( ; ; ) {
                yield return new WaitForEndOfFrame();

                if ( ( myTransform.anchoredPosition.x * myTransform.anchoredPosition.x ) <= ( step * Time.deltaTime * step * Time.deltaTime ) ) {
                    myTransform.anchoredPosition = Vector2.zero;

                    EndShowScreeen();
                    break;
                }
                else {

                    // поправить  этот апдейт
                    myTransform.anchoredPosition = Vector2.MoveTowards( myTransform.anchoredPosition, Vector2.zero, step * Time.deltaTime );
                }
            }
        }

        internal void HideScreeenImmediately ( int newSide ) {
            needPosition = new Vector2( myTransform.rect.width * newSide, myTransform.anchoredPosition.y );
            myTransform.anchoredPosition = needPosition;
            Hide();
            StartHideScreeen();
        }

        internal void HideScreeen ( int newSide ) {
            needPosition = new Vector2( myTransform.rect.width * newSide, myTransform.anchoredPosition.y );
            if ( _showUpdate != null ) {
                StopCoroutine( _showUpdate );
            }
            _hideUpdate = StartCoroutine( HideUpdate() );
            StartHideScreeen();
        }

        internal void Hide () {
            IsShown = false;
            upperLockObject.SetActive( true );
            gameObject.SetActive( false );
        }

        protected virtual void StartHideScreeen () { }

        IEnumerator HideUpdate () {
            for ( ; ; ) {
                yield return new WaitForEndOfFrame();

                if ( ( myTransform.anchoredPosition - needPosition ).sqrMagnitude <= step * Time.deltaTime * step * Time.deltaTime ) {
                    myTransform.anchoredPosition = needPosition;
                    Hide();
                    break;
                }
                else {

                    // поправить  этот апдейт
                    myTransform.anchoredPosition = Vector2.MoveTowards( myTransform.anchoredPosition, needPosition, step * Time.deltaTime );
                }
            }
        }
    }
}
