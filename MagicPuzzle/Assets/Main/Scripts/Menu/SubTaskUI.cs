﻿using UnityEngine;
using System.Collections;
using Common;
using System;
using UnityEngine.UI;

public class SubTaskUI : MonoBehaviour {
    public event Action<int> OnStartLevel = ( int param ) => { };

    [SerializeField]
    RectTransform myRectTransform;
    [SerializeField]
    Button subTasButton;
    [SerializeField]
    Image backImage;
    [SerializeField]
    Text numSubLevel;
    [SerializeField]
    TextPlaceholder numSubLevelDescription;
    [SerializeField]
    Image lockImage;
    [SerializeField]
    Image backLock2;
    [SerializeField]
    Image backLock;

    Color colorDoneText;
    Color colorCurrentText;

    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    int level;

    private void Awake () {
        subTasButton.onClick.AddListener( StartLevel );

        colorDoneText = new Color( 188f, 180f, 238f, 256f ) / 256f;
        colorCurrentText = new Color( 255f, 236f, 217f, 256f ) / 256f;
    }

    public void SetStart ( int level ) {
        numSubLevel.text = level.ToString();
        this.level = level;
    }

    public void SetNotOpen () {
        lockImage.gameObject.SetActive( true );
        numSubLevel.gameObject.SetActive( false );

        backImage.gameObject.SetActive( false );
        backLock2.gameObject.SetActive( true );

        numSubLevelDescription.SetTextById( "txt_main_puzzle_sub_level", level.ToString() );
    }

    public void SetSolved () {
        lockImage.gameObject.SetActive( false );
        numSubLevel.gameObject.SetActive( true );

        backImage.gameObject.SetActive( true );
        backLock2.gameObject.SetActive( false );

        numSubLevel.color = colorDoneText;

        numSubLevelDescription.SetTextById( "txt_main_puzzle_sub_level_desc" );
    }

    internal void SetCurrent () {
        lockImage.gameObject.SetActive( false );
        numSubLevel.gameObject.SetActive( true );

        backImage.gameObject.SetActive( true );
        backLock2.gameObject.SetActive( false );

        numSubLevelDescription.SetTextById( "txt_main_puzzle_sub_level_desc" );

        numSubLevel.color  = colorCurrentText;
    }

    void StartLevel () {
        OnStartLevel( level );
    }
}
