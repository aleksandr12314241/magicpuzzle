﻿using UnityEngine;
using Common;
using UnityEngine.UI;
using System;
using Utility;
using DG.Tweening;

namespace Menu {
    public class HowToPlayUI: WindowMonoBehaviour {
        public event Action OnHideWindow = () => { };

        [SerializeField]
        AudioSource goAudioUI;
        [SerializeField]
        Animator _myAnimator;
        [SerializeField]
        Button _closeButton;
        [SerializeField]
        Button blackBackButton;
        [SerializeField]
        RectTransform tutorialContainer;
        [SerializeField]
        GameObject[] listTutuorial;
        [SerializeField]
        GameObject[] listTutuorialDot;
        [SerializeField]
        Animator[] listTutuorialAnimators;
        [SerializeField]
        RuntimeAnimatorController tutoria2lMainAnimatorController;
        [SerializeField]
        RuntimeAnimatorController tutorial2RevertAnimatorController;
        [SerializeField]
        Button _nextButton;
        [SerializeField]
        Button _prevButton;
        [SerializeField]
        Button okButton;
        [SerializeField]
        GameObject tutuorialDotContainer;

        float stepPosTut;
        int currentTut;
        GameController _GC;

        internal override void InitAwake () {
            _closeButton.onClick.AddListener( CloseWnd );
            blackBackButton.onClick.AddListener( CloseWnd );

            _nextButton.onClick.AddListener( NextTutorial );
            _prevButton.onClick.AddListener( PrevTutorial );
            okButton.onClick.AddListener( CloseWnd );

            for ( int i = 0; i < listTutuorialAnimators.Length; i++ ) {
                DisableAnimatiobTutorial( i );
                EndAnimatiobTutorial( i );
            }

            stepPosTut = tutorialContainer.sizeDelta.x;
        }

        internal override void InitStart () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            _GC.player.OnSetLevel2RevertSequence += OnSetLevel2RevertSequence;
            SetAnimationToTutorail2();
        }

        public void ShowTutorialWnd () {
            base.Show();

            tutorialContainer.anchoredPosition = new Vector2( 0f, tutorialContainer.anchoredPosition.y );

            _myAnimator.enabled = true;
            _myAnimator.SetBool( "show", true );

            Vector2 posTut = Vector2.zero;
            currentTut = 0;

            for ( int i = 0; i < listTutuorial.Length; i++ ) {
                listTutuorialDot[i].SetActive( false );
            }

            for ( int i = 0; i < listTutuorialAnimators.Length; i++ ) {
                DisableAnimatiobTutorial( i );
                EndAnimatiobTutorial( i );
            }

            listTutuorialDot[currentTut].SetActive( true );

            _nextButton.enabled = false;
            _prevButton.enabled = false;

            this.CustomInvoke( 0.5f, () => {
                EnableAnimatiobTutorial( currentTut );
                StartAnimatiobTutorial( currentTut );

                _nextButton.enabled = true;
                _prevButton.enabled = true;
            } );

            SetButtons();

            okButton.gameObject.SetActive( false );
            _nextButton.gameObject.SetActive( true );
            _prevButton.gameObject.SetActive( true );

            tutuorialDotContainer.gameObject.SetActive( true );
        }

        public void ShowTutorialWnd ( int indexTutorial ) {
            base.Show();
            _myAnimator.enabled = true;
            _myAnimator.SetBool( "show", true );

            Vector2 posTut = Vector2.zero;
            currentTut = indexTutorial;
            tutorialContainer.anchoredPosition = new Vector2( currentTut * stepPosTut * -1, tutorialContainer.anchoredPosition.y );

            for ( int i = 0; i < listTutuorial.Length; i++ ) {
                listTutuorialDot[i].SetActive( false );
            }

            for ( int i = 0; i < listTutuorialAnimators.Length; i++ ) {
                DisableAnimatiobTutorial( i );
                EndAnimatiobTutorial( i );
            }

            okButton.enabled = false;

            this.CustomInvoke( 0.5f, () => {
                EnableAnimatiobTutorial( currentTut );
                StartAnimatiobTutorial( currentTut );

                okButton.enabled = true;
            } );

            SetButtons();

            okButton.gameObject.SetActive( true );
            _nextButton.gameObject.SetActive( false );
            _prevButton.gameObject.SetActive( false );
            tutuorialDotContainer.gameObject.SetActive( false );
        }

        void EnableAnimatiobTutorial ( int indexTutorial ) {
            listTutuorialAnimators[indexTutorial].enabled = true;
        }

        void DisableAnimatiobTutorial ( int indexTutorial ) {
            listTutuorialAnimators[indexTutorial].enabled = false;
        }

        void StartAnimatiobTutorial ( int indexTutorial ) {
            listTutuorialAnimators[indexTutorial].SetBool( "show", true );
        }

        void EndAnimatiobTutorial ( int indexTutorial ) {
            listTutuorialAnimators[indexTutorial].SetBool( "show", false );
        }

        public void CloseWnd () {
            goAudioUI.clip = Resources.Load<AudioClip>( "Common_sounds/click2" );
            goAudioUI.Play();

            OnHideWindow();

            for ( int i = 0; i < listTutuorialAnimators.Length; i++ ) {
                EndAnimatiobTutorial( i );
            }

            _myAnimator.SetBool( "show", false );

            _nextButton.enabled = false;
            _prevButton.enabled = false;

            this.CustomInvoke( 0.33f, () => {
                base.Hide();
            } );
        }

        void NextTutorial () {
            int tempCurrentTut = currentTut + 1;
            if ( tempCurrentTut >= listTutuorialAnimators.Length ) {
                return;
            }

            _nextButton.enabled = false;
            _prevButton.enabled = false;

            DisableAnimatiobTutorial( currentTut );

            tutorialContainer.DOAnchorPosX( ( tempCurrentTut * stepPosTut * -1 ), 0.2f ).OnComplete( () => {
                _nextButton.enabled = true;
                _prevButton.enabled = true;

                EnableAnimatiobTutorial( currentTut );
                EnableAnimatiobTutorial( tempCurrentTut );
                StartAnimatiobTutorial( tempCurrentTut );
                EndAnimatiobTutorial( currentTut );

                listTutuorialDot[currentTut].gameObject.SetActive( false );
                listTutuorialDot[tempCurrentTut].gameObject.SetActive( true );

                currentTut = tempCurrentTut;
                SetButtons();
            } );
        }

        void PrevTutorial () {
            int tempCurrentTut = currentTut - 1;
            if ( tempCurrentTut < 0 ) {
                return;
            }

            _nextButton.enabled = false;
            _prevButton.enabled = false;

            DisableAnimatiobTutorial( currentTut );


            tutorialContainer.DOAnchorPosX( ( tempCurrentTut * stepPosTut * -1 ), 0.2f ).OnComplete( () => {
                _nextButton.enabled = true;
                _prevButton.enabled = true;

                EnableAnimatiobTutorial( currentTut );
                EnableAnimatiobTutorial( tempCurrentTut );
                StartAnimatiobTutorial( tempCurrentTut );
                EndAnimatiobTutorial( currentTut );

                listTutuorialDot[currentTut].gameObject.SetActive( false );
                listTutuorialDot[tempCurrentTut].gameObject.SetActive( true );

                currentTut = tempCurrentTut;
                SetButtons();
            } );
        }

        void SetButtons () {
            if ( currentTut <= 0 ) {
                _prevButton.gameObject.SetActive( false );
            }
            else {
                _prevButton.gameObject.SetActive( true );
            }

            if ( currentTut >= listTutuorialAnimators.Length - 1 ) {
                _nextButton.gameObject.SetActive( false );
            }
            else {
                _nextButton.gameObject.SetActive( true );
            }
        }

        void OnSetLevel2RevertSequence () {
            SetAnimationToTutorail2();
        }

        void SetAnimationToTutorail2 () {
            if ( _GC.player.level2RevertSequence ) {
                listTutuorialAnimators[1].runtimeAnimatorController = tutoria2lMainAnimatorController;

            }
            else {
                listTutuorialAnimators[1].runtimeAnimatorController = tutorial2RevertAnimatorController;
            }
        }
    }
}
