﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Menu {
    public class MenuScreenUIScrollable: MenuScreenUI {
        [SerializeField]
        ScrollRect scrollObj;
        [SerializeField]
        RectTransform scrollContent;

        //Coroutine _scrollUpdate;
        float scrollContentPositionX;
        Vector2 scrollContentPosition;

        public ScrollRect SscrollObj {
            get {
                return scrollObj;
            }
        }
        public RectTransform ScrollContent {
            get {
                return scrollContent;
            }
        }

        public void ShowScreeen ( float newScrollContentPosition ) {
            base.ShowScreeen();

            scrollContentPositionX = newScrollContentPosition;

            scrollContentPosition = new Vector2( 0f, scrollContentPositionX );

            scrollContent.anchoredPosition = new Vector2( scrollContent.anchoredPosition.x, scrollContentPositionX );
        }

        protected override void StartShowScreeen () {
            if ( scrollContent != null ) {
                //scrollContent.anchoredPosition = new Vector2( 0f, 0f );
                scrollContent.anchoredPosition = new Vector2( scrollContent.anchoredPosition.x, scrollContentPositionX );
            }
        }

        protected override void EndShowScreeen () {
            //scrollObj.movementType = ScrollRect.MovementType.Unrestricted;
            //_scrollUpdate = StartCoroutine( ScrollUpdate() );
        }

        protected override void StartHideScreeen () {
            //if ( _scrollUpdate == null ) {
            //    return;
            //}

            scrollContent.anchoredPosition = new Vector2( scrollContent.anchoredPosition.x, scrollContentPositionX );

            //StopCoroutine( _scrollUpdate );
            //_scrollUpdate = null;
        }

        //IEnumerator ScrollUpdate () {
        //    for ( ;; ) {
        //        yield return new WaitForEndOfFrame();

        //        if ( ( scrollContent.anchoredPosition - scrollContentPosition ).sqrMagnitude <= step * step * Time.deltaTime * Time.deltaTime ) {
        //            scrollContent.anchoredPosition = new Vector2( scrollContent.anchoredPosition.x, scrollContentPositionX );
        //            //Invoke( "SetScrolToEndFale", 0.1f );
        //            scrollObj.movementType = ScrollRect.MovementType.Elastic;
        //            break;
        //        }
        //        else {
        //            scrollContent.anchoredPosition = Vector2.MoveTowards( scrollContent.anchoredPosition, scrollContentPosition, step * Time.deltaTime );
        //        }
        //    }
        //}

        public void SetSrollContentPosition ( float scrollContentPositionX ) {
            this.scrollContentPositionX = scrollContentPositionX;
            this.scrollContentPosition = new Vector2( 0f, this.scrollContentPositionX );

            scrollContent.anchoredPosition = new Vector2( scrollContent.anchoredPosition.x, this.scrollContentPositionX );
        }
    }
}
