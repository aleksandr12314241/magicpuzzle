﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Common {
    public class SubLevelButtonUI: WindowMonoBehaviour {
        public event Action OnPlayButton = () => { };
        public event Action OnInfoButton = () => { };

        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        RectTransform tailUp;
        [SerializeField]
        RectTransform tailDown;
        [SerializeField]
        RectTransform panelRectTransform;
        [SerializeField]
        Button playButton;
        [SerializeField]
        Button infoButton;

        Vector2 rangeXTailDown;
        Vector2 rangeXTailUp;
        Camera camera;

        internal override void InitAwake () {
            camera = Camera.main;

            rangeXTailDown = new Vector2( -524f, 524f );
            rangeXTailUp = new Vector2( -366f, 524f );

            playButton.onClick.AddListener( PlayButton );
            infoButton.onClick.AddListener( InfoButton );
        }

        internal void ShowInfoMousePosition ( float offset = 0f ) {
            Vector2 mousPosition = Input.mousePosition;
            Vector2 ancoredPositionOnScreen;

            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRectTransform, mousPosition, camera, out ancoredPositionOnScreen );

            TailType tailType = TailType.None;

            if ( ancoredPositionOnScreen.y >= 0 ) {
                tailType = TailType.Up;
                ancoredPositionOnScreen = new Vector2( ancoredPositionOnScreen.x, ancoredPositionOnScreen.y - offset );
            }
            else {
                tailType = TailType.Down;
                ancoredPositionOnScreen = new Vector2( ancoredPositionOnScreen.x, ancoredPositionOnScreen.y + offset );
            }

            ShowInfoWithTailAndPositionY( ancoredPositionOnScreen.y, tailType, ancoredPositionOnScreen.x );
        }

        internal void ShowInfoWithTailAndPositionY ( float panelPosY, TailType tailType, float tailPosX ) {
            base.Show();

            //_infoText.text = textInfo;

            panelRectTransform.anchoredPosition = new Vector2( 0f, panelPosY );

            ShowTail( tailType, tailPosX );
        }

        void ShowTail ( TailType tailType, float posX = 0f ) {
            float posXTemp = posX;

            switch ( tailType ) {
                case TailType.Up:
                    tailUp.gameObject.SetActive( true );
                    tailDown.gameObject.SetActive( false );

                    if ( posXTemp < rangeXTailUp.x ) {
                        posXTemp = rangeXTailUp.x;
                    }
                    else if ( posXTemp > rangeXTailUp.y ) {
                        posXTemp = rangeXTailUp.y;
                    }
                    tailUp.anchoredPosition = new Vector2( posXTemp, tailUp.anchoredPosition.y );

                    break;
                case TailType.Down:
                    tailUp.gameObject.SetActive( false );
                    tailDown.gameObject.SetActive( true );

                    if ( posXTemp < rangeXTailDown.x ) {
                        posXTemp = rangeXTailDown.x;
                    }
                    else if ( posXTemp > rangeXTailDown.y ) {
                        posXTemp = rangeXTailDown.y;
                    }
                    tailDown.anchoredPosition = new Vector2( posXTemp, tailDown.anchoredPosition.y );

                    break;
                default:
                    tailUp.gameObject.SetActive( false );
                    tailDown.gameObject.SetActive( false );
                    break;
            }
        }

        void PlayButton () {
            OnPlayButton();
        }

        void InfoButton () {
            OnInfoButton();
        }
    }
}
