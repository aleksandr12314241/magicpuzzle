﻿using UnityEngine;

namespace Common {
    public class LoadCircle : MonoBehaviour {
        public static LoadCircle instance = null; // Экземпляр объекта

        [SerializeField]
        GameObject overObj;

        internal void Awake () {
            // Теперь, проверяем существование экземпляра
            if ( instance == null ) { // Экземпляр менеджера был найден
                instance = this; // Задаем ссылку на экземпляр объекта
            }
            else if ( instance == this ) { // Экземпляр объекта уже существует на сцене
                Destroy( gameObject ); // Удаляем объект
            }
            DontDestroyOnLoad( gameObject );
        }

        internal void ShowUpdateCircle () {
#if UNITY_IOS
            Handheld.SetActivityIndicatorStyle( UnityEngine.iOS.ActivityIndicatorStyle.Gray );
#elif UNITY_ANDROID
            Handheld.SetActivityIndicatorStyle( AndroidActivityIndicatorStyle.Large );
#endif
            Handheld.StartActivityIndicator();

            overObj.SetActive( true );
        }

        internal void HideUpdateCircle () {
            Handheld.StopActivityIndicator();

            overObj.SetActive( false );
        }
    }
}
