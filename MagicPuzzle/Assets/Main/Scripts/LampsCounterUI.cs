﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Shop;

namespace Common {
    public class LampsCounterUI: MonoBehaviour {
        [SerializeField]
        Image lampOn;
        [SerializeField]
        Image lampOff;
        [SerializeField]
        Button pluseLamp;
        [SerializeField]
        Text lampCounter;

        GameController _GC;
        MenuController menuController;
        ShopUI _shopUI;

        internal void Init () {
            MenuController menuController = FindObjectOfType<MenuController>();
            _shopUI = ( ShopUI ) menuController.GetWindow<ShopUI>();

            pluseLamp.onClick.AddListener( ShowShop );

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            SetLampCount();
            _GC.player.OnChangeHintCount += SetLampCount;
        }

        void ShowShop () {
            _shopUI.Show();
        }

        internal void SetLampCount () {
            lampCounter.text = _GC.player.hints.GetValue().ToString();

            if ( _GC.player.hints.GetValue() <= 0 ) {
                lampOn.gameObject.SetActive( false );
                lampOff.gameObject.SetActive( true );
            }
            else {
                lampOn.gameObject.SetActive( true );
                lampOff.gameObject.SetActive( false );
            }
        }
    }
}
