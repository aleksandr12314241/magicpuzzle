﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Common;
using System.Text.RegularExpressions;
using System;

namespace Cheater {
    public class CheaterController: MonoBehaviour {
        [SerializeField]
        Button closeButton;
        [SerializeField]
        InputField cheaterInputField;
        [SerializeField]
        Text console;
        [SerializeField]
        Button applyButton;

        GameController _GC;

        private void Start () {
            Debug.Log( "cheater init Start" );

            closeButton.onClick.AddListener( Close );
            cheaterInputField.onEndEdit.AddListener( UseCheat );
            applyButton.onClick.AddListener( UseCheat );

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
        }

        public void Open () {
            gameObject.SetActive( true );
            //console.text = "";
            cheaterInputField.text = "";
        }

        void Close () {
            gameObject.SetActive( false );
        }

        void UseCheat () {
            string str = cheaterInputField.text;
        }

        void UseCheat ( string str ) {
            string strToLever = str.ToLower();

            if ( strToLever.Equals( "clear save" ) || strToLever.Equals( "clear saves" ) ) {
                ClearSaves();
            }
            else if ( strToLever.Contains( "set cat" ) || strToLever.Contains( "set category" ) || strToLever.Contains( "set categori" ) ) {
                SetCategory( str );
            }
            else if ( strToLever.Contains( "set level" ) ) {
                Setlevel( str );
            }
            else if ( strToLever.Contains( "pluse hint" ) || strToLever.Contains( "plus hint" ) || strToLever.Contains( "pluse hints" ) ||
                      strToLever.Contains( "plus hints" ) ) {
                AddHint( str );
            }
            else if ( strToLever.Contains( "minuse hint" ) || strToLever.Contains( "minus hint" ) || strToLever.Contains( "minuse hints" ) ||
                      strToLever.Contains( "minus hints" ) ) {
                RemoveHint( str );
            }
            else {
                NoSuchComand();
            }
        }

        void ClearSaves () {
            _GC.GameSerialization.SetStartState( _GC.player );
            _GC.SavePlayer();

            AddComandInConsole( "сейвы сброшены " );

            //SceneManager.LoadScene( "Main" );
        }

        int GetNumbersFromString ( string str ) {
            string resultString = Regex.Match( str, @"\d+" ).Value;
            if ( resultString == "" ) {
                return 0;
            }
            return int.Parse( resultString );
        }

        void SetCategory ( string str ) {
            int category = GetNumbersFromString( str ) - 1;

            if ( category < 0 ) {
                category = 0;
            }

            AddComandInConsole( "установлена категория " + category );

            string language = LocalizationManager.GetLanguageString();
            _GC.player.SetProgressByLanguageCategiriesIgonreСheck( language, category );
            //_GC.player.SetProgressByLanguageQuest( language, 0 );
            _GC.SavePlayer();
        }

        void Setlevel ( string str ) {
            int quest = GetNumbersFromString( str ) - 1;

            if ( quest < 0 ) {
                quest = 0;
            }

            string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

            //int currentCat = progressByLanguage.categiries.GetValue();

            AddComandInConsole( "установлен уровень " + quest );

            //_GC.player.SetProgressByLanguageQuest( language, quest );
            _GC.SavePlayer();
        }

        void AddHint ( string str ) {
            int hinCount = GetNumbersFromString( str );
            AddComandInConsole( "плюс +" + hinCount + " хинтов" );

            _GC.player.AddHints( hinCount );
        }

        void RemoveHint ( string str ) {
            int hinCount = GetNumbersFromString( str );

            AddComandInConsole( "минус -" + hinCount + " хинтов" );

            _GC.player.AddHints( hinCount * -1 );
        }

        void AddComandInConsole ( string str ) {
            console.text += "\n <color=green>" + str + "</color> ";
        }

        void NoSuchComand () {
            console.text += "\n <color=red>нет такой моманды</color> ";
        }
    }
}