﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Menu {
    public class StartUI: MonoBehaviour {
        [SerializeField]
        Button playButtn;

        private void Start () {
            playButtn.onClick.AddListener( StartHide );
        }

        void StartHide () {
            gameObject.SetActive( false );
        }
    }
}
