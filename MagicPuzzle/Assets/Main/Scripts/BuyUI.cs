﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Utility;

namespace Common {
    public class BuyUI: MonoBehaviour {
        internal event Action OnShowMoreShop = () => { };
        internal event Action OnHideMoreShop = () => { };

        [SerializeField]
        Animator _animator;
        [SerializeField]
        Button _freeHint;
        [SerializeField]
        Button _buyConsumable1;
        [SerializeField]
        Button _buyConsumable2;
        [SerializeField]
        Button _buyConsumable3;
        [SerializeField]
        Button _buyConsumable4;
        [SerializeField]
        Button _buyConsumable5;
        [SerializeField]
        Button _buyConsumable6;
        [SerializeField]
        Text _priceText1;
        [SerializeField]
        Text _priceText2;
        [SerializeField]
        Text _priceText3;
        [SerializeField]
        Text _priceText4;
        [SerializeField]
        Text _priceText5;
        [SerializeField]
        Text _priceText6;
        [SerializeField]
        Text _priceText1_1;
        [SerializeField]
        Text _priceText4_1;
        [SerializeField]
        Button _freeHint_1;
        [SerializeField]
        Button _buyConsumable1_1;
        [SerializeField]
        Button _buyConsumable4_1;
        [SerializeField]
        RectTransform showcaseRectTransform;
        [SerializeField]
        CanvasGroup showcaseCanvasGroup;
        [SerializeField]
        RectTransform shopItemListRectTransform;
        [SerializeField]
        Image shopItemOver;
        [SerializeField]
        Button moreButton;
        [SerializeField]
        Button backeToShowcaseButton;
        [SerializeField]
        CanvasGroup shopItemListCanvasGroup;
        [SerializeField]
        float startPosYShowcase = -405f;
        [SerializeField]
        float startPosYShopItemList = -74f;

        float offsetAnchorPosY = 200f;
        GameController _GC;
        AdsManager adsManager;
        PurchaserManager _purchaser;

        float timeAnimation = 0.25f;
        public float TimeAnimation {
            get {
                return timeAnimation;
            }
        }

        bool isMoreShop = false;

        ButtonPress moreButtonPress;
        ButtonPress backeToShowcaseButtonPress;

        internal void Init () {

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            adsManager = _GC.GetComponent<AdsManager>();
            _purchaser = _GC.GetComponent<PurchaserManager>();

            MenuController menuController = GameObject.FindObjectOfType<MenuController>();

            _freeHint.onClick.AddListener( TrySeeVideo );
            _buyConsumable1.onClick.AddListener( BuyConsumable1 );
            _buyConsumable2.onClick.AddListener( BuyConsumable2 );
            _buyConsumable3.onClick.AddListener( BuyConsumable3 );
            _buyConsumable4.onClick.AddListener( BuyConsumable4 );
            _buyConsumable5.onClick.AddListener( BuyConsumable5 );
            _buyConsumable6.onClick.AddListener( BuyConsumable6 );

            _freeHint_1.onClick.AddListener( TrySeeVideo );
            _buyConsumable1_1.onClick.AddListener( BuyConsumable1 );
            _buyConsumable4_1.onClick.AddListener( BuyConsumable2 );

            moreButton.onClick.AddListener( ShowMoreShop );
            backeToShowcaseButton.onClick.AddListener( HideMoreShop );

            moreButtonPress = moreButton.GetComponent<ButtonPress>();
            backeToShowcaseButtonPress = backeToShowcaseButton.GetComponent<ButtonPress>();

            //#if !UNITY_IOS
            if ( menuController.IsInitPrice ) {
                OnInitPrice();
            }
            else {
                menuController.OnInitPrice += OnInitPrice;
            }
            //#endif
        }

        internal void Show () {
            backeToShowcaseButtonPress.scaleRectTransform.gameObject.SetActive( false );
            backeToShowcaseButton.interactable = false;

            ShowBuiUI();

            //#if UNITY_IOS
            //HideBuy();
            //#endif
        }

        void HideBuy () {
            _buyConsumable1.gameObject.SetActive( false );
            _buyConsumable2.gameObject.SetActive( false );
            _buyConsumable3.gameObject.SetActive( false );
            _buyConsumable4.gameObject.SetActive( false );
            _buyConsumable5.gameObject.SetActive( false );
            _buyConsumable6.gameObject.SetActive( false );

            _buyConsumable1_1.gameObject.SetActive( false );
            _buyConsumable4_1.gameObject.SetActive( false );

            moreButton.gameObject.SetActive( false );
        }

        void ShowBuiUI () {
            //moreButton.gameObject.SetActive( true );
            moreButton.interactable = true;
            moreButton.image.raycastTarget = true;

            moreButtonPress.scaleRectTransform.gameObject.SetActive( true );
            moreButtonPress.SetToStartScale();

            showcaseRectTransform.gameObject.SetActive( true );
            showcaseRectTransform.anchoredPosition = new Vector2( 0f, startPosYShowcase );
            shopItemOver.color = new Color( shopItemOver.color.r, shopItemOver.color.g, shopItemOver.color.b, 0f );
            shopItemListRectTransform.anchoredPosition = new Vector2( 0f, startPosYShopItemList + offsetAnchorPosY * -1 );
            showcaseCanvasGroup.alpha = 1f;
            showcaseCanvasGroup.interactable = true;
            showcaseCanvasGroup.blocksRaycasts = true;

            shopItemListCanvasGroup.alpha = 0f;
            shopItemListCanvasGroup.interactable = false;
            shopItemListCanvasGroup.blocksRaycasts = false;
        }

        internal void ShowWithountMore () {
            backeToShowcaseButtonPress.scaleRectTransform.gameObject.SetActive( false );
            backeToShowcaseButton.interactable = false;

            ShowBuiUI();

            //#if UNITY_IOS
            //HideBuy();
            //#endif
        }

        internal void Hide () {
            showcaseRectTransform.DOKill();
            shopItemOver.DOKill();
            showcaseCanvasGroup.DOKill();
        }


        void OnInitPrice () {
            _priceText1.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable1 );
            _priceText2.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable2 );
            _priceText3.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable3 );
            _priceText4.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable4 );
            _priceText5.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable5 );
            _priceText6.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable6 );
            _priceText1_1.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable1 );
            _priceText4_1.text = _purchaser.GetPriceById( PurchaserManager.kProductIDConsumable2 );
        }

        void TrySeeVideo () {
            adsManager.ShowRewardedAdHint();
        }

        void BuyConsumable1 () {
            _purchaser.BuyConsumable1();
        }

        void BuyConsumable2 () {
            _purchaser.BuyConsumable2();
        }

        void BuyConsumable3 () {
            _purchaser.BuyConsumable3();
        }

        void BuyConsumable4 () {
            _purchaser.BuyConsumable4();
        }

        void BuyConsumable5 () {
            _purchaser.BuyConsumable5();
        }

        void BuyConsumable6 () {
            _purchaser.BuyConsumable6();
        }

        void ShowMoreShop () {
            isMoreShop = true;

            OnShowMoreShop();

            moreButton.interactable = false;
            moreButton.image.raycastTarget = false;
            moreButtonPress.scaleRectTransform.gameObject.SetActive( false );

            shopItemOver.gameObject.SetActive( true );

            showcaseRectTransform.DOAnchorPosY( startPosYShowcase + offsetAnchorPosY, timeAnimation ).OnComplete( () => {
                backeToShowcaseButtonPress.scaleRectTransform.gameObject.SetActive( true );

                showcaseCanvasGroup.interactable = false;
                showcaseCanvasGroup.blocksRaycasts = false;

                backeToShowcaseButton.interactable = true;

                shopItemListCanvasGroup.interactable = true;
                shopItemListCanvasGroup.blocksRaycasts = true;
            } );

            showcaseCanvasGroup.DOFade( 0f, timeAnimation );

            shopItemListRectTransform.DOAnchorPosY( startPosYShopItemList, timeAnimation );
            shopItemOver.DOColor( new Color( shopItemOver.color.r, shopItemOver.color.g, shopItemOver.color.b, 1f ), timeAnimation );
            shopItemListCanvasGroup.DOFade( 1f, timeAnimation );
        }

        internal void TryHideMoreShop () {
            if ( !isMoreShop ) {
                return;
            }

            HideMoreShop();
        }

        void HideMoreShop () {
            isMoreShop = false;

            OnHideMoreShop();

            backeToShowcaseButton.interactable = false;
            backeToShowcaseButtonPress.scaleRectTransform.gameObject.SetActive( false );

            shopItemListCanvasGroup.interactable = false;
            shopItemListCanvasGroup.blocksRaycasts = false;

            showcaseRectTransform.DOAnchorPosY( startPosYShowcase, timeAnimation ).OnComplete( () => {
                showcaseCanvasGroup.interactable = true;
                showcaseCanvasGroup.blocksRaycasts = true;
                moreButton.interactable = true;
                moreButton.image.raycastTarget = true;

                moreButtonPress.scaleRectTransform.gameObject.SetActive( true );
                moreButtonPress.SetToStartScale();

                shopItemOver.gameObject.SetActive( false );
            } );

            showcaseCanvasGroup.DOFade( 1, timeAnimation );

            shopItemListRectTransform.DOAnchorPosY( startPosYShopItemList, timeAnimation );
            shopItemOver.DOColor( new Color( shopItemOver.color.r, shopItemOver.color.g, shopItemOver.color.b, 0f ), timeAnimation );
            shopItemListCanvasGroup.DOFade( 0f, timeAnimation );
        }
    }
}
