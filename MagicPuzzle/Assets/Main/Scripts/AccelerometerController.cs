﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {
    public class AccelerometerController: MonoBehaviour {
        [SerializeField]
        RectTransform accelerometerRectTransform;
        [SerializeField]
        Vector2 maxDeviation;
        [SerializeField]
        Vector2 minDeviation;

        Vector2 tempAccelerometerPosition;

        private void OnEnable () {
            tempAccelerometerPosition = Vector2.zero;
            accelerometerRectTransform.anchoredPosition = tempAccelerometerPosition;
        }

        // Update is called once per frame
        void Update () {
            tempAccelerometerPosition += new Vector2( Input.acceleration.x, Input.acceleration.z );

            if ( tempAccelerometerPosition.x < minDeviation.x ) {
                tempAccelerometerPosition = new Vector2( minDeviation.x, tempAccelerometerPosition.y );
            }
            else if ( tempAccelerometerPosition.x > maxDeviation.x ) {
                tempAccelerometerPosition = new Vector2( maxDeviation.x, tempAccelerometerPosition.y );
            }

            if ( tempAccelerometerPosition.y < minDeviation.y ) {
                tempAccelerometerPosition = new Vector2( tempAccelerometerPosition.x, minDeviation.y );
            }
            else if ( tempAccelerometerPosition.y > maxDeviation.y ) {
                tempAccelerometerPosition = new Vector2( tempAccelerometerPosition.x, maxDeviation.y );
            }

            accelerometerRectTransform.anchoredPosition = tempAccelerometerPosition;
        }
    }
}