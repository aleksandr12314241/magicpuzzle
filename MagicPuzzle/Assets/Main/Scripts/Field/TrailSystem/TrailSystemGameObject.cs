﻿using System.Collections;
using UnityEngine;
using Utility;

namespace Puzzle {
    public class TrailSystemGameObject: MonoBehaviour {
        [SerializeField]
        RectTransform myRecTransform;
        //[SerializeField]
        //TrailSystemPool trailSystemPool;
        [SerializeField]
        RectTransform followObject;
        [SerializeField]
        float distanceToCreate = 20f;
        [SerializeField]
        bool animOnStart = false;

        Coroutine trailSystemFollowUpdate;
        Vector2 oldFollowPositionParticle;

        private void Start () {
            if ( animOnStart ) {
                SetFollowObject();
            }
        }

        internal void SetFollowObject () {
            if ( trailSystemFollowUpdate != null ) {
                StopCoroutine( trailSystemFollowUpdate );
                trailSystemFollowUpdate = null;
            }

            oldFollowPositionParticle = this.followObject.anchoredPosition;
            trailSystemFollowUpdate = StartCoroutine( TrailSystemFollowUpdateCoroutine() );
        }

        internal void UnsetFollowObject () {
            if ( trailSystemFollowUpdate != null ) {
                StopCoroutine( trailSystemFollowUpdate );
                trailSystemFollowUpdate = null;
            }
        }

        internal void UnsetFollowObjectImmediately () {
            if ( trailSystemFollowUpdate != null ) {
                StopCoroutine( trailSystemFollowUpdate );
                trailSystemFollowUpdate = null;
            }

            //trailSystemPool.PushAllLetterUiParticle();
        }

        IEnumerator TrailSystemFollowUpdateCoroutine () {
            while ( followObject != null ) {
                TrailSystemFollowUpdate();
                yield return null;
            }
        }

        void TrailSystemFollowUpdate () {
            if ( ( oldFollowPositionParticle - followObject.anchoredPosition ).sqrMagnitude < distanceToCreate  * distanceToCreate ) {
                return;
            }
            oldFollowPositionParticle = followObject.anchoredPosition;
            CreatSquareTrail( followObject.anchoredPosition );
        }

        void CreatSquareTrail ( Vector2 newPosition ) {

            //LetterUiParticle letterUiParticle = trailSystemPool.PullLetterUiParticle();
            //letterUiParticle.ActivateParticle( newPosition );

            //this.Invoke( 0.4f, () => {
            //    trailSystemPool.PushLetterUiParticle( letterUiParticle );
            //} );
        }
    }
}
