﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Common;

namespace Puzzle {
    public class LetterUiParticle: MonoBehaviour {
        public event Action<LetterUiParticle> OnPushLetterUiParticle = ( LetterUiParticle param ) => {};

        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Image myImage;
        [SerializeField]
        Animator myAnimator;

        GameLinks gameLinks;

        float timeAlive = 0.4f;

        internal void Init ( GameLinks gameLinks ) {
            this.gameLinks = gameLinks;
        }

        internal void ActivateParticle ( Vector2 newPosition ) {
            gameObject.SetActive( true );

            myImage.color = gameLinks.ColorDataSet.GetRandomColor();

            myRectTransform.anchoredPosition = newPosition;

            Invoke( "PushLetterUiParticle", timeAlive );
        }

        void PushLetterUiParticle () {
            OnPushLetterUiParticle( this );
        }
    }
}