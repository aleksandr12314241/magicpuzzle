﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

namespace Puzzle {
    public class TrailSystemMouse: MonoBehaviour {
        [SerializeField]
        RectTransform myRecTransform;
        [SerializeField]
        RectTransform trailRectTransform;

        Coroutine trailSystemMouseUpdate;
        Vector2 oldMousePositionParticle;

        internal void StartTrial () {
            if ( trailSystemMouseUpdate != null ) {
                StopCoroutine( trailSystemMouseUpdate );
                trailSystemMouseUpdate = null;
            }
            Vector2 mousePosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRecTransform, Input.mousePosition, Camera.main, out mousePosition );
            oldMousePositionParticle = mousePosition;
            trailSystemMouseUpdate = StartCoroutine( TrailSystemMouseUpdateCoroutine() );

            trailRectTransform.anchoredPosition = mousePosition;
            //trailRectTransform.gameObject.SetActive( true );
        }

        internal void EndTrial () {
            if ( trailSystemMouseUpdate != null ) {
                StopCoroutine( trailSystemMouseUpdate );
                trailSystemMouseUpdate = null;
            }

            //trailRectTransform.gameObject.SetActive( false );
        }

        IEnumerator TrailSystemMouseUpdateCoroutine () {
            for ( ; ; ) {
                TrailSystemMouseUpdate();
                yield return null;
            }
        }

        void TrailSystemMouseUpdate () {
            Vector2 mousePosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRecTransform, Input.mousePosition, Camera.main, out mousePosition );
            if ( ( oldMousePositionParticle - mousePosition ).sqrMagnitude < 400f ) {
                return;
            }
            oldMousePositionParticle = mousePosition;
            trailRectTransform.anchoredPosition = mousePosition;
        }
    }
}
