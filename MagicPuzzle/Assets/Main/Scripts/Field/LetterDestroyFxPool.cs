﻿using UnityEngine;

namespace Puzzle {
    public class LetterDestroyFxPool: MonoBehaviour {
        [SerializeField]
        GameObject letterDestroyFx;
        [SerializeField]
        int countFx;
        [SerializeField]
        PuzzleController mainPuzzleController;

        RectTransform[] lettersDestroyFx;
        Vector2 offset;

        void Start () {
            lettersDestroyFx = new RectTransform[countFx];
            Transform myTransform = this.transform;

            for ( int i = 0; i < countFx; i++ ) {
                lettersDestroyFx[i] = Instantiate( letterDestroyFx, myTransform ).GetComponent<RectTransform>();
                lettersDestroyFx[i].gameObject.SetActive( false );
            }

            offset = new Vector2( 0f, 200f );

            mainPuzzleController.OnStartHidePiece += OnStartHideLetter;
        }

        internal void OnStartHideLetter ( PieceFieldController pieceController ) {
            ShowLettersDestroyFx( pieceController.MyRectTransform.anchoredPosition + offset );
        }

        internal void ShowLettersDestroyFx ( Vector2 lettersDestroyFxPosition ) {
            for ( int i = 0; i < countFx; i++ ) {
                if ( !lettersDestroyFx[i].gameObject.activeSelf ) {
                    lettersDestroyFx[i].anchoredPosition = lettersDestroyFxPosition;
                    lettersDestroyFx[i].gameObject.SetActive( true );
                    break;
                }
            }
        }
    }
}
