﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupTextUIEntry : MonoBehaviour {
    [SerializeField]
    Text popUpText;
    [SerializeField]
    RectTransform popUpRectTransform;

    internal string GetPopUpText () {
        return popUpText.text;
    }

    internal void ShowPopUp ( string popUpString, Color popUpColor ) {
        popUpRectTransform.anchoredPosition = Vector2.zero;
        popUpRectTransform.DOKill();

        popUpText.color = popUpColor;
        popUpText.text = popUpString;

        gameObject.SetActive( true );
        popUpRectTransform.DOAnchorPosY( 200f, 3f );

        CancelInvoke( "HidePopUp" );
        Invoke( "HidePopUp", 3.2f );
    }

    void HidePopUp () {
        popUpRectTransform.DOKill();
        gameObject.SetActive( false );
    }
}
