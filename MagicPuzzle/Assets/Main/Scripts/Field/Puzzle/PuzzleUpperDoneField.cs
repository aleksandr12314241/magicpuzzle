﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Common;

namespace Puzzle {
    public class PuzzleUpperDoneField: MonoBehaviour {
        [SerializeField]
        RectTransform myRectTransform;
        //[SerializeField]
        //EventTrigger hintFieldEventSystem;
        //[SerializeField]
        //GameObject hintWords;

        PuzzleController puzzleController;
        PuzzleModel puzzleModel;

        internal void Init ( PuzzleController puzzleController ) {
            this.puzzleController = puzzleController;
            puzzleModel = puzzleController.GetPuzzleModel();

            InitEventTriggerHintField();
        }

        void InitEventTriggerHintField () {
            // кликн на подсказку
            //EventTrigger.Entry entryHint = new EventTrigger.Entry();
            //entryHint.eventID = EventTriggerType.PointerDown;
            //entryHint.callback.AddListener( ( data ) => {
            //    ClickOnWordsHintDown( ( PointerEventData ) data );
            //} );
            //hintFieldEventSystem.triggers.Add( entryHint );

            //entryHint = new EventTrigger.Entry();
            //entryHint.eventID = EventTriggerType.PointerUp;
            //entryHint.callback.AddListener( ( data ) => {
            //    ClickOnWordsHintUp( ( PointerEventData ) data );
            //} );
            //hintFieldEventSystem.triggers.Add( entryHint );
        }

        void ClickOnWordsHintDown ( PointerEventData data ) {

            Vector2 ancoredPositionOnScreen;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRectTransform, Input.mousePosition, Camera.main,
                    out ancoredPositionOnScreen );

            for ( int i = 0; i < puzzleModel.listPieces.Length; i++ ) {
                for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
                    if ( puzzleModel.listPieces[i][j].isEmpty ) {
                        continue;
                    }
                    puzzleModel.listPieces[i][j].pieceDoneFly.HideLetter();
                    puzzleModel.listPieces[i][j].pieceCntr.RotateCircle();
                }
            }

            //puzzleModel.letterDoneButtonI.ShowHintText();

            float totalTime = 0.5f;
            float stepTime = totalTime / puzzleModel.GetCountPuzzles();
            float timeDelay = 0;

            //for ( int i = 0; i < puzzleModel.letterDone.Count; i++ ) {
            //    for ( int j = 0; j < puzzleModel.letterDone[i].Count; j++ ) {
            //        puzzleModel.letterDone[i][j].HideLetters( ancoredPositionOnScreen.x, timeDelay );
            //        timeDelay += stepTime;
            //    }
            //}

            AudioManager.instance.PlaySoundByType( SoundType.ChooseCategory );
        }

        void ClickOnWordsHintUp ( PointerEventData data ) {

            //puzzleModel.letterDoneButtonI.HideHintText();

            float tottalTime = 0.5f;
            float stepTime = tottalTime / puzzleModel.GetCountPuzzles();
            float timeDelay = 0;

            //for ( int i = 0; i < puzzleModel.letterDone.Count; i++ ) {
            //    for ( int j = 0; j < puzzleModel.letterDone[i].Count; j++ ) {
            //        puzzleModel.letterDone[i][j].ShowLetters( timeDelay );
            //        timeDelay += stepTime;
            //    }
            //}

            for ( int i = 0; i < puzzleModel.listPieces.Length; i++ ) {
                for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
                    if ( puzzleModel.listPieces[i][j].isEmpty ) {
                        continue;
                    }
                    puzzleModel.listPieces[i][j].pieceDoneFly.ShowLetter();
                    puzzleModel.listPieces[i][j].pieceCntr.EndRotateCircle();
                }
            }

            AudioManager.instance.PlaySoundByType( SoundType.CupHitting );
        }

        internal void SetAllowHintWods ( bool isAllow ) {
            //hintWords.SetActive( isAllow );
        }
    }
}
