﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace Puzzle {
    public class LettersPool: MonoBehaviour {
        [SerializeField]
        PieceFieldController pieceControllerPrf;
        [SerializeField]
        RectTransform pieceControllerParent;
        int sizePiecePool = 25;

        [SerializeField]
        PuzzlePieceField puzzlePieceFieldPrf;
        [SerializeField]
        RectTransform puzzlePieceFieldParent;
        int sizePieceFieldPool = 25;

        [SerializeField]
        PuzzlePieceView puzzlePieceViewPrf;
        [SerializeField]
        RectTransform puzzlePieceViewParent;
        int sizePieceViewPool = 60;

        List<PuzzlePiece> puzzlePieceList;
        int sizePuzzlePiecePool = 36;
        List<int> activePuzzlePiece;

        [SerializeField]
        MainImageDone mainImageDonePrf;
        [SerializeField]
        RectTransform mainImageDoneParent;
        int sizeMainImageDonesPool = 6;

        //==========================================
        [SerializeField]
        LetterDoneFly _letterDoneFlyPrf;
        [SerializeField]
        RectTransform _letterDoneFlyParent;
        int _sizeLetterDonePool = 30;



        GameLinks gameLinks;

        //[SerializeField]
        //LetterDoneButtonI letterDoneButtonIPrf;
        //[SerializeField]
        //RectTransform letterDoneButtonIParent;
        //int sizeletterDoneButtonI = 20;

        void Awake () {
            gameLinks = GameObject.FindObjectOfType<GameLinks>();

            InitPieceControllerPool();
            InitPieceFieldPool();
            InitPieceViewPool();
            InitPuzzlePiecePool();

            //=========
            InitLetterDonePool();
            InitMainImageDonePosPool();
            //InitletterDoneButtonI();
        }

        void InitPieceControllerPool () {
            for ( int i = 0; i < sizePiecePool; i++ ) {
                CreatePiece();
            }
        }
        void InitPieceFieldPool () {
            for ( int i = 0; i < sizePieceFieldPool; i++ ) {
                CreatePieceField();
            }
        }
        void InitPieceViewPool () {
            for ( int i = 0; i < sizePieceViewPool; i++ ) {
                CreatePiece();
            }
        }
        void InitPuzzlePiecePool () {
            puzzlePieceList = new List<PuzzlePiece>();
            activePuzzlePiece = new List<int>();

            for ( int i = 0; i < sizePuzzlePiecePool; i++ ) {
                CreatePiece();
            }
        }

        //=========================
        void InitLetterDonePool () {
            for ( int i = 0; i < _sizeLetterDonePool; i++ ) {
                CreateLetterDone();
            }
        }

        void InitMainImageDonePosPool () {
            for ( int i = 0; i < sizeMainImageDonesPool; i++ ) {
                CreateMainImageDone();
            }
        }

        //void InitletterDoneButtonI () {
        //    for ( int i = 0; i < sizeletterDoneButtonI; i++ ) {

        //    }
        //}

        // ================= PieceController =================
        PieceFieldController CreatePiece () {
            PieceFieldController goPieceUI = Instantiate( pieceControllerPrf, Vector2.zero, Quaternion.identity );
            goPieceUI.transform.SetParent( pieceControllerParent, false );
            goPieceUI.gameObject.SetActive( false );
            goPieceUI.Init();
            return goPieceUI;
        }
        internal PieceFieldController PullPieceFieldController () {
            PieceFieldController piece;

            if ( pieceControllerParent.childCount <= 0 ) {
                piece = CreatePiece ();
                //letter.gameObject.SetActive( true );
                return piece;
            }

            GameObject objectPiece = null;
            for ( int i = 0; i < pieceControllerParent.childCount; i++ ) {
                if ( pieceControllerParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                objectPiece = pieceControllerParent.GetChild( i ).gameObject;
            }

            if ( objectPiece == null ) {
                piece = CreatePiece();
                //letter.gameObject.SetActive( true );
                return piece;

            }
            piece = objectPiece.GetComponent<PieceFieldController>();
            //letter.gameObject.SetActive( true );
            return piece;
        }
        internal void PushPieceFieldController ( PieceFieldController goPieceUI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( pieceControllerParent.childCount + 1 > sizePiecePool ) {
                Destroy( goPieceUI.gameObject );
                return;
            }

            goPieceUI.transform.SetParent( pieceControllerParent, false );
            goPieceUI.gameObject.SetActive( false );
        }

        // ================= PieceField =================
        PuzzlePieceField CreatePieceField () {
            PuzzlePieceField goPieceFieldUI = Instantiate( puzzlePieceFieldPrf, Vector2.zero, Quaternion.identity );
            goPieceFieldUI.transform.SetParent( puzzlePieceFieldParent, false );
            goPieceFieldUI.gameObject.SetActive( false );
            goPieceFieldUI.Init( gameLinks );
            return goPieceFieldUI;
        }
        internal PuzzlePieceField PullPuzzlePieceField () {
            PuzzlePieceField piece;

            if ( puzzlePieceFieldParent.childCount <= 0 ) {
                piece = CreatePieceField();
                //letter.gameObject.SetActive( true );
                return piece;
            }

            GameObject objectPiece = null;
            for ( int i = 0; i < puzzlePieceFieldParent.childCount; i++ ) {
                if ( puzzlePieceFieldParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                objectPiece = puzzlePieceFieldParent.GetChild( i ).gameObject;
            }

            if ( objectPiece == null ) {
                piece = CreatePieceField();
                //letter.gameObject.SetActive( true );
                return piece;

            }
            piece = objectPiece.GetComponent<PuzzlePieceField>();
            //letter.gameObject.SetActive( true );
            return piece;
        }
        internal void PushPuzzlePieceField ( PuzzlePieceField goPieceUI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( puzzlePieceFieldParent.childCount + 1 > sizePiecePool ) {
                Destroy( goPieceUI.gameObject );
                return;
            }

            goPieceUI.transform.SetParent( puzzlePieceFieldParent, false );
            goPieceUI.gameObject.SetActive( false );
        }

        // ================= PieceView  =================
        PuzzlePieceView CreatePuzzlePieceView () {
            PuzzlePieceView goPieceViewUI = Instantiate( puzzlePieceViewPrf, Vector2.zero, Quaternion.identity );
            goPieceViewUI.transform.SetParent( puzzlePieceViewParent, false );
            goPieceViewUI.gameObject.SetActive( false );
            return goPieceViewUI;
        }
        internal PuzzlePieceView PullPuzzlePieceView () {
            PuzzlePieceView piece;

            if ( puzzlePieceViewParent.childCount <= 0 ) {
                piece = CreatePuzzlePieceView();
                //letter.gameObject.SetActive( true );
                return piece;
            }

            GameObject objectPiece = null;
            for ( int i = 0; i < puzzlePieceViewParent.childCount; i++ ) {
                if ( puzzlePieceViewParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                objectPiece = puzzlePieceViewParent.GetChild( i ).gameObject;
            }

            if ( objectPiece == null ) {
                piece = CreatePuzzlePieceView();
                //letter.gameObject.SetActive( true );
                return piece;

            }
            piece = objectPiece.GetComponent<PuzzlePieceView>();
            //letter.gameObject.SetActive( true );
            return piece;
        }
        internal void PushPuzzlePieceView ( PuzzlePieceView goPieceUI ) {
            if ( puzzlePieceViewParent.childCount + 1 > sizePiecePool ) {
                Destroy( goPieceUI.gameObject );
                return;
            }

            goPieceUI.transform.SetParent( puzzlePieceViewParent, false );
            goPieceUI.gameObject.SetActive( false );
        }

        // ================= Puzzle Piece  =================
        PuzzlePiece CreatePuzzlePiece () {
            PuzzlePiece puzzlePieceObj = new PuzzlePiece( gameLinks );
            puzzlePieceList.Add( puzzlePieceObj );
            return puzzlePieceObj;
        }
        internal PuzzlePiece PullPuzzlePiece () {
            for ( int i = 0; i < puzzlePieceList.Count; i++ ) {
                if ( activePuzzlePiece.Contains( i ) ) {
                    continue;
                }
                activePuzzlePiece.Add( i );
                return puzzlePieceList[i];
            }

            PuzzlePiece piece = CreatePuzzlePiece();
            activePuzzlePiece.Add( puzzlePieceList.Count - 1 );
            return piece;
        }
        internal void PushPuzzlePiece ( PuzzlePiece goPieceUI ) {
            int index = puzzlePieceList.FindIndex( x => x == goPieceUI );
            activePuzzlePiece.Remove( index );
        }

        // ====================================================================================================
        LetterDoneFly CreateLetterDone () {
            LetterDoneFly goLetterDoneUI = Instantiate( _letterDoneFlyPrf, Vector2.zero, Quaternion.identity );
            goLetterDoneUI.transform.SetParent( _letterDoneFlyParent, false );
            goLetterDoneUI.gameObject.SetActive( false );
            return goLetterDoneUI;
        }

        internal LetterDoneFly PullLetterDoneFly () {
            LetterDoneFly letterDone;

            if ( _letterDoneFlyParent.childCount <= 0 ) {
                letterDone = CreateLetterDone();
                letterDone.gameObject.SetActive( true );
                return letterDone;
            }

            GameObject objectLetter = null;
            for ( int i = 0; i < _letterDoneFlyParent.childCount; i++ ) {
                if ( _letterDoneFlyParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                objectLetter = _letterDoneFlyParent.GetChild( i ).gameObject;
            }

            if ( objectLetter == null ) {
                letterDone = CreateLetterDone();
                letterDone.gameObject.SetActive( true );
                return letterDone;

            }
            letterDone = objectLetter.GetComponent<LetterDoneFly>();
            letterDone.gameObject.SetActive( true );
            return letterDone;
        }

        internal void PushLetterDoneFly ( LetterDoneFly goLetterDoneUI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( _letterDoneFlyParent.childCount + 1 > _sizeLetterDonePool ) {
                Destroy( goLetterDoneUI.gameObject );
                return;
            }
            goLetterDoneUI.transform.SetParent( _letterDoneFlyParent, false );
            goLetterDoneUI.gameObject.SetActive( false );
        }

        MainImageDone CreateMainImageDone () {
            MainImageDone goMainImageDoneUI = Instantiate( mainImageDonePrf, Vector2.zero, Quaternion.identity );
            goMainImageDoneUI.transform.SetParent( mainImageDoneParent, false );
            goMainImageDoneUI.gameObject.SetActive( false );
            goMainImageDoneUI.Init();
            return goMainImageDoneUI;
        }

        internal MainImageDone PullMainImageDone () {
            MainImageDone mainImageDone = null;

            if ( mainImageDoneParent.childCount <= 0 ) {
                mainImageDone = CreateMainImageDone();
                mainImageDone.gameObject.SetActive( true );
                return mainImageDone;
            }

            GameObject mainImageDonePosGo = null;
            for ( int i = 0; i < mainImageDoneParent.childCount; i++ ) {
                if ( mainImageDoneParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                mainImageDonePosGo = mainImageDoneParent.GetChild( i ).gameObject;
            }

            if ( mainImageDonePosGo == null ) {
                mainImageDone = CreateMainImageDone();
                mainImageDone.gameObject.SetActive( true );
                return mainImageDone;

            }

            mainImageDone = mainImageDonePosGo.GetComponent<MainImageDone>();
            mainImageDone.gameObject.SetActive( true );
            return mainImageDone;
        }

        internal void PushMainImageDone ( MainImageDone goMainImageDoneUI ) {

            if (  mainImageDoneParent.childCount + 1 > sizeMainImageDonesPool ) {

                Destroy( goMainImageDoneUI.gameObject );
                return;
            }

            goMainImageDoneUI.transform.SetParent( mainImageDoneParent, false );
            goMainImageDoneUI.gameObject.SetActive( false );
        }

        //LetterDoneButtonI CreateLetterDoneButtonI () {
        //    LetterDoneButtonI goLetterDoneButtonIUI = Instantiate( letterDoneButtonIPrf, Vector2.zero, Quaternion.identity );
        //    goLetterDoneButtonIUI.transform.SetParent( letterDoneButtonIParent, false );
        //    goLetterDoneButtonIUI.gameObject.SetActive( false );
        //    return goLetterDoneButtonIUI;
        //}

        //internal LetterDoneButtonI PullLetterDoneButtonI () {
        //    LetterDoneButtonI letterDoneButtonI = null;

        //    if ( letterDoneButtonIParent.childCount <= 0 ) {
        //        letterDoneButtonI = CreateLetterDoneButtonI();
        //        letterDoneButtonI.gameObject.SetActive( true );
        //        return letterDoneButtonI;
        //    }

        //    GameObject letterDoneButtonIGo = null;
        //    for ( int i = 0; i < letterDoneButtonIParent.childCount; i++ ) {
        //        if ( letterDoneButtonIParent.GetChild( i ).gameObject.activeSelf ) {
        //            continue;
        //        }
        //        letterDoneButtonIGo = letterDoneButtonIParent.GetChild( i ).gameObject;
        //    }

        //    if ( letterDoneButtonIGo == null ) {
        //        letterDoneButtonI = CreateLetterDoneButtonI();
        //        letterDoneButtonI.gameObject.SetActive( true );
        //        return letterDoneButtonI;

        //    }

        //    letterDoneButtonI = letterDoneButtonIGo.GetComponent<LetterDoneButtonI>();
        //    letterDoneButtonI.gameObject.SetActive( true );
        //    return letterDoneButtonI;
        //}

        //internal void PushLetterDoneButtonI ( LetterDoneButtonI goLetterDoneButtonI ) {
        //    //if ( goLetterUI == null ) {
        //    //    return;
        //    //}

        //    if ( letterDoneButtonIParent.childCount + 1 > sizeletterDoneButtonI ) {

        //        Destroy( goLetterDoneButtonI.gameObject );
        //        return;
        //    }

        //    goLetterDoneButtonI.transform.SetParent( letterDoneButtonIParent, false );
        //    goLetterDoneButtonI.gameObject.SetActive( false );
        //}
    }
}

