﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class PopupTextUI : WindowMonoBehaviour {
    [SerializeField]
    PopupTextUIEntry popupTextUIEntry;
    [SerializeField]
    int countPool;

    PopupTextUIEntry[] popupTextUIEntryArray;

    internal override void InitAwake () {
        popupTextUIEntryArray = new PopupTextUIEntry[countPool];
        for ( int i = 0; i < countPool; i++ ) {
            popupTextUIEntryArray[i] = Instantiate( popupTextUIEntry, this.transform );
            popupTextUIEntryArray[i].gameObject.SetActive( false );
        }
    }

    internal void ShowPopUp ( string popUpString ) {
        ShowPopUp( popUpString, Color.black );
    }

    internal void ShowPopUp ( string popUpString, Color popUpColor ) {
        int indexPopupTextUIEntry = -1;// countPool - 1;

        for ( int i = 0; i < popupTextUIEntryArray.Length; i++ ) {
            if ( popupTextUIEntryArray[i].GetPopUpText() == popUpString ) {
                indexPopupTextUIEntry = i;
                break;
            }
        }

        if ( indexPopupTextUIEntry != -1 ) {
            popupTextUIEntryArray[indexPopupTextUIEntry].ShowPopUp( popUpString, popUpColor );
            return;
        }

        indexPopupTextUIEntry = countPool - 1;

        for ( int i = 0; i < popupTextUIEntryArray.Length; i++ ) {
            if ( !popupTextUIEntryArray[i].gameObject.activeSelf ) {
                indexPopupTextUIEntry = i;
                break;
            }
        }
        popupTextUIEntryArray[indexPopupTextUIEntry].ShowPopUp( popUpString, popUpColor );
    }
}
