﻿using UnityEngine;
using UnityEngine.UI;
using Common;
using Shop;
using Menu;

namespace Puzzle {
    public class PuzzleBottomMenu: MonoBehaviour {
        [SerializeField]
        Image hintOn;
        [SerializeField]
        Image hintOff;
        [SerializeField]
        TextPlaceholder _hintText;
        [SerializeField]
        Button _hintButton;
        //[SerializeField]
        //Button _restartButton;
        //[SerializeField]
        //Button _backButton;
        [SerializeField]
        PuzzleController _puzzleController;
        [SerializeField]
        Text countHintsText;

        ShopUI _shopUI;

        PuzzleModel puzzleModel;
        GameController _GC;

        RectTransform _hintTextRectTransform;
        Text _hintTextText;

        internal void Init () {
            MenuController menuController = FindObjectOfType<MenuController>();

            WinUI _winUI = ( WinUI ) menuController.GetWindow<WinUI>();

            _shopUI = ( ShopUI ) menuController.GetWindow<ShopUI>();

            _hintTextRectTransform = _hintText.GetComponent<RectTransform>();
            _hintTextText = _hintText.GetComponent<Text>();

            //_backButton.onClick.AddListener( ClickButtonBack );
            _hintButton.onClick.AddListener( Hint );
            //_restartButton.onClick.AddListener( StartRestart );

            puzzleModel = _puzzleController.GetPuzzleModel();

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            SetHint();
            //TrySetRestartButton();

            //_puzzleController.OnStartQuest += OnStartQuest;

            _GC.player.OnChangeHintCount += OnChangeHintCount;
            _winUI.OnStartFinLevel += OnStartFinLevel;
        }

        internal void SetHint () {
            if ( _GC.player.hints <= 0 ) {
                _hintText.SetTextById( "txt_puzzle_get_more_hint" );
                hintOn.gameObject.SetActive( false );
                hintOff.gameObject.SetActive( true );
                countHintsText.gameObject.SetActive( false );
                _hintTextText.alignment = TextAnchor.MiddleRight;
                _hintTextRectTransform.sizeDelta = new Vector2( 269f, _hintTextRectTransform.sizeDelta.y );
            }
            else {
                //_hintText.SetTextById( "txt_puzzle_use_hint", _GC.player.hints.GetValue().ToString() );
                hintOn.gameObject.SetActive( true );
                hintOff.gameObject.SetActive( false );

                int totalIndexOfQuest = 0;

                if ( !puzzleModel.GetPuzzelType().Equals( PuzzleType.Daily ) ) {
                    //if ( _GC.numCatigory.GetValue() == -1 || _GC.numLevel.GetValue() == -1 || _GC.IsEndGame( _GC.numCatigory.GetValue() ) ) {
                    if ( _GC.numCatigory.GetValue() == -1 || _GC.numLevel.GetValue() == -1 ) {
                        totalIndexOfQuest = 0;
                    }
                    else {
                        totalIndexOfQuest = _GC.GetTotalIndexOfQuest( _GC.numCatigory.GetValue(), _GC.numLevel.GetValue() );
                    }
                }

                int hintPrice = puzzleModel.GetPriceHint( totalIndexOfQuest );

                if ( hintPrice == 0 ) {
                    _hintText.SetTextById( "txt_puzzle_use_hint_zero" );
                    hintOn.gameObject.SetActive( false );
                    _hintTextRectTransform.sizeDelta = new Vector2( 340f, _hintTextRectTransform.sizeDelta.y );
                    countHintsText.gameObject.SetActive( false );
                    _hintTextText.alignment = TextAnchor.MiddleCenter;
                }
                else {
                    _hintText.SetTextById( "txt_puzzle_use_hint", hintPrice.ToString() );
                    hintOn.gameObject.SetActive( true );
                    _hintTextRectTransform.sizeDelta = new Vector2( 199f, _hintTextRectTransform.sizeDelta.y );
                    countHintsText.gameObject.SetActive( true );
                    countHintsText.text = hintPrice.ToString();
                    _hintTextText.alignment = TextAnchor.MiddleRight;
                }
            }
        }

        private void Hint () {
            if ( !_puzzleController.Win && _GC.player.hints <= 0 ) {
                ShowBuyUI();
                AudioManager.instance.PlaySoundByType( SoundType.NoHints );
                return;
            }

            AudioManager.instance.PlaySoundByType( SoundType.UseHint );
            _puzzleController.Hint();
        }

        void HintAdded1 () {
            FinLevel();
        }

        void HintAdded2 () {
            FinLevel();
        }

        void GoToMenu () {
            _puzzleController.ClickButtonBack();
        }

        internal void FinLevel () {
            //_puzzleController.InitPuzzle( _GC.GetCurrentQuest( _GC.numCatigory.GetValue(), _GC.numLevel.GetValue() ) );
        }

        void OnChangeHintCount () {
            SetHint();
        }

        private void OnStartFinLevel ( PuzzleType puzzelType, CloseWinUIType levelFinishType ) {
            if ( !puzzleModel.GetPuzzelType().Equals( puzzelType ) ) {
                return;
            }

            switch ( levelFinishType ) {
                case CloseWinUIType.GoToMenu:
                    GoToMenu();
                    return;
                case CloseWinUIType.NextCategory:
                    FinLevel();
                    return;
                case CloseWinUIType.NextLevel:
                    FinLevel();
                    return;
            }
        }

        //private void StartRestart () {
        //    _puzzleController.StartRestart();
        //}

        public void ShowBuyUI () {
            _shopUI.Show();
        }

        void OnAllowShop () {
            SetHint();
        }

        //void TrySetRestartButton () {
        //    if ( puzzleModel.GetQuest() == null ) {
        //        return;
        //    }

        //    SetRestartButton( puzzleModel.GetQuest() );
        //}

        //void OnStartQuest ( Quests quest ) {
        //    //SetRestartButton( quest );
        //}

        //void SetRestartButton ( Quests quest ) {
        //    //int wordsQuestCount = quest.words[LocalizationManager.GetLanguageString()].Count;

        //    //if ( wordsQuestCount < 2 ) {
        //    //    _restartButton.gameObject.SetActive( false );
        //    //    return;
        //    //}
        //    //_restartButton.gameObject.SetActive( true );
        //}
    }
}
