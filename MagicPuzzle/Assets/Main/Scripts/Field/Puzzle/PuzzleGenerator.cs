﻿using Common;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public class PuzzleGenerator: MonoBehaviour {
        [SerializeField]
        RectTransform _letterUIField;
        [SerializeField]
        RectTransform backLetterDone;
        [SerializeField]
        RectTransform mainImageDoneContainer;
        [SerializeField]
        RectTransform puzzlePieceContainer;

        //// вынести в отдельный класс модель
        LettersPool piecesPool;

        GameController _GC;
        PuzzleController puzzleController;

        PuzzleModel puzzleModel;
        FieldLettersParameters parametersLettersField;
        FieldLettersParameters parametersLettersDone;

        float middleDonePosYGloabl;

        internal void Init ( PuzzleController puzzleController ) {
            piecesPool = FindObjectOfType<LettersPool>();

            this.puzzleController = puzzleController;
            this.puzzleModel = puzzleController.GetPuzzleModel();
        }

        internal void SetGameController ( GameController gameController ) {
            _GC = gameController;
        }

        internal void ActivateField ( bool isActive ) {
            _letterUIField.gameObject.SetActive( isActive );
        }

        internal void RestorePuzzlesFiled2 ( int sizeField, Dictionary<string, PuzzleStateData> puzzleStateData, Sprite[] spritesImages ) {
            puzzleModel.puzzleLevelPieceDoneData = new PuzzleLevelPieceDoneData[puzzleStateData.Count];
            puzzleModel.mainImagePuzzleDone = new bool[puzzleStateData.Count];
            puzzleModel.mainImageDone = new MainImageDone[puzzleStateData.Count];

            int index = 0;
            foreach ( var item in puzzleStateData ) {
                InitRestorePuzzle( item.Value, spritesImages[index], index );
            }
        }
        void InitRestorePuzzle ( PuzzleStateData puzzleStateData, Sprite spriteImage, int numPuzzelTemplate ) {
            Dictionary<CustomVector2Int, int> puzzlePieceByIndex = new Dictionary<CustomVector2Int, int>();
            Vector2 sizeImage = spriteImage.rect.size;
            Vector2 sizePiece = new Vector2( sizeImage.x / puzzleStateData.sizeMatrixImage.x,
                                             sizeImage.y / puzzleStateData.sizeMatrixImage.y );
            Vector2 startPosition = new Vector2( ( sizeImage.x - sizePiece.x ) / -2, ( sizeImage.y - sizePiece.y ) / 2f );
            Vector2 offsetSizePiece = new Vector2( sizePiece.x / 4f / 2f, sizePiece.y / 4f / 2f );
            PuzzleLevelPieceDoneData puzzleLevelPieceDoneDataTemp =
                new PuzzleLevelPieceDoneData( spriteImage.name, sizePiece, puzzleStateData.sizeMatrixImage, startPosition,
                                              puzzleStateData.GetPlayerPiecesCount() );

            List<List<PuzzlePiece>> puzzlePieces = new List<List<PuzzlePiece>>();
            List<List<CustomVector2Int>> puzzlePiecesConnection = new List<List<CustomVector2Int>>();
            List<CustomVector2Int> puzzlePiecesVisitIndex = new List<CustomVector2Int>();

            for ( int i = 0; i < puzzleStateData.sizeMatrixImage.x; i++ ) {
                puzzlePieces.Add( new List<PuzzlePiece>() );
                for ( int j = 0; j < puzzleStateData.sizeMatrixImage.y; j++ ) {
                    puzzlePieces[i].Add( piecesPool.PullPuzzlePiece() );
                }
            }
            int puzzlePieceId = 0;
            for ( int i = 0; i < puzzleStateData.playerPieces.Count; i++ ) {
                List<PuzzlePiece> puzzlePieceConnection = new List<PuzzlePiece>();
                for ( int j = 0; j < puzzleStateData.playerPieces[i].Count; j++ ) {
                    CustomVector2Int puzzlePieceIndex = puzzleStateData.playerPieces[i][j].pzzlePieceIndex;
                    int numPuzzlePiece = puzzleStateData.playerPieces[i][j].numPuzzlePieces[0];

                    PuzzlePieceView puzzlePieceViewDoneTemp = piecesPool.PullPuzzlePieceView();
                    puzzlePieceViewDoneTemp.gameObject.SetActive( false );
                    puzzlePieceViewDoneTemp.name = "puzzle_piece_done_" + puzzlePieceIndex.x + "_" + puzzlePieceIndex.y;
                    puzzlePieceViewDoneTemp.transform.SetParent( puzzlePieceContainer, false );

                    PuzzlePieceView puzzlePieceViewFieldTemp = piecesPool.PullPuzzlePieceView();
                    puzzlePieceViewFieldTemp.name = "puzzle_piece_field_" + puzzlePieceIndex.x + "_" + puzzlePieceIndex.y;
                    puzzlePieceViewFieldTemp.gameObject.SetActive( true );

                    PuzzlePiecesData puzzlePiecesData = _GC.GameLinks.PuzzlePiecesDataSet.GetPuzzlePiecesDataByIndex( numPuzzlePiece );

                    puzzlePieces[puzzlePieceIndex.x][puzzlePieceIndex.y].SetPuzzlePieces( puzzlePieceViewDoneTemp, puzzlePieceViewFieldTemp );
                    puzzlePieces[puzzlePieceIndex.x][puzzlePieceIndex.y].SetPuzzlePieceParametrs( spriteImage, numPuzzlePiece, 0, puzzlePieceIndex, sizePiece,
                            offsetSizePiece, startPosition,
                            puzzlePieceId );
                    puzzlePieceId++;
                    puzzlePieceConnection.Add( puzzlePieces[puzzlePieceIndex.x][puzzlePieceIndex.y] );

                    puzzlePieces[puzzlePieceIndex.x][puzzlePieceIndex.y].SetNumPuzzlePiece( puzzleStateData.playerPieces[i][j].numPuzzlePieces );
                }

                PuzzlePieceField puzzlePieceFieldTemp = piecesPool.PullPuzzlePieceField();
                puzzlePieceFieldTemp.SetStartAngle( puzzleStateData.playerPieces[i][0].startAngle );
                puzzlePieceFieldTemp.InitPuzzlePieceField( puzzlePieceConnection );

                for ( int k = 0; k < puzzleStateData.playerPieces[i][0].currentAngle; k++ ) {
                    puzzlePieceFieldTemp.RotatePuzzlePieces();
                }

                CustomVector2Int indexIJ = puzzleStateData.pieceFieldIndex[i];
                puzzleModel.listPieces[indexIJ.x][indexIJ.y].isEmpty = false;
                CreatePieceController( indexIJ.x, indexIJ.y, puzzlePieceFieldTemp, i, numPuzzelTemplate );
            }

            puzzleLevelPieceDoneDataTemp.SetPuzzlePiece( puzzlePieces );
            puzzleModel.puzzleLevelPieceDoneData[numPuzzelTemplate] = puzzleLevelPieceDoneDataTemp;
            puzzleModel.mainImageDone[numPuzzelTemplate] = CreatMainImageDone( numPuzzelTemplate, spriteImage );
        }

        internal void InitPuzzlesFiled2 ( LevelInitingData[] levelsInitingData, int sizeField ) {
            //internal void InitPuzzlesFiled2 ( LevelInitingData[] levelsInitingData, LevelTemplateData levelTemplateData ) {
            puzzleModel.puzzleLevelPieceDoneData = new PuzzleLevelPieceDoneData[levelsInitingData.Length];
            puzzleModel.mainImagePuzzleDone = new bool[levelsInitingData.Length];
            puzzleModel.mainImageDone = new MainImageDone[levelsInitingData.Length];

            for ( int i = 0; i < levelsInitingData.Length; i++ ) {
                InitPuzzle( i, levelsInitingData[i].spriteImage, levelsInitingData[i].puzzelTemplate, sizeField );
            }
        }

        void InitPuzzle ( int numPuzzelTemplate, Sprite spriteImage, PuzzleTemplate puzzelTemplate, int sizeField ) {
            //void InitPuzzle ( int numPuzzelTemplate, Sprite spriteImage, PuzzleTemplate puzzelTemplate, LevelTemplateData levelTemplateData ) {
            Dictionary<CustomVector2Int, int> puzzlePieceByIndex = puzzelTemplate.GetPuzzlePieceByIndex();

            Vector2 sizeImage = spriteImage.rect.size;
            Vector2 sizePiece = new Vector2( sizeImage.x / puzzelTemplate.sizeMatrixImage.x,
                                             sizeImage.y / puzzelTemplate.sizeMatrixImage.y );
            Vector2 startPosition = new Vector2( ( sizeImage.x - sizePiece.x ) / -2, ( sizeImage.y - sizePiece.y ) / 2f );
            Vector2 offsetSizePiece = new Vector2( sizePiece.x / 4f / 2f, sizePiece.y / 4f / 2f );

            PuzzleLevelPieceDoneData puzzleLevelPieceDoneDataTemp =
                new PuzzleLevelPieceDoneData( spriteImage.name, sizePiece, puzzelTemplate.sizeMatrixImage, startPosition,
                                              puzzelTemplate.GetPuzzlePieceByIndexCount() );

            List<List<PuzzlePiece>> puzzlePieces = new List<List<PuzzlePiece>>();
            List<List<CustomVector2Int>> puzzlePiecesConnection = new List<List<CustomVector2Int>>();
            List<CustomVector2Int> puzzlePiecesVisitIndex = new List<CustomVector2Int>();

            for ( int i = 0; i < puzzelTemplate.sizeMatrixImage.x; i++ ) {
                puzzlePieces.Add( new List<PuzzlePiece>() );
                for ( int j = 0; j < puzzelTemplate.sizeMatrixImage.y; j++ ) {
                    puzzlePieces[i].Add( piecesPool.PullPuzzlePiece() );
                }
            }

            int puzzlePieceId = 0;

            int endNum;
            int sign;
            int counter;

            for ( int j = 0; j < puzzelTemplate.sizeMatrixImage.y; j++ ) {
                int div = ( j + 1 ) % 2;

                if ( div == 1 ) {
                    endNum = puzzelTemplate.sizeMatrixImage.x;
                    sign = 1;
                    counter = 0;
                }
                else {
                    endNum = -1;
                    sign = -1;
                    counter = puzzelTemplate.sizeMatrixImage.x - 1;
                }

                while ( counter != endNum ) {
                    int i = counter;

                    CustomVector2Int puzzlePieceIndex = new CustomVector2Int( i, j );

                    PuzzlePieceView puzzlePieceViewDoneTemp = piecesPool.PullPuzzlePieceView();
                    puzzlePieceViewDoneTemp.gameObject.SetActive( false );
                    puzzlePieceViewDoneTemp.name = "puzzle_piece_done_" + i + "_" + j;
                    puzzlePieceViewDoneTemp.transform.SetParent( puzzlePieceContainer, false );

                    PuzzlePieceView puzzlePieceViewFieldTemp = piecesPool.PullPuzzlePieceView();
                    puzzlePieceViewFieldTemp.name = "puzzle_piece_field_" + i + "_" + j;
                    puzzlePieceViewFieldTemp.gameObject.SetActive( true );

                    int numPuzzlePiece = puzzlePieceByIndex[puzzlePieceIndex];
                    PuzzlePiecesData puzzlePiecesData = _GC.GameLinks.PuzzlePiecesDataSet.GetPuzzlePiecesDataByIndex( numPuzzlePiece );

                    puzzlePieces[i][j].SetPuzzlePieces( puzzlePieceViewDoneTemp, puzzlePieceViewFieldTemp );
                    puzzlePieces[i][j].SetPuzzlePieceParametrs( spriteImage, numPuzzlePiece, 0, puzzlePieceIndex, sizePiece, offsetSizePiece, startPosition,
                            puzzlePieceId );

                    if ( !puzzlePiecesVisitIndex.Contains( puzzlePieceIndex ) ) {
                        int indexConnection = -1;
                        for ( int k = 0; k < puzzelTemplate.GetConnectionPuzzlePiece().Count; k++ ) {
                            if ( puzzelTemplate.GetConnectionPuzzlePiece()[k].Contains( puzzlePieceIndex ) ) {
                                indexConnection = k;
                            }
                        }

                        if ( indexConnection != -1 ) {
                            puzzlePiecesConnection.Add( puzzelTemplate.GetConnectionPuzzlePiece()[indexConnection] );
                            puzzlePiecesVisitIndex.AddRange( puzzelTemplate.GetConnectionPuzzlePiece()[indexConnection] );
                        }
                        else {
                            puzzlePiecesConnection.Add( new List<CustomVector2Int>() {
                                puzzlePieceIndex
                            } );

                            puzzlePiecesVisitIndex.Add( puzzlePieceIndex );
                        }
                    }

                    counter += sign;
                }
            }

            puzzleLevelPieceDoneDataTemp.SetPuzzlePiece( puzzlePieces );

            List<CustomVector2Int> excludeIndexs = new List<CustomVector2Int>();
            for ( int n = 0; n < puzzlePiecesConnection.Count; n++ ) {
                PuzzlePieceField puzzlePieceFieldTemp = piecesPool.PullPuzzlePieceField();

                List<PuzzlePiece> puzzlePieceConnection = new List<PuzzlePiece>();
                for ( int m = 0; m < puzzlePiecesConnection[n].Count; m++ ) {
                    CustomVector2Int puzzlePieceIndex = puzzlePiecesConnection[n][m];

                    puzzlePieceConnection.Add( puzzlePieces[puzzlePieceIndex.x][puzzlePieceIndex.y] );
                }

                puzzlePieceFieldTemp.SetRandomStartAngle();
                puzzlePieceFieldTemp.InitPuzzlePieceField( puzzlePieceConnection );
                puzzlePieceFieldTemp.AddAlternativeNumType();

                CustomVector2Int indexIJ = GetrandomIndex( sizeField, excludeIndexs );
                excludeIndexs.Add( indexIJ );
                //CustomVector2Int indexIJ = levelTemplateData.GetIndexPuzzlePiece()[numPuzzelTemplate][n];
                puzzleModel.listPieces[indexIJ.x][indexIJ.y].isEmpty = false;
                CreatePieceController( indexIJ.x, indexIJ.y, puzzlePieceFieldTemp, n, numPuzzelTemplate );
            }

            puzzleModel.puzzleLevelPieceDoneData[numPuzzelTemplate] = puzzleLevelPieceDoneDataTemp;
            puzzleModel.mainImageDone[numPuzzelTemplate] = CreatMainImageDone( numPuzzelTemplate, spriteImage );
        }

        CustomVector2Int GetrandomIndex ( int sizeField, List<CustomVector2Int> excludeIndexs ) {
            List<CustomVector2Int> indexes = new List<CustomVector2Int>();
            for ( int i = 0; i < sizeField; i++ ) {
                for ( int j = 0; j < sizeField; j++ ) {
                    CustomVector2Int index = new CustomVector2Int( i, j );
                    if ( excludeIndexs.Contains( index ) ) {
                        continue;
                    }
                    indexes.Add( index );
                }
            }
            return indexes[Random.Range( 0, indexes.Count )];
        }

        internal void InitHint () {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

            //puzzleModel.lettersDoneForHints = new List<List<bool>>();
            //List<string> words = puzzleModel.quest.words[LocalizationManager.GetLanguageString()];
            //for ( int i = 0; i < words.Count; i++ ) {
            //    puzzleModel.lettersDoneForHints.Add( new List<bool>() );
            //    for ( int j = 0; j < words[i].Length; j++ ) {
            //        puzzleModel.lettersDoneForHints[i].Add( false );
            //    }
            //}

            //if ( puzzleModel.puzzelType.Equals( PuzzleType.Daily ) ) {

            //    puzzleModel.numHintQuest = _GC.FoundHintDailyPuzzle( progressByLanguage.numDaylyPuzzleQuest.GetValue() );
            //}
            //else {
            //    puzzleModel.numHintQuest = _GC.FoundHintQuest( _GC.numCatigory.GetValue(), _GC.numLevel.GetValue() );
            //}

            //if ( puzzleModel.numHintQuest != -1 ) {

            //    List<int> countShowHint = new List<int>();
            //    if ( puzzleModel.puzzelType.Equals( PuzzleType.Daily ) ) {
            //        for ( int i = 0; i < progressByLanguage.hintDailyPuzzle[puzzleModel.numHintQuest].countShow.Count; i++ ) {
            //            countShowHint.Add( progressByLanguage.hintDailyPuzzle[puzzleModel.numHintQuest].countShow[i].GetValue() );
            //        }
            //    }
            //    else {
            //        for ( int i = 0; i < progressByLanguage.hintQuest[puzzleModel.numHintQuest].countShow.Count; i++ ) {
            //            countShowHint.Add( progressByLanguage.hintQuest[puzzleModel.numHintQuest].countShow[i].GetValue() );
            //        }
            //    }

            //    for ( int i = 0; i < countShowHint.Count; i++ ) {
            //        if ( i < puzzleModel.letterDone.Count ) {
            //            for ( int j = 0; j < countShowHint[i]; j++ ) {
            //                if ( j < puzzleModel.letterDone[i].Count ) {
            //                    puzzleModel.lettersDoneForHints[i][j] = true;
            //                    //_arrayWords[i][j].transform.Find( "letter_done_ui_contaner_pos_img" ).GetComponent<UnityEngine.UI.Image>().enabled = true;
            //                    //_arrayWords[i][j].transform.Find( "letter_done_ui_contaner_pos_letter" ).GetComponent<UnityEngine.UI.Text>().enabled = true;
            //                    puzzleModel.letterDone[i][j].SetEnabledText( true );
            //                }
            //                else {
            //                    break;
            //                }
            //            }
            //        }
            //        else {
            //            break;
            //        }
            //    }
            //}
        }

        internal void InitFieldState () {
            List<int> indexWordFeildState = new List<int>();
            List<List<Vector2Int>>  inexLettersFeildState = new List<List<Vector2Int>>();

            int order = 0;
            for ( int i = 0; i < inexLettersFeildState.Count; i++ ) {
                for ( int j = 0; j < inexLettersFeildState[i].Count; j++ ) {
                    int indexI = inexLettersFeildState[i][j].x;
                    int indexJ = inexLettersFeildState[i][j].y;

                    if ( puzzleModel.listPieces[indexI][indexJ].isEmpty ) {
                        continue;
                    }

                    puzzleModel.listPieces[indexI][indexJ].done = true;
                    puzzleModel.listPieces[indexI][indexJ].pieceCntr.HideImmediately( order );
                    order++;
                }
            }

            for ( int i = 0; i < indexWordFeildState.Count; i++ ) {
                int indexI = indexWordFeildState[i];

                for ( int j = 0; j < inexLettersFeildState[i].Count; j++ ) {
                    int indexI2 = inexLettersFeildState[i][j].x;
                    int indexJ2 = inexLettersFeildState[i][j].y;

                    if ( puzzleModel.listPieces[indexI2][indexJ2].isEmpty ) {
                        continue;
                    }

                    //LetterDone letterDone = puzzleModel.letterDone[indexI][j];
                    //letterDone.SetDone( true );
                    //Vector2 posFly = letterDone.MyRectTransform.anchoredPosition;
                    //puzzleModel.listLetters[indexI2][indexJ2].letterDoneFly.EndFlyImmediately( posFly );
                }

                //for ( int j = 0; j < puzzleModel.lettersDoneForHints[indexI].Count; j++ ) {
                //    puzzleModel.lettersDoneForHints[indexI][j] = true;
                //}

                TaskDoneLevel taskDoneLevel = new TaskDoneLevel();
                taskDoneLevel.doneLetterIndex = inexLettersFeildState[i];
                taskDoneLevel.doneTask = true;
                //puzzleModel.doneTask[indexI] = taskDoneLevel;
            }
        }

        internal void SetParametersSizeField2 ( int[] levelsInitingDataArray, int sizeField ) {
            float startPosYUpper = 998f;

            float startPosYDone = -200f;

            //min, max
            float percentRatioFieldAndDone = 31f;
            float fullSizeGameFieldY = 1930f;

            //---------------------------
            float startSizeYDone = 148f;
            float indentDone = 26f;

            float maxFullSizeXDone = 0;
            for ( int i = 0; i < levelsInitingDataArray.Length; i++ ) {
                int piecesCount = levelsInitingDataArray[i];

                float maxFullSizeXDoneTemp = ( ( startSizeYDone + indentDone ) * piecesCount ) - indentDone;
                if ( maxFullSizeXDone < maxFullSizeXDoneTemp ) {
                    maxFullSizeXDone = maxFullSizeXDoneTemp;
                }
            }

            float fullSizeScreenXDone = _letterUIField.rect.width - 150f;

            float scaleFactorXDoneTemp = fullSizeScreenXDone / maxFullSizeXDone;
            if ( scaleFactorXDoneTemp > 1f ) {
                scaleFactorXDoneTemp = 1f;
            }
            float stepPosXLetterDoneTemp = ( startSizeYDone + indentDone ) * scaleFactorXDoneTemp;

            float entryFullSizeDone = ( ( startSizeYDone + indentDone ) * levelsInitingDataArray.Length ) - indentDone;

            float percentDoneTemp = ( entryFullSizeDone / fullSizeGameFieldY ) * 100f;
            percentDoneTemp = percentRatioFieldAndDone;

            float fullSizeYDone = ( percentDoneTemp * fullSizeGameFieldY ) / 100f;
            //float fullSizeYDone = 658.3f;

            float fullSizeYDoneForLettes = fullSizeYDone + startPosYDone;

            float scaleFactorYDoneTemp = fullSizeYDoneForLettes / entryFullSizeDone;

            if ( scaleFactorYDoneTemp > 1 ) {
                scaleFactorYDoneTemp = 1f;
            }

            float scaleFactorDoneTemp = scaleFactorYDoneTemp < scaleFactorXDoneTemp ? scaleFactorYDoneTemp : scaleFactorXDoneTemp;

            float stepPosLetterDoneTemp = ( startSizeYDone + indentDone ) * scaleFactorDoneTemp;
            float middleDonePosY = startPosYDone - ( fullSizeYDoneForLettes / 2 );

            float middleDonePosY2 = middleDonePosY + ( ( stepPosLetterDoneTemp * ( levelsInitingDataArray.Length - 1 ) ) / 2f );
            middleDonePosYGloabl = middleDonePosY + ( middleDonePosY / 2f ) + ( startSizeYDone / 2 );

            //parametersLettersDone.stepPosLetter = new Vector2( stepPosXLetterDoneTemp,  stepPosYLetterDoneTemp );
            parametersLettersDone.stepPosLetter = stepPosLetterDoneTemp;
            parametersLettersDone.startPosLetter = new Vector2( 0f, middleDonePosY2 );
            //parametersLettersDone.scaleFactor = scaleFactorYDoneTemp < scaleFactorXDoneTemp ? scaleFactorYDoneTemp : scaleFactorXDoneTemp;
            parametersLettersDone.scaleFactor = scaleFactorDoneTemp;

            backLetterDone.sizeDelta = new Vector2( backLetterDone.sizeDelta.x, fullSizeYDone + 60f );
            //--------------------------------------
            float startPosYDown = -890f;
            float diffPersentField = 100f - percentDoneTemp - 8;

            float fullSizeField = ( diffPersentField * fullSizeGameFieldY ) / 100f;
            //float fullSizeField = fullSizeGameFieldY - fullSizeYDone;

            float sizeLetter = 570f;
            float indentField = 20f;

            float entryFullSizeField = ( ( sizeLetter + indentField ) * sizeField ) - indentField;

            float scaleFactorFieldTemp = fullSizeField / entryFullSizeField;
            if ( scaleFactorFieldTemp > 1f ) {
                scaleFactorFieldTemp = 1f;
            }

            float stepPosLetterTemp = ( sizeLetter + indentField ) * scaleFactorFieldTemp;

            float middleFieldPosY = startPosYUpper - fullSizeYDone - ( fullSizeField / 2 );
            middleFieldPosY = startPosYDown + ( fullSizeField / 2 );

            float middleFieldPosY2 = middleFieldPosY + ( ( stepPosLetterTemp * ( sizeField - 1 ) ) / 2f );
            float middleFieldPosX2 = 0 - ( ( stepPosLetterTemp * ( sizeField - 1 ) ) / 2f );

            Vector2 startPosField = new Vector2( middleFieldPosX2, middleFieldPosY2 );

            parametersLettersField.startPosLetter = startPosField;
            parametersLettersField.stepPosLetter = stepPosLetterTemp;
            parametersLettersField.scaleFactor = scaleFactorFieldTemp;
        }

        void CreatePieceController ( int indexI, int indexJ, PuzzlePieceField puzzlePieceField, int numPuzzlePiece, int numPuzzleTemplate ) {
            PieceFieldController goPieceUI = piecesPool.PullPieceFieldController();
            goPieceUI.gameObject.name = "piece_" + indexI.ToString() + "_" + indexJ.ToString();
            Vector2 letterPosition = new Vector2( parametersLettersField.startPosLetter.x + parametersLettersField.stepPosLetter * indexI,
                                                  parametersLettersField.startPosLetter.y - parametersLettersField.stepPosLetter * indexJ );
            goPieceUI.SetPosition( letterPosition );
            goPieceUI.transform.localScale = new Vector3( parametersLettersField.scaleFactor, parametersLettersField.scaleFactor, 1f );

            // переписать
            goPieceUI.Init( indexI, indexJ, puzzlePieceField, numPuzzlePiece, numPuzzleTemplate );
            goPieceUI.transform.SetParent( _letterUIField, false );
            goPieceUI.gameObject.SetActive( true );
            puzzleModel.listPieces[indexI][indexJ].pieceCntr = goPieceUI;
            puzzleController.SubscribePieceField( goPieceUI );
        }

        //LetterDoneFly CreateLetterDoneFly ( int indexI, int indexJ, string newLetter, int numWord ) {
        //    LetterDoneFly goLetterDoneUI = piecesPool.PullLetterDoneFly();
        //    goLetterDoneUI.gameObject.name = "letter_done_fly_" + indexI.ToString() + "_" + indexJ.ToString();
        //    goLetterDoneUI.transform.SetParent( _letterDoneUIContanerFly, false );
        //    goLetterDoneUI.transform.localScale = new Vector3( parametersLettersDone.scaleFactor, parametersLettersDone.scaleFactor, 1f );
        //    Vector2 letterFlyPosition = new Vector2( parametersLettersField.startPosLetter.x + parametersLettersField.stepPosLetter * indexJ,
        //            ( parametersLettersField.startPosLetter.y - parametersLettersField.stepPosLetter * indexI ) - 1000f );
        //    goLetterDoneUI.SetStart( letterFlyPosition );

        //    puzzleModel.listPieces[indexI][indexJ].pieceDoneFly = goLetterDoneUI;

        //    return goLetterDoneUI;
        //}

        MainImageDone CreatMainImageDone ( int numMainImage, Sprite mainImageSprite ) {
            MainImageDone goMainImageUI = piecesPool.PullMainImageDone();
            goMainImageUI.name = "main_image_done_" + numMainImage.ToString();
            goMainImageUI.transform.SetParent( mainImageDoneContainer, false );

            goMainImageUI.SetDone( false );
            goMainImageUI.SetMainImage( mainImageSprite );

            puzzleController.SubscribeMainImageDone( goMainImageUI );

            return goMainImageUI;
        }

        //LetterDoneButtonI CreateLetterDoneButtonI ( int numWord, int sizeWord, float startPosXLetterDone ) {
        //    LetterDoneButtonI letterDoneButtonI = lettersPool.PullLetterDoneButtonI();
        //    letterDoneButtonI.name = "letter_done_bitton_ui_" + numWord;
        //    letterDoneButtonI.transform.SetParent( _letterDoneUIContaner, false );
        //    letterDoneButtonI.SetPositionAndScale( new Vector2( startPosXLetterDone + parametersLettersDone.stepPosLetter * ( sizeWord - 1 ),
        //                                           parametersLettersDone.startPosLetter.y - parametersLettersDone.stepPosLetter * numWord ), parametersLettersDone.scaleFactor );
        //    letterDoneButtonI.SetStart( numWord, sizeWord );

        //    return letterDoneButtonI;
        //}

        internal void ListPiecesBackToPool () {
            //puzzleModel.listLetters = new List<List<LettersLevel>>();

            for ( int i = 0; i < puzzleModel.listPieces.Length; i++ ) {
                for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
                    if ( puzzleModel.listPieces[i][j].pieceCntr != null ) {
                        puzzleController.SubscribePieceField( puzzleModel.listPieces[i][j].pieceCntr );

                        piecesPool.PushPuzzlePieceField( puzzleModel.listPieces[i][j].pieceCntr.GetPuzzlePieceField() );
                        puzzleModel.listPieces[i][j].pieceCntr.ClearPuzzlePieceCntr();

                        piecesPool.PushPieceFieldController( puzzleModel.listPieces[i][j].pieceCntr );
                        puzzleModel.listPieces[i][j].pieceCntr = null;
                    }

                    if ( puzzleModel.listPieces[i][j].pieceDoneFly != null ) {
                        piecesPool.PushLetterDoneFly( puzzleModel.listPieces[i][j].pieceDoneFly );
                        puzzleModel.listPieces[i][j].pieceDoneFly = null;
                    }
                }
                puzzleModel.listPieces[i] = null;
            }

            for ( int k = 0; k < puzzleModel.puzzleLevelPieceDoneData.Length; k++ ) {
                PuzzleLevelPieceDoneData puzzleLevelPieceDoneData = puzzleModel.GetPuzzleLevelPieceDoneDataByIndex( k );

                for ( int i = 0; i < puzzleLevelPieceDoneData.GetPuzzlePiece().Count; i++ ) {
                    for ( int j = 0; j < puzzleLevelPieceDoneData.GetPuzzlePiece()[i].Count; j++ ) {
                        PuzzlePiece puzzlePieceTemp = puzzleLevelPieceDoneData.GetPuzzlePiece()[i][j];

                        piecesPool.PushPuzzlePieceView( puzzlePieceTemp.PuzzlePieceViewDone );
                        piecesPool.PushPuzzlePieceView( puzzlePieceTemp.PuzzlePieceViewField );

                        piecesPool.PushPuzzlePiece( puzzlePieceTemp );
                    }
                }
            }

            for ( int i = 0; i < puzzleModel.mainImageDone.Length; i++ ) {
                puzzleController.UnSubscribeMainImageDone( puzzleModel.mainImageDone[i] );
                piecesPool.PushMainImageDone( puzzleModel.mainImageDone[i] );
            }
        }

        internal void PieceDoneBackToPool () {
            for ( int i = 0; i < puzzleModel.mainImageDone.Length; i++ ) {
                piecesPool.PushMainImageDone( puzzleModel.mainImageDone[i] );
            }
        }
    }
}
