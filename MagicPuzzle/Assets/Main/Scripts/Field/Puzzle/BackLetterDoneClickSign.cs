﻿using UnityEngine;

namespace Puzzle {
    public class BackLetterDoneClickSign: MonoBehaviour {
        [SerializeField]
        Animator myAnimator;

        internal void SetAllowHintWods ( bool isAllow ) {

        }

        internal void StartAnimation () {
            SetAllowHintWods( true );

            //myAnimator.enabled = true;

            myAnimator.gameObject.SetActive( true );
        }

        internal void EndAnimation () {
            SetAllowHintWods( true );

            myAnimator.gameObject.SetActive( false );
            //myAnimator.enabled = false;
        }
    }
}