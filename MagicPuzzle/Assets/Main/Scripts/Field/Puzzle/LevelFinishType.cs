﻿
namespace Puzzle {
    public enum PuzzleType {
        Common,
        Daily,
        Editor
    }

    public enum CloseWinUIType {
        GoToMenu,
        NextLevel,
        NextCategory,
        None
    }

    public enum WinAnimationSteps {
        StartShowWindow,
        FilCup,
        StartShowWinCategory,
        WaitShowWinCategory,
        WaitClsoeWindow,
        StartHide
    }
}

