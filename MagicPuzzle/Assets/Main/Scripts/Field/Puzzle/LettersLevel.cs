﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public class PieceLevel {
        internal PieceFieldController pieceCntr;
        internal LetterDoneFly pieceDoneFly;
        internal bool done;

        public int indexWord;
        public int indexLetter;

        public bool isEmpty;

        public PieceLevel () {
            isEmpty = true;
            done = false;
        }
    }
}
