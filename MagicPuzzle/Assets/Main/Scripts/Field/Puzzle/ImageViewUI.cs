﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using Common;
using Menu;
using DG.Tweening;

public class ImageViewUI : WindowMonoBehaviour {
    [SerializeField]
    RectTransform myRectTransform;
    [SerializeField]
    Button closeButton;
    [SerializeField]
    Image mainImage;

    RectTransform mainImageRectTransform;
    Vector2 startSizeImage;

    Coroutine puzzleUpdate;

    float doubleClickTime = 0.2f;
    float currentClickTime;
    float lastClickTime;

    // Start is called before the first frame update
    internal override void InitAwake () {
        closeButton.onClick.AddListener( Hide );
        mainImageRectTransform = mainImage.GetComponent<RectTransform>();
    }

    internal void Show ( Sprite mainImageSprite ) {
        base.Show();
        mainImage.sprite = mainImageSprite;
        Vector2 sizeImage = new Vector2( myRectTransform.rect.width / 1.2f, myRectTransform.rect.height / 1.6f );

        if ( sizeImage.x < mainImageSprite.rect.width && sizeImage.y < mainImageSprite.rect.height ) {
            startSizeImage = sizeImage;
        }
        else {
            startSizeImage = new Vector2( mainImageSprite.rect.width, mainImageSprite.rect.height );
        }

        mainImageRectTransform.sizeDelta = startSizeImage;

        if ( puzzleUpdate != null ) {
            StopCoroutine( puzzleUpdate );
            puzzleUpdate = null;
        }
        puzzleUpdate = StartCoroutine( PuzzleUpdateCoroutine() );
    }

    IEnumerator PuzzleUpdateCoroutine () {
        float orthoZoomSpeed = 0.001f;        // The rate of change of the orthographic size in orthographic mode.
        bool isTouchDown = false;

        for ( ; ; ) {
            if ( !IsShown ) {
                break;
            }

            if ( Input.touchCount == 2 ) {
                isTouchDown = false;

                Touch touchZero = Input.GetTouch( 0 );
                Touch touchOne = Input.GetTouch( 1 );

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevTouchDeltaMag = ( touchZeroPrevPos - touchOnePrevPos ).magnitude;
                float touchDeltaMag = ( touchZero.position - touchOne.position ).magnitude;

                float deltaMagnitudeDiff = ( prevTouchDeltaMag - touchDeltaMag ) / 2f;

                Vector2 movePosition = ( touchZero.deltaPosition + touchOne.deltaPosition ) / 2f;

                float zoomTemp = mainImageRectTransform.localScale.x;
                zoomTemp -= deltaMagnitudeDiff * orthoZoomSpeed;

                if ( zoomTemp > 2f ) {
                    zoomTemp = 2f;
                }
                else if ( zoomTemp < 0.5f ) {
                    zoomTemp = 0.5f;
                }

                mainImageRectTransform.localScale = new Vector3( zoomTemp, zoomTemp, 1 );

                MoveMainImage( movePosition );
            }
            else if ( Input.GetMouseButtonDown( 0 ) ) {
                currentClickTime = Time.time;

                if ( ( currentClickTime - lastClickTime ) < doubleClickTime ) {
                    mainImageRectTransform.DOKill();
                    mainImageRectTransform.DOScale( Vector3.one, 0.3f );
                    mainImageRectTransform.DOAnchorPos( Vector3.zero, 0.3f );
                }

                lastClickTime = currentClickTime;

                isTouchDown = true;
            }
            if ( Input.GetMouseButtonUp( 0 ) ) {
                isTouchDown = false;
            }
            if ( isTouchDown && Input.touchCount > 0 ) {
                Touch touchZero = Input.GetTouch( 0 );
                Vector2 movePosition = touchZero.deltaPosition;

                MoveMainImage( movePosition );
            }

            yield return null;
        }
    }

    void MoveMainImage ( Vector2 movePosition ) {
        Vector2 zoomFieldAnchoredPosition = mainImageRectTransform.anchoredPosition;
        zoomFieldAnchoredPosition += movePosition;

        if ( zoomFieldAnchoredPosition.x < -250f ) {
            zoomFieldAnchoredPosition = new Vector2( -250f, zoomFieldAnchoredPosition.y );
        }
        else if ( zoomFieldAnchoredPosition.x > 250f ) {
            zoomFieldAnchoredPosition = new Vector2( 250f, zoomFieldAnchoredPosition.y );
        }

        if ( zoomFieldAnchoredPosition.y < -200f ) {
            zoomFieldAnchoredPosition = new Vector2( zoomFieldAnchoredPosition.x, -200f );
        }
        else if ( zoomFieldAnchoredPosition.y > 600f ) {
            zoomFieldAnchoredPosition = new Vector2( zoomFieldAnchoredPosition.x, 600f );
        }

        mainImageRectTransform.anchoredPosition = zoomFieldAnchoredPosition;
    }

    internal override void Hide () {
        StopCoroutine( puzzleUpdate );
        puzzleUpdate = null;

        base.Hide();
    }
}
