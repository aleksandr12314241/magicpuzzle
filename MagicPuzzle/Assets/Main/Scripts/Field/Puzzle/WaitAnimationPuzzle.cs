﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using System;

namespace Puzzle {
    [RequireComponent( typeof( PuzzleController ) )]
    public class WaitAnimationPuzzle: MonoBehaviour {
        [SerializeField]
        int wrongWordStart = 3;
        [SerializeField]
        int beetwenAnimationStart = 10;
        [SerializeField]
        int beetwenDoneAnimationStart = 5;
        [SerializeField]
        PuzzleController puzzleController;

        PuzzleModel puzzleModel;

        Coroutine coroutineWaitAnimation;
        Coroutine coroutineWaitDoneAnimation;
        int wrongWordCurrent;
        int beetwenAnimationCurrent;
        int beetwenDoneAnimationCurrent;
        bool isReadyForAnimation = false;

        bool isWinCurrentQuest = false;
        bool isActivePuzzel = false;
        List<Action<PieceFieldController, int, int>> actonAnimations = new List<Action<PieceFieldController, int, int>>();

        internal void Init () {
            puzzleModel = puzzleController.GetPuzzleModel();

            isReadyForAnimation = false;

            puzzleController.OnWrongWord += OnWrongWord;
            //puzzleController.OnStartQuest += OnStartQuest;
            //puzzleController.OnActivePuzzle += OnActivePuzzle;
            puzzleController.OnStartHidePiece += OnStartHidePiece;
            //puzzleController.OnStartShowLetters += OnStartShowLetters;
            puzzleController.OnWinLevel += OnWinQuest;

            wrongWordCurrent = wrongWordStart;
            beetwenAnimationCurrent = beetwenAnimationStart;
            beetwenDoneAnimationCurrent = beetwenDoneAnimationStart;

            actonAnimations.Add( HighlightAnimation );
            actonAnimations.Add( ShakeAnimation );
            actonAnimations.Add( PositionAnimation );
        }

        void OnWinQuest () {
            isWinCurrentQuest = true;
            isReadyForAnimation = false;

            if ( coroutineWaitAnimation != null ) {
                StopCoroutine( coroutineWaitAnimation );
                coroutineWaitAnimation = null;
            }

            if ( coroutineWaitDoneAnimation != null ) {
                StopCoroutine( coroutineWaitDoneAnimation );
                coroutineWaitDoneAnimation = null;
            }
        }

        void OnWrongWord ( string word ) {
            if ( wrongWordCurrent <= 0 ) {
                return;
            }

            wrongWordCurrent--;

            if ( wrongWordCurrent <= 0 ) {
                isReadyForAnimation = true;
            }
        }

        //void OnStartQuest ( Quests quests ) {
        //    if ( coroutineWaitAnimation != null ) {
        //        StopCoroutine( coroutineWaitAnimation );
        //        coroutineWaitAnimation = null;
        //    }

        //    if ( coroutineWaitDoneAnimation != null ) {
        //        StopCoroutine( coroutineWaitDoneAnimation );
        //        coroutineWaitDoneAnimation = null;
        //    }

        //    isWinCurrentQuest = false;
        //    isReadyForAnimation = false;
        //    wrongWordCurrent = wrongWordStart;
        //}

        void Spirale () {
            //int sizeField = puzzleModel.GetQuest().sizeField;
            //TODO
            int sizeField = 4;

            int[][] matrix = new int[sizeField][];
            for ( int i = 0; i < sizeField; i++ ) {
                matrix[i] = new int[sizeField];
            }

            int row = 0;
            int col = 0;
            int dx = 1;
            int dy = 0;
            int dirChanges = 0;
            int visits = sizeField;

            List<Vector2Int> indexes = new List<Vector2Int>();

            for ( int i = 0; i < sizeField * sizeField; i++ ) {

                indexes.Add( new Vector2Int( row, col ) );

                matrix[row][col] = i + 1;
                if ( --visits == 0 ) {
                    visits = sizeField * ( dirChanges % 2 ) + sizeField * ( ( dirChanges + 1 ) % 2 ) - ( dirChanges / 2 - 1 ) - 2;
                    int temp = dx;
                    dx = -dy;
                    dy = temp;
                    dirChanges++;
                }

                col += dx;
                row += dy;
            }

            //for ( int i = 0; i < matrix.Length; i++ ) {
            //    for ( int j = 0; j < matrix[i].Length; j++ ) {
            //        Debug.Log( matrix[i][j] );
            //    }
            //}

            float stepDelay = 0.5f / indexes.Count;
            float deleySartDelay = 0f;
            for ( int i = indexes.Count - 1; i >= 0; i-- ) {
                PieceFieldController pieceController = puzzleController.GetPuzzleModel().GetPieceController( indexes[i].x, indexes[i].y );
                if ( pieceController == null ) {
                    continue;
                }

                pieceController.StartPuzzleAnimation( deleySartDelay );
                deleySartDelay += stepDelay;
            }

            //for ( int i = 0; i < indexes.Count; i++ ) {
            //    Debug.Log( indexes[i].x + " , " + indexes[i].y );
            //}
        }

        //void OnActivePuzzle ( bool isActive ) {
        //    isActivePuzzel = isActive;

        //    if ( !isActivePuzzel ) {
        //        //if ( coroutineWaitDoneAnimation != null ) {
        //        //    StopCoroutine( coroutineWaitDoneAnimation );
        //        //    coroutineWaitDoneAnimation = null;
        //        //}

        //        //if ( coroutineWaitAnimation != null ) {
        //        //    StopCoroutine( coroutineWaitAnimation );
        //        //    coroutineWaitAnimation = null;
        //        //}

        //        return;
        //    }

        //    if ( isReadyForAnimation ) {
        //        if ( coroutineWaitAnimation != null ) {
        //            StopCoroutine( coroutineWaitAnimation );
        //            coroutineWaitAnimation = null;
        //        }

        //        if ( coroutineWaitDoneAnimation != null ) {
        //            StopCoroutine( coroutineWaitDoneAnimation );
        //            coroutineWaitDoneAnimation = null;
        //        }

        //        coroutineWaitAnimation = StartCoroutine( WaitAnimationUpdate() );

        //        if ( coroutineWaitDoneAnimation != null ) {
        //            StopCoroutine( coroutineWaitDoneAnimation );
        //            coroutineWaitDoneAnimation = null;
        //        }

        //        coroutineWaitDoneAnimation = StartCoroutine( WaitAnimationDoneUpdate() );
        //    }


        //    //Spirale();
        //}

        void OnStartHidePiece ( PieceFieldController pieceController ) {
            if ( coroutineWaitAnimation != null ) {
                StopCoroutine( coroutineWaitAnimation );
                coroutineWaitAnimation = null;
            }

            if ( coroutineWaitDoneAnimation != null ) {
                StopCoroutine( coroutineWaitDoneAnimation );
                coroutineWaitDoneAnimation = null;
            }
        }

        //void OnStartShowLetters () {
        //    if ( coroutineWaitAnimation != null ) {
        //        StopCoroutine( coroutineWaitAnimation );
        //        coroutineWaitAnimation = null;
        //    }
        //    if ( coroutineWaitDoneAnimation != null ) {
        //        StopCoroutine( coroutineWaitDoneAnimation );
        //        coroutineWaitDoneAnimation = null;
        //    }

        //    if ( isReadyForAnimation ) {
        //        coroutineWaitAnimation = StartCoroutine( WaitAnimationUpdate() );
        //        beetwenDoneAnimationCurrent = beetwenDoneAnimationStart;
        //        coroutineWaitDoneAnimation = StartCoroutine( WaitAnimationDoneUpdate() );
        //    }
        //}

        //IEnumerator WaitAnimationUpdate () {
        //    for ( ;; ) {
        //        yield return new WaitForSeconds( 1f );

        //        if ( isWinCurrentQuest ) {
        //            break;
        //        }
        //        if ( isActivePuzzel ) {
        //            beetwenAnimationCurrent--;
        //            if ( beetwenAnimationCurrent <= 0 ) {
        //                StartRandomAnimation();
        //            }
        //        }
        //    }
        //}

        public void StartRandomAnimation () {
            //beetwenAnimationCurrent = beetwenAnimationStart + puzzleModel.GetQuest().sizeField;

            //int indexRandomAnimation = UnityEngine.Random.Range( 0, actonAnimations.Count );
            //Action<LetterController, int, int> actonAnimationEntry = actonAnimations[indexRandomAnimation];

            //StartRandomAnimation( actonAnimationEntry );
        }

        public void StartHighlightAnimation () {
            StartRandomAnimation( actonAnimations[0] );
        }

        public void StartShakeAnimation () {
            StartRandomAnimation( actonAnimations[1] );
        }

        public void StartPositionAnimation () {
            StartRandomAnimation( actonAnimations[2] );
        }

        void StartRandomAnimation ( Action<PieceFieldController, int, int > actonAnimationEntry ) {

            List<Vector2Int> indexes = new List<Vector2Int>();
            //TODO
            //int sizeField = puzzleModel.GetQuest().sizeField;
            int sizeField = 4;

            for ( int k = 0; k < sizeField * 2; k++ ) {
                for ( int j = 0; j <= k; j++ ) {
                    int i = k - j;
                    if ( i < sizeField && j < sizeField ) {

                        indexes.Add( new Vector2Int( i, j ) );
                    }
                }
            }

            int signCounter = 1;
            int startIndexTemp = 0;
            int endIndexTemp = 1;
            int counterBound = 1;
            int indexDelay = 0;

            int totalDelay = ( sizeField * 2 ) - 1;

            for ( int j = 0; j <= sizeField + sizeField; j++ ) {
                for ( int i = startIndexTemp; i < endIndexTemp; i++ ) {
                    PieceFieldController pieceController = puzzleController.GetPuzzleModel().GetPieceController( indexes[i].x, indexes[i].y );

                    if ( pieceController == null ) {
                        continue;
                    }

                    actonAnimationEntry( pieceController, indexDelay, totalDelay );
                }
                indexDelay++;
                startIndexTemp = endIndexTemp;
                counterBound += signCounter;
                endIndexTemp = startIndexTemp + counterBound;

                if ( counterBound >= sizeField ) {
                    signCounter = -1;
                }

                if ( endIndexTemp >= indexes.Count ) {
                    endIndexTemp = indexes.Count;
                }

                if ( counterBound <= 0 ) {
                    break;
                }
            }
        }

        void HighlightAnimation ( PieceFieldController pieceController, int indexDelay, int totalDelay ) {
            pieceController.StartHighlightAnimation( indexDelay );
        }

        void ShakeAnimation ( PieceFieldController pieceController, int indexDelay, int totalDelay ) {
            pieceController.StartShakeAnimation( indexDelay );
        }

        void PositionAnimation ( PieceFieldController pieceController, int indexDelay, int totalDelay ) {
            pieceController.StartPositionAnimation( indexDelay, totalDelay );
        }

        IEnumerator WaitAnimationDoneUpdate () {
            for ( ; ; ) {
                yield return new WaitForSeconds( 1f );

                if ( isWinCurrentQuest ) {
                    break;
                }
                if ( isActivePuzzel ) {
                    //Debug.Log( "beetwenDoneAnimationCurrent     " + beetwenDoneAnimationCurrent );
                    beetwenDoneAnimationCurrent--;
                    if ( beetwenDoneAnimationCurrent <= 0 ) {
                        beetwenDoneAnimationCurrent = beetwenDoneAnimationStart;
                        StartRandomDoneAnimation();
                    }
                }
            }
        }

        void StartRandomDoneAnimation () {
            PositionDoneAnimation();
        }

        void PositionDoneAnimation () {
            //List<TaskDoneLevel> doneTask = puzzleController.GetPuzzleModel().GetDoneTask();
            List<TaskDoneLevel> doneTask = new List<TaskDoneLevel>();

            List<int> indexDoneTask = new List<int>();
            for ( int i = 0; i < doneTask.Count; i++ ) {
                if ( !doneTask[i].doneTask ) {
                    continue;
                }

                indexDoneTask.Add( i );
            }

            if ( indexDoneTask.Count == 0 ) {
                return;
            }

            int randomIndexDoneTask = UnityEngine.Random.Range( 0, indexDoneTask.Count );

            //List<Vector2Int> doneTaskIndexLettes = puzzleController.GetPuzzleModel().GetDoneTaskIndexLettes( indexDoneTask[randomIndexDoneTask] );
            List<Vector2Int> doneTaskIndexLettes = new List<Vector2Int>();

            for ( int i = 0; i < doneTaskIndexLettes.Count; i++ ) {
                LetterDoneFly letterDoneFly = puzzleController.GetPuzzleModel().GetLetterDoneFly( doneTaskIndexLettes[i].x, doneTaskIndexLettes[i].y );
                if ( letterDoneFly != null ) {
                    letterDoneFly.StartPositionAnimation( i );
                }
            }
        }
    }
}
