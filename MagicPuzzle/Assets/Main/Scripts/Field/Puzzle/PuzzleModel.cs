﻿using System.Collections.Generic;
using UnityEngine;
using Common;

namespace Puzzle {
    internal class PuzzleModel {

        internal PuzzleType puzzelType;

        internal int sizeI; // row
        internal int sizeJ; // collumn
        //internal List<List<PieceLevel>> listPieces;
        internal PieceLevel[][] listPieces;
        internal MainImageDone[] mainImageDone;
        internal List<CustomVector2Int> downPieces;
        internal List<CustomVector2Int> upPieces;
        int countPuzzles = 0;
        Dictionary<int, int> priceHint;
        internal PuzzleLevelPieceDoneData[] puzzleLevelPieceDoneData;
        internal FillMainImagesData fillMainImageData;
        internal bool[] mainImagePuzzleDone;

        internal PuzzleModel () {
            ////letterDoneButtonsI = new List<LetterDoneButtonI>();
            //hidePieces = new List<PieceLevel>();
            //scaleLettes = new List<LettersLevel>();
            downPieces = new List<CustomVector2Int>();
            upPieces = new List<CustomVector2Int>();
            fillMainImageData = new FillMainImagesData();
            fillMainImageData.SetFillSingleMainImageData( new List<FillSingleMainImageData>() );

            priceHint = new Dictionary<int, int>() {
                { 12, 0 },
                { 1000, 1 }
            };
        }

        //GET
        internal Vector2 GetLettersLevelPositionByIndex ( int indexI, int indexJ ) {
            //if ( indexI >= listPieces.Count ) {
            //    return Vector2.zero;
            //}

            //if ( indexJ >= listPieces[indexI].Count ) {
            //    return Vector2.zero;
            //}

            return listPieces[indexI][indexJ].pieceCntr.MyRectTransform.anchoredPosition;
        }

        internal PieceFieldController GetPieceController ( int indexI, int indexJ ) {
            //if ( indexI >= listPieces.Length ) {
            //    return null;
            //}

            //if ( indexJ >= listPieces[indexI].Length ) {
            //    return null;
            //}

            if ( listPieces[indexI][indexJ].isEmpty ) {
                return null;
            }

            return listPieces[indexI][indexJ].pieceCntr;
        }

        internal LetterDoneFly GetLetterDoneFly ( int indexI, int indexJ ) {
            if ( indexI >= listPieces.Length ) {
                return null;
            }

            if ( indexJ >= listPieces[indexI].Length ) {
                return null;
            }

            if ( listPieces[indexI][indexJ].isEmpty ) {
                return null;
            }

            return listPieces[indexI][indexJ].pieceDoneFly;
        }

        internal PuzzleType GetPuzzelType () {
            return puzzelType;
        }

        internal int GetCountPuzzles () {
            return countPuzzles;
        }

        internal int GetPriceHint ( int numLevel ) {
            if ( puzzelType.Equals( PuzzleType.Daily ) ) {
                return 1;
            }

            int maxNumLevel = -1;
            foreach ( var item in priceHint ) {
                if ( numLevel <= item.Key ) {
                    maxNumLevel = item.Key;
                    break;
                }
            }

            if ( maxNumLevel == -1 ) {
                return 1;
            }

            return priceHint[maxNumLevel];
        }

        internal PuzzleLevelPieceDoneData GetPuzzleLevelPieceDoneDataByIndex ( int index ) {
            return puzzleLevelPieceDoneData[index];
        }

        //SET
        internal void SetPuzzelType ( PuzzleType puzzelType ) {
            this.puzzelType = puzzelType;
        }
        internal void SetCountPuzzles ( int countPuzzles ) {
            this.countPuzzles = countPuzzles;
        }
    }
}
