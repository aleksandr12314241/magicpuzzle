﻿using DG.Tweening;
using Menu;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Common;
using UnityEngine.Analytics;
using Utility;

namespace Puzzle {
    public class PuzzleController: MonoBehaviour {
        internal event Action OnClickButtonBack = () => { };
        //internal event Action<Quests> OnStartQuest = ( Quests param ) => { };
        internal event Action OnWinLevel = () => { };
        internal event Action<PieceFieldController> OnStartHidePiece = ( PieceFieldController param1 ) => { };
        internal event Action OnStartShowPiece = () => { };
        internal event Action<string> OnWrongWord = ( string param ) => { };
        internal event Action<bool> OnActivePuzzle = ( bool param ) => { };
        internal event Action<bool, MainImageDone> OnUseHint = ( bool param1, MainImageDone param2 ) => { };
        internal event Action<int> OnRecognizeWord = ( int param ) => { };
        internal event Action OnUseRestart = () => { };
        internal event Action OnUseRestartEnd = () => { };

        [SerializeField]
        RectTransform _letterUIParticles;
        [SerializeField]
        AudioSource _myAudioSource;
        [SerializeField]
        UnityEngine.EventSystems.EventTrigger _letterClick;
        [SerializeField]
        PuzzleGenerator generator;
        [SerializeField]
        PuzzleUpperDoneField upperDoneField;
        [SerializeField]
        WaitAnimationPuzzle waitAnimationPuzzle;
        [SerializeField]
        PuzzleBottomMenu puzzleBottomMenu;
        [SerializeField]
        PieceDonePosition pieceDonePosition;
        [SerializeField]
        PieceControlButton pieceControlButton;

        GameController _GC;
        WinUI _winUI;
        HintAddedUI hintAddedUI;
        LockUI _lockUI;
        ImageViewUI imageViewUI;
        PopupTextUI popupTextUI;

        Animator _myAnimator;

        int _mostLenghtWord;
        float _lowerEdge;

        bool _win;
        public bool Win {
            get {
                return _win;
            }
        }

        bool _isMouseDown;

        Vector2 _plusEdge;
        Vector2 _minusEdge;

        Coroutine _puzzleUpdate;
        bool _isActivePuzzle;

        TrailSystemMouse trailSystem;

        Vector2Int[] indexDirection;
        GameLinks gameLinks;

        PuzzleModel puzzleModel;

        float inputTextBackSizeY;

        float lastClick = 0f;
        float intervalClick = 0.4f;

        private void Awake () {
            puzzleModel = new PuzzleModel();
            //puzzleModel.SetLetterDoneButtonI( letterDoneButtonI );

            MenuController menuController = FindObjectOfType<MenuController>();

            generator = GetComponent<PuzzleGenerator>();
            generator.Init( this );
            upperDoneField.Init( this );

            //puzzleModel.hidePieces = new List<PieceLevel>();
            //puzzleModel.scaleLettes = new List<LettersLevel>();

            _win = false;
            _isMouseDown = false;

            _winUI = ( WinUI ) menuController.GetWindow<WinUI>();
            hintAddedUI = ( HintAddedUI ) menuController.GetWindow<HintAddedUI>();

            _lockUI = FindObjectOfType<LockUI>();
            imageViewUI = ( ImageViewUI ) menuController.GetWindow<ImageViewUI>();
            popupTextUI = ( PopupTextUI ) menuController.GetWindow<PopupTextUI>();

            trailSystem = FindObjectOfType<TrailSystemMouse>();
            InitEventTriggerLetterClick();

            indexDirection = new Vector2Int[8];
            indexDirection[0] = new Vector2Int( -1, -1 );
            indexDirection[1] = new Vector2Int( -1, 0 );
            indexDirection[2] = new Vector2Int( -1, 1 );
            indexDirection[3] = new Vector2Int( 0, 1 );
            indexDirection[4] = new Vector2Int( 1, 1 );
            indexDirection[5] = new Vector2Int( 1, 0 );
            indexDirection[6] = new Vector2Int( 1, -1 );
            indexDirection[7] = new Vector2Int( 0, -1 );
        }

        void InitEventTriggerLetterClick () {
            // кликн на игровое поле
            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            //entry.eventID = EventTriggerType.PointerDown;
            //entry.callback.AddListener( ( data ) => {
            //    ClickOnFieldDown( ( PointerEventData ) data );
            //} );
            //_letterClick.triggers.Add( entry );

            //entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            //entry.eventID = EventTriggerType.PointerUp;
            //entry.callback.AddListener( ( data ) => {
            //    ClickOnFieldUp( ( PointerEventData ) data );
            //} );
            //_letterClick.triggers.Add( entry );

            entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener( ( data ) => {
                ClickOnField( ( PointerEventData ) data );
            } );
            _letterClick.triggers.Add( entry );
        }

        internal void Init () {
            RectTransform rectTransform = GetComponent<RectTransform>();
            _lowerEdge = rectTransform.rect.height / 2;
            _plusEdge = new Vector2( rectTransform.rect.width / 2 - 5f, _lowerEdge - 100f );
            _minusEdge = new Vector2( ( rectTransform.rect.width / 2 - 5f ) * ( -1 ), ( _lowerEdge - 50f ) * ( -1 ) );

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            gameLinks = _GC.GameLinks;

            generator.SetGameController( _GC );

            _winUI.OnStartFinLevel += OnStartFinLevel;
            waitAnimationPuzzle.Init();
            puzzleBottomMenu.Init();
            pieceDonePosition.Init();
            pieceControlButton.Init();
        }

        internal PuzzleModel GetPuzzleModel () {
            return puzzleModel;
        }

        internal PuzzleUpperDoneField GetPuzzleUpperDoneField () {
            return upperDoneField;
        }

        internal bool GetIsActivePuzzle () {
            return _isActivePuzzle;
        }

        void ClickOnFieldDown ( PointerEventData data ) {
            _isMouseDown = true;
            trailSystem.StartTrial();
        }

        void ClickOnFieldUp ( PointerEventData data ) {
            _isMouseDown = false;
            StartShowAllPiece();
            StopTrail();
        }
        void ClickOnField ( PointerEventData eventData ) {
            if ( ( lastClick + intervalClick ) > Time.time ) {
                waitAnimationPuzzle.StartRandomAnimation();
            }
            lastClick = Time.time;

            StartShowAllPiece();
            StopTrail();
        }

        void StopPuzzleUpdate () {
            if ( _puzzleUpdate != null ) {
                StopCoroutine( _puzzleUpdate );
            }
        }

        void StartPuzzleUpdate () {
            StopPuzzleUpdate();

            _puzzleUpdate = StartCoroutine( PuzzleUpdateCoroutine() );
        }

        internal void SetActivePuzzle ( bool activePuzzle ) {
            _isActivePuzzle = activePuzzle;
            generator.ActivateField( activePuzzle );

            OnActivePuzzle( activePuzzle );
        }

        IEnumerator PuzzleUpdateCoroutine () {
            for ( ; ; ) {
                PuzzleUpdate();
                yield return null;
            }
        }

        void PuzzleUpdate () {
            if ( _win ) {
                return;
            }

            if ( !_isActivePuzzle ) {
                return;
            }

            Vector2 ancoredPositionOnScreen;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( _letterUIParticles, Input.mousePosition, Camera.main,
                    out ancoredPositionOnScreen );

            if ( IsEndField( ancoredPositionOnScreen ) && _isMouseDown ) {
                StartShowAllPiece();
                StopTrail();
            }
        }

        internal void SubscribePieceField ( PieceFieldController pieceController ) {
            pieceController.OnDownPiece += OnDownPiece;
            pieceController.OnUpPiece += OnUpPiece;
            pieceController.OnUpPieceNoLogic += OnUpPieceNoLogic;
            pieceController.OnAddPiece += OnAddPiece;
        }

        internal void UnSubscribePieceField ( PieceFieldController pieceController ) {
            pieceController.OnDownPiece -= OnDownPiece;
            pieceController.OnUpPiece -= OnUpPiece;
            pieceController.OnUpPieceNoLogic -= OnUpPieceNoLogic;
            pieceController.OnAddPiece -= OnAddPiece;
        }

        internal void SubscribeMainImageDone ( MainImageDone goMainImageUI ) {
            goMainImageUI.OnShowMainImage += OnShowMainImage;
            goMainImageUI.OnCanNotShowMainImage += OnCanNotShowMainImage;
        }

        internal void UnSubscribeMainImageDone ( MainImageDone goMainImageUI ) {
            goMainImageUI.OnShowMainImage -= OnShowMainImage;
            goMainImageUI.OnCanNotShowMainImage -= OnCanNotShowMainImage;
        }

        internal void InitPuzzle2 ( LevelInitingData[] levelsInitingData, int sizeField ) {
            //internal void InitPuzzle2 ( LevelInitingData[] levelsInitingData, LevelTemplateData levelTemplateData ) {
            pieceControlButton.Close();

            InitPuzzleModel( sizeField );

            int[] levelsInitingDataArray = new int[levelsInitingData.Length];
            CustomVector2Int[] sizeMatrixImageArray = new CustomVector2Int[levelsInitingData.Length];
            for ( int i = 0; i < levelsInitingData.Length; i++ ) {
                levelsInitingDataArray[i] = levelsInitingData[i].puzzelTemplate.GetPuzzlePieceByIndex().Count;
                sizeMatrixImageArray[i] = levelsInitingData[i].puzzelTemplate.sizeMatrixImage;
            }
            generator.SetParametersSizeField2( levelsInitingDataArray, sizeField );
            generator.InitPuzzlesFiled2( levelsInitingData, sizeField );

            InitDots( sizeMatrixImageArray );

            _GC.player.SetPuzzleStateData( puzzleModel );
            _GC.SavePlayer();
        }

        internal void RestorePuzzle2 ( int sizeField, Dictionary<string, PuzzleStateData> puzzleStateData, Sprite[] spritesImages ) {
            InitPuzzleModel( sizeField );

            CustomVector2Int[] sizeMatrixImageArray = new CustomVector2Int[puzzleStateData.Count];
            int[] levelsInitingDataArray = new int[puzzleStateData.Count];
            int indexSizeMatrixImageArray = 0;
            foreach ( var item in puzzleStateData ) {
                sizeMatrixImageArray[indexSizeMatrixImageArray] = item.Value.sizeMatrixImage;
                levelsInitingDataArray[indexSizeMatrixImageArray] = item.Value.playerPieces.Count;
                indexSizeMatrixImageArray++;
            }
            generator.SetParametersSizeField2( levelsInitingDataArray, sizeField );
            generator.RestorePuzzlesFiled2( sizeField, puzzleStateData, spritesImages );

            InitDots( sizeMatrixImageArray );

            puzzleModel.downPieces = new List<CustomVector2Int>();
            puzzleModel.upPieces = new List<CustomVector2Int>();
            for ( int i = 0; i < _GC.player.upPieces.Count; i++ ) {
                CustomVector2Int indexPuzzlePiece = _GC.player.upPieces[i];
                puzzleModel.listPieces[indexPuzzlePiece.x][indexPuzzlePiece.y].pieceCntr.HideVisual( i );
                OnAddPiece( indexPuzzlePiece.x, indexPuzzlePiece.y );
            }

            puzzleModel.upPieces = _GC.player.upPieces;
            pieceControlButton.Close();
        }

        void InitPuzzleModel ( int sizeField ) {
            puzzleModel.sizeI = sizeField;
            puzzleModel.sizeJ = sizeField;

            puzzleModel.listPieces = new PieceLevel[puzzleModel.sizeI][];

            for ( int i = 0; i < puzzleModel.sizeI; i++ ) {
                puzzleModel.listPieces[i] = new PieceLevel[puzzleModel.sizeJ];
                for ( int j = 0; j < puzzleModel.sizeJ; j++ ) {
                    PieceLevel lettersLevel = new PieceLevel();
                    lettersLevel.isEmpty = true;
                    puzzleModel.listPieces[i][j] = new PieceLevel();
                }
            }
        }

        void InitDots ( CustomVector2Int[] sizeMatrixImageArray ) {
            int maxCountOfDots = 0;
            for ( int i = 0; i < sizeMatrixImageArray.Length; i++ ) {
                int maxCountOfDotsTemp = sizeMatrixImageArray[i].x * sizeMatrixImageArray[i].y;

                if ( maxCountOfDotsTemp > maxCountOfDots ) {
                    maxCountOfDots = maxCountOfDotsTemp;
                }
            }
            pieceDonePosition.CreateDots( maxCountOfDots );
        }

        private void OnDownPiece ( int indexI, int indexJ ) {
            if ( !_isActivePuzzle ) {
                return;
            }
            //a
            StartHidePiece( indexI, indexJ );
            //b, b должно идти строко после а, иначе буква записывается дважды
            _isMouseDown = true;
        }

        void StartHidePiece ( int indexI, int indexJ ) {

            if ( !IsCanHidePiece( indexI, indexJ ) ) {
                return;
            }

            pieceControlButton.ShowPieceControlButton( puzzleModel.listPieces[indexI][indexJ].pieceCntr );

            CustomVector2Int positionVector2Int = new CustomVector2Int( indexI, indexJ );
            if ( puzzleModel.downPieces.Contains( positionVector2Int ) ) {
                return;
            }

            puzzleModel.listPieces[indexI][indexJ].pieceCntr.StartHide( puzzleModel.downPieces.Count );
            OnStartHidePiece( puzzleModel.listPieces[indexI][indexJ].pieceCntr );
            //puzzleModel.hidePieces.Add( puzzleModel.listPieces[indexI][indexJ] );

            //ShowPiecesAround( indexI, indexJ );
        }

        void RemovePiece ( int indexI, int indexJ ) {
            PieceFieldController pieceControllerTemp = puzzleModel.listPieces[indexI][indexJ].pieceCntr;
            PuzzlePieceField puzzlePieceFieldTemp = pieceControllerTemp.GetPuzzlePieceField();

            for ( int i = 0; i < puzzlePieceFieldTemp.PuzzlePieces.Count; i++ ) {
                CustomVector2Int puzzlePiecesIndex = puzzlePieceFieldTemp.PuzzlePieces[i].GetIndexInMainImageMatrixPrimary();
                PuzzleLevelPieceDoneData puzzleLevelPieceDoneDataTemp =
                    puzzleModel.GetPuzzleLevelPieceDoneDataByIndex( pieceControllerTemp.NumPuzzleTemplate );
                puzzleLevelPieceDoneDataTemp.GetPuzzlePiece()[puzzlePiecesIndex.x][puzzlePiecesIndex.y].PuzzlePieceViewDone.gameObject.SetActive( false );
            }

            puzzleModel.fillMainImageData.RemoveLastFillSingleMainImageData();
        }

        bool IsCanHidePiece ( int indexI, int indexJ ) {

            if ( puzzleModel.downPieces.Count <= 0 ) {
                return true;
            }

            CustomVector2Int lastDownPiece = puzzleModel.downPieces[puzzleModel.downPieces.Count - 1];

            int xDifference = lastDownPiece.x - indexI;
            int yDifference = lastDownPiece.y - indexJ;

            if ( xDifference >= 2 || xDifference <= -2 || yDifference >= 2 || yDifference <= -2 ) {
                return false;
            }

            return true;
        }

        void HidePiecesAround () {
            for ( int i = 0; i < puzzleModel.listPieces.Length; i++ ) {
                for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
                    if ( puzzleModel.listPieces[i][j].isEmpty || puzzleModel.listPieces[i][j].done ||
                            !puzzleModel.listPieces[i][j].pieceCntr.IsShow ) {
                        continue;
                    }

                    puzzleModel.listPieces[i][j].pieceCntr.ActivateLetterColor();
                    puzzleModel.listPieces[i][j].pieceCntr.EndChooseScale();
                }
            }
        }

        //void ShowPiecesAround ( int indexI, int indexJ ) {
        //    List<PieceLevel> scaleLettesTemp = new List<PieceLevel>();

        //    for ( int i = 0; i < puzzleModel.listPieces.Length; i++ ) {
        //        for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
        //            if ( puzzleModel.listPieces[i][j].isEmpty || puzzleModel.listPieces[i][j].done ||
        //                    !puzzleModel.listPieces[i][j].pieceCntr.IsShow ) {
        //                continue;
        //            }

        //            if ( Math.Abs( indexI - i ) <= 1 && Math.Abs( indexJ - j ) <= 1 ) {
        //                puzzleModel.listPieces[i][j].pieceCntr.ActivateLetterColor();
        //            }
        //            else {
        //                puzzleModel.listPieces[i][j].pieceCntr.DiActivateLetterColor();
        //            }
        //        }
        //    }
        //}

        void OnUpPieceNoLogic ( int indexI, int indexJ ) {
            trailSystem.EndTrial();

            _isMouseDown = false;

            //Vector2Int positionVector2Int = new Vector2Int( indexI, indexJ );
            //if ( !puzzleModel.upPieces.Contains( positionVector2Int ) ) {
            //    Debug.Log( "aa  up 1 " + positionVector2Int );
            //    //puzzleModel.upPieces.Add( positionVector2Int );
            //}
        }

        private void OnUpPiece ( int indexI, int indexJ ) {
            if ( !_isActivePuzzle ) {
                return;
            }
            //if ( !_isMouseDown ) {
            //    return;
            //}

            trailSystem.EndTrial();
            _isMouseDown = false;

            AddToUpPieces( indexI, indexJ );

            CheckWinLevel();
            //StartShowPiece();
        }

        void CheckWinLevel () {
            int mainImageDone = IsFindMainImage();
            if ( mainImageDone != -1 ) {
                puzzleModel.mainImagePuzzleDone[mainImageDone] = true;
                puzzleModel.mainImageDone[mainImageDone].SetDone( true );

                for ( int i = 0; i < puzzleModel.downPieces.Count; i++ ) {
                    PieceLevel pieceLevelTemp = puzzleModel.listPieces[puzzleModel.downPieces[i].x][puzzleModel.downPieces[i].y];
                    if ( pieceLevelTemp.isEmpty ) {
                        continue;
                    }
                    pieceLevelTemp.done = true;
                    pieceLevelTemp.pieceCntr.gameObject.SetActive( false );
                }

                StartShowAllPiece();
                StopTrail();
            }

            bool isDoneLevel = true;
            for ( int i = 0; i < puzzleModel.mainImagePuzzleDone.Length; i++ ) {
                if ( !puzzleModel.mainImagePuzzleDone[i] ) {
                    isDoneLevel = false;
                    break;
                }
            }

            if ( isDoneLevel ) {
                OnWinLevel();

                _win = true;
                StopPuzzleUpdate();

                StartCoroutine( WinWind( 0.3f ) );
            }
        }

        bool AddToUpPieces ( int indexI, int indexJ ) {
            CustomVector2Int positionVector2Int = new CustomVector2Int( indexI, indexJ );

            if ( !puzzleModel.upPieces.Contains( positionVector2Int ) ) {
                puzzleModel.upPieces.Add( positionVector2Int );
                _GC.player.SetUpPieces( puzzleModel.upPieces );
                _GC.SavePlayer();
                return true;
            }

            if ( puzzleModel.upPieces[puzzleModel.upPieces.Count - 1] == positionVector2Int ) {
                StartShowPiece( indexI, indexJ );
            }

            return false;
        }

        int IsFindMainImage () {
            if ( puzzleModel.downPieces.Count <= 0 || puzzleModel.fillMainImageData.GetFillSingleMainImageDataCount() <= 0 ) {
                return -1;
            }

            CustomVector2Int firstDownPiece = puzzleModel.downPieces[0];
            int numPuzzleTemplate = puzzleModel.fillMainImageData.GetNumTemplate();

            PuzzleLevelPieceDoneData puzzleLevelPieceDoneData = puzzleModel.GetPuzzleLevelPieceDoneDataByIndex( numPuzzleTemplate );

            Dictionary<CustomVector2Int, PuzzlePieceConnect> puzzlePiecesConnect = new Dictionary<CustomVector2Int, PuzzlePieceConnect>();
            CustomVector2Int sizeMatrixImage = puzzleLevelPieceDoneData.GetSizePuzzleIndex();
            List<CustomVector2Int> puzzlePiecesConnections = new List<CustomVector2Int>();
            int totalCountOfPieces = 0;

            for ( int i = 0; i < puzzleModel.fillMainImageData.GetFillSingleMainImageData().Count; i++ ) {
                for ( int j = 0; j < puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetIndexInMainImageMatrix().Count; j++ ) {
                    int numTemplateTemp = puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetNumTemplate();
                    if ( numTemplateTemp != numPuzzleTemplate ) {
                        return -1;
                    }

                    CustomVector2Int indexInMatrixTemp = puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetIndexInMainImageMatrix()[j];

                    if ( indexInMatrixTemp.x < 0 || indexInMatrixTemp.y < 0 ||
                            indexInMatrixTemp.x >= sizeMatrixImage.x ||
                            indexInMatrixTemp.y >= sizeMatrixImage.y ) {
                        return -1;
                    }

                    for ( int k = i + 1; k < puzzleModel.fillMainImageData.GetFillSingleMainImageData().Count; k++ ) {
                        if ( puzzleModel.fillMainImageData.GetFillSingleMainImageData()[k].GetIndexInMainImageMatrix().Contains( indexInMatrixTemp ) ) {
                            return -1;
                        }
                    }

                    CustomVector2Int pieceIndexTemp = puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetPieceIndex()[j];
                    PuzzlePiece puzzlePieceTemp = puzzleLevelPieceDoneData.GetPuzzlePiece()[pieceIndexTemp.x][pieceIndexTemp.y];
                    PuzzlePieceConnect puzzlePieceConnect = puzzlePieceTemp.GetPuzzlePieceConnect();

                    puzzlePiecesConnect.Add( indexInMatrixTemp, puzzlePieceConnect );
                }
                totalCountOfPieces += puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetIndexInMainImageMatrix().Count;
            }

            if ( totalCountOfPieces != puzzleLevelPieceDoneData.GetPuzzlePieceCount() ||
                    !GameHelper.IsCorrectImageCanvas( sizeMatrixImage.x, sizeMatrixImage.y, puzzlePiecesConnect ) ) {
                return -1;
            }

            return numPuzzleTemplate;
        }

        void StartShowPiece ( int indexI, int indexJ ) {
            CustomVector2Int positionVector2Int = new CustomVector2Int( indexI, indexJ );
            if ( puzzleModel.downPieces.Contains( positionVector2Int ) ) {
                puzzleModel.downPieces.Remove( positionVector2Int );
            }
            if ( puzzleModel.upPieces.Contains( positionVector2Int ) ) {
                puzzleModel.upPieces.Remove( positionVector2Int );
            }

            PieceFieldController pieceControllerTemp = puzzleModel.listPieces[positionVector2Int.x][positionVector2Int.y].pieceCntr;
            pieceControllerTemp.StartShow( 0 );

            if ( puzzleModel.downPieces.Count <= 0 ) {
                //CustomVector2Int downOrEnterPiece = puzzleModel.downPieces[puzzleModel.downPieces.Count - 1];
                //ShowPiecesAround( downOrEnterPiece.x, downOrEnterPiece.y );

                HidePiecesAround();
                pieceDonePosition.HideDots();

                for ( int i = 0; i < puzzleModel.mainImageDone.Length; i++ ) {
                    puzzleModel.mainImageDone[i].ShowImage( 0 );
                }
            }

            RemovePiece( indexI, indexJ );
        }

        void StopTrail () {
            trailSystem.EndTrial();
        }

        void StartShowAllPiece () {
            bool isPlayAudio = false;

            _isMouseDown = false;

            int countHideLettes = puzzleModel.downPieces.Count - 1;
            if ( countHideLettes == 0 ) {
                countHideLettes = 1;
            }

            for ( int i = puzzleModel.downPieces.Count - 1; i >= 0; i-- ) {
                PieceLevel pieceLevelTemp = puzzleModel.listPieces[puzzleModel.downPieces[i].x][puzzleModel.downPieces[i].y];

                if ( pieceLevelTemp.isEmpty || pieceLevelTemp.done ) {
                    continue;
                }

                float timeDelay = 1 - ( ( float ) i / countHideLettes );

                if ( pieceLevelTemp.pieceCntr != null ) {
                    pieceLevelTemp.pieceCntr.StartShow( timeDelay );
                }
            }
            puzzleModel.downPieces = new List<CustomVector2Int>();
            puzzleModel.upPieces = new List<CustomVector2Int>();

            _GC.player.SetUpPieces( puzzleModel.upPieces );
            _GC.SavePlayer();

            for ( int i = 0; i < puzzleModel.fillMainImageData.GetFillSingleMainImageData().Count; i++ ) {
                int numTemplate = puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetNumTemplate();
                PuzzleLevelPieceDoneData puzzleLevelPieceDoneDataTemp = puzzleModel.GetPuzzleLevelPieceDoneDataByIndex( numTemplate );
                for ( int j = 0; j < puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetPieceIndex().Count; j++ ) {
                    CustomVector2Int puzzlePiecesIndex = puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetPieceIndex()[j];
                    puzzleLevelPieceDoneDataTemp.GetPuzzlePiece()[puzzlePiecesIndex.x][puzzlePiecesIndex.y].PuzzlePieceViewDone.gameObject.SetActive( false );
                }
            }
            puzzleModel.fillMainImageData.ClearFillSingleMainImageData();

            HidePiecesAround();

            if ( isPlayAudio ) {
                _myAudioSource.clip = gameLinks.AudioDataSet.GetRandomWrongWordFieldAudioClip();
                _myAudioSource.Play();
            }

            pieceDonePosition.HideDots();
            OnStartShowPiece();

            for ( int i = 0; i < puzzleModel.mainImageDone.Length; i++ ) {
                puzzleModel.mainImageDone[i].ShowImage( 0 );
            }
        }

        void OnAddPiece ( int indexI, int indexJ ) {
            CustomVector2Int tempIndexMatrinx = new CustomVector2Int( indexI, indexJ );

            if ( puzzleModel.downPieces.Contains( tempIndexMatrinx ) ) {
                return;
            }
            PieceFieldController pieceControllerTemp = puzzleModel.listPieces[indexI][indexJ].pieceCntr;
            PuzzlePieceField puzzlePieceFieldTemp = pieceControllerTemp.GetPuzzlePieceField();

            int numPuzzleTemplate = pieceControllerTemp.NumPuzzleTemplate;

            PuzzleLevelPieceDoneData puzzleLevelPieceDoneData = puzzleModel.GetPuzzleLevelPieceDoneDataByIndex( numPuzzleTemplate );

            if ( puzzleModel.fillMainImageData.GetFillSingleMainImageDataCount() <= 0 ) {
                puzzleModel.fillMainImageData.SetNumTemplate( numPuzzleTemplate );

                pieceDonePosition.ShowDots( puzzleLevelPieceDoneData.GetSizePuzzleIndex(), puzzleLevelPieceDoneData.GetStartPosition(),
                                            puzzleLevelPieceDoneData.GetSizePiece() );

                for ( int i = 0; i < puzzleModel.mainImageDone.Length; i++ ) {
                    puzzleModel.mainImageDone[i].HideImage( 0 );
                }
            }

            CustomVector2Int wayofFill = puzzleLevelPieceDoneData.GetWayOfFillMainImage( puzzleModel.downPieces.Count );

            int addDownOrEnterPiecesCount = 1;
            for ( ;; ) {
                bool isBreak = true;
                for ( int i = 0; i < puzzleModel.fillMainImageData.GetFillSingleMainImageDataCount(); i++ ) {
                    if ( puzzleModel.fillMainImageData.GetFillSingleMainImageData()[i].GetIndexInMainImageMatrix().Contains( wayofFill ) ) {
                        isBreak = false;
                        break;
                    }
                }

                if ( isBreak ) {
                    break;
                }
                else {
                    wayofFill = puzzleLevelPieceDoneData.GetWayOfFillMainImage( puzzleModel.downPieces.Count + addDownOrEnterPiecesCount );
                    addDownOrEnterPiecesCount++;
                }
            }

            List<CustomVector2Int> indexesInMainImageMatrix = new List<CustomVector2Int>();
            List<CustomVector2Int> piecesIndex = new List<CustomVector2Int>();

            CustomVector2Int puzzlePieceIndex = puzzlePieceFieldTemp.PuzzlePieces[0].GetIndexInMainImageMatrixPrimary();
            CustomVector2Int wayofFillTemp = wayofFill;
            Vector2 piecePositionStart = puzzleLevelPieceDoneData.GetPiecePositionByIndex( wayofFillTemp.x, wayofFillTemp.y );
            PuzzleLevelPieceDoneData puzzleLevelPieceDoneDataTemp = puzzleModel.GetPuzzleLevelPieceDoneDataByIndex( numPuzzleTemplate );
            PuzzlePiece puzzlePieceTemp = puzzleLevelPieceDoneDataTemp.GetPuzzlePiece()[puzzlePieceIndex.x][puzzlePieceIndex.y];
            puzzlePieceTemp.PuzzlePieceViewDone.gameObject.SetActive( true );
            puzzlePieceTemp.SetPieceDonePosition( piecePositionStart );
            indexesInMainImageMatrix.Add( wayofFillTemp );
            piecesIndex.Add( puzzlePieceIndex );

            Vector2 pieceSize = puzzleLevelPieceDoneDataTemp.GetSizePiece();
            if ( puzzlePieceFieldTemp.IsOnTheEdge() ) {
                pieceSize = new Vector2( pieceSize.y, pieceSize.x );
            }

            for ( int i = 1; i < puzzlePieceFieldTemp.GetPuzzlePiecesOffset().Count; i++ ) {
                Vector2 piecePositionTemp = Vector2.zero;

                puzzlePieceIndex = puzzlePieceFieldTemp.PuzzlePieces[i].GetIndexInMainImageMatrixPrimary();

                wayofFillTemp = wayofFill + puzzlePieceFieldTemp.GetPuzzlePiecesOffset()[i];

                piecePositionTemp = piecePositionStart + ( pieceSize * puzzlePieceFieldTemp.GetPuzzlePiecesOffset()[i] *  new Vector2( 1, -1 ) );

                puzzlePieceTemp = puzzleLevelPieceDoneDataTemp.GetPuzzlePiece()[puzzlePieceIndex.x][puzzlePieceIndex.y];
                puzzlePieceTemp.PuzzlePieceViewDone.gameObject.SetActive( true );
                puzzlePieceTemp.SetPieceDonePosition( piecePositionTemp );

                indexesInMainImageMatrix.Add( wayofFillTemp );
                piecesIndex.Add( puzzlePieceIndex );
            }

            puzzleModel.fillMainImageData.AddFillSingleMainImageData( new FillSingleMainImageData( numPuzzleTemplate, indexesInMainImageMatrix,
                    piecesIndex ) );

            puzzleModel.downPieces.Add( tempIndexMatrinx );
            trailSystem.StartTrial();
        }

        void OnShowMainImage ( Sprite mainImageDoneSprite ) {
            imageViewUI.Show( mainImageDoneSprite );
        }

        void OnCanNotShowMainImage () {
            popupTextUI.ShowPopUp( "сначала решите пазл" );
        }

        bool IsEndField ( Vector2 newPosition ) {

            return newPosition.x > _plusEdge.x || newPosition.x < _minusEdge.x ||
                   newPosition.y > _plusEdge.y || newPosition.y < _minusEdge.y;
        }

        void RecognizeImageCanvas () {

        }

        bool IsWinTasks () {
            return true;
        }

        IEnumerator WinWind ( float timeWait ) {
            _lockUI.Lock( true );

            yield return new WaitForSeconds( timeWait );

            _lockUI.Lock( false );

            //for ( int i = 0; i < puzzleModel.quest.words[LocalizationManager.GetLanguageString()].Count; i++ ) {

            //    for ( int j = 0; j < puzzleModel.quest.words[LocalizationManager.GetLanguageString()][i].Length; j++ ) {

            //        puzzleModel.quest.indexLetters[i].indexLetters[j].isHint = false;
            //    }
            //}

            //финально завершение пазла
            //_levelUIGroup.GetComponent<Animator>().SetTrigger( "startHideEnd" );
            //_canvasGroup.DOFade( 0f, 0.6f );

            GameObject goSound = GameObject.Find( "sound_object_2" );

            if ( goSound != null ) {
                if ( goSound.GetComponent<AudioSource>() != null ) {

                    goSound.GetComponent<AudioSource>().clip = Resources.Load<AudioClip>( "Common_sounds/startGame" );
                    goSound.GetComponent<AudioSource>().Play();
                }
            }

            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage( language );

            Vector2Int catigories = new Vector2Int( _GC.numCatigory.GetValue(), _GC.numCatigory.GetValue() );
            Vector2Int levels = new Vector2Int( _GC.numLevel.GetValue(), _GC.numLevel.GetValue() );

            // ежедневный квест
            if ( puzzleModel.puzzelType.Equals( PuzzleType.Daily ) ) {

                //_GC.player.AddProgressByLanguageDaylyPuzzleQuest( language );
                //_GC.player.ClearProgressByLanguageFieldStateDailyPuzzle( language );

                _GC.player.AddHints( 1 );
                _GC.SavePlayer();
                //Analytics.CustomEvent( "daily_level_end_" + puzzleModel.quest.nameLevel.ToString() );

                hintAddedUI.ShowHintDailyAdd( 1 );
            }
            // играешь категорией ниже
            //else if ( progressByLanguage.categiries > _GC.numCatigory ) {
            //    levels = new Vector2Int( 0, 0 );

            //    ShowWinWindow( PuzzleType.Common, catigories, levels, CloseWinUIType.GoToMenu );

            //    //if ( puzzleModel.numHintQuest != -1 ) {
            //    //    _GC.DelHintCountShow( puzzleModel.numHintQuest );
            //    //}
            //}
            // игрешь уровнем ниже чем прошёл максимум
            //else if ( progressByLanguage.categiries <= _GC.numCatigory && progressByLanguage.quest > _GC.numLevel ) {
            //    ShowWinWindow( PuzzleType.Common, catigories, levels, CloseWinUIType.GoToMenu );

            //    //if ( puzzleModel.numHintQuest != -1 ) {
            //    //    _GC.DelHintCountShow( puzzleModel.numHintQuest );
            //    //}
            //}
            else {
                //if ( puzzleModel.numHintQuest != -1 ) {
                //    _GC.DelHintCountShow( puzzleModel.numHintQuest );
                //}

                //_GC.player.ClearProgressByLanguageFieldState( language );

                //Analytics.CustomEvent( "main_level_end " + puzzleModel.quest.nameLevel.ToString() );

                if ( _GC.IsEndCategiries() ) {
                    //Analytics.CustomEvent( "category_end " + progressByLanguage.categiries );

                    //int newQuest = progressByLanguage.categiries.GetValue() + 1;
                    _GC.numCatigory += 1;
                    //_GC.player.SetProgressByLanguageCategiries( language, _GC.numCatigory.GetValue() );
                    //_GC.player.SetProgressByLanguageQuest( language, 0 );
                    _GC.numLevel = new SecureInt( 0 );
                    catigories = new Vector2Int( catigories.x, _GC.numCatigory.GetValue() );
                    levels = new Vector2Int( levels.x, 0 );

                    //if ( _GC.IsEndGame( newQuest ) ) {
                    //    ShowWinWindow( PuzzleType.Common, catigories, levels, CloseWinUIType.GoToMenu );
                    //}
                    //else {
                    //}

                    _GC.numCatigoriesAnimation = _GC.numCatigory.GetValue();
                    ShowWinWindow( PuzzleType.Common, catigories, levels, CloseWinUIType.GoToMenu );

                    //ShowWinWindow( PuzzleType.Common, catigories, levels, CloseWinUIType.NextCategory );
                }
                else {

                    int newQuest = _GC.numLevel.GetValue() + 1;
                    _GC.numLevel = new SecureInt( newQuest );
                    //_GC.player.AddProgressByLanguageQuest( language, _GC.numLevel.GetValue() );

                    levels = new Vector2Int( levels.x, newQuest );

                    ShowWinWindow( PuzzleType.Common, catigories, levels,  CloseWinUIType.NextLevel );
                }

                _GC.SavePlayer();
            }
        }

        void ShowWinWindow ( PuzzleType puzzelType, Vector2Int categories, Vector2Int levels, CloseWinUIType levelFinishType ) {

            _winUI.ShowWin( new WinUIParams() {
                puzzelType = puzzelType,
                categories = categories,
                levels = levels,
                levelFinishType = levelFinishType
            } );
        }

        void EndLevel() {

            //_canvasGroup.DOFade( 1f, 0.6f ).OnComplete( () => {
            //    RestartEnd();
            //} );
        }

        void InitTasks () {
            //puzzleModel.stringTask = "";
            //_inputText.text = "";
            //SetInputTextSizeBack();

            //puzzleModel.doneTask = new List<TaskDoneLevel>();

            //if ( puzzleModel.lettersTask == null ) {
            //    return;
            //}

            //for ( int i = 0; i < puzzleModel.lettersTask.Count; i++ ) {
            //    TaskDoneLevel taskDoneLevel = new TaskDoneLevel();
            //    taskDoneLevel.doneTask = false;
            //    taskDoneLevel.doneLetterIndex = new List<Vector2Int>();
            //    puzzleModel.doneTask.Add( taskDoneLevel );
            //}

            //int notDoneTask = puzzleModel.GetNotDoneTask();
        }

        public void Hint () {
            //OnUseHint( isUse, letterDone );
        }

        void RestartEnd () {
            StartPuzzleUpdate();

            _lockUI.Lock( false );

            OnUseRestartEnd();
        }

        void ClearField () {
            HidePiecesAround();

            generator.ListPiecesBackToPool();
            generator.PieceDoneBackToPool();

            puzzleModel.fillMainImageData.SetFillSingleMainImageData( new List<FillSingleMainImageData>() );
            puzzleModel.puzzleLevelPieceDoneData = null;

            puzzleModel.listPieces = null;
            puzzleModel.downPieces = new List<CustomVector2Int>();
            puzzleModel.upPieces = new List<CustomVector2Int>();

            _GC.player.SetUpPieces( puzzleModel.upPieces );
            _GC.SavePlayer();

            StopPuzzleUpdate();

            if ( _winUI.IsShown ) {
                _winUI.HideWin();
            }
        }

        internal void ClickButtonBack () {
            ClearField();

            OnClickButtonBack();
        }

        internal void OnStartFinLevel ( PuzzleType puzzelType, CloseWinUIType levelFinishType ) {
            if ( puzzleModel.GetPuzzelType().Equals( PuzzleType.Common ) ) {
                _GC.AddCountWinsInSession();
            }

            if ( !levelFinishType.Equals( CloseWinUIType.GoToMenu ) || levelFinishType.Equals( CloseWinUIType.None ) ) {
                return;
            }

            if ( _GC.numLevel != -1 ) {
                return;
            }
            ClearField();
        }
    }
}

