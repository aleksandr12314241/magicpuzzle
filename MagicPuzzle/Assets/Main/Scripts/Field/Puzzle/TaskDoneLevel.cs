﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TaskDoneLevel {
    public bool doneTask;
    public List<Vector2Int> doneLetterIndex;
}
