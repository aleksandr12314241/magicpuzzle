﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzelTemplatesNameByImageName {
    public Dictionary<string, PuzzelTemplatesName> puzzelTemplatesNameByImageName;

    public PuzzelTemplatesNameByImageName () {
        puzzelTemplatesNameByImageName = new Dictionary<string, PuzzelTemplatesName>();
    }

    //GET
    public List<string> GetPuzzelTemplatesNameByImageName ( string imageName ) {
        if ( puzzelTemplatesNameByImageName.ContainsKey( imageName ) ) {
            return puzzelTemplatesNameByImageName[imageName].GetPuzzleTemplateName();
        }
        return new List<string>();
    }

    //ADD
    public void AddPuzzelTemplateNameByImageName ( string imageName, string templateName ) {
        if ( !puzzelTemplatesNameByImageName.ContainsKey( imageName ) ) {
            puzzelTemplatesNameByImageName.Add( imageName, new PuzzelTemplatesName() );
        }

        puzzelTemplatesNameByImageName[imageName].AddTemplateName( templateName );
    }
}
