﻿using UnityEngine;
using Common;

public class PuzzlePiece {
    PuzzlePieceView puzzlePieceViewDone;
    PuzzlePieceView puzzlePieceViewField;

    GameLinks gameLinks;
    float offsetXY = 0.1f;

    int puzzlePieceId;
    int[] numPuzzlePieces;
    int currentNumPuzzlePieces;
    int typePuzzlePiece;
    CustomVector2Int indexInMainImageMatrixPrimary;
    CustomVector2Int indexInMainImageMatrixTemp;
    Vector2 sizePiece;
    Vector2 offsetSizePiece;
    Vector2 startPosition;

    PuzzlePieceConnect puzzlePieceConnect;

    public PuzzlePiece ( GameLinks gameLinks ) {
        this.gameLinks = gameLinks;
        offsetXY = 0.1f;
        currentNumPuzzlePieces = 0;
    }

    public void SetPuzzlePieces ( PuzzlePieceView puzzlePieceViewDone, PuzzlePieceView puzzlePieceViewField ) {
        this.puzzlePieceViewDone = puzzlePieceViewDone;
        this.puzzlePieceViewField = puzzlePieceViewField;
    }

    internal void SetPuzzlePieceParametrs ( Sprite mainImage, int numPuzzlePiece, int typePuzzlePiece, CustomVector2Int puzzlePieceIndex,
                                            Vector2 sizePiece,
                                            Vector2 offsetSizePiece, Vector2 startPosition, int puzzlePieceId ) {
        puzzlePieceViewDone.SetMainImage( mainImage );
        puzzlePieceViewField.SetMainImage( mainImage );

        this.offsetSizePiece = offsetSizePiece;
        this.startPosition = startPosition;
        indexInMainImageMatrixTemp = puzzlePieceIndex;
        indexInMainImageMatrixPrimary = puzzlePieceIndex;

        this.puzzlePieceId = puzzlePieceId;

        this.sizePiece = sizePiece;

        numPuzzlePieces = new int[4];
        numPuzzlePieces[0] = numPuzzlePiece;

        this.typePuzzlePiece = typePuzzlePiece;

        currentNumPuzzlePieces = 0;

        ChangeNumPiece( currentNumPuzzlePieces );
    }

    //internal void CopyPuzzlePiece ( PuzzlePiece puzzlePiece ) {
    //    pieceImage.sprite = puzzlePiece.pieceImage.sprite;
    //    pieceImage.SetNativeSize();

    //    numPuzzlePieces = new int[4];
    //    for ( int i = 0; i < puzzlePiece.numPuzzlePieces.Length; i++ ) {
    //        numPuzzlePieces[i] = puzzlePiece.numPuzzlePieces[i];
    //    }

    //    this.gameLinks = puzzlePiece.gameLinks;
    //    this.offsetSizePiece = puzzlePiece.offsetSizePiece;
    //    this.startPosition = puzzlePiece.startPosition;
    //    this.sizePiece = puzzlePiece.sizePiece;

    //    SetMainImage( puzzlePiece.mainImage.sprite );

    //    pieceRectTransform.pivot = puzzlePiece.pieceRectTransform.pivot;
    //    pieceRectTransform.sizeDelta = puzzlePiece.pieceRectTransform.sizeDelta;
    //    mainImageRectTransform.anchoredPosition = puzzlePiece.mainImageRectTransform.anchoredPosition;

    //    puzzlePieceConnect = puzzlePiece.GetPuzzlePieceConnect();
    //    puzzlePieceIndex = puzzlePiece.puzzlePieceIndex;
    //}

    //GET
    internal PuzzlePieceConnect GetPuzzlePieceConnect () {
        return puzzlePieceConnect;
    }
    internal CustomVector2Int GetIndexInMainImageMatrixPrimary () {
        return indexInMainImageMatrixPrimary;
    }
    internal CustomVector2Int GetIndexInMainImageMatrixTemp () {
        return indexInMainImageMatrixTemp;
    }
    internal int[] GetNumPuzzlePiece () {
        return numPuzzlePieces;
    }
    internal int GetNumPuzzlePieceByIndex ( int index ) {
        return numPuzzlePieces[index];
    }
    internal Vector2 GetSizePiece () {
        return sizePiece;
    }
    internal PuzzlePieceView PuzzlePieceViewDone {
        get {
            return puzzlePieceViewDone;
        }
    }
    internal PuzzlePieceView PuzzlePieceViewField {
        get {
            return puzzlePieceViewField;
        }
    }

    //SET
    internal void SetPieceDonePosition ( Vector2 piecePosition ) {
        puzzlePieceViewDone.SetPiecePosition( piecePosition );
    }
    internal void SetPieceFieldPosition ( Vector2 piecePosition ) {
        puzzlePieceViewField.SetPiecePosition( piecePosition );
    }
    internal void SetNumPuzzlePiece ( int[] numPuzzlePieces ) {
        this.numPuzzlePieces = numPuzzlePieces;
    }
    internal void SetNumTypeByIndex ( int index, int numTypeAlternative ) {
        numPuzzlePieces[index] = numTypeAlternative;
    }
    void SetPieceImageByNumPiece ( int indexNumPuzzlePiece ) {
        PuzzlePiecesData puzzlePiecesData = gameLinks.PuzzlePiecesDataSet.GetPuzzlePiecesDataByIndex( numPuzzlePieces[indexNumPuzzlePiece] );

        puzzlePieceViewDone.SetPieceImage( puzzlePiecesData.PiceSprites[typePuzzlePiece] );
        puzzlePieceViewField.SetPieceImage( puzzlePiecesData.PiceSprites[typePuzzlePiece] );

        UpdatePieceImageSize();
        UpdateMainImagePositionInPiece( puzzlePiecesData );
        UpdatePieceImagePivot( puzzlePiecesData );
        UpdatePuzzlePieceConection( puzzlePiecesData );
    }
    internal void SetIndexInMainImageMatrixTemp ( CustomVector2Int newPuzzlePieceIndex ) {
        indexInMainImageMatrixTemp = newPuzzlePieceIndex;
    }
    //internal void SetIndexInMainImageMatrixTemp ( Vector2Int newPuzzlePieceIndex ) {
    //    indexInMainImageMatrixTemp = newPuzzlePieceIndex;
    //}

    //UPDATE
    void UpdatePieceImagePivot ( PuzzlePiecesData puzzlePiecesData ) {

        float offsetX = ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Right ) * -1f * offsetXY ) +
                        ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Left ) * offsetXY );
        float offsetY = ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Up ) * -1f * offsetXY ) +
                        ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Down ) * offsetXY );

        Vector2 piecePivot = new Vector2( 0.5f, 0.5f ) + new Vector2( offsetX, offsetY );
        puzzlePieceViewDone.SetPiecePivot( piecePivot );
        puzzlePieceViewField.SetPiecePivot( piecePivot );
    }

    void UpdatePieceImageSize () {
        Vector2 diff = new Vector2( sizePiece.x / 300f, sizePiece.y / 300f );

        Vector2 sizeDelta = puzzlePieceViewField.GetPieceSize() * diff;

        puzzlePieceViewDone.SetPieceSize( sizeDelta );
        puzzlePieceViewField.SetPieceSize( sizeDelta );
    }

    void UpdateMainImagePositionInPiece ( PuzzlePiecesData puzzlePiecesData ) {
        float offsetXSizePiece = ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Right ) * -1f * offsetSizePiece.x ) +
                                 ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Left ) * offsetSizePiece.x );
        float offsetYSizePiece = ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Up ) * -1f * offsetSizePiece.y ) +
                                 ( GameHelper.GetOffsetPivot( puzzlePiecesData.PuzzlePieceConnect.Down ) * offsetSizePiece.y );

        Vector2 piecePosition = new Vector2( startPosition.x + ( indexInMainImageMatrixPrimary.x * sizePiece.x ),
                                             startPosition.y - ( indexInMainImageMatrixPrimary.y * sizePiece.y ) );
        Vector2 imagePosition = Vector2.zero - piecePosition + new Vector2( offsetXSizePiece, offsetYSizePiece );

        puzzlePieceViewDone.SetMainImagePosition( imagePosition );
        puzzlePieceViewField.SetMainImagePosition( imagePosition );
    }

    void UpdatePuzzlePieceConection ( PuzzlePiecesData puzzlePiecesData ) {
        puzzlePieceConnect = puzzlePiecesData.PuzzlePieceConnect;
    }

    //OTHER
    internal void RotatePuzzlePiece ( int angle ) {
        RotatePuzzlePieceConnect();

        puzzlePieceViewDone.AddPieceAngle( angle );
    }

    void RotatePuzzlePieceConnect () {
        PuzzlePieceConnectType up = puzzlePieceConnect.Right;
        PuzzlePieceConnectType right = puzzlePieceConnect.Down;
        PuzzlePieceConnectType down = puzzlePieceConnect.Left;
        PuzzlePieceConnectType left = puzzlePieceConnect.Up;

        puzzlePieceConnect = new PuzzlePieceConnect( up, right, down, left );
    }

    internal void RotateToAngle ( int angle ) {
        int angleTemp = 0;

        for ( ;; ) {
            if ( angleTemp == angle ) {
                break;
            }

            angleTemp += 90;
            RotatePuzzlePieceConnect();
        }
    }

    internal void AddCurrentNumPuzzlePieces () {
        currentNumPuzzlePieces++;
        if ( currentNumPuzzlePieces >= numPuzzlePieces.Length ) {
            currentNumPuzzlePieces = 0;
        }
        ChangeNumPiece( currentNumPuzzlePieces );
    }

    void ChangeNumPiece ( int indexNumPuzzlePiece ) {
        SetPieceImageByNumPiece( indexNumPuzzlePiece );
    }
}