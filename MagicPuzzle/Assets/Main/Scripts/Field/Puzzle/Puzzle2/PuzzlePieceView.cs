﻿using UnityEngine;
using UnityEngine.UI;

public class PuzzlePieceView: MonoBehaviour {
    [SerializeField]
    RectTransform pieceRectTransform;
    [SerializeField]
    RectTransform mainImageRectTransform;
    [SerializeField]
    Image pieceImage;
    [SerializeField]
    Image mainImage;

    //GET PIECE
    internal Vector2 GetPieceSize () {
        return pieceRectTransform.rect.size;
    }
    internal Vector2 GetPieceAngle () {
        return pieceRectTransform.localEulerAngles;
    }
    internal string GetMainImageName () {
        return mainImage.sprite.name;
    }

    //SET PIECE
    internal void SetPieceImage ( Sprite pieceSprite ) {
        pieceImage.sprite = pieceSprite;
        pieceImage.SetNativeSize();
    }
    internal void SetPiecePivot ( Vector2 pivot ) {
        pieceRectTransform.pivot = pivot;
    }
    internal void SetPieceSize ( Vector2 pieceSizeDelta ) {
        pieceRectTransform.sizeDelta = pieceSizeDelta;
    }
    internal void SetPiecePosition ( Vector2 piecePosition ) {
        pieceRectTransform.anchoredPosition = piecePosition;
    }
    internal void AddPieceAngle ( int angle ) {
        pieceRectTransform.localEulerAngles = new Vector3( pieceRectTransform.localEulerAngles.x,
                pieceRectTransform.localEulerAngles.y, angle );
    }

    //MAIN IMAGE
    internal void SetMainImage ( Sprite mainImageSprite ) {
        mainImage.sprite = mainImageSprite;
        mainImage.SetNativeSize();
    }
    internal void SetMainImagePosition ( Vector2 imagePosition ) {
        mainImageRectTransform.anchoredPosition = imagePosition;
    }
}
