﻿using UnityEngine;

public struct LevelInitingData  {
    public Sprite spriteImage {
        get;
    }
    public PuzzleTemplate puzzelTemplate {
        get;
    }

    public LevelInitingData ( Sprite spriteImage, PuzzleTemplate puzzelTemplate ) {
        this.spriteImage = spriteImage;
        this.puzzelTemplate = puzzelTemplate;
    }
}
