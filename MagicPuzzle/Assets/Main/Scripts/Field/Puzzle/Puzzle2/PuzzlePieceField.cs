﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class PuzzlePieceField : MonoBehaviour {
    [SerializeField]
    RectTransform myRectTransform;

    List<PuzzlePiece> puzzlePieces;
    internal List<PuzzlePiece> PuzzlePieces {
        get {
            return puzzlePieces;
        }
    }

    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    Vector2Int maxSizePuzzlePieceElement;
    GameLinks gameLinks;
    PuzzlePiecesDataSet puzzlePiecesDataSet;
    int currentAngle = 0;
    int startAngle;
    int startNumPuzzlePiece;

    internal void Init ( GameLinks gameLinks ) {
        this.gameLinks = gameLinks;
    }

    internal void InitPuzzlePieceField ( List<PuzzlePiece> puzzlePieces ) {
        currentAngle = 0;


        this.puzzlePieces = puzzlePieces;

        Vector2Int sizePuzzlePieceElementStart = new Vector2Int( puzzlePieces[0].GetIndexInMainImageMatrixPrimary().x,
                puzzlePieces[0].GetIndexInMainImageMatrixPrimary().y );
        maxSizePuzzlePieceElement = Vector2Int.one;
        for ( int i = 0; i < puzzlePieces.Count; i++ ) {
            puzzlePieces[i].PuzzlePieceViewField.transform.SetParent( transform, false );

            Vector2Int differenceIndex = new Vector2Int( Mathf.Abs( sizePuzzlePieceElementStart.x -
                    puzzlePieces[i].GetIndexInMainImageMatrixPrimary().x ),
                    Mathf.Abs( sizePuzzlePieceElementStart.y - puzzlePieces[i].GetIndexInMainImageMatrixPrimary().y ) ) + Vector2Int.one;

            if ( differenceIndex.x > maxSizePuzzlePieceElement.x ) {
                maxSizePuzzlePieceElement = new Vector2Int( differenceIndex.x, maxSizePuzzlePieceElement.y );
            }

            if ( differenceIndex.y > maxSizePuzzlePieceElement.y ) {
                maxSizePuzzlePieceElement = new Vector2Int( maxSizePuzzlePieceElement.x, differenceIndex.y );
            }
        }

        Vector2 sizePiece = puzzlePieces[0].GetSizePiece();
        Vector2 sizeImage = new Vector2( sizePiece.x * maxSizePuzzlePieceElement.x, sizePiece.y * maxSizePuzzlePieceElement.y );

        Vector2 startPosition = new Vector2( ( sizeImage.x - sizePiece.x ) / -2, ( sizeImage.y - sizePiece.y ) / 2f );

        List<CustomVector2Int> puzzlePiecesOffset = GetPuzzlePiecesOffset();

        for ( int i = 0; i < puzzlePieces.Count; i++ ) {
            CustomVector2Int puzzlePiecesIndex = puzzlePiecesOffset[i];
            Vector2 piecePosition = new Vector2( startPosition.x + ( puzzlePiecesIndex.x * sizePiece.x ),
                                                 startPosition.y - ( puzzlePiecesIndex.y * sizePiece.y ) );
            puzzlePieces[i].SetPieceFieldPosition( piecePosition );
        }

        puzzlePiecesDataSet = gameLinks.PuzzlePiecesDataSet;

        //AddAlternativeNumType();
    }

    internal void AddAlternativeNumType () {
        List<List<int>> listPuzzlePieceType = new List<List<int>>( puzzlePieces.Count );
        List<string> exitPieces = new List<string>();

        for ( int i = 0; i < puzzlePieces.Count; i++ ) {
            listPuzzlePieceType.Add( new List<int>() );
            PuzzlePieceConnect puzzlePieceConnect = puzzlePieces[i].GetPuzzlePieceConnect();
            exitPieces.Add( GameHelper.GetExitPieceByIndex( puzzlePieceConnect.Up, puzzlePieceConnect.Right, puzzlePieceConnect.Down,
                            puzzlePieceConnect.Left ) );
            int numPuzzlePieceType = puzzlePieces[i].GetNumPuzzlePieceByIndex( 0 );
            listPuzzlePieceType[i].Add( numPuzzlePieceType );
        }

        int numTypeByIndex = 1;
        while ( numTypeByIndex < 4 ) {
            for ( int i = 0; i < puzzlePieces.Count; i++ ) {
                List<int> puzzlePiecesNumByExitPiece = puzzlePiecesDataSet.GetPuzzlePiecesNumByExitPiece( exitPieces[i], listPuzzlePieceType[i] );
                List<int> indexesPieceCanConectd;

                if ( i > 0 ) {
                    int numPuzzlePieceTypePrev = puzzlePieces[i - 1].GetNumPuzzlePieceByIndex( numTypeByIndex );
                    PuzzlePieceConnect puzzlePieceConnectPrev = puzzlePiecesDataSet.puzzlePieces[numPuzzlePieceTypePrev].puzzlePieceConnect;

                    CustomVector2Int diffIndex = puzzlePieces[i].GetIndexInMainImageMatrixPrimary() - puzzlePieces[i - 1].GetIndexInMainImageMatrixPrimary();

                    List<int> removedIndexesPieceConectd = new List<int>();
                    for ( int j = 0; j < puzzlePiecesNumByExitPiece.Count; j++ ) {

                        PuzzlePieceConnectType first = PuzzlePieceConnectType.Flat;
                        PuzzlePieceConnectType second = PuzzlePieceConnectType.Flat;

                        int numPuzzlePiecesTmp = puzzlePiecesNumByExitPiece[j];
                        PuzzlePieceConnect puzzlePieceConnectTmp = puzzlePiecesDataSet.puzzlePieces[numPuzzlePiecesTmp].PuzzlePieceConnect;

                        if ( diffIndex.x == 1 ) {
                            first = puzzlePieceConnectTmp.Left;
                            second = puzzlePieceConnectPrev.Right;
                        }
                        else if ( diffIndex.x == -1 ) {
                            first = puzzlePieceConnectTmp.Right;
                            second = puzzlePieceConnectPrev.Left;
                        }
                        else if ( diffIndex.y == 1 ) {
                            first = puzzlePieceConnectTmp.Up;
                            second = puzzlePieceConnectPrev.Down;
                        }
                        else if ( diffIndex.y == -1 ) {
                            first = puzzlePieceConnectTmp.Down;
                            second = puzzlePieceConnectPrev.Up;
                        }

                        if ( !GameHelper.CanPuzzlePieceConnectConnect( first, second ) ) {
                            removedIndexesPieceConectd.Add( j );
                        }
                    }

                    indexesPieceCanConectd = new List<int>();
                    for ( int k = 0; k < puzzlePiecesNumByExitPiece.Count; k++ ) {
                        if ( !removedIndexesPieceConectd.Contains( k ) ) {
                            indexesPieceCanConectd.Add( puzzlePiecesNumByExitPiece[k] );
                        }
                    }
                }
                else {
                    indexesPieceCanConectd = new List<int>();

                    for ( int k = 0; k < puzzlePiecesNumByExitPiece.Count; k++ ) {
                        indexesPieceCanConectd.Add( puzzlePiecesNumByExitPiece[k] );
                    }
                }

                int randomNumPuzzleieceType = indexesPieceCanConectd[Random.Range( 0, indexesPieceCanConectd.Count )];
                puzzlePieces[i].SetNumTypeByIndex( numTypeByIndex, randomNumPuzzleieceType );

                listPuzzlePieceType[i].Add( randomNumPuzzleieceType );
            }
            numTypeByIndex++;
        }

        //List<int> indexPieces = new List<int>() {
        //    0, 1, 2, 3
        //};

        //int startIndexNumTypePiece = Random.Range( 0, 4 );
    }
    internal void SetRandomStartAngle () {
        startAngle = Random.Range( 0, 4 );
    }
    internal void SetStartAngle ( int startAngle ) {
        this.startAngle = startAngle;
    }

    //GET
    internal Dictionary<CustomVector2Int, PuzzlePieceConnect> GetPuzzlePiecesConnectByOffset () {
        Dictionary<CustomVector2Int, PuzzlePieceConnect> puzzlePiecesConnectByOffset = new Dictionary<CustomVector2Int, PuzzlePieceConnect>();
        CustomVector2Int startIndex = puzzlePieces[0].GetIndexInMainImageMatrixPrimary();
        puzzlePiecesConnectByOffset.Add( new CustomVector2Int( 0, 0 ), puzzlePieces[0].GetPuzzlePieceConnect() );

        for ( int i = 1; i < puzzlePieces.Count; i++ ) {
            CustomVector2Int offsetIndex = puzzlePieces[i].GetIndexInMainImageMatrixPrimary() - startIndex;
            puzzlePiecesConnectByOffset.Add( offsetIndex, puzzlePieces[i].GetPuzzlePieceConnect() );
        }

        return puzzlePiecesConnectByOffset;
    }
    internal int GetPhantomStartAngle () {
        return startAngle * 90;
    }
    internal int GetStartAngle () {
        return startAngle;
    }
    internal int GetCurrentAngle () {
        return currentAngle;
    }
    internal int GetPhantomNumPuzzlePiece () {
        return startNumPuzzlePiece;
    }

    internal bool IsOnTheEdge () {
        return ( currentAngle >= 90 && currentAngle < 180 ) || ( currentAngle >= 270 );
    }

    PuzzlePiece[][] GetArrayPuzzlePiece () {

        CustomVector2Int startMatrix = puzzlePieces[0].GetIndexInMainImageMatrixTemp();
        CustomVector2Int endMatrix = puzzlePieces[0].GetIndexInMainImageMatrixTemp();

        for ( int i = 0; i < puzzlePieces.Count; i++ ) {
            if ( puzzlePieces[i].GetIndexInMainImageMatrixTemp().x < startMatrix.x ) {
                startMatrix = new CustomVector2Int( puzzlePieces[i].GetIndexInMainImageMatrixTemp().x, startMatrix.y );
            }
            else if ( puzzlePieces[i].GetIndexInMainImageMatrixTemp().x > endMatrix.x ) {
                endMatrix = new CustomVector2Int( puzzlePieces[i].GetIndexInMainImageMatrixTemp().x, endMatrix.y );
            }

            if ( puzzlePieces[i].GetIndexInMainImageMatrixTemp().y < startMatrix.y ) {
                startMatrix = new CustomVector2Int( startMatrix.x, puzzlePieces[i].GetIndexInMainImageMatrixTemp().y );
            }
            else if ( puzzlePieces[i].GetIndexInMainImageMatrixTemp().y > endMatrix.y ) {
                endMatrix = new CustomVector2Int( endMatrix.x, puzzlePieces[i].GetIndexInMainImageMatrixTemp().y );
            }

            //Debug.Log( puzzlePieces[i].GetIndexInMainImageMatrixPrimary().x + " , " + puzzlePieces[i].GetIndexInMainImageMatrixPrimary().y );
        }

        CustomVector2Int sizeMatrix = new CustomVector2Int( ( endMatrix.x + 1 ) - startMatrix.x, ( endMatrix.y + 1 ) - startMatrix.y );

        //Debug.Log( "startMatrix    " + startMatrix );

        int indexI = 0;
        CustomVector2Int[][] puzzlePiecesMatrix = new CustomVector2Int[sizeMatrix.x][];
        for ( int i = startMatrix.x; i <= endMatrix.x; i++ ) {
            int indexJ = 0;
            puzzlePiecesMatrix[indexI] = new CustomVector2Int[sizeMatrix.y];
            for ( int j = startMatrix.y; j <= endMatrix.y; j++ ) {
                puzzlePiecesMatrix[indexI][indexJ] = new CustomVector2Int( i, j );
                indexJ++;
            }
            indexI++;
        }

        //for ( int i = 0; i < puzzlePiecesMatrix.Length; i++ ) {
        //    string str = "";
        //    for ( int j = 0; j < puzzlePiecesMatrix[i].Length; j++ ) {
        //        str += puzzlePiecesMatrix[i][j].x.ToString() + "," + puzzlePiecesMatrix[i][j].y.ToString() + "__";
        //    }
        //    Debug.Log( str );
        //}
        //Debug.Log( "----------" );

        CustomVector2Int[][] rot = Rotate( puzzlePiecesMatrix );
        //for ( int i = 0; i < rot.Length; i++ ) {
        //    string str = "";
        //    for ( int j = 0; j < rot[i].Length; j++ ) {
        //        str += rot[i][j].x.ToString() + "," + rot[i][j].y.ToString() + "__";
        //    }
        //    Debug.Log( str );
        //}

        //for ( int i = 0; i < puzzlePieces.Count; i++ ) {
        //    Debug.Log( "index  " + puzzlePieces[i].GetIndexInMainImageMatrixTemp() );
        //}

        List<PuzzlePiece> newArray = new List<PuzzlePiece>();
        PuzzlePiece[][] puzzlePiecesNewArray = new PuzzlePiece[rot[0].Length][];
        Dictionary<CustomVector2Int, PuzzlePiece> pieceAndNewIndex = new Dictionary<CustomVector2Int, PuzzlePiece>();

        for ( int j = 0; j < rot[0].Length; j++ ) {
            puzzlePiecesNewArray[j] = new PuzzlePiece[rot.Length];
            for ( int i = 0; i < rot.Length; i++ ) {
                //Debug.Log( "rot[i][j]  " + rot[i][j] );
                PuzzlePiece puzzlePieceTemp = puzzlePieces.Find( x => x.GetIndexInMainImageMatrixTemp().x == rot[i][j].x &&
                                              x.GetIndexInMainImageMatrixTemp().y == rot[i][j].y );
                //Debug.Log( puzzlePieceTemp );
                //Debug.Log( puzzlePieceTemp.GetIndexInMainImageMatrixPrimary() );
                //Debug.Log( puzzlePieceTemp.GetIndexInMainImageMatrixTemp() );
                if ( puzzlePieceTemp != null ) {
                    newArray.Add( puzzlePieceTemp );
                    puzzlePiecesNewArray[j][i] = puzzlePieceTemp;
                    CustomVector2Int newPuzzlePieceIndex = new CustomVector2Int( startMatrix.x + i, startMatrix.y + j );
                    //puzzlePieceTemp.SetIndexInMainImageMatrixTemp( newPuzzlePieceIndex );
                    pieceAndNewIndex.Add( newPuzzlePieceIndex, puzzlePieceTemp );
                }
            }
        }

        foreach ( var item in pieceAndNewIndex ) {
            item.Value.SetIndexInMainImageMatrixTemp( item.Key );
        }

        //Debug.Log( "++++++++" );
        puzzlePieces = newArray;

        //for ( int i = 0; i < newArray.Count; i++ ) {
        //    Debug.Log( newArray[i].GetIndexInMainImageMatrixPrimary().x + " " + newArray[i].GetIndexInMainImageMatrixPrimary().y + " now is " +
        //               newArray[i].GetIndexInMainImageMatrixTemp().x + " " + newArray[i].GetIndexInMainImageMatrixTemp().y );
        //}
        //Debug.Log( "=========== =========== =========== ===========" );
        return new PuzzlePiece[1][];
    }

    Vector2Int[][] RotateCounterClockwise ( Vector2Int[][] m ) {
        var result = new Vector2Int[m[0].Length][];
        for ( int i = 0; i < result.Length; i++ ) {
            result[i] = new Vector2Int[m.Length];
        }

        for ( int i = 0; i < m[0].Length; i++ ) {
            for ( int j = 0; j < result.Length; j++ ) {
                result[i][j] = m[j][m[0].Length - i - 1];
            }
        }
        return result;
    }

    CustomVector2Int[][] Rotate ( CustomVector2Int[][] m ) {
        var result = new CustomVector2Int[m[0].Length][ ];
        for ( int i = 0; i < result.Length; i++ ) {
            result[i] = new CustomVector2Int[m.Length];
        }

        for ( int i = 0; i < m[0].Length; i++ ) {
            for ( int j = 0; j < m.Length; j++ ) {
                result[i][j] = m[m.Length - j - 1][i];
            }
        }

        return result;
    }

    internal List<CustomVector2Int> GetPuzzlePiecesOffset () {
        List<CustomVector2Int> puzzlePiecesOffset = new List<CustomVector2Int>();
        puzzlePiecesOffset.Add( new CustomVector2Int( 0, 0 ) );
        CustomVector2Int startIndex = puzzlePieces[0].GetIndexInMainImageMatrixTemp();

        for ( int i = 1; i < puzzlePieces.Count; i++ ) {
            CustomVector2Int offsetIndex = puzzlePieces[i].GetIndexInMainImageMatrixTemp() - startIndex;
            puzzlePiecesOffset.Add( offsetIndex );
        }

        return puzzlePiecesOffset;
    }

    internal void RotatePuzzlePieces () {
        GetArrayPuzzlePiece();

        currentAngle += 90;
        currentAngle += 1;

        if ( currentAngle >= 360 ) {
            currentAngle = 0;
            currentAngle = 0;
        }

        myRectTransform.localEulerAngles = new Vector3( myRectTransform.localEulerAngles.x, myRectTransform.localEulerAngles.y, currentAngle );

        for ( int i = 0; i < puzzlePieces.Count; i++ ) {
            puzzlePieces[i].RotatePuzzlePiece( currentAngle );
        }
    }

    internal void ChangeNumPiece () {
        startNumPuzzlePiece += 1;
        if ( startNumPuzzlePiece >= 4 ) {
            startNumPuzzlePiece = 0;
        }

        for ( int i = 0; i < puzzlePieces.Count; i++ ) {
            puzzlePieces[i].AddCurrentNumPuzzlePieces();
            puzzlePieces[i].RotateToAngle( currentAngle );
        }
    }
}
