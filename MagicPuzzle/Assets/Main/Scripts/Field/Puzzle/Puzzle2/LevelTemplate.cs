﻿using System.Collections.Generic;
using UnityEngine;

public class LevelTemplate  {
    public List<LevelTemplateData> levelsTemplateData;

    public LevelTemplate () {
        levelsTemplateData = new List<LevelTemplateData>();
    }

    //GET
    public List<LevelTemplateData> GetLevelsTemplateData () {
        return levelsTemplateData;
    }
    public LevelTemplateData GetLevelsTemplateDataByIndex ( int index ) {
        if ( index <= 0 && index >= levelsTemplateData.Count ) {
            return null;
        }

        return levelsTemplateData[index];
    }

    public LevelTemplateData GetRandonLevelsTemplateData () {
        return levelsTemplateData[Random.Range( 0, levelsTemplateData.Count )];
    }

    //ADD
    public void AddLevelTemplateData ( LevelTemplateData levelTemplateData ) {
        levelsTemplateData.Add( levelTemplateData );
    }

    //REMOVE
    public void RemoveLevelTemplateData ( int indexRemove ) {
        if ( indexRemove < 0 && indexRemove >= levelsTemplateData.Count ) {
            return;
        }

        levelsTemplateData.RemoveAt( indexRemove );
    }
}
