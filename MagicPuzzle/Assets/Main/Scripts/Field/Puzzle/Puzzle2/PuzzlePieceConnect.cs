﻿using UnityEngine;

[System.Serializable]
public struct PuzzlePieceConnect {
    [SerializeField]
    PuzzlePieceConnectType up;
    [SerializeField]
    PuzzlePieceConnectType right;
    [SerializeField]
    PuzzlePieceConnectType down;
    [SerializeField]
    PuzzlePieceConnectType left;

    internal PuzzlePieceConnect ( PuzzlePieceConnectType up, PuzzlePieceConnectType right,
                                  PuzzlePieceConnectType down, PuzzlePieceConnectType left ) {
        this.up = up;
        this.right = right;
        this.down = down;
        this.left = left;
    }

    internal PuzzlePieceConnectType Up {
        get {
            return up;
        }
    }

    internal PuzzlePieceConnectType Right {
        get {
            return right;
        }
    }

    internal PuzzlePieceConnectType Down {
        get {
            return down;
        }
    }

    internal PuzzlePieceConnectType Left {
        get {
            return left;
        }
    }
}
