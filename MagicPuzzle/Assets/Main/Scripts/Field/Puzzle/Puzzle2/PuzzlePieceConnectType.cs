﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PuzzlePieceConnectType {
    In,
    Out,
    Flat
}
