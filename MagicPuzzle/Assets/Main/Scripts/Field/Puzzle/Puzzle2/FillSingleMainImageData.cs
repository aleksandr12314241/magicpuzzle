﻿using System.Collections.Generic;
using UnityEngine;

public struct FillSingleMainImageData {

    int numTemplate;
    List<CustomVector2Int> indexInMainImageMatrix;
    List<CustomVector2Int> pieceIndex;

    public FillSingleMainImageData ( int numTemplate, List<CustomVector2Int> indexInMainImageMatrix, List<CustomVector2Int> pieceIndex ) {
        this.numTemplate = numTemplate;
        this.indexInMainImageMatrix = indexInMainImageMatrix;
        this.pieceIndex = pieceIndex;
    }

    //GET
    public int GetNumTemplate () {
        return numTemplate;
    }
    public List<CustomVector2Int> GetIndexInMainImageMatrix () {
        return indexInMainImageMatrix;
    }
    public List<CustomVector2Int> GetPieceIndex () {
        return pieceIndex;
    }
}
