﻿using System.Collections.Generic;
using UnityEngine;

public class PuzzleLevelPieceDoneData {
    string mainImageName;
    Vector2 sizePiece;
    List<List<PuzzlePiece>> puzzlePiece;
    CustomVector2Int sizePuzzleIndex;
    Vector2 startPosition;
    int puzzlePieceCount;

    public PuzzleLevelPieceDoneData ( string mainImageName, Vector2 sizePiece, CustomVector2Int sizePuzzleIndex, Vector2 startPosition,
                                      int puzzlePieceCount ) {
        this.mainImageName = mainImageName;
        this.sizePiece = sizePiece;
        this.sizePuzzleIndex = sizePuzzleIndex;
        this.startPosition = startPosition;
        this.puzzlePieceCount = puzzlePieceCount;
        puzzlePiece = new List<List<PuzzlePiece>>();
    }

    //GET
    public Vector2 GetSizePiece () {
        return sizePiece;
    }
    public List<List<PuzzlePiece>> GetPuzzlePiece () {
        return puzzlePiece;
    }
    internal CustomVector2Int GetWayOfFillMainImage ( int length ) {
        int indexJ = length / sizePuzzleIndex.x;
        int div = ( indexJ + 1 ) % 2;
        int indexI = ( div == 1 ) ? length % sizePuzzleIndex.x : sizePuzzleIndex.x - 1 - ( length % sizePuzzleIndex.x );
        int sign = ( div == 1 ) ? 1 : -1;
        return new CustomVector2Int( indexI, indexJ );
    }
    internal Vector2 GetPiecePositionByIndex ( int indexI, int indexJ ) {
        return new Vector2( startPosition.x + ( indexI * sizePiece.x ), startPosition.y - ( indexJ * sizePiece.y ) );
    }
    internal int GetPuzzlePieceCount () {
        return puzzlePieceCount;
    }
    internal CustomVector2Int GetSizePuzzleIndex () {
        return sizePuzzleIndex;
    }
    internal Vector2 GetStartPosition () {
        return startPosition;
    }
    internal string GetMainImageName () {
        return mainImageName;
    }

    //SET
    public void SetPuzzlePiece ( List<List<PuzzlePiece>> puzzlePiece ) {
        this.puzzlePiece = puzzlePiece;
    }
}
