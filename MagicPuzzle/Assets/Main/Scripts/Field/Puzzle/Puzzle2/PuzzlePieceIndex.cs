﻿using UnityEngine;

public class PuzzlePieceIndex {
    public CustomVector2Int pzzlePieceIndex;
    public int numPuzzlePiece;

    public PuzzlePieceIndex ( CustomVector2Int pzzlePieceIndex, int numPuzzlePiece ) {
        this.pzzlePieceIndex = pzzlePieceIndex;
        this.numPuzzlePiece = numPuzzlePiece;
    }

    //GET
    public CustomVector2Int GetPzzlePieceIndex () {
        return pzzlePieceIndex;
    }
    public int GetNumPzzlePiece () {
        return numPuzzlePiece;
    }


    //SET
    public void SetPzzlePieceIndex ( CustomVector2Int pzzlePieceIndex ) {
        this.pzzlePieceIndex = pzzlePieceIndex;
    }
    public void SetNumPzzlePiece ( int numPuzzlePiece ) {
        this.numPuzzlePiece = numPuzzlePiece;
    }
}
