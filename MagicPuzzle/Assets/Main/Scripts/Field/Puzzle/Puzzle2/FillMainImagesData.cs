﻿using System.Collections.Generic;
using UnityEngine;

public struct FillMainImagesData {
    int numTemplate;
    List<FillSingleMainImageData> fillSingleMainImageData;

    //GET
    public int GetNumTemplate () {
        return numTemplate;
    }
    public List<FillSingleMainImageData> GetFillSingleMainImageData () {
        return fillSingleMainImageData;
    }
    public int GetFillSingleMainImageDataCount () {
        return fillSingleMainImageData.Count;
    }

    //SET
    public void SetNumTemplate ( int numTemplate ) {
        this.numTemplate = numTemplate;
    }
    public void SetFillSingleMainImageData ( List<FillSingleMainImageData> fillSingleMainImageData ) {
        this.fillSingleMainImageData = fillSingleMainImageData;
    }

    //ADD
    public void AddFillSingleMainImageData ( FillSingleMainImageData fillSingleMainImageDataEntry ) {
        fillSingleMainImageData.Add( fillSingleMainImageDataEntry );
    }

    //REMOVE
    public void RemoveLastFillSingleMainImageData () {
        fillSingleMainImageData.RemoveAt( fillSingleMainImageData.Count - 1 );
    }
    public void RemoveFillSingleMainImageData () {
        fillSingleMainImageData = new List<FillSingleMainImageData>();
    }

    //CLEAR
    public void ClearFillSingleMainImageData () {
        numTemplate = -1;
        fillSingleMainImageData = new List<FillSingleMainImageData>();
    }
}
