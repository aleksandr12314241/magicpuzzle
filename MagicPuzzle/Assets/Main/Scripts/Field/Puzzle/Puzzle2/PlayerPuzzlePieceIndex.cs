﻿using Newtonsoft.Json;

public struct PlayerPuzzlePieceIndex {
    public CustomVector2Int pzzlePieceIndex;
    public int[] numPuzzlePieces;
    public int startAngle;
    public int currentAngle;

    [JsonConstructor]
    public PlayerPuzzlePieceIndex ( CustomVector2Int pzzlePieceIndex, int[] numPuzzlePieces, int startAngle, int currentAngle ) {
        this.pzzlePieceIndex = pzzlePieceIndex;
        this.numPuzzlePieces = numPuzzlePieces;
        this.startAngle = startAngle;
        this.currentAngle = currentAngle;
    }
}
