﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzelTemplatesName {
    public List<string> puzzleTemplateName;

    public PuzzelTemplatesName () {
        puzzleTemplateName = new List<string>();
    }

    //GET
    public List<string> GetPuzzleTemplateName () {
        return puzzleTemplateName;
    }

    //ADD
    public void AddTemplateName ( string templateName ) {
        if ( puzzleTemplateName.Contains( templateName ) ) {
            return;
        }
        puzzleTemplateName.Add( templateName );
    }
}
