﻿using System.Collections.Generic;
using UnityEngine;

public class LevelTemplateData {
    public int sizeField;
    public List<List<CustomVector2Int>> indexsPuzzlePiece;

    public LevelTemplateData () {
        indexsPuzzlePiece = new List<List<CustomVector2Int>>();
    }

    public LevelTemplateData ( List<List<CustomVector2Int>> indexsPuzzlePiece ) {
        this.indexsPuzzlePiece = indexsPuzzlePiece;
    }

    //SET
    internal void SetSizeField ( int sizeField ) {
        this.sizeField = sizeField;
    }

    //GET
    internal int GetIndexPuzzlePieceCount () {
        return indexsPuzzlePiece.Count;
    }
    internal List<List<CustomVector2Int>> GetIndexPuzzlePiece () {
        return indexsPuzzlePiece;
    }
    internal List<CustomVector2Int> GetArrayVector2IntByIndex ( int indI ) {
        return indexsPuzzlePiece[indI];
    }
    internal CustomVector2Int GetVector2IntByIndex ( int indI, int indJ ) {
        return indexsPuzzlePiece[indI][indJ];
    }
    internal int GetSizeField () {
        return sizeField;
    }

    //ADD
    internal void AddIndexPuzzlePiece ( List<CustomVector2Int> indexPuzzlePiece ) {
        indexsPuzzlePiece.Add( indexPuzzlePiece );
    }
}
