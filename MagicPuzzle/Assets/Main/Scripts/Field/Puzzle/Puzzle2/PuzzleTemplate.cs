﻿using System.Collections.Generic;
using UnityEngine;

public class PuzzleTemplate {
    public CustomVector2Int sizeMatrixImage;
    public List<PuzzlePieceIndex> puzzlePieceByIndex;
    public List<List<CustomVector2Int>> connectionPuzzlePiece;

    public PuzzleTemplate ( CustomVector2Int sizeMatrixImage ) {
        this.sizeMatrixImage = sizeMatrixImage;
        puzzlePieceByIndex = new List<PuzzlePieceIndex>();
        connectionPuzzlePiece = new List<List<CustomVector2Int>>();
    }

    //GET
    public CustomVector2Int GetSizeMatrixImage () {
        return sizeMatrixImage;
    }
    public Dictionary<CustomVector2Int, int> GetPuzzlePieceByIndex () {
        Dictionary<CustomVector2Int, int> puzzlePieceByIndexDictionary = new Dictionary<CustomVector2Int, int>();
        for ( int i = 0; i < this.puzzlePieceByIndex.Count; i++ ) {
            puzzlePieceByIndexDictionary.Add( this.puzzlePieceByIndex[i].GetPzzlePieceIndex(),
                                              this.puzzlePieceByIndex[i].GetNumPzzlePiece() );
        }
        return puzzlePieceByIndexDictionary;
    }
    public List<List<CustomVector2Int>> GetConnectionPuzzlePiece () {
        return connectionPuzzlePiece;
    }
    public List<CustomVector2Int> GetConnectionPuzzlePieceByIndex ( int index ) {
        return connectionPuzzlePiece[index];
    }
    public int GetPuzzlePieceByIndexCount () {
        return puzzlePieceByIndex.Count;
    }

    //SET
    public void SetSizeMatrixImage ( CustomVector2Int sizeMatrixImage ) {
        this.sizeMatrixImage = sizeMatrixImage;
    }
    public void SetPuzzlePieceByIndex ( Dictionary<CustomVector2Int, int> puzzlePieceByIndex ) {
        this.puzzlePieceByIndex = new List<PuzzlePieceIndex>();
        foreach ( var item in puzzlePieceByIndex ) {
            PuzzlePieceIndex puzzlePieceIndexTmp = new PuzzlePieceIndex( item.Key, item.Value );
            this.puzzlePieceByIndex.Add( puzzlePieceIndexTmp );
        }
    }
    public void SetConnectionPuzzlePiece ( List<List<CustomVector2Int>> connectionPuzzlePiece ) {
        this.connectionPuzzlePiece = connectionPuzzlePiece;
    }
}
