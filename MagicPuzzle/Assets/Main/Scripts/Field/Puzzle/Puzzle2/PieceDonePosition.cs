﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceDonePosition : MonoBehaviour {
    [SerializeField]
    RectTransform pieceDotParent;
    [SerializeField]
    GameObject pieceDotPrf;

    List<RectTransform> pieceDots;
    internal void Init () {
        pieceDots = new List<RectTransform>();
    }

    internal void CreateDots ( int countDots ) {
        int startCount = pieceDots.Count;
        for ( int i = startCount; i < countDots; i++ ) {
            GameObject pieceDotGameObject = Instantiate( pieceDotPrf, pieceDotParent );
            pieceDotGameObject.gameObject.SetActive( false );
            pieceDots.Add( pieceDotGameObject.GetComponent<RectTransform>() );
        }
    }

    internal void ShowDots ( CustomVector2Int sizeindexMainImage, Vector2 startPosition, Vector2 sizePiece ) {
        int index = 0;
        for ( int i = 0; i < sizeindexMainImage.x; i++ ) {
            for ( int j = 0; j < sizeindexMainImage.y; j++ ) {
                pieceDots[index].anchoredPosition = new Vector2( startPosition.x + ( i * sizePiece.x ), startPosition.y - ( j * sizePiece.y ) );
                pieceDots[index].gameObject.SetActive( true );
                index++;
            }
        }
    }

    internal void HideDots () {
        for ( int i = 0; i < pieceDots.Count; i++ ) {
            if ( !pieceDots[i].gameObject.activeSelf ) {
                break;
            }
            pieceDots[i].gameObject.SetActive( false );
        }
    }
}
