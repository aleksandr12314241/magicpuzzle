﻿using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public struct PuzzleStateData {
    public CustomVector2Int sizeMatrixImage;
    public List<List<PlayerPuzzlePieceIndex>> playerPieces;
    public List<CustomVector2Int> pieceFieldIndex;

    [JsonConstructor]
    public PuzzleStateData ( CustomVector2Int sizeMatrixImage, List<List<PlayerPuzzlePieceIndex>> playerPieces,
                             List<CustomVector2Int> pieceFieldIndex ) {
        this.sizeMatrixImage = sizeMatrixImage;
        this.playerPieces = playerPieces;
        this.pieceFieldIndex = pieceFieldIndex;
    }

    internal int GetPlayerPiecesCount () {
        int playerPiecesCount = 0;
        for ( int i = 0; i < playerPieces.Count; i++ ) {
            playerPiecesCount += playerPieces[i].Count;
        }
        return playerPiecesCount;
    }
}
