﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Puzzle;

public class PieceControlButton : MonoBehaviour {
    [SerializeField]
    RectTransform myRectTransform;
    [SerializeField]
    Button rotationButton;
    [SerializeField]
    Button changePieceButton;
    [SerializeField]
    Button closeButton;

    PieceFieldController pieceFieldController;

    internal void Init () {
        rotationButton.onClick.AddListener( Rotate );
        changePieceButton.onClick.AddListener( ChangePiece );
        closeButton.onClick.AddListener( Close );
    }

    internal void ShowPieceControlButton ( PieceFieldController pieceFieldController ) {
        return;

        if ( this.pieceFieldController == pieceFieldController ) {
            this.pieceFieldController = null;
            Close();
            return;
        }

        myRectTransform.anchoredPosition = pieceFieldController.MyRectTransform.anchoredPosition +
                                           new Vector2( 0f, ( 375f / 2f ) * pieceFieldController.transform.localScale.y );

        this.pieceFieldController = pieceFieldController;
        gameObject.SetActive( true );
    }

    void Rotate () {

    }

    void ChangePiece () {

    }

    internal void Close () {
        gameObject.SetActive( false );
    }
}
