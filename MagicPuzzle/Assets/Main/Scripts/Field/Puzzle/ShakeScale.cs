﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ShakeScale : MonoBehaviour {
    [SerializeField]
    Button button;
    [SerializeField]
    Transform tr;

    [SerializeField]
    float duratation;
    [SerializeField]
    float strength;
    [SerializeField]
    int vibrato;
    [SerializeField]
    float randomness;
    [SerializeField]
    bool fadeOut;


    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    bool isShake = false;

    private void Awake () {
        button.onClick.AddListener( Shake );
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if ( !isShake ) {
            return;
        }
        // Define a target position above and behind the target transform
        Vector3 targetPosition =  new Vector3( 2, 2, 0 );

        // Smoothly move the camera towards that target position
        tr.localScale = Vector3.SmoothDamp( tr.localScale, targetPosition, ref velocity, smoothTime );
    }

    void Shake () {
        if ( isShake ) {
            isShake = false;
        }
        else {
            isShake = true;
        }
        //tr.DOShakeScale( duratation, strength, vibrato, randomness, fadeOut );
    }
}
