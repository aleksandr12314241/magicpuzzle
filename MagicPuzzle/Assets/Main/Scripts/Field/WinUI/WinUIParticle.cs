﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public class WinUIParticle: MonoBehaviour {
        [SerializeField]
        RectTransform[] particlesItem;
        [SerializeField]
        Vector2 startPoint;
        [SerializeField]
        Vector2 endPoint;

        internal void DiactivateParticlesItem () {
            for ( int i = 0; i < particlesItem.Length; i++ ) {
                particlesItem[i].gameObject.SetActive( false );
            }
        }

        internal void ShowRandomParticle () {
            for ( int i = 0; i < particlesItem.Length; i++ ) {
                if ( !particlesItem[i].gameObject.activeSelf ) {
                    ShowRandomParticleByIndex( i );
                    return;
                }
            }
        }

        void ShowRandomParticleByIndex ( int indexParticles ) {
            float shownParticlePosX = 0f;
            for ( int i = 0; i < particlesItem.Length; i++ ) {
                if ( particlesItem[i].gameObject.activeSelf ) {
                    shownParticlePosX = particlesItem[i].anchoredPosition.x;
                }
            }

            Vector2 randomPosition;
            if ( shownParticlePosX != 0 ) {
                float randomX = 0;
                if ( shownParticlePosX > 0 ) {
                    randomX = Random.Range( 0f, 10f );
                }
                else {
                    randomX = Random.Range( -10f, 0f );
                }

                randomPosition = new Vector2( ( shownParticlePosX * -1 ) + randomX, Random.Range( startPoint.y, endPoint.y ) );
            }
            else {
                randomPosition = new Vector2( Random.Range( startPoint.x, endPoint.x ), Random.Range( startPoint.y, endPoint.y ) );
            }

            particlesItem[indexParticles].anchoredPosition = randomPosition;
            particlesItem[indexParticles].gameObject.SetActive( true );
        }
    }
}
