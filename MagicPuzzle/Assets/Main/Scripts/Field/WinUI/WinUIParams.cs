﻿using UnityEngine;
using Puzzle;

public struct WinUIParams {
    public PuzzleType puzzelType;
    public Vector2Int categories;
    public Vector2Int levels;
    public CloseWinUIType levelFinishType;
}
