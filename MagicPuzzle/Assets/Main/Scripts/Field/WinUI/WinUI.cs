﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Common;
using Utility;
using DG.Tweening;

namespace Puzzle {
    public class WinUI: WindowMonoBehaviour {
        public event Action<PuzzleType, int, int> OnStartShow = ( PuzzleType param1, int param2, int param3 ) => { };
        public event Action<PuzzleType, CloseWinUIType> OnStartFinLevel = ( PuzzleType param1, CloseWinUIType  param2 ) => { };

        [SerializeField]
        Text title;
        [SerializeField]
        Text titleSecond;
        [SerializeField]
        Text numLevelFirst;
        [SerializeField]
        Text numLevelSecond;
        [SerializeField]
        Text totalLevels;
        [SerializeField]
        Animator changerLevel;
        [SerializeField]
        AudioSource myAudioSource;
        [SerializeField]
        Animator _myAnimator;
        [SerializeField]
        Button _winClick;
        [SerializeField]
        WinUIParticle winUIParticleOne;
        [SerializeField]
        WinUIParticle winUIParticleTwo;
        [SerializeField]
        Button nextButton;
        [SerializeField]
        Image cupSilhouetteImage;
        [SerializeField]
        Image cupFillImage;
        [SerializeField]
        Image cupMask;
        [SerializeField]
        GameObject streamerContainer;
        [SerializeField]
        GameObject rayGameObject;
        [SerializeField]
        AudioClip winLevelAudioClip;
        [SerializeField]
        AudioClip winCategoryAudioClip;

        PuzzleType _puzzelType;
        CloseWinUIType _levelFinishType;

        string[] cangritWords;
        int fromLevel;
        int toLevel;
        int maxLevels;

        bool newCategory;

        WinAnimationSteps winAnimationSteps;

        GameController _GC;
        GameLinks gameLinks;

        bool isContinueButton;

        internal override void InitStart () {
            _winClick.onClick.AddListener( SetEndStartWinWindow );
            nextButton.onClick.AddListener( StartHide );

            _myAnimator.enabled = false;
            cangritWords = new string[] { "txt_puzzle_win_tittle_1", "txt_puzzle_win_tittle_2", "txt_puzzle_win_tittle_3", "txt_puzzle_win_tittle_4" };

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            gameLinks = _GC.GameLinks;
            //            gameLinks = _GC.GameLinks.IconDataSet.GetCategoriesIconByIndex( numCatigories );
        }

        public void ShowWin ( WinUIParams winUIParams ) {
            isContinueButton = false;
            rayGameObject.gameObject.SetActive( false );

            _puzzelType = winUIParams.puzzelType;
            _levelFinishType = winUIParams.levelFinishType;

            int rnadomNum = UnityEngine.Random.Range( 0, cangritWords.Length );
            title.text = LocalizationManager.GetText( cangritWords[rnadomNum] );
            titleSecond.text = title.text;

            totalLevels.text = "/" + maxLevels.ToString();
            base.Show();

            //int toLevelSecond = toLevel;

            IconDataSet.CupsSet cupsSet = gameLinks.IconDataSet.GetCategoriesIconByIndex( winUIParams.categories.x );
            cupSilhouetteImage.sprite = cupsSet.CategoriesIconSilhouette;
            cupFillImage.sprite = cupsSet.CategoriesIcon;
            cupMask.sprite = cupsSet.CategoriesIconSilhouette;

            if ( winUIParams.categories.y > winUIParams.categories.x ) {
                newCategory = true;
                //new category
                fromLevel = winUIParams.levels.x;
                toLevel = maxLevels;
                //toLevelSecond = toLevel;
            }
            else {
                newCategory = false;

                if ( maxLevels == winUIParams.levels.y ) {
                    fromLevel = maxLevels;
                    toLevel = maxLevels;
                    //toLevelSecond = toLevel;
                }
                else if ( winUIParams.levels.x == winUIParams.levels.y ) {
                    fromLevel = winUIParams.levels.x;
                    toLevel = winUIParams.levels.y;
                    //toLevelSecond = toLevel;
                }
                else {
                    fromLevel = winUIParams.levels.x;
                    toLevel = winUIParams.levels.y;
                    //toLevelSecond = toLevel;

                    //toLevelSecond++;
                    //if ( toLevelSecond > maxLevels ) {
                    //    toLevelSecond = maxLevels;
                    //}
                }
            }

            numLevelFirst.text = fromLevel.ToString();
            numLevelSecond.text = toLevel.ToString();

            cupFillImage.fillAmount = ( float ) fromLevel / ( float ) maxLevels;

            totalLevels.gameObject.SetActive( false );
            changerLevel.gameObject.SetActive( false );

            nextButton.gameObject.SetActive( false );

            _myAnimator.enabled = true;
            _myAnimator.Play( "win" );

            OnStartShow( winUIParams.puzzelType, winUIParams.levels.x, winUIParams.levels.y );

            winAnimationSteps = WinAnimationSteps.StartShowWindow;

            //Invoke( "ShowCangratulationWord", 0.3f );
            //ShowCangratulationWord();
            streamerContainer.gameObject.SetActive( false );

            ShowCup();
        }

        void ShowCup () {
            if ( winAnimationSteps != WinAnimationSteps.StartShowWindow ) {
                return;
            }

            if ( !newCategory ) {
                for ( int i = 0; i < 3; i++ ) {
                    float timeDelay = i / 5f;
                    this.CustomInvoke( timeDelay, () => {
                        winUIParticleOne.ShowRandomParticle();
                    } );
                }
            }
            myAudioSource.PlayOneShot( winLevelAudioClip );
            Invoke( "EndShowCapTemp", 1.5f );
        }

        void EndShowCapTemp () {
            if ( winAnimationSteps != WinAnimationSteps.StartShowWindow ) {
                return;
            }

            winAnimationSteps = WinAnimationSteps.FilCup;
            EndShowCap();
        }

        void EndShowCap () {
            AudioManager.instance.PlaySoundByType( SoundType.CupHitting );

            if ( winAnimationSteps != WinAnimationSteps.FilCup ) {
                return;
            }

            _myAnimator.Play( "win_cup_idle" );
            changerLevel.gameObject.SetActive( true );
            totalLevels.gameObject.SetActive( true );
            if ( numLevelFirst.text != numLevelSecond.text ) {
                changerLevel.SetTrigger( "startChange" );
            }

            float endFillValue = ( float ) toLevel / ( float ) maxLevels;
            cupFillImage.DOFillAmount( endFillValue, 0.5f ).OnComplete( () => {
                ChechEndCategory();
            } );
            AudioManager.instance.PlaySoundByType( SoundType.CupFill );
        }

        void ChechEndCategory () {
            if ( winAnimationSteps != WinAnimationSteps.FilCup ) {
                return;
            }

            cupFillImage.DOKill();
            float endFillValue = ( float ) toLevel / ( float ) maxLevels;
            cupFillImage.fillAmount = endFillValue;


            if ( !newCategory ) {
                //Invoke( "ShowContinueButton", 0.2f );
                ShowContinueButton();
                return;
            }

            for ( int i = 0; i < 2; i++ ) {
                float timeDelay = i / 5f;
                this.CustomInvoke( timeDelay, () => {
                    winUIParticleOne.ShowRandomParticle();
                } );
            }

            for ( int i = 0; i < 3; i++ ) {
                float timeDelay = i / 3f;
                this.CustomInvoke( timeDelay, () => {
                    winUIParticleTwo.ShowRandomParticle();
                } );
            }

            winAnimationSteps = WinAnimationSteps.StartShowWinCategory;

            rayGameObject.gameObject.SetActive( true );
            streamerContainer.gameObject.SetActive( true );
            EndCategorie();

            Invoke( "EndShowCategory", 0.8f );
        }

        void EndShowCategory () {
            winAnimationSteps = WinAnimationSteps.WaitShowWinCategory;
        }

        void EndCategorie () {
            _myAnimator.SetTrigger( "winCupCategorie" );
            myAudioSource.PlayOneShot( winCategoryAudioClip );

            Invoke( "ShowContinueButton", 1.5f );
        }

        void SartHideShowCategory () {
            if ( winAnimationSteps != WinAnimationSteps.WaitShowWinCategory ) {
                return;
            }

            _myAnimator.Play( "win_cup_categorie_hide" );

            ShowContinueButton();
        }

        void ShowContinueButton () {
            if ( winAnimationSteps == WinAnimationSteps.WaitClsoeWindow ) {
                return;
            }

            winAnimationSteps = WinAnimationSteps.WaitClsoeWindow;

            nextButton.interactable = true;
            nextButton.gameObject.SetActive( true );
            isContinueButton = true;
        }

        void StartHide () {
            if ( winAnimationSteps != WinAnimationSteps.WaitClsoeWindow ) {
                return;
            }

            winAnimationSteps = WinAnimationSteps.StartHide;

            rayGameObject.gameObject.SetActive( false );
            nextButton.interactable = false;
            nextButton.gameObject.SetActive( false );

            if ( IsShown ) {
                HideWin();

                OnStartFinLevel( _puzzelType, _levelFinishType );
            }
        }

        internal void HideWin () {
            _myAnimator.Play( "hide" );
            Invoke( "HideEnd", 0.3f );
        }

        void HideEnd () {
            base.Hide();

            _myAnimator.enabled = false;
        }

        internal void SetLevelFinishType ( CloseWinUIType levelFinishType ) {
            this._levelFinishType = levelFinishType;
        }

        void SetEndStartWinWindow () {
            if ( winAnimationSteps == WinAnimationSteps.StartShowWindow ) {
                EndShowCapTemp();
            }
            else if ( winAnimationSteps == WinAnimationSteps.FilCup ) {
                ChechEndCategory();
            }
            else if ( winAnimationSteps == WinAnimationSteps.WaitShowWinCategory ) {

            }
            else if ( isContinueButton && winAnimationSteps == WinAnimationSteps.WaitClsoeWindow ) {
                StartHide();
            }
        }
    }
}
