﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Utility;

namespace Common {
    public class LampFly: MonoBehaviour {
        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        GameObject psEndFlyLamp;
        [SerializeField]
        Image lamp;

        List<Vector2> controlPoints;
        int currentIndex = 0;
        int segmentsPerCurve = 20;
        float timeStep;
        Vector2 startPoint;

        internal void Init () {
        }

        internal void FlyToUppaer ( Vector2 startPosition, Vector2 endPosition ) {
            gameObject.SetActive( true );
            myRectTransform.anchoredPosition = startPosition;

            lamp.gameObject.SetActive( true );
            psEndFlyLamp.gameObject.SetActive( false );

            Fly( startPosition, endPosition, Random.Range( 0.4f, 0.7f ) );
        }

        internal void FlyToHint ( Vector2 startPosition, Vector2 endPosition ) {
            float timeFly = 0.3f;

            Fly( startPosition, endPosition, timeFly );
        }

        void Fly ( Vector2 startPosition, Vector2 endPosition, float timeFly ) {
            gameObject.SetActive( true );
            myRectTransform.anchoredPosition = startPosition;

            lamp.gameObject.SetActive( true );
            psEndFlyLamp.gameObject.SetActive( false );

            float jumpPowerOffset = endPosition.y / 6f;
            float jumpPower = -endPosition.y / 2f + Random.Range( -jumpPowerOffset, jumpPowerOffset );
            myRectTransform.DOJumpAnchorPos( endPosition, jumpPower, 1, timeFly );
            myRectTransform.DOScale( new Vector3( 2f, 2f, 2f ), timeFly / 2f ).OnComplete( () => {
                myRectTransform.DOScale( new Vector3( 1f, 1f, 1f ), timeFly / 2f ).OnComplete( () => {
                    lamp.gameObject.SetActive( false );
                    psEndFlyLamp.gameObject.SetActive( true );
                } );
            } );

            myRectTransform.DORotate( new Vector3( 0f, 0f, -45f ), timeFly / 3f ).OnComplete( () => {
                myRectTransform.DORotate( new Vector3( 0f, 0f, 45f ), ( timeFly - ( timeFly / 3f ) ) );
            } );

            Invoke( "EndFly", ( timeFly * 5 ) );
        }

        void EndFly () {
            gameObject.SetActive( false );
        }
    }
}
