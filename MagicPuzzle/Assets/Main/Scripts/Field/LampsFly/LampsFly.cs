﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Puzzle;

namespace Common {
    public class LampsFly: MonoBehaviour {
        [SerializeField]
        RectTransform mainRectTransform;
        [SerializeField]
        UpperPanel upperPanel;
        [SerializeField]
        PuzzleController mainPuzzleController;
        [SerializeField]
        LampFly[] lampsFly;

        private void Start () {
            mainPuzzleController.OnUseHint += OnUseHint;

            for ( int i = 0; i < lampsFly.Length; i++ ) {
                lampsFly[i].Init();
            }
        }

        void OnUseHint ( bool isUse, MainImageDone letterDone ) {
            if ( !isUse ) {
                return;
            }

            Vector2 endLampPosition = new Vector2( letterDone.MyRectTransform.anchoredPosition.x,
                                                   ( ( mainRectTransform.rect.height ) / 2f ) +
                                                   letterDone.MyRectTransform.anchoredPosition.y -
                                                   ( letterDone.MyRectTransform.rect.height / 2 )  );
            StartFlyLampsToHint( endLampPosition );
        }

        internal void StartFlyLampsToHint ( Vector2 endLampPosition ) {
            Vector2 startLampPosition = new Vector2( ( ( mainRectTransform.rect.width ) / 2f ) + upperPanel.GetLampsPosition().x,
                    ( ( mainRectTransform.rect.height ) / 2f ) + upperPanel.GetLampsPosition().y );

            for ( int i = 0; i < lampsFly.Length; i++ ) {
                if ( !lampsFly[i].gameObject.activeSelf ) {
                    lampsFly[i].FlyToHint( startLampPosition, endLampPosition );
                    break;
                }
            }
        }

        public void StartFlyLampsToUI ( int countLamps ) {
            int countLampsTemp = countLamps < lampsFly.Length ? countLamps : lampsFly.Length;

            Vector2 startLampPosition = Vector2.zero + new Vector2( Random.Range( -50, 50 ), Random.Range( -50, 50 ) );

            Vector2 endLampPosition = new Vector2( ( ( mainRectTransform.rect.width ) / 2f ) + upperPanel.GetLampsPosition().x,
                                                   ( ( mainRectTransform.rect.height ) / 2f ) + upperPanel.GetLampsPosition().y );

            for ( int i = 0; i < countLampsTemp; i++ ) {
                if ( !lampsFly[i].gameObject.activeSelf ) {
                    lampsFly[i].FlyToUppaer( startLampPosition, endLampPosition );
                }
            }
        }
    }
}
