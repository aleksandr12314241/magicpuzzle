﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Utility;
using System;

namespace Puzzle {
    public class MainImageDone: MonoBehaviour {
        internal event Action<Sprite> OnShowMainImage = ( Sprite mainImageSprite ) => { };
        internal event Action OnCanNotShowMainImage = () => { };

        [SerializeField]
        Image frameBack;
        [SerializeField]
        Image mainImage;
        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        RectTransform zoomRectTransform;
        [SerializeField]
        Image lockImage;
        [SerializeField]
        Button mainImageDoneButton;

        public RectTransform MyRectTransform {
            get {
                return myRectTransform;
            }
        }

        bool isDone = false;

        internal void Init () {
            mainImageDoneButton.onClick.AddListener( ShowMainImage );
        }

        internal void SetMainImage ( Sprite mainImageSprite ) {
            mainImage.sprite = mainImageSprite;
        }

        internal void SetDone ( bool isDone ) {
            this.isDone = isDone;
            //if ( isDone ) {
            //}
            //else {
            //    lockImage.gameObject.SetActive( true );
            //}
            lockImage.gameObject.SetActive( !isDone );
            //frameBack.color = new Color( frameBack.color.r, frameBack.color.g, frameBack.color.b, 1f );
            Color colorMainImage = isDone ? new Color( 1f, 1f, 1f, 1f ) : new Color( 0.3f, 0.3f, 0.3f, 1f );
            mainImage.color = colorMainImage;
        }

        internal void SetPosition ( Vector2 newPosition ) {
            myRectTransform.anchoredPosition = newPosition;
        }

        internal void ShowImage ( float delay ) {
            //letterText.gameObject.SetActive( true );
            //myRectTransform.DOAnchorPosX( needPositionX, 0.2f );

            zoomRectTransform.DOKill();
            this.CustomInvoke( delay, () => {
                zoomRectTransform.DOScale( new Vector3( 1f, 1f ), 0.1f );
                //zoomRectTransform.DOShakeScale( 0.2f, 1f, 0, 0f, false );
            } );
        }

        internal void HideImage ( float delay ) {
            //letterText.gameObject.SetActive( false );

            //float sign = 1;
            //if ( clickPosX < 0 ) {
            //    sign = -1;
            //}

            //int revertIndex = sizeWord - 1 - indexLetter;
            //float movePositionX = ( 460f - ( offset * revertIndex ) ) * sign;
            //myRectTransform.DOAnchorPosX( movePositionX, 0.2f );

            zoomRectTransform.DOKill();
            this.CustomInvoke( delay, () => {
                zoomRectTransform.DOScale( new Vector3( 0f, 0f ), 0.1f );
            } );
        }

        void ShowMainImage () {
            if ( isDone ) {
                OnShowMainImage( mainImage.sprite );
                return;
            }

            OnCanNotShowMainImage();
        }
    }
}
