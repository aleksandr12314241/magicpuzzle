﻿using UnityEngine;

namespace Tutorial {
    public class HintClickFinger: MonoBehaviour {
        [SerializeField]
        RectTransform hintFingerClick;

        internal void SetFingerClickPivots ( Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot ) {
            hintFingerClick.anchorMin = anchorMin;
            hintFingerClick.anchorMax = anchorMax;
            hintFingerClick.pivot = pivot;
        }

        internal void ShowFingerClick ( Vector2 fingerClickPosition ) {
            hintFingerClick.anchoredPosition = fingerClickPosition;
            hintFingerClick.gameObject.SetActive( true );
        }

        internal void HideFingerClick () {
            hintFingerClick.gameObject.SetActive( false );
        }
    }
}