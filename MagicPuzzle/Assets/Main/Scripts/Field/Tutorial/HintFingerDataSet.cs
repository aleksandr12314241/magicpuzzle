﻿using System.Collections.Generic;
using Newtonsoft.Json;

public struct HintFingerDataSet {
    public List<string> levelNameWithTutorial;
    public List<List<string>> levelNameByWrongWords;

    [JsonConstructor]
    public HintFingerDataSet ( List<string> levelNameWithTutorial, List<List<string>> levelNameByWrongWords ) {
        this.levelNameWithTutorial = levelNameWithTutorial;
        this.levelNameByWrongWords = levelNameByWrongWords;
    }
}
