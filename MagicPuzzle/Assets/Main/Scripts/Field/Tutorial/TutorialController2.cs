﻿using UnityEngine;
using Common;
using Puzzle;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Tutorial {
    public class TutorialController2: MonoBehaviour {
        [SerializeField]
        PuzzleController puzzleController;
        [SerializeField]
        HintFinger hintFinger;
        [SerializeField]
        TabsContainer tabsContainer;
        [SerializeField]
        Animator shopAnimator;
        [SerializeField]
        Animator hintAnimator;
        [SerializeField]
        InfoUI infoUI;
        [SerializeField]
        Animator restartAnimator;
        [SerializeField]
        SelectionAreaSystem selectionAreaSystem;
        [SerializeField]
        HintClickFinger hintClickFinger;

        PuzzleModel puzzleModel;
        GameController _GC;

        IEnumerator Start () {
            puzzleModel = puzzleController.GetPuzzleModel();

            //yield return new WaitUntil( () => puzzleModel.GetQuest() != null );
            yield return new WaitUntil( () => true );

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            StartTutorial();
            //puzzleController.OnStartQuest += OnStartQuest;
            puzzleController.OnUseHint += OnUseHint;
            tabsContainer.OnOpenTab += OnOpenTab;
            puzzleController.OnWinLevel += OnWinQuest;
        }

        void StartTutorial () {
            CheckIsAnimShop();
            CheckIsAnimHint();
            puzzleController.OnWinLevel -= OnWinQuest_Tutorial3;
            puzzleController.OnRecognizeWord -= OnRecognizeWord_Tutorial4;
            puzzleController.OnUseRestartEnd -= OnUseRestartEnd_Tutorial4;
            puzzleController.OnWrongWord -= OnWrongWord_Tutorial4;

            puzzleController.GetPuzzleUpperDoneField().SetAllowHintWods( true );

            hintClickFinger.HideFingerClick();

            if ( _GC.numCatigory == 0 && _GC.numLevel == 0 ) {
                puzzleController.GetPuzzleUpperDoneField().SetAllowHintWods( false );
                return;
            }

            //if ( _GC.numCatigories == 0 && _GC.numQuest == 1 ) {
            //    InitTutorial2();
            //    return;
            //}

            if ( _GC.numCatigory == 0 && _GC.numLevel == 2 ) {
                InitTutorial3();
                return;
            }

            if ( _GC.numCatigory == 0 && _GC.numLevel == 3 ) {
                InitTutorial4();
                return;
            }
        }

        string startWordOfQuest;
        void InitTutorial2 () {
            //if ( !_GC.player.tutorialProgress2["rewert_word"] ) {
            //    startWordOfQuest = puzzleModel.GetLettersTaskByIndex( 0 );

            //    puzzleModel.SetLettersTaskByIndex( 0, "" );

            //    puzzleController.OnWrongWord -= OnWrongWord_Tutorial2;
            //    puzzleController.OnWrongWord += OnWrongWord_Tutorial2;

            //    puzzleController.GetPuzzleUpperDoneField().SetAllowHintWods( false );

            //    return;
            //}

            //ChechRewertQuest( puzzleModel.quest );
        }

        void OnWrongWord_Tutorial2 ( string word ) {
            //if ( _GC.numCatigory != 0 || _GC.numLevel != 1 ) {
            //    return;
            //}

            //string mainWord = puzzleModel.GetQuest().words[LocalizationManager.GetLanguageString()][0].ToUpper();
            //char[] mainCharArray = mainWord.ToCharArray();
            //Array.Reverse( mainCharArray );
            //string reverseWord = new string( mainCharArray );

            //if ( word.Length != startWordOfQuest.Length ) {
            //    return;
            //}

            //if ( ( !mainWord.Equals( word ) && !reverseWord.Equals( word ) ) ) {
            //    return;
            //}

            //puzzleController.OnWrongWord -= OnWrongWord_Tutorial2;


            //if ( !_GC.player.tutorialProgress2["rewert_word"] ) {
            //    _GC.player.DoneTutorial2( "rewert_word" );
            //    _GC.SavePlayer();
            //}

            //char[] charArray = word.ToCharArray();
            //Array.Reverse( charArray );
            //string revertWord = new string( charArray );
            //if ( !startWordOfQuest.Equals( revertWord ) ) {

            //    _GC.player.SetLevel2RevertSequence( true );
            //    _GC.SavePlayer();
            //}

            //ChechRewertQuest( puzzleModel.GetQuest() );

            //puzzleController.GetPuzzleUpperDoneField().SetAllowHintWods( true );
            //infoUI.ShowInfoWithTailAndPositionY( LocalizationManager.GetText( "txt_info_window_word_hint" ), 293f, BlockType.None, TailType.Up, 524f,
            //                                     false );
            //selectionAreaSystem.TryShowArea( "upHint" );

            //hintClickFinger.SetFingerClickPivots( new Vector2( 0.5f, 0.5f ), new Vector2( 0.5f, 0.5f ), new Vector2( 0.5f, 0.5f ) );
            //hintClickFinger.ShowFingerClick( new Vector2( 600f, 611f ) );
        }

        //void ChechRewertQuest ( Quests quests ) {
        //    //if ( _GC.numCatigory != 0 || _GC.numLevel != 1 ) {
        //    //    return;
        //    //}

        //    ////старт анимации клика

        //    //if ( !_GC.player.level2RevertSequence ) {
        //    //    hintFinger.StartShowFingerAnimation( quests );
        //    //    //puzzleModel.SetLettersTaskByIndex( 0, quests.words[LocalizationManager.GetLanguageString()][0] );
        //    //    return;
        //    //}

        //    //string word = quests.words[LocalizationManager.GetLanguageString()][0];
        //    //char[] charArray = word.ToCharArray();
        //    //Array.Reverse( charArray );

        //    //string letter = puzzleModel.letterDone[0][0].Letter;
        //    //puzzleModel.letterDone[0][0].SetLetter( puzzleModel.letterDone[0][2].Letter );
        //    //puzzleModel.letterDone[0][2].SetLetter( letter );

        //    //puzzleModel.SetLetterDoneHintByIndex( LocalizationManager.GetText( "txt_tutorial_puzzle_2_hint" ) );
        //    //puzzleModel.SetLettersTaskByIndex( 0, new string( charArray ) );
        //    //hintFinger.StartShowFingerAnimation( quests );
        //    //hintFinger.SetRevertSequence();
        //}

        void InitTutorial3 () {
            if ( !_GC.player.tutorialProgress2["is_click_on_hint"] && !_GC.player.tutorialProgress2["is_anim_hint"] ) {
                _GC.player.DoneTutorial2( "is_anim_hint" );
                _GC.SavePlayer();
            }
            CheckIsAnimHint();

            puzzleController.OnWinLevel -= OnWinQuest_Tutorial3;
            puzzleController.OnWinLevel += OnWinQuest_Tutorial3;
        }

        void OnWinQuest_Tutorial3 () {
            puzzleController.OnWinLevel -= OnWinQuest_Tutorial3;

            _GC.player.DoneTutorial2( "is_anim_shop" );
            _GC.SavePlayer();

            CheckIsAnimShop();
        }

        void OnUseHint ( bool isUse, MainImageDone letterDone ) {
            hintClickFinger.HideFingerClick();

            if ( _GC.player.tutorialProgress2["is_click_on_hint"] ) {
                return;
            }

            _GC.player.DoneTutorial2( "is_click_on_hint" );
            _GC.SavePlayer();

            CheckIsAnimHint();

            hintAnimator.enabled = false;

            RectTransform hintAnimatorRectTransform = hintAnimator.GetComponent<RectTransform>();
            hintAnimatorRectTransform.anchoredPosition = Vector2.zero;
            hintAnimatorRectTransform.rotation = Quaternion.identity;
            hintAnimatorRectTransform.localScale = Vector3.one;

            infoUI.ShowInfoWithTailAndPositionY(
                LocalizationManager.GetText( "txt_info_window_upper_hint_letter" ), 472f, BlockType.None, TailType.Up, -220f, false );
            selectionAreaSystem.TryShowArea( "hintOpenLetter" );

            if ( tabsContainer.OpenTab == 1 ) {
                InitTutorialDaily();
            }
        }

        void CheckIsAnimHint () {
            if ( !_GC.player.tutorialProgress2["is_anim_hint"] ) {
                return;
            }

            if ( _GC.player.tutorialProgress2["is_click_on_hint"] ) {
                return;
            }

            hintAnimator.enabled = true;
            selectionAreaSystem.TryShowArea( "hint" );
            infoUI.ShowInfoWithTailAndPositionY( LocalizationManager.GetText( "txt_info_window_hint" ), -411f, BlockType.None, TailType.Down, 392,
                                                 false );

            hintClickFinger.SetFingerClickPivots( new Vector2( 1f, 0f ), new Vector2( 1f, 0f ), new Vector2( 0.5f, 0.5f ) );
            hintClickFinger.ShowFingerClick( new Vector2( -437f, 517f ) );
        }

        void CheckIsAnimShop () {
            if ( !_GC.player.tutorialProgress2["is_anim_shop"] ) {
                return;
            }

            if ( _GC.player.tutorialProgress2["see_shop_tutorial"] ) {
                return;
            }

            shopAnimator.enabled = true;
        }

        void OnOpenTab ( int indexTab ) {
            if ( indexTab == 3 ) {
                InitTutorialShop();
            }
            else if ( indexTab == 1 ) {
                InitTutorialDaily();
            }
        }

        void InitTutorialShop () {
            if ( !_GC.player.tutorialProgress2["is_anim_shop"] ) {
                return;
            }

            if ( _GC.player.tutorialProgress2["see_shop_tutorial"] ) {
                return;
            }

            _GC.player.DoneTutorial2( "see_shop_tutorial" );
            _GC.SavePlayer();
            CheckIsAnimShop();

            shopAnimator.enabled = false;

            RectTransform shopAnimatorRectTransform = shopAnimator.GetComponent<RectTransform>();
            shopAnimatorRectTransform.anchoredPosition = Vector2.zero;
            shopAnimatorRectTransform.rotation = Quaternion.identity;
            shopAnimatorRectTransform.localScale = Vector3.one;

            infoUI.ShowInfo( LocalizationManager.GetText( "txt_info_window_shop" ), BlockType.Block );
        }

        void InitTutorialDaily() {
            if ( !_GC.player.tutorialProgress2["is_click_on_hint"] ) {
                return;
            }

            if ( _GC.player.tutorialProgress2["see_daily_tutorial"] ) {
                return;
            }

            _GC.player.DoneTutorial2( "see_daily_tutorial" );
            _GC.SavePlayer();

            infoUI.ShowInfo( LocalizationManager.GetText( "txt_info_window_daily_puzzle" ) );
        }

        void InitTutorial4 () {
            if ( !_GC.player.tutorialProgress2["two_words_wrong_word_first"] ) {
                puzzleController.OnRecognizeWord -= OnRecognizeWord_Tutorial4;
                puzzleController.OnRecognizeWord += OnRecognizeWord_Tutorial4;

                puzzleModel.listPieces[1][0].pieceCntr.DiActivateLetter();
                puzzleModel.listPieces[0][2].pieceCntr.DiActivateLetter();
                puzzleModel.listPieces[1][2].pieceCntr.DiActivateLetter();

                List<ArrayIndexMatrinx> indexLetters = new List<ArrayIndexMatrinx>();
                ArrayIndexMatrinx arrayIndexMatrinx = new ArrayIndexMatrinx();
                arrayIndexMatrinx.AddNewLetterIndex( new IndexMatrinx( 1, 1 ) );
                arrayIndexMatrinx.AddNewLetterIndex( new IndexMatrinx( 0, 1 ) );
                arrayIndexMatrinx.AddNewLetterIndex( new IndexMatrinx( 0, 0 ) );
                indexLetters.Add( arrayIndexMatrinx );

                //hintFinger.StartShowFingerAnimation( puzzleModel.quest.nameLevel, indexLetters, 0 );
                return;
            }
            else if ( !_GC.player.tutorialProgress2["two_words_wrong_word_second"] ) {
                hintFinger.StopTutorial();

                puzzleController.OnWrongWord -= OnWrongWord_Tutorial4;
                puzzleController.OnWrongWord += OnWrongWord_Tutorial4;

                puzzleController.OnUseRestartEnd -= OnUseRestartEnd_Tutorial4;
                puzzleController.OnUseRestartEnd += OnUseRestartEnd_Tutorial4;
                return;
            }
            SetHintFinger_Tutorial4();
        }

        void OnRecognizeWord_Tutorial4 ( int indexTemp ) {
            hintFinger.StopTutorial();

            puzzleController.OnRecognizeWord -= OnRecognizeWord_Tutorial4;
            puzzleController.OnUseRestartEnd -= OnUseRestartEnd_Tutorial4;
            puzzleController.OnUseRestartEnd += OnUseRestartEnd_Tutorial4;

            puzzleModel.listPieces[1][0].pieceCntr.ActivateLetter();
            puzzleModel.listPieces[0][2].pieceCntr.ActivateLetter();
            puzzleModel.listPieces[1][2].pieceCntr.ActivateLetter();

            puzzleController.OnWrongWord -= OnWrongWord_Tutorial4;
            puzzleController.OnWrongWord += OnWrongWord_Tutorial4;

            _GC.player.DoneTutorial2( "two_words_wrong_word_first" );

            SaveFieldState_Tutorial4();
        }

        void SaveFieldState_Tutorial4 () {
            string[] arrayLanguages = Enum.GetNames( typeof( LocalizationManager.Language ) );
            for ( int i = 0; i < arrayLanguages.Length; i++ ) {
                if ( arrayLanguages[i] == LocalizationManager.GetLanguageString() ) {
                    continue;
                }

                //_GC.player.AddProgressByLanguageFieldState( arrayLanguages[i], 1, new List<Vector2Int>() {
                //    new Vector2Int( 1, 1 ),
                //        new Vector2Int( 0, 1 ),
                //        new Vector2Int( 0, 0 )
                //} );
            }
        }

        void OnWrongWord_Tutorial4 ( string word ) {
            if ( word == "" ) {
                return;
            }

            infoUI.ShowInfoWithTailAndPositionY( LocalizationManager.GetText( "txt_info_window_restart" ), -411f, BlockType.None, TailType.Down, -392,
                                                 false );
            selectionAreaSystem.TryShowArea( "restart" );
            hintClickFinger.SetFingerClickPivots( Vector2.zero, Vector2.zero, new Vector2( 0.5f, 0.5f ) );
            hintClickFinger.ShowFingerClick( new Vector2( 226f, 508f ) );

            restartAnimator.enabled = true;
        }

        void OnUseRestartEnd_Tutorial4 () {
            puzzleController.OnUseRestartEnd -= OnUseRestartEnd_Tutorial4;
            puzzleController.OnWrongWord -= OnWrongWord_Tutorial4;

            hintClickFinger.HideFingerClick();

            _GC.player.DoneTutorial2( "two_words_wrong_word_first" );
            _GC.player.DoneTutorial2( "two_words_wrong_word_second" );
            _GC.SavePlayer();

            restartAnimator.enabled = false;
            RectTransform restartAnimatorRectTransform = restartAnimator.GetComponent<RectTransform>();
            restartAnimatorRectTransform.anchoredPosition = Vector2.zero;
            restartAnimatorRectTransform.rotation = Quaternion.identity;
            restartAnimatorRectTransform.localScale = Vector3.one;

            selectionAreaSystem.TryShowArea( "restart_second" );
            infoUI.ShowInfoWithTailAndPositionY( LocalizationManager.GetText( "txt_info_window_restart_second" ), -541f, BlockType.None, TailType.Up,
                                                 336f,
                                                 false );

            for ( int i = 1; i < puzzleModel.listPieces.Length - 1; i++ ) {
                for ( int j = 0; j < puzzleModel.listPieces[i].Length; j++ ) {
                    if ( puzzleModel.listPieces[i][j].isEmpty ) {
                        continue;
                    }

                    puzzleModel.listPieces[i][j].pieceCntr.ActivateLetter();
                }
            }

            SetHintFinger_Tutorial4();
            ClearFieldState_Tutorial4();
        }

        private void ClearFieldState_Tutorial4 () {
            string[] arrayLanguages = Enum.GetNames( typeof( LocalizationManager.Language ) );
            for ( int i = 0; i < arrayLanguages.Length; i++ ) {
                if ( arrayLanguages[i] == LocalizationManager.GetLanguageString() ) {
                    continue;
                }

                //_GC.player.ClearProgressByLanguageFieldState( arrayLanguages[i] );
            }
        }

        void SetHintFinger_Tutorial4 () {
            //hintFinger.StartShowFingerAnimation( puzzleModel.GetQuest() );
        }

        void OnWinQuest () {
            if ( _GC.numCatigory == 0 && _GC.numLevel == 0 ) {
                SaveQuestToAllLanguage( 1 );
            }
            else if ( _GC.numCatigory == 0 && _GC.numLevel == 1 ) {
                SaveQuestToAllLanguage( 2 );
            }
            else if ( _GC.numCatigory == 0 && _GC.numLevel == 2 ) {
                SaveQuestToAllLanguage( 3 );
            }
            else if ( _GC.numCatigory == 0 && _GC.numLevel == 3 ) {
                SaveQuestToAllLanguage( 4 );
                ClearFieldState_Tutorial4();
            }
        }

        void SaveQuestToAllLanguage ( int numQuest ) {
            string[] arrayLanguages = Enum.GetNames( typeof( LocalizationManager.Language ) );
            for ( int i = 0; i < arrayLanguages.Length; i++ ) {
                if ( arrayLanguages[i] == LocalizationManager.GetLanguageString() ) {
                    continue;
                }

                //if ( _GC.player.progressByLanguage[arrayLanguages[i]].quest >= numQuest ) {
                //    continue;
                //}

                //_GC.player.AddProgressByLanguageQuestNoAction( arrayLanguages[i], numQuest );
            }
        }
    }
}
