﻿using UnityEngine;
using Puzzle;
using Common;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Tutorial {
    public class HintFinger: MonoBehaviour {
        [SerializeField]
        RectTransform _fingerRectTransform;
        [SerializeField]
        Image _fingerImage;
        [SerializeField]
        PuzzleController _puzzelController;
        [SerializeField]
        RectTransform mainPuzzelTab;
        [SerializeField]
        TrailSystemGameObject trailSystem;

        int letterIndex;
        int correctLetter;
        ArrayIndexMatrinx arayIndexMatrinx;
        bool showFinger;
        Vector2 offsetFinger;
        GameController _GC;
        int wrongWordCount;
        string nameLevel;
        PuzzleModel puzzleModel;

        void Start () {

            nameLevel = "";
            wrongWordCount = 0;
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            puzzleModel = _puzzelController.GetPuzzleModel();

            //if ( puzzleModel.GetQuest() != null ) {
            //if ( _GC.hintFingerDataSet.levelNameWithTutorial.Contains( puzzleModel.GetQuest().nameLevel ) ) {
            //    StartShowFingerAnimation( puzzleModel.GetQuest() );
            //}
            //}
            offsetFinger = new Vector2( 0f, ( mainPuzzelTab.sizeDelta.y / 2 ) * -1 );

            //_puzzelController.OnStartQuest += OnStartQuest;
            _puzzelController.OnWrongWord += OnWrongWord;
            _puzzelController.OnWinLevel += OnWinQuest;
            _puzzelController.OnClickButtonBack += OnClickButtonBack;
            _puzzelController.OnActivePuzzle += OnActivePuzzle;
            _puzzelController.OnRecognizeWord += OnRecognizeWord;
        }

        internal void StartShowFingerAnimation ( string nameLevel, List<ArrayIndexMatrinx> indexLetters ) {
            //int notDoneTask = puzzleModel.GetNotDoneTask();

            //StartShowFingerAnimation( nameLevel, indexLetters, notDoneTask );
        }

        internal void StartShowFingerAnimation ( string nameLevel, List<ArrayIndexMatrinx> indexLetters, int notDoneTask ) {
            this.nameLevel = nameLevel;

            if ( notDoneTask < 0 ) {
                return;
            }

            wrongWordCount = 0;
            correctLetter = 0;

            _fingerImage.DOKill();
            _fingerRectTransform.DOKill();

            _fingerImage.color = new Color( 1f, 1f, 1f, 1f );

            arayIndexMatrinx = indexLetters[notDoneTask];
            ShowTutorial();
        }

        internal void SetRevertSequence () {
            if ( arayIndexMatrinx == null ) {
                return;
            }

            HideTutorial();

            List<IndexMatrinx> indexLettersRevert = new List<IndexMatrinx>();
            for ( int i = arayIndexMatrinx.indexLetters.Count - 1; i >= 0; i-- ) {
                indexLettersRevert.Add( arayIndexMatrinx.indexLetters[i] );
            }

            arayIndexMatrinx.indexLetters = indexLettersRevert;
            ShowTutorial();
        }

        void EndMoveToLetter () {
            if ( !showFinger ) {
                return;
            }

            float timeAnimation = 0.8f;

            letterIndex++;
            if ( letterIndex >= arayIndexMatrinx.indexLetters.Count ) {
                letterIndex = 0;
                timeAnimation = 1f;

                trailSystem.UnsetFollowObject();
                Color colorTemp = _fingerImage.color;

                _fingerImage.DOColor( new Color( colorTemp.r, colorTemp.g, colorTemp.b, 0f ), 0.5f ).OnComplete( () => {
                    int indexITemp = arayIndexMatrinx.indexLetters[letterIndex].indI;
                    int indexJTemp = arayIndexMatrinx.indexLetters[letterIndex].indJ;
                    Vector2 positionTemp = puzzleModel.GetLettersLevelPositionByIndex( indexITemp, indexJTemp ) + offsetFinger;
                    _fingerRectTransform.anchoredPosition = positionTemp;
                    letterIndex++;
                    indexITemp = arayIndexMatrinx.indexLetters[letterIndex].indI;
                    indexJTemp = arayIndexMatrinx.indexLetters[letterIndex].indJ;

                    positionTemp = puzzleModel.GetLettersLevelPositionByIndex( indexITemp, indexJTemp ) + offsetFinger;

                    _fingerImage.DOColor( new Color( colorTemp.r, colorTemp.g, colorTemp.b, 1f ), 0.5f ).OnComplete( () => {
                        trailSystem.SetFollowObject();

                        _fingerRectTransform.DOAnchorPos( positionTemp, timeAnimation ).OnComplete( EndMoveToLetter );
                    } );
                } );
                return;
            }

            trailSystem.SetFollowObject();
            int indexI = arayIndexMatrinx.indexLetters[letterIndex].indI;
            int indexJ = arayIndexMatrinx.indexLetters[letterIndex].indJ;
            Vector2 position = puzzleModel.GetLettersLevelPositionByIndex( indexI, indexJ ) + offsetFinger;
            _fingerRectTransform.DOAnchorPos( position, timeAnimation ).OnComplete( EndMoveToLetter );
        }

        void OnStartHideLetter ( PieceFieldController pieceController ) {
            correctLetter++;

            if ( correctLetter >= ( arayIndexMatrinx.indexLetters.Count / 2 ) ) {
                HideTutorial();
            }
        }

        void OnWrongWord ( string word ) {
            _fingerImage.DOKill();

            //if ( !nameLevel.Equals( puzzleModel.GetQuest().nameLevel ) ) {
            //    if ( wrongWordCount < _GC.hintFingerDataSet.levelNameByWrongWords.Count ) {
            //        if ( _GC.hintFingerDataSet.levelNameByWrongWords[wrongWordCount].Contains( puzzleModel.GetQuest().nameLevel ) ) {
            //            StartShowFingerAnimation( puzzleModel.GetQuest() );
            //        }
            //        wrongWordCount++;
            //    }
            //    return;
            //}

            if ( showFinger ) {
                return;
            }

            showFinger = true;
            correctLetter = 0;
            _fingerRectTransform.gameObject.SetActive( true );

            letterIndex = 0;
            int indexI = arayIndexMatrinx.indexLetters[letterIndex].indI;
            int indexJ = arayIndexMatrinx.indexLetters[letterIndex].indJ;

            _fingerImage.color = new Color( 1f, 1f, 1f, 1f );

            Vector2 position = puzzleModel.GetLettersLevelPositionByIndex( indexI, indexJ ) + offsetFinger;
            _fingerRectTransform.anchoredPosition = position;
            _fingerRectTransform.DOAnchorPos( position, 0.5f ).OnComplete( EndMoveToLetter );
        }

        internal void ShowTutorial () {
            if ( !_puzzelController.GetIsActivePuzzle() ) {
                return;
            }

            if ( arayIndexMatrinx == null ) {
                return;
            }

            if ( showFinger ) {
                return;
            }

            _fingerRectTransform.gameObject.SetActive( true );
            showFinger = true;
            letterIndex = 0;
            int indexI = arayIndexMatrinx.indexLetters[letterIndex].indI;
            int indexJ = arayIndexMatrinx.indexLetters[letterIndex].indJ;

            Vector2 position = puzzleModel.GetLettersLevelPositionByIndex( indexI, indexJ ) + offsetFinger;
            _fingerRectTransform.anchoredPosition = position;
            _fingerRectTransform.DOAnchorPos( position, 0.8f ).OnComplete( EndMoveToLetter );

            trailSystem.SetFollowObject();

            _puzzelController.OnStartHidePiece -= OnStartHideLetter;
            _puzzelController.OnStartHidePiece += OnStartHideLetter;
        }

        internal void HideTutorial () {
            _fingerImage.DOKill();
            _fingerRectTransform.DOKill();
            _fingerRectTransform.gameObject.SetActive( false );
            showFinger = false;
            trailSystem.UnsetFollowObject();
            trailSystem.UnsetFollowObjectImmediately();
        }

        internal void StopTutorial () {
            nameLevel = "";
            HideTutorial();
        }

        void OnWinQuest () {
            _puzzelController.OnStartHidePiece -= OnStartHideLetter;

            wrongWordCount = 0;
            nameLevel = "";
            HideTutorial();
        }

        void OnClickButtonBack () {
            _puzzelController.OnStartHidePiece -= OnStartHideLetter;

            wrongWordCount = 0;
            nameLevel = "";
            HideTutorial();
        }

        void OnActivePuzzle ( bool activePuzzle ) {
            //if ( puzzleModel.GetQuest() == null ) {
            //    return;
            //}

            //if ( !puzzleModel.GetQuest().nameLevel.Equals( nameLevel ) ) {
            //    return;
            //}

            if ( activePuzzle ) {
                ShowTutorial();
            }
            else {
                HideTutorial();
            }
        }

        void OnRecognizeWord ( int indexWord ) {
            _fingerImage.DOKill();

            //if ( !nameLevel.Equals( puzzleModel.GetQuest().nameLevel ) ) {
            //    return;
            //}

            //int notDoneTask = puzzleModel.GetNotDoneTask();

            //if ( notDoneTask == -1 ) {
            //    return;
            //}

            //StartShowFingerAnimation( puzzleModel.GetQuest() );
        }
    }
}
