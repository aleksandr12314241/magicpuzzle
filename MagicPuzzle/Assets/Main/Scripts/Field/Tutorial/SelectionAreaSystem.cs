﻿using UnityEngine;
using Utility;

namespace Tutorial {
    public class SelectionAreaSystem: MonoBehaviour {
        [SerializeField]
        GameObject selectionContainer;
        [SerializeField]
        RectTransform upImage;
        [SerializeField]
        RectTransform rightImage;
        [SerializeField]
        RectTransform leftImage;
        [SerializeField]
        RectTransform downImage;
        [SerializeField]
        SelectionAreaSystemDataSet selectionAreaSystemDataSet;

        bool isShow = false;
        internal void TryShowArea ( string tutorialId ) {
            SelectionAreaSystemDataSet.SelectionAreaSet selectionAreaSet =
                selectionAreaSystemDataSet.GetSelectionAreaSetById( tutorialId );

            if ( selectionAreaSet == null ) {
                return;
            }

            upImage.anchoredPosition = selectionAreaSet.UpPosition;
            rightImage.anchoredPosition = selectionAreaSet.RightPosition;
            leftImage.anchoredPosition = selectionAreaSet.LeftPosition;
            downImage.anchoredPosition = selectionAreaSet.DownPosition;

            selectionContainer.gameObject.SetActive( true );

            this.CustomInvoke( 0.5f, () => {
                isShow = true;
            } );
        }

        private void Update () {
            if ( !isShow ) {
                return;
            }

            if ( Input.GetMouseButtonDown( 0 ) ) {
                HideArea();
            }
        }

        void HideArea () {
            isShow = false;

            selectionContainer.gameObject.SetActive( false );
        }
    }
}