﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryData {
    public string nameIdCategory;
    public List<SubCategoryData> subCategoryData;

    public CategoryData ( string nameIdCategory, List<SubCategoryData> subCategoryData ) {
        this.nameIdCategory = nameIdCategory;
        this.subCategoryData = subCategoryData;
    }

    //GET
    public string GetNameIdCategory () {
        return nameIdCategory;
    }
    public List<SubCategoryData> GetSubCategoryData () {
        return subCategoryData;
    }
    public SubCategoryData GetSubCategoryDataByIndex ( int indexSubCategory ) {
        return subCategoryData[indexSubCategory];
    }
}
