﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Common {
    public struct CatigoriesQuests {
        public string nameCatigorie;
        public List<string> nameQuest;

        [JsonConstructor]
        public CatigoriesQuests ( string nameCatigorie, List<string> nameQuest ) {
            this.nameCatigorie = nameCatigorie;
            this.nameQuest = nameQuest;
        }
    }
}
