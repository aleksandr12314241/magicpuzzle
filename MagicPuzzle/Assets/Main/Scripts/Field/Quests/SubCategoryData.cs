﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubCategoryData {
    public string nameIdSubCategory;
    public List<string> imagesName;

    public SubCategoryData ( string nameIdSubCategory, List<string> imageName ) {
        this.nameIdSubCategory = nameIdSubCategory;
        this.imagesName = imageName;
    }

    //GET
    public string GetNameIdSubCategory () {
        return nameIdSubCategory;
    }
    public List<string> GetImagesName () {
        return imagesName;
    }
    public string GetImageNameByIndex ( int indexImage ) {
        return imagesName[indexImage];
    }
}
