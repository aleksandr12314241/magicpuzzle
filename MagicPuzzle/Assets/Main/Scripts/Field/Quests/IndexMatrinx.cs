﻿
namespace Common {
    public class IndexMatrinx {
        public int indI;
        public int indJ;

        public bool isHint;

        public IndexMatrinx () {

            indI = -1;
            indJ = -1;
            isHint = false;
        }

        public IndexMatrinx ( int newIndI, int newIndJ ) {

            indI = newIndI;
            indJ = newIndJ;
            isHint = false;
        }

        public IndexMatrinx ( IndexMatrinx indexMatrinx ) {

            indI = indexMatrinx.indI;
            indJ = indexMatrinx.indJ;
            isHint = false;
        }

        public override int GetHashCode () {
            return base.GetHashCode();
        }

        public override bool Equals ( object obj ) {
            if ( !( obj is IndexMatrinx ) ) {
                return false;
            }
            IndexMatrinx indexMatrinx = ( IndexMatrinx ) obj;
            return indI == indexMatrinx.indI && indJ == indexMatrinx.indJ;
        }
    }
}
