﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryList {
    public List<CategoryData> category;

    public CategoryList ( List<CategoryData> category ) {
        this.category = category;
    }

    //GET
    public List<CategoryData> GetCategory () {
        return category;
    }
    public CategoryData GetCategoryByIndex ( int index ) {
        return category[index];
    }
}
