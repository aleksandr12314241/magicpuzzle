﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Utility;

namespace Puzzle {
    internal class LetterDoneFly: MonoBehaviour {
        [SerializeField]
        GameObject letterDoneUIFlyContainer;
        [SerializeField]
        RectTransform myRecTransform;
        [SerializeField]
        Text letterText;
        [SerializeField]
        Text letterTextAnimation;
        [SerializeField]
        Animator letterAnimator;

        Vector2 startPosition;
        Vector2 endPosition;

        RectTransform letterTextRectTransform;
        Color colorBack;

        private void Awake () {
            letterTextRectTransform = letterText.GetComponent<RectTransform>();
        }

        internal void SetStart ( Vector2 newStartPosition ) {
            startPosition = newStartPosition;
            myRecTransform.anchoredPosition = startPosition;
            //letterText.text = letter;
            //letterTextAnimation.text = letter;
            letterText.enabled = false;

            letterAnimator.SetBool( "end_fly_animation", false );
        }

        internal void StartFly ( Vector2 newEndPos, float timeFly ) {
            letterText.enabled = true;

            endPosition = newEndPos;
            myRecTransform.anchoredPosition = startPosition;
            myRecTransform.DOAnchorPos( endPosition, timeFly ).OnComplete( () => {
                letterAnimator.SetBool( "end_fly_animation", true );
            } );
        }

        internal void EndFlyImmediately ( Vector2 newEndPos ) {
            letterText.enabled = true;
            myRecTransform.anchoredPosition = newEndPos;
        }

        internal void RestartHide () {
            myRecTransform.DOKill();

            letterAnimator.SetBool( "end_fly_animation", false );
            myRecTransform.anchoredPosition = startPosition;
            letterText.enabled = false;
        }

        internal void StartPositionAnimation ( int indexDelay ) {
            if ( !gameObject.activeSelf ) {
                return;
            }

            letterTextRectTransform.DOKill();

            float delay = indexDelay / 4f;

            this.CustomInvoke( delay, () => {
                if ( !gameObject.activeSelf ) {
                    return;
                }

                letterTextRectTransform.DOAnchorPosY( 20f, 0.5f ).OnComplete( () => {
                    letterTextRectTransform.DOAnchorPosY( 0f, 0.5f );
                } );
            } );
        }

        internal void ShowLetter () {
            gameObject.SetActive( true );
        }

        internal void HideLetter () {
            gameObject.SetActive( false );
        }
    }
}