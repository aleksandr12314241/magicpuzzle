﻿using UnityEngine;
using UnityEngine.UI;

public class CircleAnimUIOne : MonoBehaviour {
    [SerializeField]
    Image circleImage;
    [SerializeField]
    Vector2 scaleRange;
    [SerializeField]
    Vector2 alpaRange;

    Vector2 needPosition;
    float speed;
    RectTransform myTansform;

    internal void Init ( Color colorCircle ) {
        myTansform = this.gameObject.GetComponent<RectTransform>();
        myTansform.anchoredPosition = new Vector2( Random.Range( -2000f, 2000f ), Random.Range( -1000f, 1000f ) );
        needPosition = new Vector2( Random.Range( -1000f, 1000f ), Random.Range( -1000f, 1000f ) );

        speed = Random.Range( 10f, 30f );
        float scaleFactor = Random.Range( scaleRange.x, scaleRange.y );
        myTansform.localScale = new Vector3( scaleFactor, scaleFactor, 1f );
        circleImage.color = new Color( colorCircle.r, colorCircle.g, colorCircle.b, Random.Range( alpaRange.x, alpaRange.y ) );
    }

    internal void UpdateCircle () {

        if ( Vector2.Distance( myTansform.anchoredPosition, needPosition ) <= speed * Time.deltaTime ) {

            needPosition = new Vector2( Random.Range( -1000f, 1000f ), Random.Range( -1000f, 1000f ) );
            speed = Random.Range( 10f, 30f );
        }
        else {
            myTansform.anchoredPosition = Vector2.MoveTowards( myTansform.anchoredPosition, needPosition, speed * Time.deltaTime );
        }
    }
}
