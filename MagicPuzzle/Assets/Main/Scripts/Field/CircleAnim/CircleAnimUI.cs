﻿using UnityEngine;
using Common;
namespace Puzzle {
    public class CircleAnimUI: MonoBehaviour {
        [SerializeField]
        ColorDataSet colorDataSet;
        [SerializeField]
        CircleAnimUIOne[] circlesAnimUIOne;

        void Start () {
            for ( int i = 0; i < circlesAnimUIOne.Length; i++ ) {
                circlesAnimUIOne[i].Init( colorDataSet.GetRandomColor() );
            }
        }

        void Update () {
            for ( int i = 0; i < circlesAnimUIOne.Length; i++ ) {
                circlesAnimUIOne[i].UpdateCircle();
            }
        }
    }
}
