﻿using UnityEngine;

namespace Menu {
    public class LampPool: MonoBehaviour {
        [SerializeField]
        RectTransform[] lamps;

        internal int GetSizePool () {
            return lamps.Length;
        }

        internal void DiactivateAllLamps () {
            for ( int i = 0; i < lamps.Length; i++ ) {
                lamps[i].gameObject.SetActive( false );
                lamps[i].anchoredPosition = new Vector2( 0f, 1013f );
            }
        }

        internal void ActivateRandomLamp () {
            for ( int i = 0; i < lamps.Length; i++ ) {
                if ( !lamps[i].gameObject.activeSelf ) {
                    ActivateRandomLamp( i );
                    return;
                }
            }
        }

        void ActivateRandomLamp ( int indexLamp ) {
            lamps[indexLamp].anchoredPosition = new Vector2( Random.Range( -473f, 473f ), 1013f );
            lamps[indexLamp].localEulerAngles = new Vector3( 0f, 0f, Random.Range( -45f, 45f ) );
            lamps[indexLamp].gameObject.SetActive( true );
        }
    }
}
