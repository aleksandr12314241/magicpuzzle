﻿using UnityEngine;
using System.Collections;
using Common;
using UnityEngine.UI;
using Utility;
using System;

namespace Menu {
    public class HintAddedUI: WindowMonoBehaviour {
        public event Action OnFinDailyLevel = () => { };

        [SerializeField]
        AudioSource myAudioSource;
        [SerializeField]
        Text hintAddedText;
        [SerializeField]
        Animator _myAnimator;
        [SerializeField]
        Button _backClick;
        [SerializeField]
        GameObject floor;
        [SerializeField]
        LampPool lampPool;
        [SerializeField]
        Animator lampAnimator;
        [SerializeField]
        Button okButton;
        [SerializeField]
        Button moreButton;
        [SerializeField]
        Image lightImage;

        bool _isStartHide;
        bool _isDaily;

        int countHintAnimation;
        RectTransform okButtonRectTransform;
        RectTransform moreButtonRectTransform;

        GameController _GC;
        AdsManager adsManager;

        LampsFly lampsFly;

        internal override void InitAwake () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            adsManager = _GC.GetComponent<AdsManager>();

            lampsFly = GameObject.FindObjectOfType<LampsFly>();

            _myAnimator.enabled = false;
            _backClick.onClick.AddListener( OnBackClick );
            okButton.onClick.AddListener( OnBackClick );
            okButtonRectTransform = okButton.GetComponent<RectTransform>();
            moreButton.onClick.AddListener( OnMoreButton );
            moreButtonRectTransform = moreButton.GetComponent<RectTransform>();
        }

        public void ShowHintAdd ( int newHint, HintAddedType hintAddedType ) {
            base.Show();
            SetCountHintAnimation( newHint );
            SetButtons( hintAddedType );

            _isDaily = false;

            lampAnimator.gameObject.SetActive( false );
            lampAnimator.enabled = false;
            lampAnimator.gameObject.SetActive( true );

            hintAddedText.text = "+" + newHint.ToString();

            myAudioSource.Play();
            _isStartHide = false;
            _myAnimator.enabled = true;

            _backClick.interactable = false;
            okButton.interactable = false;

            Invoke( "EndShow", 0.4f );
        }

        public void ShowHintDailyAdd ( int newHint ) {
            base.Show();
            SetCountHintAnimation( newHint );
            SetButtons( HintAddedType.ByDaily );

            _isDaily = true;

            lampAnimator.gameObject.SetActive( false );
            lampAnimator.enabled = false;
            lampAnimator.gameObject.SetActive( true );

            hintAddedText.text = "+" + newHint.ToString();

            myAudioSource.Play();
            _isStartHide = false;
            _myAnimator.enabled = true;

            _backClick.interactable = false;
            okButton.interactable = false;

            Invoke( "EndShow", 0.4f );
        }

        void SetButtons ( HintAddedType hintAddedType ) {
            if ( hintAddedType.Equals( HintAddedType.ByVideo1 ) ) {
                moreButtonRectTransform.gameObject.SetActive( true );
                okButtonRectTransform.anchoredPosition = new Vector2( -300f, okButtonRectTransform.anchoredPosition.y );
                moreButtonRectTransform.anchoredPosition = new Vector2( 300f, moreButtonRectTransform.anchoredPosition.y );
                return;
            }

            moreButtonRectTransform.gameObject.SetActive( false );
            okButtonRectTransform.anchoredPosition = new Vector2( 0f, okButtonRectTransform.anchoredPosition.y );
        }

        void SetCountHintAnimation ( int newHint ) {
            countHintAnimation = newHint;
            if ( countHintAnimation > lampPool.GetSizePool() ) {
                countHintAnimation = lampPool.GetSizePool();
            }
        }

        void EndShow () {
            lampAnimator.enabled = true;
            floor.SetActive( true );

            _backClick.interactable = true;
            okButton.interactable = true;

            for ( int i = 0; i < countHintAnimation; i++ ) {
                Invoke( "ActivateRandomLamp", i / 10f );

                //this.CustomInvoke( i / 10f, () => {
                //    lampPool.ActivateRandomLamp();
                //} );
            }
            if ( !_isStartHide ) {
                //Invoke( "ActivateRandomLamp", i / 10f );
                //this.CustomInvoke( 0.8f, () => {
                //    AudioManager.instance.PlaySoundByType( SoundType.AddLamp );
                //} );
                Invoke( "PlaySound", 0.8f );

            }

        }

        void ActivateRandomLamp () {
            if ( _isStartHide ) {
                return;
            }

            lampPool.ActivateRandomLamp();
        }

        void PlaySound () {
            if ( _isStartHide ) {
                return;
            }

            AudioManager.instance.PlaySoundByType( SoundType.AddLamp );
        }

        IEnumerator StartHideWindow () {
            yield return new WaitForSeconds( 1.5f );
            if ( !_isStartHide ) {
                StartHide();
            }
        }

        void OnBackClick () {
            StartHide();
        }

        void OnMoreButton () {
            if( !adsManager.ShowRewardedAdHint2() ) {
                return;
            }

            HideEnd();
        }

        void StartHide () {
            if ( _isStartHide ) {
                return;
            }

            if ( _isDaily ) {
                OnFinDailyLevel();
            }

            _isStartHide = true;
            _myAnimator.SetTrigger( "startHide" );
            Invoke( "HideEnd", 0.3f );
        }

        void HideEnd () {
            lampsFly.StartFlyLampsToUI( countHintAnimation );

            base.Hide();

            lampPool.DiactivateAllLamps();
            _myAnimator.enabled = false;
            lampAnimator.Play( "hint_added_ui_lamp" );
            lampAnimator.gameObject.SetActive( false );
            lightImage.color = new Color( lightImage.color.r, lightImage.color.g, lightImage.color.b, 0f );
        }
    }
}
