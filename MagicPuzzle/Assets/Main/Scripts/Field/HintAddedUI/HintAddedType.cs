﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu {
    public enum HintAddedType {
        ByVideo1,
        ByVideo2,
        ByBuying,
        ByDaily
    }
}
