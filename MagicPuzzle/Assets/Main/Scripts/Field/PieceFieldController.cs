﻿using UnityEngine;
using System;
using UnityEngine.UI;
using Common;
using DG.Tweening;
using System.Collections;
using UnityEngine.EventSystems;
using Utility;

namespace Puzzle {
    internal class PieceFieldController: MonoBehaviour {

        internal event Action<int, int> OnDownPiece = ( int indexI, int indexJ ) => { };
        internal event Action<int, int> OnUpPiece = ( int indexI, int indexJ ) => { };
        internal event Action<int, int> OnUpPieceNoLogic = ( int indexI, int indexJ ) => { };
        //internal event Action<int, int> OnEnterPiece = ( int indexI, int indexJ ) => { };
        //internal event Action<int, int> OnExitPiece = ( int indexI, int indexJ ) => { };
        internal event Action<int, int> OnAddPiece = ( int indexI, int indexJ ) => { };

        [SerializeField]
        EventTrigger letterClick;
        [SerializeField]
        RectTransform _myRectTransform;
        [SerializeField]
        AudioSource myAudioSourse;
        [SerializeField]
        Image backImage;
        [SerializeField]
        CircleCollider2D myCollider;
        [SerializeField]
        RectTransform childTransform;
        [SerializeField]
        GameObject imageHighlightMask;
        [SerializeField]
        RectTransform imageHighlight;
        [SerializeField]
        RectTransform overlayTransform;
        [SerializeField]
        CanvasGroup zoomCanvasGroup;
        [SerializeField]
        Image fillCircleImage;
        [SerializeField]
        GameObject revertObject;
        [SerializeField]
        Text revertText;
        [SerializeField]
        GameObject changePieceObject;
        [SerializeField]
        Text changePieceText;
        [SerializeField]
        Text order;

        int indexI;
        int indexJ;

        internal RectTransform MyRectTransform {
            get {
                return _myRectTransform;
            }
        }

        bool isInit = false;
        GameLinks gameLinks;

        float endScaleShowAnimationValue;
        int countScaleShowAnimation;
        bool isShow = true;
        internal bool IsShow {
            get {
                return isShow;
            }
        }
        bool endHideAnimation = false;

        Coroutine hideAnimation;
        Coroutine showAnimation;
        Coroutine startPuzzleAnimation;

        Coroutine highlightAnimation;
        Coroutine shakeAnimation;
        Coroutine positionAnimation;

        Coroutine customClick;

        Color colorBack;
        bool isDiactivate = false;

        int numPuzzlePiece;
        public int NumPuzzlePiece {
            get {
                return numPuzzlePiece;
            }
        }
        int numPuzzleTemplate;
        public int NumPuzzleTemplate {
            get {
                return numPuzzleTemplate;
            }
        }
        PuzzlePieceField puzzlePieceField;

        float doubleClickTime = 0.25f;
        float currentClickTime;
        float lastClickTime;

        bool isDownPiece;

        internal void Init () {
            isInit = true;
            gameLinks = GameObject.FindObjectOfType<GameLinks>();

            InitEventTrigger();
        }

        void InitEventTrigger () {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener( ( data ) => {
                DownLetter();
            } );
            letterClick.triggers.Add( entry );

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener( ( data ) => {
                UpLetter();
            } );
            letterClick.triggers.Add( entry );
        }

        internal void Init ( int newIndexI, int newIndexJ, PuzzlePieceField puzzlePieceField, int numPuzzlePiece, int numPuzzleTemplate ) {
            indexI = newIndexI;
            indexJ = newIndexJ;

            this.puzzlePieceField = puzzlePieceField;
            puzzlePieceField.transform.SetParent( childTransform, false );
            puzzlePieceField.gameObject.SetActive( true );

            backImage.color = gameLinks.ColorDataSet.GetRandomColor();

            colorBack = backImage.color;

            RestartShow();
            this.numPuzzlePiece = numPuzzlePiece;
            this.numPuzzleTemplate = numPuzzleTemplate;

            revertObject.transform.SetAsLastSibling();
            changePieceObject.transform.SetAsLastSibling();
            this.order.enabled = false;
            this.order.transform.SetAsLastSibling();
        }

        internal Color GetcolorBack () {
            return colorBack;
        }
        //internal PuzzlePieceConnect GetPuzzlePieceConnect () {
        //    return puzzlePieceConnect;
        //}

        //void OnMouseUp () {
        //    OnUpPiece( indexI, indexJ );
        //}

        //void OnMouseEnter () {
        //    OnEnterPiece( indexI, indexJ );
        //}

        //void OnMouseExit () {
        //    fillCircleImage.DOKill();
        //    fillCircleImage.fillAmount = 0;

        //    OnExitPiece( indexI, indexJ );
        //}

        internal void StartHide ( int order ) {

            myAudioSourse.clip = gameLinks.AudioDataSet.GetRandomLetterFieldAudioClip();
            myAudioSourse.Play();

            OnAddPiece( indexI, indexJ );

            HideVisual( order );
        }

        internal void HideVisual ( int order ) {
            endHideAnimation = false;

            //myCollider.enabled = false;
            isShow = false;

            StopHighlightAnimation();
            StopShakeAnimation();
            StopPositionAnimation();

            if ( hideAnimation != null ) {
                StopCoroutine( hideAnimation );
                hideAnimation = null;
            }

            if ( showAnimation != null ) {
                StopCoroutine( showAnimation );
                showAnimation = null;
            }

            backImage.DOKill();
            backImage.DOColor( new Color( 0f, 0f, 0f, 0.6f ), 0.2f );

            this.order.text = order.ToString();
            this.order.enabled = true;
        }

        internal void HideImmediately ( int order ) {
            myCollider.enabled = false;

            isShow = false;

            StopHighlightAnimation();
            StopShakeAnimation();
            StopPositionAnimation();

            endHideAnimation = true;

            backImage.DOKill();
            backImage.color = new Color( 0f, 0f, 0f, 0.6f );

            this.order.text = order.ToString();
            this.order.enabled = true;
        }

        internal void StartShow ( float timeDelay ) {
            if ( isShow ) {
                return;
            }

            isShow = true;
            float delayShow = 0;
            if ( endHideAnimation ) {
                delayShow = timeDelay / 10f;
            }

            myCollider.enabled = true;

            countScaleShowAnimation = 4;
            endScaleShowAnimationValue = 1f + ( countScaleShowAnimation * countScaleShowAnimation / 100f );

            childTransform.DOKill();

            if ( hideAnimation != null ) {
                StopCoroutine( hideAnimation );
                hideAnimation = null;
            }

            if ( showAnimation != null ) {
                StopCoroutine( showAnimation );
                showAnimation = null;
            }

            //showAnimation = StartCoroutine( ShowAnimation( delayShow ) );

            backImage.DOColor( new Color( colorBack.r, colorBack.g, colorBack.b, 1f ), 0.2f );

            this.order.enabled = false;
        }

        internal void ClearPuzzlePieceCntr () {
            puzzlePieceField = null;
        }

        internal PuzzlePieceField GetPuzzlePieceField () {
            return puzzlePieceField;
        }

        internal void RestartShow () {
            isShow = true;
            myCollider.enabled = true;
            //childTransform.localScale = new Vector3( 1f, 1f );
            ActivateLetter();
        }

        internal void Activate () {
            myCollider.enabled = true;
        }

        internal void DiActivate () {
            myCollider.enabled = false;
        }

        internal void StartHideRestart ( int order ) {
            HideVisual( order );
        }

        internal void DownLetter () {
            currentClickTime = Time.time;

            //if ( ( currentClickTime - lastClickTime ) < doubleClickTime ) {
            //if ( isShow ) {
            //        RotatePuzzlePieces();
            //    }
            //}
            //else {
            isDownPiece = true;
            OnDownPiece( indexI, indexJ );
            //}

            //lastClickTime = currentClickTime;

            //fillCircleImage.DOKill();
            //fillCircleImage.DOFillAmount( 0f, 0.15f ).OnComplete( () => {
            //    fillCircleImage.DOFillAmount( 1, 0.7f ).OnComplete( () => {
            //        ChangeNumPiece();
            //        fillCircleImage.fillAmount = 0;
            //        isDownPiece = false;
            //    } );
            //} );
        }

        internal void UpLetter () {
            //if ( isDownPiece ) {
            OnUpPiece( indexI, indexJ );
            //}
            //else {
            //    OnUpPieceNoLogic( indexI, indexJ );
            //}

            isDownPiece = false;

            fillCircleImage.DOKill();
            fillCircleImage.fillAmount = 0;
        }

        internal void ChangeNumPiece () {
            puzzlePieceField.ChangeNumPiece();

            changePieceText.text = puzzlePieceField.GetPhantomNumPuzzlePiece().ToString();
            changePieceObject.gameObject.SetActive( true );

            changePieceText.DOKill();
            changePieceText.DOFade( 1, 0.4f ).OnComplete( () => {
                changePieceObject.gameObject.SetActive( false );
            } );
        }

        internal void RotatePuzzlePieces () {
            puzzlePieceField.RotatePuzzlePieces();

            int startCurrentAngle = puzzlePieceField.GetPhantomStartAngle();
            revertText.text = startCurrentAngle.ToString() + "°";
            revertObject.gameObject.SetActive( true );

            revertText.DOKill();
            revertText.DOFade( 1f, 0.4f ).OnComplete( () => {
                revertObject.gameObject.SetActive( false );
            } );
        }

        internal void ActivateLetter () {
            isDiactivate = false;
            myCollider.enabled = true;
            ActivateLetterColor();
            letterClick.enabled = true;
        }

        internal void ActivateLetterColor () {
            if ( isDiactivate ) {
                return;
            }

            zoomCanvasGroup.alpha = 1f;
        }

        internal void DiActivateLetter () {
            isDiactivate = true;
            myCollider.enabled = false;
            DiActivateLetterColor();
            letterClick.enabled = false;
        }

        internal void DiActivateLetterColor () {
            zoomCanvasGroup.alpha = 0.5f;
        }

        internal void SetPosition ( Vector2 letterPosition ) {
            _myRectTransform.anchoredPosition = letterPosition;
        }

        internal void StartChooseScale () {
            childTransform.DOKill();

            childTransform.DOScale( new Vector3( 1.05f, 1.05f, 1.05f ), 0.2f );
        }

        internal void EndChooseScale () {
            childTransform.DOKill();

            if ( isShow ) {
                childTransform.DOScale( new Vector3( 1f, 1f, 1f ), 0.2f );
            }
        }

        //IEnumerator HideAnimation () {
        //    Vector2 childTransformLocalScale = childTransform.localScale;

        //    for ( ; ; ) {
        //        yield return new WaitForEndOfFrame();

        //        if ( childTransformLocalScale.x <= 0 ) {
        //            childTransform.localScale = new Vector2( 0f, 0f );
        //            endHideAnimation = true;

        //            break;
        //        }
        //        else {
        //            childTransform.localScale = childTransformLocalScale;
        //            childTransformLocalScale -= new Vector2( 0.4f, 0.4f )*Time.deltaTime*TimeDeltaTimeFixer.scaleFixer;
        //        }
        //    }
        //}

        //IEnumerator ShowAnimation ( float delay ) {
        //    yield return new WaitForSeconds( delay );

        //    float sign = 1f;
        //    Vector2 childTransformLocalScale = childTransform.localScale;

        //    for ( ; ; ) {
        //        yield return new WaitForEndOfFrame();
        //        if ( sign == 1 && childTransformLocalScale.x >= endScaleShowAnimationValue ) {
        //            countScaleShowAnimation--;
        //            childTransformLocalScale = new Vector2( endScaleShowAnimationValue, endScaleShowAnimationValue );
        //            endScaleShowAnimationValue = 1f - ( countScaleShowAnimation * countScaleShowAnimation / 100f );
        //            sign = -1;
        //        }
        //        else if ( sign == -1 && childTransformLocalScale.x <= endScaleShowAnimationValue ) {
        //            countScaleShowAnimation--;
        //            childTransformLocalScale = new Vector2( endScaleShowAnimationValue, endScaleShowAnimationValue );
        //            endScaleShowAnimationValue = 1f + ( countScaleShowAnimation * countScaleShowAnimation / 100f );
        //            sign = 1;
        //        }

        //        if ( countScaleShowAnimation <= 0 ) {
        //            sign = Mathf.Sign( 1f - childTransformLocalScale.x );
        //            childTransform.localScale = new Vector3( 1f, 1f, 1f );
        //            break;
        //        }

        //        childTransform.localScale = childTransformLocalScale;
        //        childTransformLocalScale += new Vector2( 0.05f, 0.05f )*sign*Time.deltaTime*TimeDeltaTimeFixer.scaleFixer;
        //    }
        //}

        internal void StartPuzzleAnimation ( float delay ) {
            StopPuzzleAnimation();

            countScaleShowAnimation = 4;
            endScaleShowAnimationValue = 1f + ( countScaleShowAnimation * countScaleShowAnimation / 100f );

            //childTransform.localScale = new Vector2( 0.7f, 0.7f );

            zoomCanvasGroup.DOKill();
            zoomCanvasGroup.DOFade( 0.5f, 0.2f + delay );
            //startPuzzleAnimation = StartCoroutine( ShowAnimation( delay ) );
        }

        internal void StopPuzzleAnimation () {
            if ( startPuzzleAnimation == null ) {
                return;
            }

            StopCoroutine( startPuzzleAnimation );
            highlightAnimation = null;

            childTransform.localScale = new Vector2( 1f, 1f );
        }

        IEnumerator StartPuzzleAnimationUpdate ( float delay ) {
            yield return new WaitForSeconds( delay );

            float stepSacle = 0.1f;
            for ( ;; ) {
                yield return new WaitForEndOfFrame();
                childTransform.localScale += new Vector3( stepSacle, stepSacle );

                if ( childTransform.localScale.x >= 1f ) {
                    childTransform.localScale = new Vector3( 1f, 1f, 1f );
                    break;
                }
            }
        }

        internal void StartHighlightAnimation ( int indexDelay ) {
            StopHighlightAnimation();
            StopPositionAnimation();
            StopShakeAnimation();

            highlightAnimation = StartCoroutine( HighlightAnimationUpdate( indexDelay ) );
        }

        internal void StopHighlightAnimation () {
            if ( highlightAnimation == null ) {
                return;
            }

            StopCoroutine( highlightAnimation );
            highlightAnimation = null;

            imageHighlightMask.gameObject.SetActive( false );
            imageHighlight.anchoredPosition = new Vector2( -265f, 265f );
        }

        IEnumerator HighlightAnimationUpdate ( int indexDelay ) {
            float delay = ( float )indexDelay;

            yield return new WaitForSeconds( delay / 5f );
            imageHighlight.gameObject.SetActive( true );
            imageHighlightMask.gameObject.SetActive( true );
            imageHighlight.anchoredPosition = new Vector2( -265f, 265f );

            float step = Vector2.Distance( imageHighlight.anchoredPosition, new Vector2( 265f, - -265 ) ) / delay;
            step = 25f;

            for ( ;; ) {
                yield return new WaitForEndOfFrame();
                imageHighlight.anchoredPosition += new Vector2( step, -step )*Time.deltaTime*TimeDeltaTimeFixer.scaleFixer;
                if ( imageHighlight.anchoredPosition.x >= 265f || imageHighlight.anchoredPosition.y <= -265f ) {
                    break;
                }
            }

            imageHighlight.gameObject.SetActive( false );
        }

        internal void StartShakeAnimation ( int indexDelay ) {
            StopHighlightAnimation();
            StopPositionAnimation();
            StopShakeAnimation();

            shakeAnimation = StartCoroutine( ShakeAnimationUpdate( indexDelay ) );
        }

        internal void StopShakeAnimation () {
            if ( shakeAnimation == null ) {
                return;
            }

            StopCoroutine( shakeAnimation );
            shakeAnimation = null;

            childTransform.localEulerAngles = new Vector3( 0f, 0f, 0f );
        }

        IEnumerator ShakeAnimationUpdate ( float indexDelay ) {
            yield return new WaitForSeconds( indexDelay / 5f );

            float signShake = 1f;
            float rangeShake = 10f;
            float stepShake = 1.5f;
            float angleZ = 0;

            int countShake = 2;

            for ( ; ; ) {
                yield return new WaitForEndOfFrame();

                if ( countShake <= 0 ) {
                    angleZ += stepShake;
                    childTransform.localEulerAngles = new Vector3( 0f, 0f, angleZ );

                    if ( angleZ >= 0 ) {
                        break;
                    }
                }

                if ( angleZ >= rangeShake ) {
                    angleZ = rangeShake;
                    signShake = -1;
                }
                else if ( angleZ <= rangeShake * -1 ) {
                    angleZ = rangeShake * -1;
                    signShake = 1;

                    countShake--;
                }

                angleZ += stepShake * signShake * Time.deltaTime * TimeDeltaTimeFixer.scaleFixer;
                childTransform.localEulerAngles = new Vector3( 0f, 0f, angleZ );
            }

            childTransform.localEulerAngles = new Vector3( 0f, 0f, 0f );
        }

        internal void StartPositionAnimation ( int indexDelay, int totalDelay ) {
            StopHighlightAnimation();
            StopPositionAnimation();
            StopShakeAnimation();

            positionAnimation = StartCoroutine( PositionAnimationUpdate( indexDelay, totalDelay ) );
        }

        internal void StopPositionAnimation () {
            if ( positionAnimation == null ) {
                return;
            }

            StopCoroutine( positionAnimation );
            positionAnimation = null;

            childTransform.anchoredPosition = new Vector2( childTransform.anchoredPosition.x, 0f );
        }

        IEnumerator PositionAnimationUpdate ( int indexDelay, int totalDelay ) {
            float divider = 8f;

            yield return new WaitForSeconds( indexDelay / divider );

            float positionY = childTransform.anchoredPosition.y;
            float positionYOffset = 50f;
            float stepPosition = 4;
            int countAnimation = 2;

            for ( int i = 0; i < countAnimation; i++ ) {
                for ( ; ; ) {
                    yield return new WaitForEndOfFrame();

                    childTransform.anchoredPosition += new Vector2( 0f, stepPosition ) * Time.deltaTime*TimeDeltaTimeFixer.scaleFixer;

                    if ( childTransform.anchoredPosition.y >= ( positionY + positionYOffset ) ) {
                        break;
                    }
                }

                for ( ;; ) {
                    yield return new WaitForEndOfFrame();

                    childTransform.anchoredPosition -= new Vector2( 0f, stepPosition ) *Time.deltaTime*TimeDeltaTimeFixer.scaleFixer;

                    if ( childTransform.anchoredPosition.y <= positionY ) {
                        break;
                    }
                }

                yield return new WaitForSeconds( totalDelay / divider );
            }

            childTransform.anchoredPosition = new Vector2( childTransform.anchoredPosition.x, positionY );
        }

        internal void RotateCircle (  ) {
            overlayTransform.DORotate( new Vector3( 0f, 0f, -10f ), 0.02f );
        }

        internal void EndRotateCircle () {
            overlayTransform.DORotate( new Vector3( 0f, 0f, 0f ), 0.02f );
        }
    }
}
