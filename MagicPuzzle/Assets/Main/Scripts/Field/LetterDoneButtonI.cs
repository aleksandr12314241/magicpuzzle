﻿using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using Utility;

namespace Puzzle {
    public class LetterDoneButtonI: MonoBehaviour {
        public event Action<int> OnClickButtinI = ( int patram1 ) => { };

        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Text hintText;
        [SerializeField]
        RectTransform hintRectTransform;
        [SerializeField]
        RectTransform hintTextRectTransform;
        [SerializeField]
        RectTransform hintTextBackImage;

        bool isHint = false;
        CharacterInfo characterInfo = new CharacterInfo();
        Font myFont;  //chatText is my Text component

        internal void SetStart () {
            myFont = hintText.font;
        }

        internal void SetPositionAndScale ( Vector2 newPosition ) {
            myRectTransform.anchoredPosition = newPosition;
        }

        internal void SetHintText ( string hintString ) {
            char[] arr = hintString.ToCharArray();

            int totalLength = 70;

            foreach ( char c in arr ) {
                myFont.RequestCharactersInTexture( c.ToString(), hintText.fontSize, hintText.fontStyle );
                myFont.GetCharacterInfo( c, out characterInfo, hintText.fontSize );

                totalLength += characterInfo.advance;
            }
            if ( totalLength > hintRectTransform.sizeDelta.x ) {
                totalLength = ( int ) hintRectTransform.sizeDelta.x + 70;
            }

            hintTextBackImage.sizeDelta = new Vector2( totalLength, hintTextBackImage.sizeDelta.y );

            if ( hintString == "" ) {
                isHint = false;
                //hintText.text = new string( '-', 5 );
                hintTextBackImage.gameObject.SetActive( false );
                hintText.gameObject.SetActive( false );
                return;
            }

            hintTextBackImage.gameObject.SetActive( true );
            hintText.gameObject.SetActive( true );

            isHint = true;
            //iconI.gameObject.SetActive( true );
            hintText.text = hintString;
        }

        internal void ShowHintText () {

            hintTextRectTransform.DOKill();
            hintTextRectTransform.DOScale( new Vector3( 1f, 1f ), 0.1f );
        }

        internal void HideHintText () {

            hintTextRectTransform.DOKill();
            hintTextRectTransform.DOScale( new Vector3( 0f, 0f ), 0.1f );
        }
    }
}
