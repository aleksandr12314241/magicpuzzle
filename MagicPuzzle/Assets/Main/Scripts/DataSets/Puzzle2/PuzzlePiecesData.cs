﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct PuzzlePiecesData {
    [SerializeField]
    public Sprite[] piceSprites;
    [SerializeField]
    public PuzzlePieceConnect puzzlePieceConnect;

    internal Sprite[] PiceSprites {
        get {
            return piceSprites;
        }
    }

    internal PuzzlePieceConnect PuzzlePieceConnect {
        get {
            return puzzlePieceConnect;
        }
    }
}