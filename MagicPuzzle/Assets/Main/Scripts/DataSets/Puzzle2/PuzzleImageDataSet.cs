﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "PuzzleImageDataSet", menuName = "MagicPuzzle/PuzzleImageDataSet", order = 1 )]
public class PuzzleImageDataSet : ScriptableObject {
    [SerializeField]
    List<Sprite> images;

    public Sprite GetSpriteByName ( string spriteName ) {
        for ( int i = 0; i < images.Count; i++ ) {
            if ( images[i].name == spriteName ) {
                return images[i];
            }
        }

        return null;
    }
}
