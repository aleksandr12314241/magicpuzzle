﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

[CustomEditor( typeof( PuzzlePiecesDataSet ) )]
public class PuzzlePiecesDataSetInspector : Editor {
    PuzzlePiecesDataSet myTarget;

    private void OnEnable () {
        myTarget = ( PuzzlePiecesDataSet ) target;
    }

    public override void OnInspectorGUI () {

        if ( GUILayout.Button( "заполнить треугольные данные/fill triangles in the data" ) ) {
            FixArrayTriangle();
        }

        var serializedObject = new SerializedObject( target );
        var property = serializedObject.FindProperty( "puzzlePieces" );
        serializedObject.Update();
        EditorGUILayout.PropertyField( property, true );
        serializedObject.ApplyModifiedProperties();

        //myTarget.experience = EditorGUILayout.IntField( "Experience", myTarget.experience );
        //EditorGUILayout.LabelField( "Level", myTarget.Level.ToString() );

        if ( GUI.changed ) {
            EditorUtility.SetDirty( myTarget );
            //EditorSceneManager.MarkSceneDirty( castedTarget.gameObject.scene );
        }
    }

    void FixArrayTriangle () {
        Debug.Log( "заполнить данные/fill in the data" );

        string pathDecipher = Application.dataPath + "/Main/Sprites/Puzzle2/TrianglesPiece/";
        var info = new DirectoryInfo( pathDecipher );
        var fileInfo = info.GetFiles();

        List<string> fullFileName = new List<string>();
        List<string>  fileName = new List<string>();

        for ( int i = 0; i < fileInfo.Length; i++ ) {
            FileInfo file = fileInfo[i];

            int indexStart = file.Name.Length - 4;
            string fileExpand = file.Name.Substring( indexStart );
            if ( fileExpand.Equals( "meta" ) ) {
                continue;
            }
            fileName.Add( file.Name );
        }

        myTarget.puzzlePieces = new PuzzlePiecesData[fileName.Count];

        for ( int i = 0; i < fileName.Count; i++ ) {
            string subStr = fileName[i].Substring( 11, fileName[i].Length - 4 - 11 );

            //up
            int indexDirection = subStr.IndexOf( "u" );
            int numberDirection = int.Parse( subStr.Substring( indexDirection + 1, 1 ) );
            PuzzlePieceConnectType up = GetPuzzlePieceConnectTypeByNumber( numberDirection );
            //right
            indexDirection = subStr.IndexOf( "r" );
            numberDirection = int.Parse( subStr.Substring( indexDirection + 1, 1 ) );
            PuzzlePieceConnectType right = GetPuzzlePieceConnectTypeByNumber( numberDirection );
            //down
            indexDirection = subStr.IndexOf( "d" );
            numberDirection = int.Parse( subStr.Substring( indexDirection + 1, 1 ) );
            PuzzlePieceConnectType down = GetPuzzlePieceConnectTypeByNumber( numberDirection );
            //down
            indexDirection = subStr.IndexOf( "l" );
            numberDirection = int.Parse( subStr.Substring( indexDirection + 1, 1 ) );
            PuzzlePieceConnectType left = GetPuzzlePieceConnectTypeByNumber( numberDirection );

            Sprite spritesTriangle = AssetDatabase.LoadAssetAtPath<Sprite>( "Assets/Main/Sprites/Puzzle2/TrianglesPiece/" + fileName[i] );

            myTarget.puzzlePieces[i].piceSprites = new Sprite[1];
            myTarget.puzzlePieces[i].piceSprites[0] = spritesTriangle;
            myTarget.puzzlePieces[i].puzzlePieceConnect = new  PuzzlePieceConnect( up, right, down, left );
        }
    }

    PuzzlePieceConnectType GetPuzzlePieceConnectTypeByNumber ( int number ) {
        switch ( number ) {
            case 0:
                return PuzzlePieceConnectType.In;
            case 1:
                return PuzzlePieceConnectType.Flat;
            default:
                return PuzzlePieceConnectType.Out;
        }
    }
}
#endif