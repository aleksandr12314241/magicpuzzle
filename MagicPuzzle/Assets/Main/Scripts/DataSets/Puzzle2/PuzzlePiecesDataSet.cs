﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "PuzzlePiecesDataSet", menuName = "MagicPuzzle/PuzzlePiecesDataSet", order = 1 )]
public class PuzzlePiecesDataSet : ScriptableObject {
    [SerializeField]
    public PuzzlePiecesData[] puzzlePieces;

    public int GetNumPuzzlePiecesData ( PuzzlePieceConnect puzzlePieceConnect ) {
        for ( int i = 0; i < puzzlePieces.Length; i++ ) {
            if ( puzzlePieces[i].PuzzlePieceConnect.Equals( puzzlePieceConnect ) ) {
                return  i;
            }
        }

        return -1;
    }

    public PuzzlePiecesData GetPuzzlePiecesData ( PuzzlePieceConnect puzzlePieceConnect ) {
        for ( int i = 0; i < puzzlePieces.Length; i++ ) {
            if ( puzzlePieces[i].PuzzlePieceConnect.Equals( puzzlePieceConnect ) ) {
                return puzzlePieces[i];
            }
        }

        return new PuzzlePiecesData();
    }

    public PuzzlePiecesData GetPuzzlePiecesDataByIndex ( int index ) {
        return puzzlePieces[index];
    }

    internal List<int> GetPuzzlePiecesNumByExitPiece ( string exitPiece, List<int> numPuzzlePiecesException ) {
        List<int> numPuzzlePieces = GetIndexPuzzlePiecesByExitPiece( exitPiece );

        for ( int i = 0; i < numPuzzlePiecesException.Count; i++ ) {
            if ( numPuzzlePieces.Contains( numPuzzlePiecesException[i] ) ) {
                numPuzzlePieces.Remove( numPuzzlePiecesException[i] );
            }
        }

        return numPuzzlePieces;
    }

    internal PuzzlePiecesData[] GetPuzzlePiecesByExitPiece ( string exitPiece ) {
        List<int> indexesPuzzlePieces = GetIndexPuzzlePiecesByExitPiece( exitPiece );

        PuzzlePiecesData[] puzzlePiecesByExitPiece = new PuzzlePiecesData[indexesPuzzlePieces.Count];
        for ( int i = 0; i < puzzlePiecesByExitPiece.Length; i++ ) {
            puzzlePiecesByExitPiece[i] = puzzlePieces[indexesPuzzlePieces[i]];
        }

        return puzzlePiecesByExitPiece;
    }

    internal List<int> GetIndexPuzzlePiecesByExitPiece ( string exitPiece ) {
        Dictionary<string, List<PuzzlePieceConnectType>> connectionTypeByExitPiece = GameHelper.GetPieceConnection( exitPiece );

        List<int> indexesPuzzlePieces = new List<int>();
        for ( int i = 0; i < puzzlePieces.Length; i++ ) {
            bool[] check = new bool[4];

            if ( connectionTypeByExitPiece["u"].Contains( puzzlePieces[i].PuzzlePieceConnect.Up ) ) {
                check[0] = true;
            }

            if ( connectionTypeByExitPiece["r"].Contains( puzzlePieces[i].PuzzlePieceConnect.Right ) ) {
                check[1] = true;
            }

            if ( connectionTypeByExitPiece["d"].Contains( puzzlePieces[i].PuzzlePieceConnect.Down ) ) {
                check[2] = true;
            }

            if ( connectionTypeByExitPiece["l"].Contains( puzzlePieces[i].PuzzlePieceConnect.Left ) ) {
                check[3] = true;
            }

            if ( check[0] && check[1] && check[2] && check[3] ) {
                indexesPuzzlePieces.Add( i );
            }
        }

        return indexesPuzzlePieces;
    }
}
