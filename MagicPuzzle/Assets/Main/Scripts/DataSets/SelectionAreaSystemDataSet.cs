﻿using UnityEngine;

namespace Tutorial {
    [CreateAssetMenu( fileName = "SelectionAreaDataSet", menuName = "WordSquare/SelectionAreaDataSet", order = 1 )]

    public class SelectionAreaSystemDataSet: ScriptableObject {
        [SerializeField]
        SelectionAreaSet[] selectionAreaSet;

        internal SelectionAreaSet GetSelectionAreaSetById ( string tutorialId ) {
            for ( int i = 0; i < selectionAreaSet.Length; i++ ) {
                if ( selectionAreaSet[i].TutorialId.Equals( tutorialId ) ) {
                    return selectionAreaSet[i];
                }
            }

            return null;
        }

        [System.Serializable]
        internal class SelectionAreaSet {
            [SerializeField]
            string tutorialId;
            [SerializeField]
            Vector2 upPosition;
            [SerializeField]
            Vector2 downPosition;
            [SerializeField]
            Vector2 leftPosition;
            [SerializeField]
            Vector2 rightPosition;

            internal string TutorialId {
                get {
                    return tutorialId;
                }
            }

            internal Vector2 UpPosition {
                get {
                    return upPosition;
                }
            }

            internal Vector2 DownPosition {
                get {
                    return downPosition;
                }
            }

            internal Vector2 LeftPosition {
                get {
                    return leftPosition;
                }
            }

            internal Vector2 RightPosition {
                get {
                    return rightPosition;
                }
            }
        }
    }
}
