﻿using UnityEngine;

namespace Common {
    [CreateAssetMenu( fileName = "AudioDataSet", menuName = "WordSquare/AudioDataSet", order = 1 )]
    public class AudioDataSet: ScriptableObject {
        [SerializeField]
        AudioClip[] letterFieldAudioClip;
        [SerializeField]
        AudioClip[] wrongWordFieldAudioClip;
        [SerializeField]
        AudioClip recognizeWord;
        [SerializeField]
        AudioClipByType[] audioClipByType;

        internal AudioClip GetRandomLetterFieldAudioClip () {
            int indexRandom = Random.Range( 0, letterFieldAudioClip.Length );

            return letterFieldAudioClip[indexRandom];
        }

        internal AudioClip GetRandomWrongWordFieldAudioClip () {
            int indexRandom = Random.Range( 0, wrongWordFieldAudioClip.Length );

            return wrongWordFieldAudioClip[indexRandom];
        }

        internal AudioClip RecognizeWord {
            get {
                return recognizeWord;
            }
        }

        internal AudioClip GetAudioClipByType ( SoundType soundType ) {
            for ( int i = 0; i < audioClipByType.Length; i++ ) {
                if ( audioClipByType[i].soundType == soundType ) {
                    return audioClipByType[i].audioClip;
                }
            }

            return audioClipByType[0].audioClip;
        }
    }

    [System.Serializable]
    internal class AudioClipByType {
        [SerializeField]
        internal AudioClip audioClip;
        [SerializeField]
        internal SoundType soundType;
    }
}
