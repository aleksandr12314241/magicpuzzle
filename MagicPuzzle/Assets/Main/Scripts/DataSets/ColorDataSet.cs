﻿using UnityEngine;

namespace Common {
    [CreateAssetMenu( fileName = "ColorDataSet", menuName = "WordSquare/ColorDataSet", order = 1 )]
    public class ColorDataSet: ScriptableObject {
        [SerializeField]
        Color[] lettersColor;

        internal Color GetRandomColor () {
            int indexRandom = Random.Range( 0, lettersColor.Length );

            return lettersColor[indexRandom];
        }
    }
}
