﻿using UnityEngine;

namespace Common {
    [CreateAssetMenu( fileName = "IconDataSet", menuName = "WordSquare/IconDataSet", order = 1 )]
    public class IconDataSet: ScriptableObject {
        [SerializeField]
        CupsSet[] categoriesIcon;

        internal CupsSet GetCategoriesIconByIndex ( int indexIcon ) {
            return categoriesIcon[indexIcon];
        }

        [System.Serializable]
        internal struct CupsSet {
            [SerializeField]
            Sprite categoriesIcon;
            [SerializeField]
            Sprite categoriesIconSilhouette;

            internal Sprite CategoriesIcon {
                get {
                    return categoriesIcon;
                }
            }

            internal Sprite CategoriesIconSilhouette {
                get {
                    return categoriesIconSilhouette;
                }
            }
        }
    }
}
