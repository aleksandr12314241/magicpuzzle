﻿using UnityEngine;
using System;
using Common;
using System.Collections;

namespace Menu {
    public class MainPuzzleScroll: MonoBehaviour {
        internal event Action<int> OnChangeScreen = ( int param ) => { };

        [SerializeField]
        MenuScreenUI[] _menuScreensUI;

        int _indexOpenScreen;
        internal int IndexOpenScreen {
            get {
                return _indexOpenScreen;
            }
            private set {
                _indexOpenScreen = value;
                OnChangeScreen( _indexOpenScreen );
            }
        }

        internal void Init () {
            ScreenFixer screenFixer = GameObject.FindObjectOfType<ScreenFixer>();
            for ( int i = 0; i < _menuScreensUI.Length; i++ ) {
                _menuScreensUI[i].Init();
            }

            //if ( screenFixer.IsInit ) {
            //    OpenStartSceen();
            //    return;
            //}

            //StartCoroutine( WaitForScreenFixerInit( screenFixer ) );
        }

        IEnumerator WaitForScreenFixerInit ( ScreenFixer screenFixer ) {
            yield return new WaitUntil( () => screenFixer.IsInit );

            OpenStartSceen();
        }

        void OpenStartSceen () {
            IndexOpenScreen = 2;
            for ( int i = 0; i < _menuScreensUI.Length; i++ ) {
                if ( IndexOpenScreen == i ) {
                    _menuScreensUI[i].ShowScreeenImmediately();

                    continue;
                }
                _menuScreensUI[i].HideScreeenImmediately( -1 );
            }
        }

        internal void ShowScreen ( int indexScreen, int sideHide ) {
            if ( indexScreen == IndexOpenScreen ) {
                return;
            }
            _menuScreensUI[IndexOpenScreen].HideScreeen( sideHide );

            IndexOpenScreen = indexScreen;
            _menuScreensUI[indexScreen].ShowScreeen();
        }

        internal void ShowScreen ( int indexScreen, int sideHide, float scrollContentPosition ) {
            _menuScreensUI[IndexOpenScreen].HideScreeen( sideHide );

            IndexOpenScreen = indexScreen;
            MenuScreenUIScrollable menuScreenUIScrollable = ( MenuScreenUIScrollable ) _menuScreensUI[indexScreen];
            menuScreenUIScrollable.ShowScreeen( scrollContentPosition );
        }

        internal void SetScrollContentPosition ( int indexScreen, float scrollContentPosition ) {
            MenuScreenUIScrollable menuScreenUIScrollable = ( MenuScreenUIScrollable ) _menuScreensUI[indexScreen];
            menuScreenUIScrollable.SetSrollContentPosition( scrollContentPosition );
        }

        internal void SetScreenShow ( int indexScreen ) {
            IndexOpenScreen = indexScreen;
            _menuScreensUI[indexScreen].ShowScreeenImmediately();
        }

        internal void SetScreenHide ( int indexScreen, int sideHide ) {
            _menuScreensUI[indexScreen].HideScreeenImmediately( sideHide );
        }

        internal MenuScreenUI GetMenuScreenUI ( int indexScreen ) {
            return _menuScreensUI[indexScreen];
        }
    }
}
