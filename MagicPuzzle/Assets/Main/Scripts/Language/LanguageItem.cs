﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace Menu {
    public class LanguageItem: MonoBehaviour {
        internal event Action<LocalizationManager.Language> OnChooseLanguage = ( LocalizationManager.Language param ) => { };

        [SerializeField]
        LocalizationManager.Language _language;
        [SerializeField]
        Button _onChooseButton;

        private void Awake () {
            _onChooseButton.onClick.AddListener( ChooseLanguage );
        }

        private void ChooseLanguage () {
            OnChooseLanguage( _language );
        }
    }
}
