﻿using UnityEngine;
using Common;
using UnityEngine.UI;

namespace Menu {
    public class LanguageUI: WindowMonoBehaviour {
        [SerializeField]
        Button _closeButton;
        [SerializeField]
        Button _backButton;
        [SerializeField]
        LanguageItem[] _languageItems;

        GameController _GC;

        internal override void InitAwake () {

            _closeButton.onClick.AddListener( HideWin );
            _backButton.onClick.AddListener( HideWin );

            for ( int i = 0; i < _languageItems.Length; i++ ) {
                _languageItems[i].OnChooseLanguage += OnChooseLanguage;
            }

            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
        }

        internal void ShowWin () {
            base.Show();

            //_myAnimator.enabled = true;
            //_myAnimator.Play( "win" );
        }

        internal void HideWin () {
            HideEnd();

            //_myAnimator.Play( "hide" );
            //Invoke( "HideEnd", 0.3f );
        }

        void HideEnd () {
            base.Hide();

            //_myAnimator.enabled = false;
        }

        void OnChooseLanguage ( LocalizationManager.Language language ) {
            HideWin();

            _GC.player.language = language.ToString();
            _GC.SavePlayer();

            LocalizationManager.SetLanguage( language );
        }
    }
}
