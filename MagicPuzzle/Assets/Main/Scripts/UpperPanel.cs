﻿using UnityEngine;
using UnityEngine.UI;
using Common;

namespace Puzzle {
    public class UpperPanel: MonoBehaviour {
        [SerializeField]
        Button backButton;
        [SerializeField]
        Text categoryProgress;
        [SerializeField]
        PuzzleController puzzleController;
        [SerializeField]
        RectTransform lampsPosition;

        GameController _GC;

        RectTransform myRectTransform;

        internal void Init () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
            backButton.onClick.AddListener( ClickButtonBack );

            //puzzleController.OnStartQuest += OnStartQuest;

            myRectTransform = GetComponent<RectTransform>();
        }

        void ClickButtonBack () {
            puzzleController.ClickButtonBack();
        }

        internal void SetActiveButtonBack ( bool isActive ) {
            backButton.gameObject.SetActive( isActive );
        }

        //void OnStartQuest ( Quests quests ) {
        //    int numCatigories = _GC.numCatigory.GetValue();
        //    int maxLevels = 0;
        //    int numQuest = _GC.numLevel.GetValue() + 1;

        //    categoryProgress.text = numQuest + "/" + maxLevels;
        //}

        public void FlyLamps () {
            float posLamp = ( myRectTransform.rect.width / 2 ) - 121;
            Debug.Log( posLamp );
        }

        internal Vector2 GetLampsPosition () {
            return lampsPosition.anchoredPosition;
        }
    }
}
