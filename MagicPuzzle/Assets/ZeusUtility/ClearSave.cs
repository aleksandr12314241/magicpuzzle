﻿#if UNITY_EDITOR
using UnityEditor;
using System;
using UnityEngine;
using System.IO;

//: EditorWindow
public class ClearSave  {
    [MenuItem( "Tools/Clear Save" )]
    static void StartClearSave () {
        Debug.Log( "save was removed" );

        string savePath = Application.dataPath + "/Data/saves.json";
        if ( File.Exists( savePath ) ) {
            File.Delete( savePath );
            File.Delete( Application.dataPath + "/Data/saves_decipher.json" );
        }
    }
}
#endif