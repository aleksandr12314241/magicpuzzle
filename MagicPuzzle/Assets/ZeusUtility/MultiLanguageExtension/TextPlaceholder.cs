﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Common {
    [RequireComponent( typeof( Text ) )]
    public class TextPlaceholder: MonoBehaviour {
        [SerializeField]
        string _textId;
        [SerializeField]
        bool _dontSetTextOnStart;

        Text _text;
        List<string> _textParams;

        private void Awake () {
            _text = GetComponent<Text>();
        }

        void Start () {

            StartCoroutine( Init() );
        }

        IEnumerator Init () {
            while ( !LocalizationManager.GetIsReady() ) {
                yield return null;
            }

#if UNITY_EDITOR
            if ( !_dontSetTextOnStart && _textId == "" ) {
                Debug.LogWarning( "_textId is empty on " + gameObject.name );
            }
#endif

            UpdateTextById();
        }

        internal void SetTextById ( string textId ) {
            if ( _text == null ) {
                _text = GetComponent<Text>();
            }
            _textParams = null;
            _textId = textId;
            UpdateTextNoParams();
        }

        internal void SetTextById ( string textId, string textParam ) {
            if ( _text == null ) {
                _text = GetComponent<Text>();
            }

            _textId = textId;
            _textParams = new List<string>() {
                textParam
            };

            UpdateTextWithParams();
        }

        internal void SetTextById ( string textId, List<string> textParams ) {
            if ( _text == null ) {
                _text = GetComponent<Text>();
            }

            _textId = textId;
            _textParams = new List<string>();
            for ( int i = 0; i < textParams.Count; i++ ) {
                _textParams.Add( textParams[i] );
            }

            UpdateTextWithParams();
        }

        internal void UpdateTextParams ( List<string> textParams ) {

            _textParams = new List<string>();
            for ( int i = 0; i < textParams.Count; i++ ) {
                _textParams.Add( textParams[i] );
            }

            UpdateTextWithParams();
        }

        internal void UpdateTextById () {
            if ( _text == null ) {
                _text = GetComponent<Text>();
            }
            if ( _textId == "" ) {
                return;
            }

            if ( _textParams == null ) {
                UpdateTextNoParams();
                return;
            }
            UpdateTextWithParams();
        }

        void UpdateTextNoParams () {
            _text.text = LocalizationManager.GetText( _textId );
        }

        void UpdateTextWithParams () {
            string textTemp = LocalizationManager.GetText( _textId );

            string replacer;
            for ( int i = 0; i < _textParams.Count; i++ ) {
                replacer = "<" + i + ">";
                if ( !textTemp.Contains( replacer ) ) {
                    continue;
                }

                textTemp = textTemp.Replace( replacer, _textParams[i] );
            }

            _text.text = textTemp;
        }

        internal void SetText ( string text ) {
            _text.text = text;
        }
    }
}
