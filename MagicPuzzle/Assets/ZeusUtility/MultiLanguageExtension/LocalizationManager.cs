﻿using System.Collections.Generic;
using UnityEngine;
using System;

internal static class LocalizationManager {
    internal static event Action<Language> OnChangeLanguage = ( Language param ) => { };

    internal enum Language {
        Ru,
        En
    }

    static TextAsset _textAsset;
    static Dictionary<string, LocalizationDataSet> _textContainer;
    static Language _language = Language.Ru;
    static Language[] _languageId;
    static bool _isReady = false;

    static int FindEndIndex ( string rowStr, int startIndex, char findSimbol ) {
        int endIndex = rowStr.Length;
        for ( int i = startIndex; i < rowStr.Length; i++ ) {
            if ( rowStr[i] == findSimbol ) {
                endIndex = i;
                break;
            }
        }

        return endIndex;
    }

    internal static void Init () {
        _textAsset = Resources.Load<TextAsset>( "TextLocalization" );

        string[] data = _textAsset.text.Split( new char[] { '\n' } );
        _textContainer = new Dictionary<string, LocalizationDataSet>();

        _languageId = new Language[2];
        _languageId[0] = Language.Ru;
        _languageId[1] = Language.En;

        char quotes = '"';
        char comma = ',';

        for ( int i = 1; i < data.Length; i++ ) {

            int startIndex = -1;
            int endIndex = 0;

            string[] row2 = new string[6] {
                "", "", "", "", "", ""
            };
            int row2Index = 0;

            int indexTemp = 0;

            while ( indexTemp < data[i].Length ) {
                if ( startIndex == - 1 ) {
                    endIndex = FindEndIndex( data[i], 0, comma );

                    row2[row2Index] = data[i].Substring( 0, endIndex );
                    row2Index++;

                    indexTemp = endIndex;
                    startIndex = endIndex + 1;
                }
                else if ( data[i][indexTemp] == comma ) {
                    int quotesCheker = indexTemp + 1;
                    if ( quotesCheker < data[i].Length ) {
                        if ( data[i][quotesCheker] == quotes ) {
                            endIndex = FindEndIndex( data[i], quotesCheker + 1, quotes ) ;

                            row2[row2Index] = GetStringNoCommaAndQuotes( data[i].Substring( startIndex, endIndex - startIndex ) );

                            row2Index++;
                            indexTemp = endIndex + 1;
                            startIndex = endIndex + 1;
                        }
                        else {
                            endIndex = FindEndIndex( data[i], startIndex, comma );

                            row2[row2Index] = GetStringNoCommaAndQuotes( data[i].Substring( startIndex, endIndex - startIndex ) );

                            row2Index++;
                            indexTemp = endIndex;
                            startIndex = endIndex + 1;
                        }
                    }
                    else {
                        endIndex = FindEndIndex( data[i], startIndex, comma );

                        row2[row2Index] = GetStringNoCommaAndQuotes( data[i].Substring( startIndex, endIndex - startIndex ) );
                        row2Index++;
                        indexTemp = endIndex;
                        startIndex = endIndex + 1;
                    }
                }
                else {
                    indexTemp++;
                }
            }

            LocalizationDataSet languageStruct = new LocalizationDataSet();
            languageStruct.texts = new string[5];

            languageStruct.texts[0] = row2[1];
            languageStruct.texts[1] = row2[2];


            _textContainer.Add( row2[0], languageStruct );
        }

        _isReady = true;
    }

    static string GetStringNoCommaAndQuotes ( string str ) {
        char quotes = '"';
        char comma = ',';
        string row2TempNoSpace = str.Trim();
        return row2TempNoSpace.Trim( new char[2] { quotes, comma } );
    }

    internal static string GetText ( string id ) {
        if ( !_textContainer.ContainsKey( id ) ) {
            Debug.LogWarning( "Warning !!!!! нет такого идентификатора " + id );
            return id;
        }
        return _textContainer[id].texts[Array.IndexOf( _languageId, _language )];
    }

    internal static bool GetIsReady () {
        return _isReady;
    }

    internal static Language GetLanguage () {
        return _language;
    }

    internal static String GetLanguageString () {
        return _language.ToString();
    }

    internal static void SetLanguageNoEvent ( Language language ) {
        _language = language;
    }

    internal static void SetLanguage ( Language language ) {
        _language = language;
        OnChangeLanguage( language );
    }
}

