﻿using Newtonsoft.Json;

namespace Utility {
    public struct SecureInt {
        public readonly int offset;
        public readonly int value;

        [JsonConstructor]
        public SecureInt ( int offset, int value ) {
            this.offset = offset;
            this.value = value;
        }

        public SecureInt ( int value ) {
            this.offset = UnityEngine.Random.Range ( -100, 100 );
            this.value = value + this.offset;
        }

        public int GetValue () {
            return value - offset;
        }

        public override bool Equals ( object obj ) {
            if ( !( obj is SecureInt ) ) {
                return false;
            }
            SecureInt secureInt = ( SecureInt ) obj;
            return secureInt.GetValue() == GetValue();
        }
        public override int GetHashCode () {
            return base.GetHashCode();
        }

        public static SecureInt operator +( SecureInt value1, SecureInt value2 ) {
            return new SecureInt( value1.GetValue() + value2.GetValue() );
        }
        public static SecureInt operator - ( SecureInt value1, SecureInt value2 ) {
            return new SecureInt( value1.GetValue() - value2.GetValue() );
        }
        public static SecureInt operator * ( SecureInt value1, SecureInt value2 ) {
            return new SecureInt( value1.GetValue()*value2.GetValue() );
        }
        public static SecureInt operator / ( SecureInt value1, SecureInt value2 ) {
            return new SecureInt( value1.GetValue() / value2.GetValue() );
        }
        public static SecureInt operator % ( SecureInt value1, SecureInt value2 ) {
            return new SecureInt( value1.GetValue() % value2.GetValue() );
        }
        public static SecureInt operator + ( SecureInt value1, int value2 ) {
            return new SecureInt( value1.GetValue() + value2 );
        }
        public static SecureInt operator ++ ( SecureInt value1 ) {
            return new SecureInt( value1.GetValue() + 1 );
        }
        public static SecureInt operator -- ( SecureInt value1 ) {
            return new SecureInt( value1.GetValue() - 1 );
        }

        public static bool operator == ( SecureInt value1, SecureInt value2 ) {
            return value1.GetValue() == value2.GetValue();
        }
        public static bool operator != ( SecureInt value1, SecureInt value2 ) {
            return value1.GetValue() != value2.GetValue();
        }
        public static bool operator < ( SecureInt value1, SecureInt value2 ) {
            return value1.GetValue() < value2.GetValue();
        }
        public static bool operator <= ( SecureInt value1, SecureInt value2 ) {
            return value1.GetValue() <= value2.GetValue();
        }
        public static bool operator > ( SecureInt value1, SecureInt value2 ) {
            return value1.GetValue() > value2.GetValue();
        }
        public static bool operator >= ( SecureInt value1, SecureInt value2 ) {
            return value1.GetValue() >= value2.GetValue();
        }

        public static bool operator == ( SecureInt value1, int value2 ) {
            return value1.GetValue() == value2;
        }
        public static bool operator != ( SecureInt value1, int value2 ) {
            return value1.GetValue() != value2;
        }
        public static bool operator < ( SecureInt value1, int value2 ) {
            return value1.GetValue() < value2;
        }
        public static bool operator <= ( SecureInt value1, int value2 ) {
            return value1.GetValue() <= value2;
        }
        public static bool operator > ( SecureInt value1, int value2 ) {
            return value1.GetValue() > value2;
        }
        public static bool operator >= ( SecureInt value1, int value2 ) {
            return value1.GetValue() >= value2;
        }

        public static bool operator == ( int value1, SecureInt value2 ) {
            return value1 == value2.GetValue();
        }
        public static bool operator != ( int value1, SecureInt value2 ) {
            return value1 != value2.GetValue();
        }
        public static bool operator < ( int value1, SecureInt value2 ) {
            return value1 < value2.GetValue();
        }
        public static bool operator <= ( int value1, SecureInt value2 ) {
            return value1 <= value2.GetValue();
        }
        public static bool operator > ( int value1, SecureInt value2 ) {
            return value1 > value2.GetValue();
        }
        public static bool operator >= ( int value1, SecureInt value2 ) {
            return value1 >= value2.GetValue();
        }
    }

    public struct SecureFloat {
        public readonly float offset;
        public readonly float value;

        internal SecureFloat ( float value ) {
            this.offset = UnityEngine.Random.Range( -100f, 100f );
            this.value = value + this.offset;
        }

        internal float GetValue () {
            return value - offset;
        }

        public override bool Equals ( object obj ) {
            if ( !( obj is SecureFloat ) ) {
                return false;
            }
            SecureFloat secureFloat = ( SecureFloat ) obj;
            return secureFloat.GetValue() == GetValue();
        }
        public override int GetHashCode () {
            return base.GetHashCode();
        }

        public static SecureFloat operator + ( SecureFloat value1, SecureFloat value2 ) {
            return new SecureFloat( value1.GetValue() + value2.GetValue() );
        }
        public static SecureFloat operator - ( SecureFloat value1, SecureFloat value2 ) {
            return new SecureFloat( value1.GetValue() - value2.GetValue() );
        }
        public static SecureFloat operator * ( SecureFloat value1, SecureFloat value2 ) {
            return new SecureFloat( value1.GetValue()*value2.GetValue() );
        }
        public static SecureFloat operator / ( SecureFloat value1, SecureFloat value2 ) {
            return new SecureFloat( value1.GetValue() / value2.GetValue() );
        }
        public static SecureFloat operator % ( SecureFloat value1, SecureFloat value2 ) {
            return new SecureFloat( value1.GetValue() % value2.GetValue() );
        }
        public static bool operator == ( SecureFloat value1, SecureFloat value2 ) {
            return value1.GetValue() == value2.GetValue();
        }
        public static bool operator != ( SecureFloat value1, SecureFloat value2 ) {
            return value1.GetValue() != value2.GetValue();
        }
        public static bool operator < ( SecureFloat value1, SecureFloat value2 ) {
            return value1.GetValue() < value2.GetValue();
        }
        public static bool operator <= ( SecureFloat value1, SecureFloat value2 ) {
            return value1.GetValue() <= value2.GetValue();
        }
        public static bool operator > ( SecureFloat value1, SecureFloat value2 ) {
            return value1.GetValue() > value2.GetValue();
        }
        public static bool operator >= ( SecureFloat value1, SecureFloat value2 ) {
            return value1.GetValue() > value2.GetValue();
        }
    }
}
