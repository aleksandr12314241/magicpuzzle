﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Menu {
    public class Timer: MonoBehaviour {
        internal event Action OnEndTimer = () => { };

        [SerializeField]
        Text _timeText;

        Coroutine _timeUpdate;
        DateTime _dateTime;
        bool _isGo;

        internal string GetTimeText () {
            return _timeText.text;
        }

        //private void OnApplicationPause ( bool pause ) {
        //    Debug.Log( "pause   " + pause );
        //}

        internal void StartTimer ( DateTime dateTime ) {
            if ( _timeUpdate  != null ) {
                StopCoroutine( _timeUpdate );
                _timeUpdate = null;
            }

            i_time = 0;
            _dateTime = dateTime;
            _timeUpdate = StartCoroutine( TimeUpdate() );
        }

        internal void StopTimer () {
            if ( _timeUpdate != null ) {
                StopCoroutine( _timeUpdate );
                _timeUpdate = null;
            }
        }

        int i_time = 0;
        IEnumerator TimeUpdate () {
            TimeSpan tempTime = _dateTime - DateTime.Now;
            _timeText.text = tempTime.Days.ToString() + ":" + tempTime.Hours.ToString() + ":" + tempTime.Minutes.ToString() + ":" +
                             tempTime.Seconds.ToString();

            for ( ;; ) {
                yield return new WaitForSeconds( 1f );
                //Debug.Log( i_time );
                i_time++;
                tempTime = _dateTime - DateTime.Now;
                _timeText.text = tempTime.Days.ToString() + ":" + tempTime.Hours.ToString() + ":" + tempTime.Minutes.ToString() + ":" +
                                 tempTime.Seconds.ToString();

                if ( tempTime.TotalSeconds <= 0  ) {
                    EndTimer();
                }
            }
        }

        void EndTimer () {
            if ( _timeUpdate != null ) {
                StopCoroutine( _timeUpdate );
            }

            OnEndTimer();
        }
    }
}