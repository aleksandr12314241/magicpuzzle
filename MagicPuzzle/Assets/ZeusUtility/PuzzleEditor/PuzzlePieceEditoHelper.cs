﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class PuzzlePieceEditoHelper {
    PuzzlePiecesDataSet puzzlePiecesDataSet;

    internal PuzzlePieceEditoHelper ( PuzzlePiecesDataSet puzzlePiecesDataSet ) {
        this.puzzlePiecesDataSet = puzzlePiecesDataSet;
    }
}
#endif
