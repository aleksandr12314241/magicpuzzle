﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using System.IO;
using System.Text;

public class PuzzlePieceEditorWindow: EditorWindow {
    const string Title = "Puzzle Piece Editor Window";

    PuzzlePieceEditoHelper puzzlePieceEditoHelper;

    PuzzlePiecesDataSet puzzlePiecesDataSet;
    Vector2 scrollPosition = Vector2.zero;

    public int selGridPuzzlePiece = 0;
    public Texture[] piecesTexture;
    public List<int> indexesPieceTexture;

    Sprite cutterSprite;
    Sprite cutterSpriteOld;
    public string[] cutterSpriteTemplateName;
    public int selCutterSpriteTemplate = 0;
    public int selCutterSpriteTemplateOld = 0;

    PuzzelTemplatesNameByImageName puzzelTemplatesNameByImageName;

    Rect rectChoosePiseField;
    float offsetRectChoosePiseField;

    int xTmp;
    int yTmp;
    int col;
    int row;

    Texture textureEmpty;
    Dictionary<CustomVector2Int, int> puzzlePieceByIndex;
    string[] exitPieceName;
    int indexExitPieceName = 0;
    Texture textureRed;
    Texture textureGreen;
    Texture2D texture2DRed;

    bool isDrowConnect;
    List<List<CustomVector2Int>> connectionPuzzlePiece;
    List<CustomVector2Int> connectionPuzzlePieceTemp = new List<CustomVector2Int>();

    string loadPuzlleTemplateName;

    [MenuItem( "Tools/PuzzlePieceEditorWindow" )]
    static void OpenEditorWindow () {
        var win = GetWindow<PuzzlePieceEditorWindow>( true );
        win.titleContent.text = Title;
    }

    void OnEnable () {
        puzzlePiecesDataSet = AssetDatabase.LoadAssetAtPath<PuzzlePiecesDataSet>( "Assets/Main/DataSets/PuzzlePiecesDataSet.asset" );
        puzzlePieceEditoHelper = new PuzzlePieceEditoHelper( puzzlePiecesDataSet );

        piecesTexture = new Texture[puzzlePiecesDataSet.puzzlePieces.Length];

        for ( int i = 0; i < piecesTexture.Length; i++ ) {
            piecesTexture[i] = puzzlePiecesDataSet.puzzlePieces[i].piceSprites[0].texture;
        }

        cutterSprite = AssetDatabase.LoadAssetAtPath<Sprite>( "Assets/Main/Sprites/Puzzle2/img_flowers_900_600.jpg" );
        cutterSpriteOld = cutterSprite;

        string saveObjectPath = Application.dataPath + "/StreamingAssets/puzzle_template_by_image.json";
        string laodObject = File.ReadAllText( saveObjectPath );
        puzzelTemplatesNameByImageName = JsonConvert.DeserializeObject<PuzzelTemplatesNameByImageName>( laodObject );
        UpdateCuttersSprite();

        rectChoosePiseField = new Rect( 25, 25, 650, 200 );
        offsetRectChoosePiseField = 20f;

        textureEmpty = new Texture2D( 1, 1 );
        texture2DRed = new Texture2D( 50, 50 );
        for ( int y = 0; y < texture2DRed.height; y++ ) {
            for ( int x = 0; x < texture2DRed.width; x++ ) {
                texture2DRed.SetPixel( x, y, Color.red );
            }
        }
        textureRed = texture2DRed;

        puzzlePieceByIndex = new Dictionary<CustomVector2Int, int>();

        exitPieceName = new string[10];
        exitPieceName[0] = "all";
        exitPieceName[1] = "up_left";
        exitPieceName[2] = "up";
        exitPieceName[3] = "up_right";
        exitPieceName[4] = "right";
        exitPieceName[5] = "down_right";
        exitPieceName[6] = "down";
        exitPieceName[7] = "down_left";
        exitPieceName[8] = "left";
        exitPieceName[9] = "center";

        isDrowConnect = false;
        connectionPuzzlePiece = new List<List<CustomVector2Int>>();
        loadPuzlleTemplateName = "";
    }

    void OnDisable () {
        puzzlePieceEditoHelper = null;
    }

    void OnGUI () {
        GUI.BeginGroup( new Rect( Screen.width / 200f, Screen.height / 200f, Screen.width / 1.45f, Screen.height / 3.5f ) );
        GUI.Box( new Rect( Screen.width / 200f, Screen.height / 200f, Screen.width / 1.45f, Screen.height / 3.5f ), "Pieces" );
        selGridPuzzlePiece = GUI.SelectionGrid( new Rect( Screen.width / 60f, Screen.height / 30f, Screen.width / 1.5f, Screen.height / 4f ),
                                                selGridPuzzlePiece, piecesTexture, 15 );
        GUI.EndGroup();

        scrollPosition = GUILayout.BeginScrollView( scrollPosition );
        GUI.BeginGroup( new Rect( Screen.width / 1.45f, Screen.height / 200f, Screen.width / 5f, Screen.height / 3.5f ) );
        EditorGUILayout.LabelField( "фильтр/filter" );
        indexExitPieceName = GUI.SelectionGrid( new Rect( 25, 25, 200, 210 ), indexExitPieceName, exitPieceName, 1,
                                                EditorStyles.radioButton );

        SetSelTextureTemp();
        GUI.EndGroup();
        GUILayout.EndScrollView();

        if ( selGridPuzzlePiece < piecesTexture.Length ) {
            EditorGUILayout.LabelField( piecesTexture[selGridPuzzlePiece].name );
        }

        Rect rectImage = new Rect( Screen.width / 200f, Screen.height / 2.8f, cutterSprite.rect.width / 2, cutterSprite.rect.height / 2 );

        cutterSprite = ( Sprite ) EditorGUI.ObjectField( new Rect( rectImage.x, rectImage.y / 1.2f, 190, 190 ),
                       "Картинка пазла",
                       cutterSprite,
                       typeof( Sprite ), true );

        GUI.DrawTexture( rectImage, cutterSprite.texture, ScaleMode.ScaleToFit );

        EditorGUI.LabelField( new Rect( Screen.width / 2.25f, Screen.height / 3.2f, 50, 20 ), "колон" );
        EditorGUI.LabelField( new Rect( Screen.width / 2.05f, Screen.height / 3.2f, 50, 20 ), "строк" );
        xTmp = EditorGUI.IntField( new Rect( Screen.width / 2.25f, Screen.height / 3f, 50, 20 ), xTmp );
        EditorGUI.LabelField( new Rect( Screen.width / 2.06f, Screen.height / 3f, 50, 20 ), "x" );
        yTmp = EditorGUI.IntField( new Rect( Screen.width / 2f, Screen.height / 3f, 50, 20 ), yTmp );

        GUILayout.BeginArea( new Rect( Screen.width / 2.25f, Screen.height / 2.7f, 140, 20 ) );
        if ( GUILayout.Button( "сформировать сетку" ) ) {
            col = xTmp;
            row = yTmp;
            puzzlePieceByIndex = new Dictionary<CustomVector2Int, int>();
            connectionPuzzlePiece = new List<List<CustomVector2Int>>();
            connectionPuzzlePieceTemp = new List<CustomVector2Int>();
            isDrowConnect = false;

            if ( selGridPuzzlePiece >= 1000 ) {
                selGridPuzzlePiece = 0;
            }
        }
        GUILayout.EndArea();

        bool isChangeMatrix = false;
        if ( col != 0 && row != 0 ) {

            Vector2 stepTexture = new Vector2( rectImage.width / col, rectImage.height / row );

            GUILayout.BeginArea( new Rect( Screen.width / 2.25f, Screen.height / 2.5f, 140, 20 ) );
            if ( GUILayout.Button( "заполнить случайно" ) ) {
                FillPuzzleRandomPiece();
            }
            GUILayout.EndArea();

            if ( ( Event.current.type == EventType.MouseDown ) && ( Event.current.button == 0 ) &&
                    rectImage.Contains( Event.current.mousePosition )  ) {
                CustomVector2Int indexArray = new CustomVector2Int( ( int ) ( ( Event.current.mousePosition.x - rectImage.x ) / stepTexture.x ),
                        ( int ) ( ( Event.current.mousePosition.y - rectImage.y ) / stepTexture.y ) );

                if ( selGridPuzzlePiece < indexesPieceTexture.Count ) {
                    int indexPieceTexture = indexesPieceTexture[selGridPuzzlePiece];

                    if ( puzzlePieceByIndex.ContainsKey( indexArray ) ) {
                        puzzlePieceByIndex[indexArray] = indexPieceTexture;
                    }
                    else {
                        puzzlePieceByIndex.Add( indexArray, indexPieceTexture );
                    }

                }
                else if ( isDrowConnect ) {
                    bool isContainsInconnectionPuzzlePiece = false;
                    for ( int i = 0; i < connectionPuzzlePiece.Count; i++ ) {
                        if ( connectionPuzzlePiece[i].Contains( indexArray ) ) {
                            isContainsInconnectionPuzzlePiece = true;
                            break;
                        }
                    }

                    if ( !connectionPuzzlePieceTemp.Contains( indexArray ) && !isContainsInconnectionPuzzlePiece ) {

                        if ( connectionPuzzlePieceTemp.Count > 0 ) {
                            CustomVector2Int indexArrayLast = connectionPuzzlePieceTemp[connectionPuzzlePieceTemp.Count - 1];

                            CustomVector2Int differenceIndex = new CustomVector2Int( Mathf.Abs( indexArray.x - indexArrayLast.x ),
                                    Mathf.Abs( indexArray.y - indexArrayLast.y ) );

                            if ( differenceIndex.x <= 1 && differenceIndex.y <= 1 &&
                                    ( differenceIndex.x == 0 || differenceIndex.y == 0 ) ) {
                                connectionPuzzlePieceTemp.Add( indexArray );
                            }
                        }
                        else {
                            connectionPuzzlePieceTemp.Add( indexArray );
                        }
                    }
                }

                isChangeMatrix = true;
            }

            for ( int j = 0; j < row; j++ ) {
                for ( int i = 0; i < col; i++ ) {
                    GUI.DrawTexture( new Rect( rectImage.x + ( i * stepTexture.x ), rectImage.y + ( j * stepTexture.y ),
                                               stepTexture.x / 1.1f, stepTexture.y / 1.1f ), textureEmpty, ScaleMode.ScaleToFit );
                    CustomVector2Int indexArrayTemp = new CustomVector2Int( i, j );
                    if ( puzzlePieceByIndex.ContainsKey( indexArrayTemp ) ) {
                        Texture pieceTexture = puzzlePiecesDataSet.puzzlePieces[puzzlePieceByIndex[indexArrayTemp]].PiceSprites[0].texture;

                        GUI.DrawTexture( new Rect( rectImage.x + ( i * stepTexture.x ), rectImage.y + ( j * stepTexture.y ),
                                                   stepTexture.x / 1.1f, stepTexture.y / 1.1f ), pieceTexture, ScaleMode.ScaleToFit );
                    }
                    else {
                        GUI.DrawTexture( new Rect( rectImage.x + ( i * stepTexture.x ), rectImage.y + ( j * stepTexture.y ),
                                                   stepTexture.x / 1.1f, stepTexture.y / 1.1f ), textureEmpty, ScaleMode.ScaleToFit );
                    }
                }
            }

            for ( int i = 0; i < connectionPuzzlePiece.Count; i++ ) {
                for ( int j = 0; j < connectionPuzzlePiece[i].Count - 1; j++ ) {
                    Vector2 firstPosition = new Vector2( rectImage.x + ( connectionPuzzlePiece[i][j].x * stepTexture.x ),
                                                         rectImage.y + ( connectionPuzzlePiece[i][j].y * stepTexture.y ) ) + stepTexture / 1.1f / 2f;
                    Vector2 secondPosition = new Vector2( rectImage.x + ( connectionPuzzlePiece[i][j + 1].x * stepTexture.x ),
                                                          rectImage.y + ( connectionPuzzlePiece[i][j + 1].y * stepTexture.y ) ) + stepTexture / 1.1f / 2f;

                    Handles.color = new Color( 0, 0, 0, 1 );
                    Handles.DrawLine( firstPosition, secondPosition );
                }
            }

            for ( int i = 0; i < connectionPuzzlePieceTemp.Count - 1; i++ ) {
                Vector2 firstPosition = new Vector2( rectImage.x + ( connectionPuzzlePieceTemp[i].x * stepTexture.x ),
                                                     rectImage.y + ( connectionPuzzlePieceTemp[i].y * stepTexture.y ) ) + stepTexture / 1.1f / 2f;
                Vector2 secondPosition = new Vector2( rectImage.x + ( connectionPuzzlePieceTemp[i + 1].x * stepTexture.x ),
                                                      rectImage.y + ( connectionPuzzlePieceTemp[i + 1].y * stepTexture.y ) ) + stepTexture / 1.1f / 2f;

                Handles.color = new Color( 0, 0, 0, 1 );
                //Handles.RadiusHandle = new Vector2( 10, 10 );
                Handles.DrawLine( firstPosition, secondPosition );
            }

            bool isCorrectArray = IsCorrectArray();
            if ( isCorrectArray ) {
                isDrowConnect = GUI.Toggle( new Rect( Screen.width / 2.25f, Screen.height / 2f, 140, 20 ), isDrowConnect,
                                            "сделать соединение" );
                selGridPuzzlePiece = isDrowConnect ? 1000 : 0;

                GUILayout.BeginArea( new Rect( Screen.width / 2.25f, Screen.height / 1.9f, 140, 20 ) );
                if ( GUILayout.Button( "следующая линия" ) ) {
                    if ( connectionPuzzlePieceTemp.Count >= 2 ) {
                        if ( connectionPuzzlePieceTemp[connectionPuzzlePieceTemp.Count - 1].y < connectionPuzzlePieceTemp[0].y ) {
                            connectionPuzzlePieceTemp.Reverse();
                        }
                        else if ( connectionPuzzlePieceTemp[connectionPuzzlePieceTemp.Count - 1].x < connectionPuzzlePieceTemp[0].x ) {
                            connectionPuzzlePieceTemp.Reverse();
                        }

                        //Debug.Log( "===================" );
                        //for ( int i = 0; i < connectionPuzzlePieceTemp.Count; i++ ) {
                        //    Debug.Log( connectionPuzzlePieceTemp[i] );
                        //}

                        connectionPuzzlePiece.Add( connectionPuzzlePieceTemp );
                    }
                    connectionPuzzlePieceTemp = new List<CustomVector2Int>();

                }
                GUILayout.EndArea();

                GUILayout.BeginArea( new Rect( Screen.width / 2.25f, Screen.height / 1.8f, 140, 20 ) );
                if ( GUILayout.Button( "сбросить" ) ) {
                    connectionPuzzlePieceTemp = new List<CustomVector2Int>();
                }
                GUILayout.EndArea();

                float sizeTexture = 35f;
                for ( int i = 0; i < connectionPuzzlePieceTemp.Count; i++ ) {
                    Texture pieceTextureTemp = puzzlePiecesDataSet.puzzlePieces[puzzlePieceByIndex[connectionPuzzlePieceTemp[i]]].PiceSprites[0].texture;

                    GUI.DrawTexture( new Rect( Screen.width / 2.25f + ( i * sizeTexture ), Screen.height / 1.73f,
                                               sizeTexture, sizeTexture ), pieceTextureTemp, ScaleMode.ScaleToFit );
                }
            }

            Color colorRect = isCorrectArray ? Color.green : Color.red;
            string textRect = isCorrectArray ? "правильно, можно сохранять" : "неправильно";

            EditorGUI.LabelField( new Rect( Screen.width / 2.1f, Screen.height / 1.6f, 200, 20 ), textRect );
            EditorGUI.DrawRect( new Rect( Screen.width / 2.25f, Screen.height / 1.6f, 20, 20 ), colorRect );
        }

        if ( col != 0 && row != 0 && IsCorrectArray() ) {

            GUILayout.BeginArea( new Rect( Screen.width / 2.5f, Screen.height / 1.35f, 140, 20 ) );
            if ( GUILayout.Button( "Привязать к картинке" ) ) {
                JoinTemplateToImage();
            }
            GUILayout.EndArea();

            GUILayout.BeginArea( new Rect( Screen.width / 1.5f, Screen.height / 1.35f, 140, 20 ) );
            if ( GUILayout.Button( "Сохранить как новый" ) ) {
                SaveTemplateAsNew();
            }
            GUILayout.EndArea();

            GUILayout.BeginArea( new Rect( Screen.width / 1.5f, Screen.height / 1.30f, 140, 20 ) );
            if ( GUILayout.Button( "Переписать шаблон" ) ) {
                SaveTemplateAsOld();
            }
            GUILayout.EndArea();
        }

        if ( selCutterSpriteTemplate < cutterSpriteTemplateName.Length ) {
            GUILayout.BeginArea( new Rect( Screen.width / 1.7f, Screen.height / 3f, 20, 20 ) );
            if ( GUILayout.Button( "x" ) ) {
                selCutterSpriteTemplate = cutterSpriteTemplateName.Length;
                loadPuzlleTemplateName = "";
            }
            GUILayout.EndArea();
        }

        //GUI.BeginGroup( new Rect( Screen.width / 2f, Screen.height / 1.5f, Screen.width / 5f, Screen.height / 5f ) );
        GUI.Box( new Rect( Screen.width / 1.7f, Screen.height / 2.6f - 20, Screen.width / 5.5f, Screen.height / 3.5f + 20 ), "Шаблоны" );
        selCutterSpriteTemplate = GUI.SelectionGrid( new Rect( Screen.width / 1.7f, Screen.height / 2.6f, Screen.width / 5.5f,
                                  Screen.height / 3.5f ),
                                  selCutterSpriteTemplate, cutterSpriteTemplateName, 1, EditorStyles.radioButton );

        if ( selCutterSpriteTemplate != selCutterSpriteTemplateOld ) {
            if ( selCutterSpriteTemplate < cutterSpriteTemplateName.Length ) {
                loadPuzlleTemplateName = cutterSpriteTemplateName[selCutterSpriteTemplate];
            }
        }

        loadPuzlleTemplateName = EditorGUI.TextField( new Rect( Screen.width / 2.2f, Screen.height / 1.47f, 350, 20 ),
                                 "Имя шаблона:",
                                 loadPuzlleTemplateName );

        GUILayout.BeginArea( new Rect( Screen.width / 1.9f, Screen.height / 1.35f, 140, 20 ) );
        if ( GUILayout.Button( "Загрузить шаблон" ) ) {
            LoadTemplate();
        }
        GUILayout.EndArea();

        if ( isChangeMatrix ) {
            Repaint();
        }
    }

    void SetSelTextureTemp () {
        indexesPieceTexture = puzzlePiecesDataSet.GetIndexPuzzlePiecesByExitPiece( exitPieceName[indexExitPieceName] );
        piecesTexture = new Texture[indexesPieceTexture.Count];

        for ( int i = 0; i < indexesPieceTexture.Count; i++ ) {
            piecesTexture[i] = puzzlePiecesDataSet.puzzlePieces[indexesPieceTexture[i]].PiceSprites[0].texture;
        }
    }

    void FillPuzzleRandomPiece () {
        puzzlePieceByIndex = new Dictionary<CustomVector2Int, int>();

        for ( int i = 0; i < col; i++ ) {
            for ( int j = 0; j < row; j++ ) {
                string exitPieceByIndex = GetExitPieceByIndex( new CustomVector2Int( i, j ) );

                List<int> indexesPieceTextureTemp = puzzlePiecesDataSet.GetIndexPuzzlePiecesByExitPiece( exitPieceByIndex );

                CustomVector2Int leftPiece = new CustomVector2Int( i - 1, j );
                CustomVector2Int upPiece = new CustomVector2Int( i, j - 1 );

                List<int> removedIndexesPieceConectd = new List<int>();

                if ( leftPiece.x >= 0 && puzzlePieceByIndex.ContainsKey( leftPiece ) ) {
                    int indexLeftPiece = puzzlePieceByIndex[leftPiece];
                    PuzzlePieceConnectType right = puzzlePiecesDataSet.puzzlePieces[indexLeftPiece].puzzlePieceConnect.Right;

                    for ( int k = 0; k < indexesPieceTextureTemp.Count; k++ ) {
                        PuzzlePieceConnectType left = puzzlePiecesDataSet.puzzlePieces[indexesPieceTextureTemp[k]].puzzlePieceConnect.Left;

                        if ( !GameHelper.CanPuzzlePieceConnectConnect( right, left ) ) {
                            removedIndexesPieceConectd.Add( k );
                        }
                    }
                }

                if ( upPiece.y >= 0 && puzzlePieceByIndex.ContainsKey( upPiece ) ) {
                    int indexUpPiece = puzzlePieceByIndex[upPiece];

                    PuzzlePieceConnectType down = puzzlePiecesDataSet.puzzlePieces[indexUpPiece].puzzlePieceConnect.Down;

                    for ( int k = 0; k < indexesPieceTextureTemp.Count; k++ ) {
                        PuzzlePieceConnectType up = puzzlePiecesDataSet.puzzlePieces[indexesPieceTextureTemp[k]].puzzlePieceConnect.Up;

                        if ( !GameHelper.CanPuzzlePieceConnectConnect( down, up ) ) {
                            if ( !removedIndexesPieceConectd.Contains( k ) ) {
                                removedIndexesPieceConectd.Add( k );
                            }
                        }
                    }
                }

                List<int> indexesPieceCanConectd = new List<int>();
                for ( int k = 0; k < indexesPieceTextureTemp.Count; k++ ) {
                    if ( !removedIndexesPieceConectd.Contains( k ) ) {
                        indexesPieceCanConectd.Add( indexesPieceTextureTemp[k] );
                    }
                }

                int randomIndexPiece = indexesPieceCanConectd[Random.Range( 0, indexesPieceCanConectd.Count )];
                puzzlePieceByIndex.Add( new CustomVector2Int( i, j ), randomIndexPiece );
            }
        }
    }

    string GetExitPieceByIndex ( CustomVector2Int index ) {
        if ( index.x == 0 && index.y == 0 ) {//up_left
            return "up_left";
        }
        else if ( index.x == ( col - 1 ) && index.y == 0 ) {//up_right
            return "up_right";
        }
        else if ( index.x == ( col - 1 ) && index.y == ( row - 1 ) ) {//down_right
            return "down_right";
        }
        else if ( index.x == 0 && index.y == ( row - 1 ) ) {//down_left
            return "down_left";
        }
        else if ( index.y == 0 ) {
            return "up";
        }
        else if ( index.x == ( col - 1 ) ) {
            return "right";
        }
        else if ( index.y == ( row - 1 ) ) {
            return "down";
        }
        else if ( index.x == 0 ) {
            return "left";
        }

        return "center";
    }

    bool IsCorrectArray () {
        if ( puzzlePieceByIndex.Count < col * row ) {
            return false;
        }

        for ( int i = 0; i < col; i++ ) {
            for ( int j = 0; j < row; j++ ) {

                CustomVector2Int leftPiece = new CustomVector2Int( i - 1, j );
                CustomVector2Int upPiece = new CustomVector2Int( i, j - 1 );

                int indexPiece = puzzlePieceByIndex[new CustomVector2Int( i, j )];
                PuzzlePieceConnectType checkPieceLeft = puzzlePiecesDataSet.puzzlePieces[indexPiece].puzzlePieceConnect.Left;
                PuzzlePieceConnectType checkPieceUp = puzzlePiecesDataSet.puzzlePieces[indexPiece].puzzlePieceConnect.Up;

                if ( leftPiece.x >= 0 && puzzlePieceByIndex.ContainsKey( leftPiece ) ) {
                    int indexLeftPieceLeft = puzzlePieceByIndex[leftPiece];
                    PuzzlePieceConnectType right = puzzlePiecesDataSet.puzzlePieces[indexLeftPieceLeft].puzzlePieceConnect.Right;

                    if ( !GameHelper.CanPuzzlePieceConnectConnect( checkPieceLeft, right ) ) {
                        return false;
                    }
                }

                if ( upPiece.y >= 0 && puzzlePieceByIndex.ContainsKey( upPiece ) ) {
                    int indexUpPiece = puzzlePieceByIndex[upPiece];

                    PuzzlePieceConnectType down = puzzlePiecesDataSet.puzzlePieces[indexUpPiece].puzzlePieceConnect.Down;

                    if ( !GameHelper.CanPuzzlePieceConnectConnect( checkPieceUp, down ) ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    void UpdateCuttersSprite () {
        string cutterSpriteName = cutterSprite.name;

        List<string> puzzelTemplatesName = puzzelTemplatesNameByImageName.GetPuzzelTemplatesNameByImageName( cutterSpriteName );
        cutterSpriteTemplateName = new string[puzzelTemplatesName.Count];
        selCutterSpriteTemplate = puzzelTemplatesName.Count;
        selCutterSpriteTemplateOld = selCutterSpriteTemplate;

        for ( int i = 0; i < cutterSpriteTemplateName.Length; i++ ) {
            cutterSpriteTemplateName[i] = puzzelTemplatesName[i];
        }
    }

    void SaveTemplateAsOld () {
        if ( loadPuzlleTemplateName == "" ) {
            SaveTemplateAsNew();
            return;
        }

        string savePath = Application.dataPath + "/StreamingAssets/PuzzleTemplate/" + loadPuzlleTemplateName;
        if ( !File.Exists( savePath ) ) {
            SaveTemplateAsNew();
            return;
        }

        string loadObject = File.ReadAllText( savePath );
        PuzzleTemplate puzzelTemplate = JsonConvert.DeserializeObject<PuzzleTemplate>( loadObject );
        puzzelTemplate.SetSizeMatrixImage( new CustomVector2Int( col, row ) );
        puzzelTemplate.SetPuzzlePieceByIndex( puzzlePieceByIndex );
        puzzelTemplate.SetConnectionPuzzlePiece( connectionPuzzlePiece );

        SaveTemplate( loadPuzlleTemplateName, puzzelTemplate );
    }

    void SaveTemplateAsNew () {
        string pistfix = "_" + col.ToString() + "_" + row.ToString() + "_" + connectionPuzzlePiece.Count.ToString();

        List<string>  puzzleTemplateFiles = GetPuzzleTemplateFiles();

        int counterTemplate = 0;
        for ( int i = 0; i < puzzleTemplateFiles.Count; i++ ) {
            if ( puzzleTemplateFiles[i].Contains( pistfix ) ) {
                counterTemplate++;
            }
        }

        pistfix += "_" + counterTemplate;
        string namePuzzleTemplate = "puzzle_teplate" + pistfix;
        string namePuzzleTemplateFile = namePuzzleTemplate + ".json";

        PuzzleTemplate puzzelTemplate = new PuzzleTemplate( new CustomVector2Int( col, row ) );
        puzzelTemplate.SetPuzzlePieceByIndex( puzzlePieceByIndex );
        puzzelTemplate.SetConnectionPuzzlePiece( connectionPuzzlePiece );

        SaveTemplate( namePuzzleTemplate + ".json", puzzelTemplate );

        selCutterSpriteTemplate = cutterSpriteTemplateName.Length;
        selCutterSpriteTemplateOld = selCutterSpriteTemplate;
        loadPuzlleTemplateName = namePuzzleTemplateFile;
    }

    void SaveTemplate ( string fileName, PuzzleTemplate puzzelTemplate ) {
        string saveObjectPath = Application.dataPath + "/StreamingAssets/PuzzleTemplate/" + fileName;

        string json = JsonConvert.SerializeObject( puzzelTemplate );
        File.WriteAllText( saveObjectPath, json, Encoding.UTF8 );
    }

    List<string> GetPuzzleTemplateFiles () {
        string pathToTemplate = Application.dataPath + "/StreamingAssets/PuzzleTemplate/";
        var info = new DirectoryInfo( pathToTemplate );
        var fileInfo = info.GetFiles();
        List<string> filesNameTemplate = new List<string>();

        for ( int i = 0; i < fileInfo.Length; i++ ) {
            FileInfo file = fileInfo[i];

            int indexStart = file.Name.Length - 4;
            string fileExpand = file.Name.Substring( indexStart );
            if ( fileExpand.Equals( "meta" ) ) {
                continue;
            }

            filesNameTemplate.Add( file.Name );
        }

        return filesNameTemplate;
    }

    void LoadTemplate () {
        if ( loadPuzlleTemplateName == "" ) {
            return;
        }

        string savePath = Application.dataPath + "/StreamingAssets/PuzzleTemplate/" + loadPuzlleTemplateName;
        if ( !File.Exists( savePath ) ) {
            return;
        }

        string loadObject = File.ReadAllText( savePath );
        PuzzleTemplate puzzelTemplate = JsonConvert.DeserializeObject<PuzzleTemplate>( loadObject );

        UpdtaeTemplateState( puzzelTemplate );
    }

    void UpdtaeTemplateState ( PuzzleTemplate puzzelTemplate ) {
        xTmp = puzzelTemplate.sizeMatrixImage.x;
        yTmp = puzzelTemplate.sizeMatrixImage.y;
        col = puzzelTemplate.sizeMatrixImage.x;
        row = puzzelTemplate.sizeMatrixImage.y;

        puzzlePieceByIndex = new Dictionary<CustomVector2Int, int>();
        foreach ( var item in puzzelTemplate.GetPuzzlePieceByIndex() ) {
            puzzlePieceByIndex.Add( item.Key, item.Value );
        }

        connectionPuzzlePiece = new List<List<CustomVector2Int>>();
        for ( int i = 0 ; i < puzzelTemplate.connectionPuzzlePiece.Count; i++ ) {
            connectionPuzzlePiece.Add( puzzelTemplate.connectionPuzzlePiece[i] );
        }
    }

    void JoinTemplateToImage () {
        if ( loadPuzlleTemplateName == "" ) {
            return;
        }

        string savePath = Application.dataPath + "/StreamingAssets/puzzle_template_by_image.json";
        if ( !File.Exists( savePath ) ) {
            return;
        }

        puzzelTemplatesNameByImageName.AddPuzzelTemplateNameByImageName( cutterSprite.name, loadPuzlleTemplateName );

        string json = JsonConvert.SerializeObject( puzzelTemplatesNameByImageName );
        File.WriteAllText( savePath, json, Encoding.UTF8 );

        UpdateCuttersSprite();
    }
}
#endif