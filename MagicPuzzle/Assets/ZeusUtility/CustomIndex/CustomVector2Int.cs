﻿using Newtonsoft.Json;
using UnityEngine;

public struct CustomVector2Int {
    public readonly int x;
    public readonly int y;

    [JsonConstructor]
    public CustomVector2Int ( int x, int y ) {
        this.x = x;
        this.y = y;
    }

    //CustomVector2Int * CustomVector2Int
    public static CustomVector2Int operator + ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return new CustomVector2Int( value1.x + value2.x, value1.y + value2.y );
    }
    public static CustomVector2Int operator - ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return new CustomVector2Int( value1.x - value2.x, value1.y - value2.y );
    }
    public static CustomVector2Int operator * ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return new CustomVector2Int( value1.x * value2.x, value1.y * value2.y );
    }
    public static CustomVector2Int operator / ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return new CustomVector2Int( value1.x / value2.x, value1.y / value2.y );
    }
    public static CustomVector2Int operator % ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return new CustomVector2Int( value1.x % value2.x, value1.y % value2.y );
    }

    //CustomVector2Int * int
    public static CustomVector2Int operator * ( CustomVector2Int value1, int value2 ) {
        return new CustomVector2Int( value1.x * value2, value1.y * value2 );
    }
    public static CustomVector2Int operator / ( CustomVector2Int value1, int value2 ) {
        return new CustomVector2Int( value1.x / value2, value1.y / value2 );
    }
    public static CustomVector2Int operator % ( CustomVector2Int value1, int value2 ) {
        return new CustomVector2Int( value1.x % value2, value1.y % value2 );
    }
    //int * CustomVector2Int
    public static CustomVector2Int operator * ( int value1, CustomVector2Int value2 ) {
        return new CustomVector2Int( value1 * value2.x, value1 * value2.y );
    }
    //CustomVector2Int * float
    public static Vector2 operator * ( CustomVector2Int value1, float value2 ) {
        return new Vector2( value1.x * value2, value1.y * value2 );
    }
    public static Vector2 operator / ( CustomVector2Int value1, float value2 ) {
        return new Vector2( value1.x / value2, value1.y / value2 );
    }

    //float * CustomVector2Int
    public static Vector2 operator * ( float value1, CustomVector2Int value2 ) {
        return new Vector2( value1 * value2.x, value1 * value2.y );
    }

    //CustomVector2Int * Vector2
    public static Vector2 operator + ( CustomVector2Int value1, Vector2 value2 ) {
        return new Vector2( value1.x + value2.x, value1.y + value2.y );
    }
    public static Vector2 operator - ( CustomVector2Int value1, Vector2 value2 ) {
        return new Vector2( value1.x - value2.x, value1.y - value2.y );
    }
    public static Vector2 operator * ( CustomVector2Int value1, Vector2 value2 ) {
        return new Vector2( value1.x * value2.x, value1.y * value2.y );
    }
    public static Vector2 operator / ( CustomVector2Int value1, Vector2 value2 ) {
        return new Vector2( value1.x / value2.x, value1.y / value2.y );
    }

    //Vector2 * CustomVector2Int
    public static Vector2 operator + ( Vector2 value1, CustomVector2Int value2 ) {
        return new Vector2( value1.x + value2.x, value1.y + value2.y );
    }
    public static Vector2 operator - ( Vector2 value1, CustomVector2Int value2 ) {
        return new Vector2( value1.x - value2.x, value1.y - value2.y );
    }
    public static Vector2 operator * ( Vector2 value1, CustomVector2Int value2 ) {
        return new Vector2( value1.x * value2.x, value1.y * value2.y );
    }
    public static Vector2 operator / ( Vector2 value1, CustomVector2Int value2 ) {
        return new Vector2( value1.x / value2.x, value1.y / value2.y );
    }

    public static bool operator == ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return value1.x == value2.x && value1.y == value2.y;
    }
    public static bool operator != ( CustomVector2Int value1, CustomVector2Int value2 ) {
        return value1.x != value2.x || value1.y != value2.y;
    }
}
