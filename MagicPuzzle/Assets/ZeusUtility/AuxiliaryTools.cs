﻿using System.IO;
using System.Text;
using System;

namespace Utility {
    public static class AuxiliaryTools {
        internal static string GetEncryptString ( string str ) {

            byte[] bytes = Encoding.UTF8.GetBytes( str );
            string hex = BitConverter.ToString( bytes );

            return hex.Replace( "-", "" );
        }

        internal static string GetJsonFromEncrypterString ( string saveObject ) {
            int charCounts = saveObject.Length;
            byte[] bytes = new byte[charCounts / 2];
            for ( int i = 0; i < charCounts; i += 2 ) {
                bytes[i / 2] = Convert.ToByte( saveObject.Substring( i, 2 ), 16 );
            }
            saveObject = Encoding.UTF8.GetString( bytes, 0, bytes.Length );

            return saveObject;
        }
    }
}
