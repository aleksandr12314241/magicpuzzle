﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility {
    [RequireComponent( typeof( ParticleSystem ) )]
    public class AutoDestroyParticles: MonoBehaviour {
        ParticleSystem particleSystemObj;

        bool isInit = false;
        void Init () {

        }

        private void Awake () {
            particleSystemObj = GetComponent<ParticleSystem>();
        }

        private void OnEnable () {
            float startLifetime = particleSystemObj.main.startLifetime.constant;
            Invoke( "EndLifeTime", startLifetime );
        }

        void EndLifeTime () {
            gameObject.SetActive( false );
        }
    }
}
