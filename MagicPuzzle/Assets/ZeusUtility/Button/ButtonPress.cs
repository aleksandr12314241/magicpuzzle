﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

namespace Utility {
    [RequireComponent( typeof( CustomButton ), typeof( AudioSource ) )]
    public class ButtonPress: MonoBehaviour {
        [SerializeField]
        public RectTransform scaleRectTransform;
        [SerializeField]
        public float scaleOffset = 0.1f;

        public AudioClip commonAudioClip;
        public AudioClip singleAudioClip;
        public List<AudioClip> randomAudioClipsList = new List<AudioClip>();
        public ButtonPressSoundType buttonPressSoundType = ButtonPressSoundType.CommonSound;

        AudioSource myAudioSourse;

        Vector3 startScale;

        CustomButton myButton;

        void Awake () {
            commonAudioClip = Resources.Load<AudioClip>( "click" );

            myButton = GetComponent<CustomButton>();
            myAudioSourse = GetComponent<AudioSource>();
            myAudioSourse.playOnAwake = false;

            startScale = scaleRectTransform.localScale;

            myButton.OnPointerDownAction += OnPointerDown;
            myButton.OnPointerUpAction += OnPointerUp;
        }

        private void OnEnable () {
            scaleRectTransform.localScale = startScale;
        }

#if UNITY_EDITOR
        private void OnValidate () {
            if ( buttonPressSoundType == ButtonPressSoundType.RandomFromList && randomAudioClipsList.Count == 0 ) {
                Debug.LogError( "add sounds in list " + gameObject.name );
            }

            if ( scaleRectTransform == null ) {
                Debug.Log( "gameObject.name    " + gameObject.name );
            }
        }
#endif

        public void SetButtonPressSoundType ( ButtonPressSoundType buttonPressSoundType ) {
            this.buttonPressSoundType = buttonPressSoundType;
        }

        void OnPointerDown () {
            Vector3 endScale = new Vector3( startScale.x - scaleOffset, startScale.y - scaleOffset, startScale.z );
            scaleRectTransform.DOKill();
            scaleRectTransform.DOScale( endScale, 0.1f );

            PlaySound();
        }

        void OnPointerUp () {
            scaleRectTransform.DOKill();
            scaleRectTransform.DOScale( startScale, 0.01f );
        }

        void PlaySound () {
            switch ( buttonPressSoundType ) {
                case ButtonPressSoundType.CommonSound:
                    myAudioSourse.PlayOneShot( commonAudioClip );
                    break;
                case ButtonPressSoundType.SingleSound:
                    myAudioSourse.PlayOneShot( singleAudioClip );
                    break;
                case ButtonPressSoundType.RandomFromList:
                    myAudioSourse.PlayOneShot( randomAudioClipsList[Random.Range( 0, randomAudioClipsList.Count )] );
                    break;
            }
        }



        internal void SetToStartScale () {
            scaleRectTransform.localScale = startScale;
        }
    }
}
