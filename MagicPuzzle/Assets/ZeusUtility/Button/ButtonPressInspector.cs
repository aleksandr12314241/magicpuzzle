﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using System.Collections.Generic;

namespace Utility {
    [CustomEditor( typeof( ButtonPress ) )]
    [CanEditMultipleObjects]
    public class ButtonPressInspector: Editor {
        int countAudioClips;
        ButtonPress myTarget;

        void OnEnable () {
            myTarget = ( ButtonPress ) target;
        }

        public override void OnInspectorGUI () {
            serializedObject.Update();

            myTarget.scaleRectTransform = EditorGUILayout.ObjectField( "scaleRectTransform", myTarget.scaleRectTransform, typeof( RectTransform ),
                                          true ) as RectTransform;
            myTarget.scaleOffset = EditorGUILayout.FloatField( "scaleOffset", myTarget.scaleOffset );

            myTarget.buttonPressSoundType = ( ButtonPressSoundType ) EditorGUILayout.EnumPopup( "Sound Type:", myTarget.buttonPressSoundType );
            switch ( myTarget.buttonPressSoundType ) {
                case ButtonPressSoundType.CommonSound:
                    EditorGUILayout.LabelField( "Common sound is", "click" );
                    break;
                case ButtonPressSoundType.SingleSound:
                    myTarget.singleAudioClip = ( AudioClip ) EditorGUILayout.ObjectField( myTarget.singleAudioClip, typeof( AudioClip ), true );
                    break;
                case ButtonPressSoundType.RandomFromList:
                    EditorGUILayout.PropertyField( serializedObject.FindProperty( "randomAudioClipsList" ), true );

                    if ( myTarget.randomAudioClipsList.Count == 0 ) {
                        Debug.LogWarning( "You need add sounds in list " + myTarget.gameObject.name );
                    }
                    break;
                default:
                    EditorGUILayout.LabelField( "Common sound is", "no sound" );
                    break;
            }

            //myTarget.buttonPressSoundType  = EditorGUILayout.IntField( "Experience", myTarget.buttonPressSoundType );
            //EditorGUILayout.LabelField( "Level", myTarget.Level.ToString() );

            if ( GUI.changed ) {
                EditorUtility.SetDirty( target );
                serializedObject.ApplyModifiedProperties();
                EditorSceneManager.MarkSceneDirty( SceneManager.GetActiveScene() );
            }
        }
    }
}
#endif
