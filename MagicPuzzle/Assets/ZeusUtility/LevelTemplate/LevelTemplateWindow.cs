﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Newtonsoft.Json;
using System.Text;

public class LevelTemplateWindow : EditorWindow {
    const string Title = "Level Template Window";

    List<int> countElements;
    List<List<CustomVector2Int>> fillMatrix;
    List<Color> colorElements;
    string[] countElementsString;

    Vector2 startButtonPluseMinus = new Vector2( 30f, 30f );
    int indexChooseCountElements;
    int sizeField;

    string[] filesName;
    int indexFileName;
    int indexFileNameOld;
    string[] levelTemplatesName;
    int indexLevelTemplateName;

    Vector2 scrollPosition = Vector2.zero;
    LevelTemplate levelTemplate;

    [MenuItem( "Tools/LevelTemplateWindow" )]
    static void OpenEditorWindow () {
        var win = GetWindow<LevelTemplateWindow>( true );
        win.titleContent.text = Title;
    }

    void OnEnable () {
        ResetAllDataTemplate();

        startButtonPluseMinus = new Vector2( 30f, 30f );

        UpdateFiles();
        indexFileName = filesName.Length + 1;
        indexFileNameOld = indexFileName;

        levelTemplatesName = new string[0];
        indexLevelTemplateName = 1;
    }

    void ResetAllDataTemplate () {
        countElements = new List<int>();
        fillMatrix = new List<List<CustomVector2Int>>();
        colorElements = new List<Color>();
    }

    void UpdateFiles () {
        string pathToTemplate = Application.dataPath + "/StreamingAssets/LevelTemplate/";
        var info = new DirectoryInfo( pathToTemplate );
        var fileInfo = info.GetFiles();
        List<string> filesNameTemp = new List<string>();

        for ( int i = 0; i < fileInfo.Length; i++ ) {
            FileInfo file = fileInfo[i];

            int indexStart = file.Name.Length - 4;
            string fileExpand = file.Name.Substring( indexStart );
            if ( fileExpand.Equals( "meta" ) ) {
                continue;
            }

            filesNameTemp.Add( file.Name );
        }
        filesName = new string[filesNameTemp.Count];
        for ( int i = 0; i < filesName.Length; i++ ) {
            filesName[i] = filesNameTemp[i];
        }
    }

    void UpdateLevelTemplatesName () {
        levelTemplatesName = new string[levelTemplate.GetLevelsTemplateData().Count];
        for ( int i = 0; i < levelTemplatesName.Length; i++ ) {
            levelTemplatesName[i] = i.ToString();
        }
    }

    void OnDisable () {
    }

    void OnGUI () {
        //float stepButtonPluseMinus = Screen.width < Screen.height ? Screen.width / 25f : Screen.height / 25f;
        float stepButtonPluseMinus = 30f;
        if ( GUI.Button( new Rect( startButtonPluseMinus.x, startButtonPluseMinus.y * 1.5f + ( stepButtonPluseMinus * countElements.Count ),
                                   stepButtonPluseMinus, stepButtonPluseMinus ), "+" ) ) {
            AddCountElements();
        }

        bool isChangeMatrix = false;

        if ( countElements.Count > 0 ) {
            if ( GUI.Button( new Rect( startButtonPluseMinus.x + ( stepButtonPluseMinus + stepButtonPluseMinus / 3f ),
                                       startButtonPluseMinus.y * 1.5f + ( stepButtonPluseMinus * countElements.Count ),
                                       stepButtonPluseMinus, stepButtonPluseMinus ), "-" ) ) {
                RemoveCountElements();
            }

            float sizeIndexChooseSelectionGrid = startButtonPluseMinus.y + ( stepButtonPluseMinus * countElements.Count );
            float stepCountElementsButtons = sizeIndexChooseSelectionGrid / countElements.Count;
            indexChooseCountElements = GUI.SelectionGrid( new Rect( startButtonPluseMinus.x,
                                       startButtonPluseMinus.y - stepButtonPluseMinus / 2,
                                       stepButtonPluseMinus * 2,
                                       sizeIndexChooseSelectionGrid ),
                                       indexChooseCountElements, countElementsString, 1,
                                       EditorStyles.radioButton );

            for ( int i = 0; i < countElements.Count; i++ ) {
                if ( GUI.Button( new Rect( startButtonPluseMinus.x + ( stepButtonPluseMinus * 2 ),
                                           startButtonPluseMinus.y - stepCountElementsButtons / 3f + ( stepCountElementsButtons * i ),
                                           stepButtonPluseMinus, stepButtonPluseMinus / 2f ), "+" ) ) {
                    AddNumberToElements( i, 1 );
                }

                if ( GUI.Button( new Rect( startButtonPluseMinus.x + ( stepButtonPluseMinus * 2 + stepButtonPluseMinus ),
                                           startButtonPluseMinus.y - stepCountElementsButtons / 3f + ( stepCountElementsButtons * i ),
                                           stepButtonPluseMinus, stepButtonPluseMinus / 2f ), "-" ) ) {
                    AddNumberToElements( i, -1 );
                }

                if ( GUI.Button( new Rect( startButtonPluseMinus.x + ( stepButtonPluseMinus * 2 + stepButtonPluseMinus * 2 ),
                                           startButtonPluseMinus.y - stepCountElementsButtons / 3f + ( stepCountElementsButtons * i ),
                                           stepButtonPluseMinus * 2.5f, stepButtonPluseMinus / 2f ), "сменить->" ) ) {
                    ChangeColor( i );
                }

                EditorGUI.DrawRect( new Rect( startButtonPluseMinus.x + ( stepButtonPluseMinus * 2f + stepButtonPluseMinus * 4f ),
                                              startButtonPluseMinus.y - stepCountElementsButtons / 3f + ( stepCountElementsButtons * i ),
                                              stepButtonPluseMinus / 2f, stepButtonPluseMinus / 2f ), colorElements[i] );

                Color isEndFillElement = countElements[i] == fillMatrix[i].Count ? Color.green : Color.red;
                EditorGUI.DrawRect( new Rect( startButtonPluseMinus.x + ( stepButtonPluseMinus * 2 ) - stepButtonPluseMinus / 1.5f,
                                              startButtonPluseMinus.y - stepCountElementsButtons / 3f + ( stepCountElementsButtons * i ),
                                              stepButtonPluseMinus / 2f, stepButtonPluseMinus / 2f ), isEndFillElement );
            }

            Vector2 startPosRect = new Vector2( Screen.width / 1.8f, stepButtonPluseMinus );
            GUI.Box( new Rect( startPosRect.x - stepButtonPluseMinus, startPosRect.y - stepButtonPluseMinus,
                               ( stepButtonPluseMinus * sizeField ) + stepButtonPluseMinus * 2,
                               ( stepButtonPluseMinus * sizeField ) + stepButtonPluseMinus * 2 ), "Matrix" );

            for ( int i = 0; i < sizeField; i++ ) {
                for ( int j = 0; j < sizeField; j++ ) {
                    EditorGUI.DrawRect( new Rect( startPosRect.x + ( stepButtonPluseMinus * i ), startPosRect.y + ( stepButtonPluseMinus * j ),
                                                  stepButtonPluseMinus / 1.1f, stepButtonPluseMinus / 1.1f ), Color.grey );
                }
            }

            for ( int i = 0; i < fillMatrix.Count; i++ ) {
                for ( int j = 0; j < fillMatrix[i].Count; j++ ) {
                    EditorGUI.DrawRect( new Rect( startPosRect.x + ( stepButtonPluseMinus * fillMatrix[i][j].x ),
                                                  startPosRect.y + ( stepButtonPluseMinus * fillMatrix[i][j].y ),
                                                  stepButtonPluseMinus / 1.1f, stepButtonPluseMinus / 1.1f ), colorElements[i] );
                    EditorGUI.LabelField( new Rect( startPosRect.x + ( stepButtonPluseMinus * fillMatrix[i][j].x ),
                                                    startPosRect.y + ( stepButtonPluseMinus * fillMatrix[i][j].y ),
                                                    stepButtonPluseMinus / 1.1f, stepButtonPluseMinus / 1.1f ), j.ToString() );
                }
            }

            Rect rectMatrix = new Rect( startPosRect.x, startPosRect.y,
                                        stepButtonPluseMinus * sizeField, stepButtonPluseMinus * sizeField );

            if ( ( Event.current.type == EventType.MouseDown ) && ( Event.current.button == 0 ) &&
                    rectMatrix.Contains( Event.current.mousePosition ) ) {
                CustomVector2Int indexArray = new CustomVector2Int( ( int ) ( ( Event.current.mousePosition.x - rectMatrix.x ) / stepButtonPluseMinus ),
                        ( int ) ( ( Event.current.mousePosition.y - rectMatrix.y ) / stepButtonPluseMinus ) );

                int sizeMatrixChooseCountElements = fillMatrix[indexChooseCountElements].Count;
                if ( sizeMatrixChooseCountElements > 0 &&
                        ( fillMatrix[indexChooseCountElements][sizeMatrixChooseCountElements - 1].x == indexArray.x &&
                          fillMatrix[indexChooseCountElements][sizeMatrixChooseCountElements - 1].y == indexArray.y ) ) {
                    fillMatrix[indexChooseCountElements].RemoveAt( sizeMatrixChooseCountElements - 1 );
                    isChangeMatrix = true;
                }
                else if( fillMatrix[indexChooseCountElements].Count < countElements[indexChooseCountElements] ) {
                    bool isCanSetMatrix = true;
                    for ( int i = 0; i < fillMatrix.Count; i++ ) {
                        for ( int j = 0; j < fillMatrix[i].Count; j++ ) {
                            if ( fillMatrix[i].Contains( indexArray ) ) {
                                isCanSetMatrix = false;
                                break;
                            }
                        }
                    }

                    if ( isCanSetMatrix ) {
                        fillMatrix[indexChooseCountElements].Add( indexArray );
                        isChangeMatrix = true;
                    }
                }
            }

            bool isCorrectMatrix = GetIsCorrectMatrix();

            //if ( GUI.Button( new Rect( startButtonPluseMinus.x, startButtonPluseMinus.y * 1.5f + ( stepButtonPluseMinus * countElements.Count ),
            //                           stepButtonPluseMinus, stepButtonPluseMinus ), "+" ) ) {
            if ( isCorrectMatrix ) {
                if ( GUI.Button( new Rect( startButtonPluseMinus.x, Screen.height / 1.4f,
                                           stepButtonPluseMinus * 4, stepButtonPluseMinus ), "Сохранить" ) ) {
                    SaveTemplate();
                }
            }
        }

        GUI.Box( new Rect( Screen.width / 4, ( Screen.height / 2 ) - 30f,
                           Screen.width / 4, ( Screen.height / 5f ) + 30f ), "Шаблоны" );
        //scrollPosition = GUILayout.BeginScrollView( scrollPosition, GUILayout.Width( 10 ), GUILayout.Height( 10 ) );
        indexFileName = GUI.SelectionGrid( new Rect( Screen.width / 4, Screen.height / 2,
                                           Screen.width / 4, Screen.height / 5f ),
                                           indexFileName, filesName, 2,
                                           EditorStyles.radioButton );
        if ( indexFileName != indexFileNameOld ) {
            indexFileNameOld = indexFileName;
            ChooseTemplate();
        }

        GUI.Box( new Rect( Screen.width / 2, ( Screen.height / 2 ) - 30f,
                           Screen.width / 4, ( Screen.height / 5f ) + 30f ), "Уровни" );
        if ( indexFileName < filesName.Length ) {
            indexLevelTemplateName = GUI.SelectionGrid( new Rect( Screen.width / 2, Screen.height / 2,
                                     Screen.width / 4, Screen.height / 5f ),
                                     indexLevelTemplateName, levelTemplatesName, 3,
                                     EditorStyles.radioButton );
        }

        if ( GUI.Button( new Rect( startButtonPluseMinus.x * 2f + ( stepButtonPluseMinus * 10f ), Screen.height / 1.4f,
                                   stepButtonPluseMinus * 4, stepButtonPluseMinus ), "Загрузить" ) ) {
            LoadTemplate();
        }

        if ( indexFileName < filesName.Length && indexLevelTemplateName < levelTemplatesName.Length ) {
            if ( GUI.Button( new Rect( startButtonPluseMinus.x * 7f + ( stepButtonPluseMinus * 10f ), Screen.height / 1.4f,
                                       stepButtonPluseMinus * 4.5f, stepButtonPluseMinus ), "Удалить подшаблон" ) ) {
                DeleteTemplate();
            }
        }

        if ( GUI.Button( new Rect( startButtonPluseMinus.x * 2f + ( stepButtonPluseMinus * 4f ), Screen.height / 1.4f,
                                   stepButtonPluseMinus * 4, stepButtonPluseMinus ), "Сбросить шаблон" ) ) {
            ResetAllDataTemplate();
        }

        if ( isChangeMatrix ) {
            Repaint();
        }
    }

    void AddCountElements () {
        countElements.Add( 2 );
        fillMatrix.Add( new List<CustomVector2Int>() );
        colorElements.Add( new Color( Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), 1 ) );

        UpdateCountElements();
    }

    void RemoveCountElements () {
        if ( indexChooseCountElements < countElements.Count ) {
            countElements.RemoveAt( countElements.Count - 1 );
            fillMatrix.RemoveAt( countElements.Count - 1 );
            colorElements.RemoveAt( countElements.Count - 1 );

            UpdateCountElements();
        }
    }

    void AddNumberToElements ( int indexElement, int value ) {
        countElements[indexElement] += value;
        UpdateCountElements();
    }

    void ChangeColor ( int index ) {
        colorElements[index] = new Color( Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), 1 );
    }

    void UpdateCountElements () {
        countElementsString = new string[countElements.Count];

        for ( int i = 0; i < countElements.Count; i++ ) {
            countElementsString[i] = countElements[i].ToString();
        }

        sizeField = GetSizeField();
    }

    int GetSizeField () {
        int maxLength = 0;

        foreach ( int s in countElements ) {
            maxLength += s;
        }
        int newSizeField = 0;

        if ( maxLength > 0 ) {
            float root2 = Mathf.Sqrt( maxLength );
            newSizeField = Mathf.CeilToInt( root2 );
        }

        return newSizeField;
    }

    bool GetIsCorrectMatrix () {
        for ( int i = 0; i < fillMatrix.Count; i++ ) {
            if ( fillMatrix[i].Count < countElements[i] ) {
                return false;
            }
        }

        return true;
    }

    void SaveTemplate () {
        string pistfix = "";
        for ( int i = 0; i < fillMatrix.Count; i++ ) {
            string pistfixTmp = "_" + fillMatrix[i].Count;
            pistfix += pistfixTmp;
        }

        string nameLevelTemplate = "level_teplate" + pistfix;
        string nameLevelTemplateFile = nameLevelTemplate + ".json";
        bool isNewTemplate = true;
        for( int i = 0; i < filesName.Length; i++ ) {
            if ( nameLevelTemplateFile == filesName[i] ) {
                isNewTemplate = false;
                break;
            }
        }

        LevelTemplate loadLevelTemplateTmp;
        string saveObjectPath = Application.dataPath + "/StreamingAssets/LevelTemplate/" + nameLevelTemplate + ".json";

        if ( isNewTemplate ) {
            loadLevelTemplateTmp = new LevelTemplate();
        }
        else {
            string laodObject = File.ReadAllText( saveObjectPath );
            loadLevelTemplateTmp = JsonConvert.DeserializeObject<LevelTemplate>( laodObject );
            Debug.Log( "GetLevelsTemplateData   " + loadLevelTemplateTmp.GetLevelsTemplateData().Count );
        }

        LevelTemplateData levelTemplateData = new LevelTemplateData( fillMatrix );
        levelTemplateData.SetSizeField( sizeField );
        loadLevelTemplateTmp.AddLevelTemplateData( levelTemplateData );

        if ( indexFileName < filesName.Length ) {
            if ( filesName[indexFileName] == nameLevelTemplateFile ) {
                levelTemplate = loadLevelTemplateTmp;
            }
            UpdateLevelTemplatesName();
        }

        SaveTemplate( nameLevelTemplate + ".json", loadLevelTemplateTmp );
        UpdateFiles();
    }

    void LoadTemplate () {
        LevelTemplateData levelTemplateData = levelTemplate.GetLevelsTemplateDataByIndex( indexLevelTemplateName );

        countElements = new List<int>();
        fillMatrix = new List<List<CustomVector2Int>>();
        colorElements = new List<Color>();

        for ( int i = 0; i < levelTemplateData.GetIndexPuzzlePieceCount(); i++ ) {
            countElements.Add( levelTemplateData.GetArrayVector2IntByIndex( i ).Count );

            fillMatrix.Add( new List<CustomVector2Int>() );
            colorElements.Add( new Color( Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), 1 ) );
            for ( int j = 0; j < levelTemplateData.GetArrayVector2IntByIndex( i ).Count; j++ ) {
                fillMatrix[i].Add( levelTemplateData.GetVector2IntByIndex( i, j ) );
            }
        }

        UpdateCountElements();
    }

    void DeleteTemplate () {
        levelTemplate.RemoveLevelTemplateData( indexLevelTemplateName );

        SaveTemplate( filesName[indexFileName], levelTemplate );

        UpdateLevelTemplatesName();

        indexLevelTemplateName = levelTemplate.GetLevelsTemplateData().Count;
    }

    void ChooseTemplate () {
        string saveObjectPath = Application.dataPath + "/StreamingAssets/LevelTemplate/" + filesName[indexFileName];

        string loadObject = File.ReadAllText( saveObjectPath );
        levelTemplate = JsonConvert.DeserializeObject<LevelTemplate>( loadObject );

        UpdateLevelTemplatesName();

        indexLevelTemplateName = 0;
    }

    void SaveTemplate ( string fileName, LevelTemplate loadLevelTemplate ) {
        Debug.Log( "fileName    " + fileName );

        string saveObjectPath = Application.dataPath + "/StreamingAssets/LevelTemplate/" + fileName;

        string json = JsonConvert.SerializeObject( loadLevelTemplate );
        File.WriteAllText( saveObjectPath, json, Encoding.UTF8 );

        Debug.Log( "сохранено" );
    }
}
#endif