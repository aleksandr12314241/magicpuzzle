﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System;
using Utility;
using Newtonsoft.Json;
using Common;

namespace Tools {
    public class GameplayEncrypterWindow: EditorWindow {
        [MenuItem( "Tools/GameplayEncrypterWindow" )]
        static void OpenEditorWindow () {
            var win = GetWindow<GameplayEncrypterWindow>( true );
            win.titleContent.text = Title;
        }

        const string Title = "Gameplay Encrypter Window";

        List<string> fileName;
        List<bool> toggleByFileName;
        Vector2 scrollPosition = Vector2.zero;

        void OnEnable () {
            string pathDecipher = Application.dataPath + "/JsonDatasetDecipher/";
            var info = new DirectoryInfo( pathDecipher );
            var fileInfo = info.GetFiles();

            fileName = new List<string>();
            toggleByFileName = new List<bool>();

            for ( int i = 0; i < fileInfo.Length; i++ ) {
                FileInfo file = fileInfo[i];

                int indexStart = file.Name.Length - 4;
                string fileExpand = file.Name.Substring( indexStart );
                if ( fileExpand.Equals( "meta" ) ) {
                    continue;
                }

                fileName.Add( file.Name );
                toggleByFileName.Add( false );
            }
        }

        void OnGUI () {

            if ( GUILayout.Button( "Encrypt Save" ) ) {
                SaveEncrypt();
            }
            GUILayout.Space( 30 );
            //if ( GUILayout.Button( "CreateDecipherFiles" ) ) {
            //    CreateDecipherFiles();
            //}

            GUILayout.BeginHorizontal();
            if ( GUILayout.Button( "Encrypt" ) ) {
                EncryptFiles();
            }

            if ( GUILayout.Button( "Encrypt All" ) ) {
                EncryptAllFiles();
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.LabelField( "json files" );
            scrollPosition = GUILayout.BeginScrollView( scrollPosition );
            for ( int i = 0; i < toggleByFileName.Count; i++ ) {
                toggleByFileName[i] = EditorGUILayout.Toggle( fileName[i], toggleByFileName[i] );
            }
            GUILayout.EndScrollView();
        }

        //этот код нужен когда заново нужно сгенерировать json для JsonDatasetDecipher из StreamingAssets
        void CreateDecipherFiles () {
            string pathEncrypter = Application.dataPath + "/StreamingAssets/";
            var info = new DirectoryInfo( pathEncrypter );
            var fileInfo = info.GetFiles();

            string pathDecipher = Application.dataPath + "/JsonDatasetDecipher/";
            string pathToLoad;
            string pathToSave;
            foreach ( var file in fileInfo ) {
                int indexStart = file.Name.Length - 4;
                string fileExpand = file.Name.Substring( indexStart );
                if ( fileExpand.Equals( "meta" ) ) {
                    continue;
                }

                pathToLoad = pathEncrypter + file.Name;
                pathToSave = pathDecipher + file.Name;
                if ( !File.Exists( pathToSave ) ) {
                    string saveObject = File.ReadAllText( pathToLoad );
                    File.WriteAllText( pathToSave, saveObject, Encoding.UTF8 );

                    Debug.Log( "Decipher json save " + file.Name );
                }
            }
        }

        void EncryptFiles () {
            for ( int i = 0; i < toggleByFileName.Count; i++ ) {
                if ( !toggleByFileName[i] ) {
                    continue;
                }

                EncryptFileByName( fileName[i] );
            }
        }

        void EncryptAllFiles () {
            for ( int i = 0; i < fileName.Count; i++ ) {
                EncryptFileByName( fileName[i] );
            }
        }

        void EncryptFileByName ( string fileName ) {
            string pathEncrypter = Application.dataPath + "/StreamingAssets/" + fileName;
            string pathDecipher = Application.dataPath + "/JsonDatasetDecipher/" + fileName;

            string saveObject = File.ReadAllText( pathDecipher );
            byte[] bytes = Encoding.UTF8.GetBytes( saveObject );
            string hex = BitConverter.ToString( bytes );

            File.WriteAllText( pathEncrypter, hex.Replace( "-", "" ), Encoding.UTF8 );
        }

        void SaveEncrypt () {
            string savePathDecipher = Application.dataPath + "/Data/saves_decipher.json";
            if ( !File.Exists( savePathDecipher ) ) {
                Debug.Log( "файла сейвов не существует" );
                return;
            }

            string saveObject = File.ReadAllText( savePathDecipher );

            byte[] bytes = Encoding.UTF8.GetBytes( saveObject );
            string hex = BitConverter.ToString( bytes );
            string savePathEncrypter = Application.dataPath + "/Data/saves.json";

            File.WriteAllText( savePathEncrypter, hex.Replace( "-", "" ), Encoding.UTF8 );
        }

        void SaveEncryptToNew () {
            for ( int i = 0; i < fileName.Count; i++ ) {
                if ( fileName[i].Contains( "hint_finger_dataset" ) || fileName[i].Contains( "category_quest" ) ) {
                    continue;
                }

                EncryptFileAddHintWord( fileName[i] );
            }
        }

        void EncryptFileAddHintWord ( string fileName ) {
            string pathDecipher = Application.dataPath + "/JsonDatasetDecipher/" + fileName;
            string pathEncrypter = Application.dataPath + "/StreamingAssets/" + fileName;

            string saveObject = File.ReadAllText( pathDecipher );

            //Quests tempLoadQuest = JsonConvert.DeserializeObject<Quests>( saveObject );
            //tempLoadQuest.wordsHints = new Dictionary<string, List<string>>();

            //foreach ( var item in tempLoadQuest.words ) {
            //    tempLoadQuest.wordsHints.Add( item.Key, new List<string>() );
            //}

            //string json = JsonConvert.SerializeObject( tempLoadQuest );
            //File.WriteAllText( pathDecipher, json, Encoding.UTF8 );

            //string encryptText = AuxiliaryTools.GetEncryptString( json );
            //File.WriteAllText( pathEncrypter, encryptText, Encoding.UTF8 );
        }
    }
}
#endif
